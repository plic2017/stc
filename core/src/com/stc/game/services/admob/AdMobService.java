package com.stc.game.services.admob;

public interface AdMobService {
  void showMainBannerAd();
  void hideMainBannerAd();
}
