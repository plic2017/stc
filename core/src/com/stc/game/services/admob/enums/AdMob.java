package com.stc.game.services.admob.enums;

import com.stc.game.services.Services;

import lombok.Getter;

public enum AdMob {
  HIDE_AD(0),
  SHOW_AD(1),
  RESTART_AD(2);

  @Getter
  private final int value;

  AdMob(final int value) {
    this.value = value;
  }

  public static void handleScreenParams(final Object... params) {
    if (params.length != 0 && params[0] instanceof Integer) {
      Integer value = (Integer) params[0];
      if (Services.getNetworkService().isConnectedToWiFi() && ((value == SHOW_AD.getValue()) || value == RESTART_AD.getValue())) {
        Services.getAdMobService().showMainBannerAd();
      } else if (value == HIDE_AD.getValue()) {
        Services.getAdMobService().hideMainBannerAd();
      }
    }
  }
}
