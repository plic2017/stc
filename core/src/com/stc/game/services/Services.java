package com.stc.game.services;

import com.stc.game.services.admob.AdMobService;
import com.stc.game.services.chat.ChatService;
import com.stc.game.services.gpgs.GooglePlayGamesServices;
import com.stc.game.services.nearbyconnections.NearbyConnectionsService;
import com.stc.game.services.network.NetworkService;

import lombok.Getter;
import lombok.Setter;

public class Services {
  @Getter @Setter
  private static GooglePlayGamesServices gpgs;
  @Getter @Setter
  private static NearbyConnectionsService nearbyConnectionsService;
  @Getter @Setter
  private static AdMobService adMobService;
  @Getter @Setter
  private static ChatService chatService;
  @Getter @Setter
  private static NetworkService networkService;
}
