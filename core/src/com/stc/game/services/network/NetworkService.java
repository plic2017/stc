package com.stc.game.services.network;

public interface NetworkService {
    boolean isConnectedToWiFi();
    boolean isConnectedToWiFiOrEthernet();
    boolean isConnectedToWifiOrMobile();
}
