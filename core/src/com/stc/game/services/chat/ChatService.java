package com.stc.game.services.chat;

public interface ChatService {
  void sendChatMessage(final String recipient, final String message);
}
