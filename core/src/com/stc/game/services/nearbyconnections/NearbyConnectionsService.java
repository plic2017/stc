package com.stc.game.services.nearbyconnections;

import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.multiplayer.enums.NearbyConnectionsMultiplayerStartingType;

public interface NearbyConnectionsService {
  void startAdvertising();
  void startDiscovering();
  void stopNearbyConnectionsActivity(final NearbyConnectionsMultiplayerStartingType startingType);
  void sendPlayer(final Player player);
  void sendPlayerAttack(final int attackId, final String targetId, final int colorId);
  void sendAttackEffectEntityCollision(final int attackId, final float damageCoef, final String attackedEntityId);
  void sendRemoveTeamDeathmatchTicket();
  void rejectConnectionRequests();
  boolean isHost();
}
