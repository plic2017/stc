package com.stc.game.services.gpgs.gamegifts;

import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.ui.tools.I18N;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import lombok.Getter;

public enum GameGift {
  HEALTH_BONUS_GIFT((byte) 0);

  @Getter
  private final byte gameGiftId;

  @Getter
  private static final Map<Integer, String> giftsDescriptions = new HashMap<Integer, String>() {
    {
      put((int) HEALTH_BONUS_GIFT.getGameGiftId(), I18N.getTranslation(I18N.Text.HEALTH_BONUS));
    }
  };

  GameGift(final byte gameGiftId) {
    this.gameGiftId = gameGiftId;
  }

  public static void acceptGameGift(final byte[] data) {
    ByteBuffer bf = ByteBuffer.wrap(data);
    byte gameGiftId = bf.get();

    if (gameGiftId == HEALTH_BONUS_GIFT.getGameGiftId()) {
      Player currentPlayer = Player.getCurrent();
      if (currentPlayer != null) {
        currentPlayer.setCurrentLife(currentPlayer.getMaxLife());
      }
    }
  }
}
