package com.stc.game.services.gpgs.gamegifts;

import lombok.Getter;

public enum RequestType {
  TYPE_GIFT(0),
  TYPE_WISH(1);

  @Getter
  private final int requestTypeId;

  RequestType(final int requestTypeId) {
    this.requestTypeId = requestTypeId;
  }
}
