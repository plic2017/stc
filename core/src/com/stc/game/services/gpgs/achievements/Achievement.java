package com.stc.game.services.gpgs.achievements;

import com.stc.game.gameplay.engine.wave.WaveEngine;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.services.Services;
import lombok.Getter;

public enum Achievement {
  HELLO_WORLD("CgkI2MnO4bsUEAIQBw"),
  THANK_YOU_FOR_YOUR_FEEDBACK("CgkI2MnO4bsUEAIQCg"),
  REALTIME_MUTLIPLAYER_BEGINNER("CgkI2MnO4bsUEAIQCw"),
  REALTIME_MULTIPLAYER_LOVER("CgkI2MnO4bsUEAIQDA"),
  REALTIME_MULTIPLAYER_PROFESSIONAL("CgkI2MnO4bsUEAIQBQ"),
  TEAM_DEATHMATCH_BEGINNER("CgkI2MnO4bsUEAIQCA"),
  TEAM_DEATHMATCH_LOVER("CgkI2MnO4bsUEAIQDg"),
  TEAM_DEATHMATCH_PROFESSIONAL("CgkI2MnO4bsUEAIQDw"),
  DEATHMATCH_BEGINNER("CgkI2MnO4bsUEAIQEQ"),
  DEATHMATCH_LOVER("CgkI2MnO4bsUEAIQEg"),
  DEATHMATCH_PROFESSIONAL("CgkI2MnO4bsUEAIQEw"),
  WAVE_10("CgkI2MnO4bsUEAIQFA"),
  WAVE_25("CgkI2MnO4bsUEAIQFQ");

  private static final int MIN_NUMBER_OF_PARTICIPANTS_IN_REALTIME_MULTIPLAYER = 2;
  private static final int AVERAGE_NUMBER_OF_PARTICIPANTS_IN_REALTIME_MULTIPLAYER = 4;
  private static final int MAX_NUMBER_OF_PARTICIPANTS_IN_REALTIME_MULTIPLAYER = 8;
  private static final int AVERAGE_WAVE_LEVEL = 11;

  @Getter
  private final String achievementId;

  Achievement(final String achievementId) {
    this.achievementId = achievementId;
  }

  public static void checkCurrentNumberOfParticipants() {
    if (MultiplayerContext.numberOfParticipants == MIN_NUMBER_OF_PARTICIPANTS_IN_REALTIME_MULTIPLAYER) {
      Services.getGpgs().unlockAchievement(REALTIME_MUTLIPLAYER_BEGINNER.getAchievementId());
    } else if (MultiplayerContext.numberOfParticipants == AVERAGE_NUMBER_OF_PARTICIPANTS_IN_REALTIME_MULTIPLAYER) {
      Services.getGpgs().unlockAchievement(REALTIME_MULTIPLAYER_LOVER.getAchievementId());
    } else if (MultiplayerContext.numberOfParticipants == MAX_NUMBER_OF_PARTICIPANTS_IN_REALTIME_MULTIPLAYER) {
      Services.getGpgs().unlockAchievement(REALTIME_MULTIPLAYER_PROFESSIONAL.getAchievementId());
    }
  }

  public static void checkHelloWorld(final String text) {
    if ("Hello, World!".equals(text)) {
      Services.getGpgs().unlockAchievement(HELLO_WORLD.getAchievementId());
    }
  }

  public static void checkCurrentWaveLevel() {
    if (WaveEngine.getWaveLevel() == AVERAGE_WAVE_LEVEL) {
      Services.getGpgs().unlockAchievement(WAVE_10.getAchievementId());
    }
  }

  public static void incrementDeathmatchAchievements() {
    Services.getGpgs().unlockAchievement(DEATHMATCH_BEGINNER.getAchievementId());
    Services.getGpgs().incrementAchievement(DEATHMATCH_LOVER.getAchievementId(), 1);
    Services.getGpgs().incrementAchievement(DEATHMATCH_PROFESSIONAL.getAchievementId(), 1);
  }

  public static void incrementTeamDeathmatchAchievements() {
    Services.getGpgs().unlockAchievement(TEAM_DEATHMATCH_BEGINNER.getAchievementId());
    Services.getGpgs().incrementAchievement(TEAM_DEATHMATCH_LOVER.getAchievementId(), 1);
    Services.getGpgs().incrementAchievement(TEAM_DEATHMATCH_PROFESSIONAL.getAchievementId(), 1);
  }
}
