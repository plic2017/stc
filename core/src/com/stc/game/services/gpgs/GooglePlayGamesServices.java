package com.stc.game.services.gpgs;

import com.stc.game.gameplay.entities.world.alive.player.Player;

public interface GooglePlayGamesServices {
  // Sign in
  void signIn();
  boolean isSignedIn();
  boolean isConnecting();

  // Sign out
  void signOut();

  // Rating
  void rateTheSTCGame();

  // Achievements
  void unlockAchievement(final String achievementId);
  void incrementAchievement(final String achievementId, final int increment);
  void showAchievements();

  // Leaderboards
  void submitScore(final String leaderboardId, final long score);
  void showLeaderboards();

  // Real-time Multiplayer
  void quickMatch(final int minAutoMatchPlayers, final int maxAutoMathPlayers, final int variant);
  void invitePlayers(final int minAutoMatchPlayers, final int maxAutoMathPlayers);
  void showInvitations();
  void sendPlayer(final Player player);
  void sendPlayerAttack(final int attackId, final String targetId, final int colorId);
  void sendAttackEffectEntityCollision(final int attackId, final float damageCoef, final String attackedEntityId);
  void sendRemoveTeamDeathmatchTicket();
  void sendChatMessage(final String recipient, final String message);
  void leaveRoom();

  // Saved Games
  void showSnapshots(final String snapshotsListTitle, final boolean allowAddButton, final boolean allowDeleteButton, final int maxSnapshots);
  void createOrSaveSnapshot(final String snapshotTitleToOverwrite, final byte[] data, final byte[] pixels);

  // Game Gifts
  void sendGameGiftsRequest(final int requestTypeId, final byte gameGiftId);
  void showRequestsInbox();
}
