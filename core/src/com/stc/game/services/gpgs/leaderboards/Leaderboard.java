package com.stc.game.services.gpgs.leaderboards;

import lombok.Getter;

public enum Leaderboard {
  LONGEST_SOLO_SESSION("CgkI2MnO4bsUEAIQAw"),
  LONGEST_MULTIPLAYER_SESSION("CgkI2MnO4bsUEAIQBA");

  @Getter
  private final String leaderboardId;

  Leaderboard(final String leaderboardId) {
    this.leaderboardId = leaderboardId;
  }
}
