package com.stc.game.services.gpgs.savedgames;

import com.stc.game.gameplay.engine.wave.WaveEngine;
import lombok.Getter;

import java.nio.ByteBuffer;

public class SavedGames {
  private final byte[] data;

  @Getter
  private static String playerName;

  public SavedGames(final byte[] data) {
    this.data = data;
  }

  public void loadData() {
    ByteBuffer bf = ByteBuffer.wrap(data);

    int playerNameLength = bf.getInt();
    String playerName = "";
    for (int i = 0; i < playerNameLength; i++) {
      playerName += (char) bf.get();
    }
    int waveLevel = bf.getInt();

    SavedGames.playerName = playerName;
    WaveEngine.setWaveLevel(waveLevel);
  }
}
