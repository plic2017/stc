package com.stc.game.solo;

import com.stc.game.services.Services;
import com.stc.game.services.gpgs.leaderboards.Leaderboard;

public class SoloContext {
    public static boolean solo = true;
    public static boolean loadedSavedGames = false;
    public static String snapshotTitleToOverwrite = null;

    public static void dispose(final long elapsedTime) {
        Services.getGpgs().submitScore(Leaderboard.LONGEST_SOLO_SESSION.getLeaderboardId(), elapsedTime);
        loadedSavedGames = false;
        snapshotTitleToOverwrite = null;
    }
}
