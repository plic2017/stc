package com.stc.game.ui.dialogs;

import com.badlogic.gdx.Gdx;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.tools.MultiplayerSender;
import com.stc.game.ui.screens.PauseScreen;
import com.stc.game.ui.tools.I18N;
import de.tomgrill.gdxdialogs.core.GDXDialogs;
import de.tomgrill.gdxdialogs.core.GDXDialogsSystem;
import de.tomgrill.gdxdialogs.core.dialogs.GDXButtonDialog;
import de.tomgrill.gdxdialogs.core.dialogs.GDXProgressDialog;
import de.tomgrill.gdxdialogs.core.dialogs.GDXTextPrompt;
import de.tomgrill.gdxdialogs.core.listener.ButtonClickListener;
import de.tomgrill.gdxdialogs.core.listener.TextPromptListener;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public class Dialogs {
    private static final String TAG = "Dialogs";
    private static final String CHAT_TITLE = "Chat";

    public static void showButtonDialog(final String title, final String message) {
        GDXDialogs dialogs = GDXDialogsSystem.install();
        final GDXButtonDialog buttonDialog = dialogs.newDialog(GDXButtonDialog.class);
        buttonDialog.setTitle(title);
        buttonDialog.setMessage(message);
        buttonDialog.setClickListener(new ButtonClickListener() {
            @Override
            public void click(int button) {
                buttonDialog.dismiss();
            }
        });
        buttonDialog.addButton(I18N.getTranslation(I18N.Text.CLOSE));
        buttonDialog.build().show();
    }

    public static void showHelpButtonDialog(final String title, final String message) {
        GDXDialogs dialogs = GDXDialogsSystem.install();
        final GDXButtonDialog buttonDialog = dialogs.newDialog(GDXButtonDialog.class);
        buttonDialog.setTitle(title);
        buttonDialog.setMessage(message);
        buttonDialog.setClickListener(new ButtonClickListener() {
            @Override
            public void click(int button) {
                buttonDialog.dismiss();
                PauseScreen.resume();
            }
        });
        buttonDialog.addButton(I18N.getTranslation(I18N.Text.CLOSE));
        buttonDialog.build().show();
    }

    public static void showYesNoButtonDialog(final String title, final String message, final Callable<Void> func) {
        GDXDialogs dialogs = GDXDialogsSystem.install();
        final GDXButtonDialog buttonDialog = dialogs.newDialog(GDXButtonDialog.class);
        buttonDialog.setTitle(title);
        buttonDialog.setMessage(message);
        buttonDialog.setClickListener(new ButtonClickListener() {
            @Override
            public void click(int button) {
                switch (button) {
                    case 0:
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Gdx.app.postRunnable(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            func.call();
                                            buttonDialog.dismiss();
                                        } catch (Exception e) {
                                            Gdx.app.error(TAG, e.getMessage());
                                        }
                                    }
                                });
                            }
                        }).start();
                        break;
                    case 1:
                        buttonDialog.dismiss();
                        break;
                    default:
                        break;
                }
            }
        });
        buttonDialog.addButton(I18N.getTranslation(I18N.Text.YES));
        buttonDialog.addButton(I18N.getTranslation(I18N.Text.NO));
        buttonDialog.build().show();
    }

    public static void showChatButtonDialog(final String playerName, final String message) {
        GDXDialogs dialogs = GDXDialogsSystem.install();
        final GDXButtonDialog buttonDialog = dialogs.newDialog(GDXButtonDialog.class);
        buttonDialog.setTitle(CHAT_TITLE);
        buttonDialog.setMessage(message);
        buttonDialog.setClickListener(new ButtonClickListener() {
            @Override
            public void click(int button) {
                switch (button) {
                    case 0:
                        buttonDialog.dismiss();
                        answerToPlayer(playerName);
                        break;
                    case 1:
                        if (MultiplayerContext.blockedPlayers == null) {
                            MultiplayerContext.blockedPlayers = new ArrayList<>();
                        }
                        MultiplayerContext.blockedPlayers.add(playerName);
                        buttonDialog.dismiss();
                        break;
                    case 2:
                        buttonDialog.dismiss();
                        break;
                    default:
                        break;
                }
            }
        });
        buttonDialog.addButton(I18N.getTranslation(I18N.Text.ANSWER));
        buttonDialog.addButton(I18N.getTranslation(I18N.Text.BLOCK));
        buttonDialog.addButton(I18N.getTranslation(I18N.Text.CLOSE));
        buttonDialog.build().show();
    }

    private static void answerToPlayer(final String playerName) {
        GDXDialogs dialogs = GDXDialogsSystem.install();
        final GDXTextPrompt textPrompt = dialogs.newDialog(GDXTextPrompt.class);
        textPrompt.setTitle(CHAT_TITLE);
        textPrompt.setMessage(I18N.getTranslation(I18N.Text.WRITE_YOUR_MESSAGE));
        textPrompt.setCancelButtonLabel(I18N.getTranslation(I18N.Text.CANCEL));
        textPrompt.setConfirmButtonLabel(I18N.getTranslation(I18N.Text.SEND));
        textPrompt.setTextPromptListener(new TextPromptListener() {
            @Override
            public void confirm(String text) {
                MultiplayerSender.sendChatMessage(playerName, text);
                textPrompt.dismiss();
            }

            @Override
            public void cancel() {
                textPrompt.dismiss();
            }
        });
        textPrompt.build().show();
    }

    public static GDXTextPrompt getChatTextPrompt() {
        GDXDialogs dialogs = GDXDialogsSystem.install();
        dialogs.newDialog(GDXTextPrompt.class);
        final GDXTextPrompt textPrompt = dialogs.newDialog(GDXTextPrompt.class);
        textPrompt.setTitle(CHAT_TITLE);
        textPrompt.setMessage(I18N.getTranslation(I18N.Text.WRITE_YOUR_MESSAGE));
        textPrompt.setCancelButtonLabel(I18N.getTranslation(I18N.Text.CANCEL));
        textPrompt.setConfirmButtonLabel(I18N.getTranslation(I18N.Text.SEND));
        textPrompt.setTextPromptListener(new TextPromptListener() {
            @Override
            public void confirm(String text) {
                MultiplayerSender.sendChatMessage(null, text);
                textPrompt.dismiss();
                PauseScreen.resume();
            }

            @Override
            public void cancel() {
                textPrompt.dismiss();
            }
        });
        return textPrompt;
    }

    public static GDXProgressDialog getProgressDialog(final String title, final String message) {
        GDXDialogs dialogs = GDXDialogsSystem.install();
        GDXProgressDialog progressDialog = dialogs.newDialog(GDXProgressDialog.class);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        return progressDialog;
    }
}
