package com.stc.game.ui.tools;

import com.stc.game.ui.enums.Language;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

public final class I18N {
    public enum Text {
        PLAY,
        OPTIONS,
        SIGN_OUT,

        SOLO,
        MULTIPLAYER,
        BACK,

        NEW_GAME,
        LOAD_GAME,
        LOADING,
        LOADING_SAVED_GAMES,
        LOADING_GAME,
        SAVED_GAME,
        SAVED_GAMES,

        SURVIVAL,
        DEATHMATCH,
        TEAM_DEATHMATCH,
        SHOW_INVITATIONS,

        QUICK_GAME,
        INVITE_PLAYERS,
        MIN_AUTO_MATCH_PLAYERS,
        MAX_AUTO_MATCH_PLAYERS,
        GO,
        NEARBY_CONNECTIONS,
        START_ADVERTISING,
        START_DISCOVERING,

        STATS,
        LEADERBOARDS,
        ACHIEVEMENTS,
        LANGUAGE,

        SURVIVE,

        RESUME,
        RESTART,
        ARE_YOU_SURE_YOU_WANT_TO_RESTART,
        VICTORY,
        SAVE,
        SAVING_PROGRESS,
        EXIT,
        ARE_YOU_SURE_YOU_WANT_TO_EXIT,

        GPGS_REQUIRED,
        ALREADY_SIGNED_OUT,
        SIGNED_OUT,

        WRITE_YOUR_MESSAGE,
        SEND,
        CANCEL,
        FROM,
        ANSWER,
        BLOCK,
        CLOSE,

        WIFI_OR_ETHERNET_REQUIRED,
        ENDPOINTS_FOUND,
        CONNECTION_REQUEST,
        DO_YOU_WANT_TO_CONNECT_TO,
        CONNECT,

        YES,
        NO,

        STATUS_ERROR,
        STATUS_ALREADY_ADVERTISING,
        STATUS_ALREADY_DISCOVERING,
        STATUS_ALREADY_CONNECTED_TO_ENDPOINT,
        STATUS_CONNECTION_REJECTED,
        STATUS_NOT_CONNECTED_TO_ENDPOINT,

        DISCONNECTED,
        RECONNECTION_IN_PROGRESS,
        RECONNECTION_TIMEOUT,

        SURVIVAL_MODE_ERROR,
        SURVIVAL_MODE_ERROR_DESCRIPTION,

        HEALTH_BONUS,
        DO_YOU_WANT_TO_ACCEPT_THE_REQUEST,

        LAST_WAVE_LEVEL_REACHED,

        HOST_DISCONNECTED,
        NEARBY_CONNECTIONS_START_DESCRIPTION,

        HELP,
        SURVIVAL_HELP,
        NEARBY_CONNECTIONS_HOST_SURVIVAL_HELP,
        DEATHMATCH_HELP,
        NEARBY_CONNECTIONS_HOST_DEATHMATCH_HELP,
        TEAM_DEATHMATCH_HELP,
        NEARBY_CONNECTIONS_HOST_TEAM_DEATHMATCH_HELP
    }

    @Getter @Setter
    private static Language defaultLanguage;
    private static Map<Text, String> ENGLISH;
    private static Map<Text, String> FRENCH;

    public static void initialize(final Language defaultLanguage) {
        I18N.defaultLanguage = defaultLanguage;

        ENGLISH = new HashMap<>();
        ENGLISH.put(Text.PLAY, "play");
        ENGLISH.put(Text.OPTIONS, "options");
        ENGLISH.put(Text.SIGN_OUT, "sign out");
        ENGLISH.put(Text.SOLO, "solo");
        ENGLISH.put(Text.MULTIPLAYER, "multiplayer");
        ENGLISH.put(Text.NEW_GAME, "new game");
        ENGLISH.put(Text.LOAD_GAME, "load game");
        ENGLISH.put(Text.LOADING, "Loading");
        ENGLISH.put(Text.QUICK_GAME, "quick game");
        ENGLISH.put(Text.INVITE_PLAYERS, "invite players");
        ENGLISH.put(Text.NEARBY_CONNECTIONS, "nearby connections");
        ENGLISH.put(Text.START_ADVERTISING, "start advertising");
        ENGLISH.put(Text.START_DISCOVERING, "start discovering");
        ENGLISH.put(Text.SHOW_INVITATIONS, "show invitations");
        ENGLISH.put(Text.LEADERBOARDS, "leaderboards");
        ENGLISH.put(Text.ACHIEVEMENTS, "achievements");
        ENGLISH.put(Text.RESUME, "resume");
        ENGLISH.put(Text.SAVE, "save");
        ENGLISH.put(Text.EXIT, "exit");
        ENGLISH.put(Text.ARE_YOU_SURE_YOU_WANT_TO_EXIT, "Are you sure you want to exit? As a host player, this will stop the game for the other players.");
        ENGLISH.put(Text.LANGUAGE, "francais");
        ENGLISH.put(Text.SURVIVE, "survive!");
        ENGLISH.put(Text.MIN_AUTO_MATCH_PLAYERS, "min players");
        ENGLISH.put(Text.MAX_AUTO_MATCH_PLAYERS, "max players");
        ENGLISH.put(Text.GO, "go");
        ENGLISH.put(Text.BACK, "back");
        ENGLISH.put(Text.GPGS_REQUIRED, "You must be signed in to the Google Play Games Services.");
        ENGLISH.put(Text.ALREADY_SIGNED_OUT, "You're already signed out from the Google Play Games Services.");
        ENGLISH.put(Text.SIGNED_OUT, "You're now signed out from the Google Play Games Services.");
        ENGLISH.put(Text.CLOSE, "Close");
        ENGLISH.put(Text.CANCEL, "Cancel");
        ENGLISH.put(Text.SEND, "Send");
        ENGLISH.put(Text.WRITE_YOUR_MESSAGE, "Write your message");
        ENGLISH.put(Text.SAVING_PROGRESS, "Saving progress…");
        ENGLISH.put(Text.FROM, "From");
        ENGLISH.put(Text.ANSWER, "Answer");
        ENGLISH.put(Text.RESTART, "restart");
        ENGLISH.put(Text.ARE_YOU_SURE_YOU_WANT_TO_RESTART, "Are you sure you want to restart? As a host player, this will stop the game for the other players.");
        ENGLISH.put(Text.WIFI_OR_ETHERNET_REQUIRED, "You must be connected to a Wi-Fi or Ethernet network.");
        ENGLISH.put(Text.CONNECTION_REQUEST, "Connection request");
        ENGLISH.put(Text.NO, "No");
        ENGLISH.put(Text.ENDPOINTS_FOUND, "Endpoints found");
        ENGLISH.put(Text.CONNECT, "Connect");
        ENGLISH.put(Text.DO_YOU_WANT_TO_CONNECT_TO, "Do you want to connect to ");
        ENGLISH.put(Text.STATUS_CONNECTION_REJECTED, "Your connection request has been rejected.");
        ENGLISH.put(Text.SURVIVAL, "survival");
        ENGLISH.put(Text.DEATHMATCH, "deathmatch");
        ENGLISH.put(Text.TEAM_DEATHMATCH, "team deathmatch");
        ENGLISH.put(Text.SAVED_GAME, "Saved Game");
        ENGLISH.put(Text.SAVED_GAMES, "Saved Games");
        ENGLISH.put(Text.LOADING_SAVED_GAMES, "Loading the saved game…");
        ENGLISH.put(Text.LOADING_GAME, "Loading game…");
        ENGLISH.put(Text.STATUS_ERROR, "The operation failed, without any more information.");
        ENGLISH.put(Text.STATUS_ALREADY_ADVERTISING, "The app is already advertising.");
        ENGLISH.put(Text.STATUS_ALREADY_DISCOVERING, "The app is already discovering.");
        ENGLISH.put(Text.STATUS_ALREADY_CONNECTED_TO_ENDPOINT, "The app is already connected to the specified endpoint.");
        ENGLISH.put(Text.STATUS_NOT_CONNECTED_TO_ENDPOINT, "The remote endpoint is not connected; messages cannot be sent to it.");
        ENGLISH.put(Text.STATS, "stats");
        ENGLISH.put(Text.DISCONNECTED, "Disconnected");
        ENGLISH.put(Text.RECONNECTION_IN_PROGRESS, "Reconnection in progress…");
        ENGLISH.put(Text.BLOCK, "Block");
        ENGLISH.put(Text.SURVIVAL_MODE_ERROR, "Survival mode error");
        ENGLISH.put(Text.SURVIVAL_MODE_ERROR_DESCRIPTION, "Problem encountered when resolving the host player.");
        ENGLISH.put(Text.RECONNECTION_TIMEOUT, "The reconnection attempt failed.");
        ENGLISH.put(Text.HEALTH_BONUS, "Health Bonus");
        ENGLISH.put(Text.YES, "Yes");
        ENGLISH.put(Text.DO_YOU_WANT_TO_ACCEPT_THE_REQUEST, "Accept the request for the following item?");
        ENGLISH.put(Text.LAST_WAVE_LEVEL_REACHED, "Last wave level reached: ");
        ENGLISH.put(Text.VICTORY, "victory");
        ENGLISH.put(Text.HOST_DISCONNECTED, "The host player left the game. You can exit this game.");
        ENGLISH.put(Text.NEARBY_CONNECTIONS_START_DESCRIPTION, "Do you want to start the game with the current players? The following connection requests for this game will be automatically rejected.");
        ENGLISH.put(Text.HELP, "help");
        ENGLISH.put(Text.SURVIVAL_HELP, "Use one of your 3 attacks with the right color to destroy an enemy with similar color.");
        ENGLISH.put(Text.NEARBY_CONNECTIONS_HOST_SURVIVAL_HELP, "Use one of your 3 attacks with the right color to destroy an enemy with similar color. Press the \"start\" button to start the game.");
        ENGLISH.put(Text.DEATHMATCH_HELP, "Use all of your attacks to destroy your opponents until they have no more ticket.");
        ENGLISH.put(Text.NEARBY_CONNECTIONS_HOST_DEATHMATCH_HELP, "Use all of your attacks to destroy your opponents until they have no more ticket. Press the \"start\" button to start the game.");
        ENGLISH.put(Text.TEAM_DEATHMATCH_HELP, "Use all of your attacks to destroy the players of the opposing team until it has no more ticket.");
        ENGLISH.put(Text.NEARBY_CONNECTIONS_HOST_TEAM_DEATHMATCH_HELP, "Use all of your attacks to destroy the players of the opposing team until it has no more ticket. Press the \"start\" button to start the game.");

        FRENCH = new HashMap<>();
        FRENCH.put(Text.PLAY, "jouer");
        FRENCH.put(Text.OPTIONS, "options");
        FRENCH.put(Text.SIGN_OUT, "deconnexion");
        FRENCH.put(Text.SOLO, "solo");
        FRENCH.put(Text.MULTIPLAYER, "multijoueur");
        FRENCH.put(Text.NEW_GAME, "nouvelle partie");
        FRENCH.put(Text.LOAD_GAME, "charger une partie");
        FRENCH.put(Text.LOADING, "Chargement");
        FRENCH.put(Text.QUICK_GAME, "partie rapide");
        FRENCH.put(Text.INVITE_PLAYERS, "inviter des joueurs");
        FRENCH.put(Text.NEARBY_CONNECTIONS, "connexions proches");
        FRENCH.put(Text.START_ADVERTISING, "heberger une partie");
        FRENCH.put(Text.START_DISCOVERING, "trouver une partie");
        FRENCH.put(Text.SHOW_INVITATIONS, "mes invitations");
        FRENCH.put(Text.LEADERBOARDS, "classements");
        FRENCH.put(Text.ACHIEVEMENTS, "succes");
        FRENCH.put(Text.RESUME, "reprendre");
        FRENCH.put(Text.SAVE, "sauvegarder");
        FRENCH.put(Text.EXIT, "quitter");
        FRENCH.put(Text.ARE_YOU_SURE_YOU_WANT_TO_EXIT, "Êtes-vous sûr de vouloir quitter ? En tant que joueur hôte, cela arrêtera le match pour les autres joueurs.");
        FRENCH.put(Text.LANGUAGE, "english");
        FRENCH.put(Text.SURVIVE, "survivez !");
        FRENCH.put(Text.MIN_AUTO_MATCH_PLAYERS, "joueurs min");
        FRENCH.put(Text.MAX_AUTO_MATCH_PLAYERS, "joueurs max");
        FRENCH.put(Text.GO, "lancer");
        FRENCH.put(Text.BACK, "retour");
        FRENCH.put(Text.GPGS_REQUIRED, "Vous devez être connecté aux services de jeux Google Play.");
        FRENCH.put(Text.ALREADY_SIGNED_OUT, "Vous êtes déjà déconnecté des services de jeux Google Play.");
        FRENCH.put(Text.SIGNED_OUT, "Vous êtes maintenant déconnecté des services de jeux Google Play.");
        FRENCH.put(Text.CLOSE, "Fermer");
        FRENCH.put(Text.CANCEL, "Annuler");
        FRENCH.put(Text.SEND, "Envoyer");
        FRENCH.put(Text.WRITE_YOUR_MESSAGE, "Écrivez votre message");
        FRENCH.put(Text.SAVING_PROGRESS, "Sauvegarde de la progression…");
        FRENCH.put(Text.FROM, "De");
        FRENCH.put(Text.ANSWER, "Répondre");
        FRENCH.put(Text.RESTART, "rejouer");
        FRENCH.put(Text.ARE_YOU_SURE_YOU_WANT_TO_RESTART, "Êtes-vous sûr de vouloir recommencer ? En tant que joueur hôte, cela arrêtera le match pour les autres joueurs.");
        FRENCH.put(Text.WIFI_OR_ETHERNET_REQUIRED, "Vous devez être connecté à un réseau Wi-Fi ou Ethernet.");
        FRENCH.put(Text.CONNECTION_REQUEST, "Demande de connexion");
        FRENCH.put(Text.STATUS_CONNECTION_REJECTED, "Votre demande de connexion a été rejetée.");
        FRENCH.put(Text.NO, "Non");
        FRENCH.put(Text.ENDPOINTS_FOUND, "Parties trouvées");
        FRENCH.put(Text.CONNECT, "Se connecter");
        FRENCH.put(Text.DO_YOU_WANT_TO_CONNECT_TO, "Voulez-vous vous connecter à ");
        FRENCH.put(Text.SURVIVAL, "mode survie");
        FRENCH.put(Text.DEATHMATCH, "match a mort");
        FRENCH.put(Text.TEAM_DEATHMATCH, "match a mort par equipe");
        FRENCH.put(Text.SAVED_GAME, "Partie enregistrée");
        FRENCH.put(Text.SAVED_GAMES, "Parties enregistrées");
        FRENCH.put(Text.LOADING_SAVED_GAMES, "Chargement de la partie enregistrée…");
        FRENCH.put(Text.LOADING_GAME, "Chargement du jeu…");
        FRENCH.put(Text.STATUS_ERROR, "L'opération a échoué sans aucune information supplémentaire.");
        FRENCH.put(Text.STATUS_ALREADY_ADVERTISING, "L'application est déjà en train d'héberger une partie.");
        FRENCH.put(Text.STATUS_ALREADY_DISCOVERING, "L'application est déjà en train de rechercher une partie.");
        FRENCH.put(Text.STATUS_ALREADY_CONNECTED_TO_ENDPOINT, "L'application est déjà connectée au point d'accès spécifié.");
        FRENCH.put(Text.STATUS_NOT_CONNECTED_TO_ENDPOINT, "L'application n'a pas pu se connecter au point d'accès spécifié; aucune donnée ne peut être envoyée.");
        FRENCH.put(Text.STATS, "stats");
        FRENCH.put(Text.DISCONNECTED, "Déconnecté");
        FRENCH.put(Text.RECONNECTION_IN_PROGRESS, "Reconnexion en cours…");
        FRENCH.put(Text.BLOCK, "Bloquer");
        FRENCH.put(Text.SURVIVAL_MODE_ERROR, "Erreur en mode survie");
        FRENCH.put(Text.SURVIVAL_MODE_ERROR_DESCRIPTION, "Problème rencontré lors de la résolution du joueur hôte.");
        FRENCH.put(Text.RECONNECTION_TIMEOUT, "La tentative de reconnexion a échoué.");
        FRENCH.put(Text.HEALTH_BONUS, "Bonus de santé");
        FRENCH.put(Text.YES, "Oui");
        FRENCH.put(Text.DO_YOU_WANT_TO_ACCEPT_THE_REQUEST, "Accepter la demande pour l'objet suivant ?");
        FRENCH.put(Text.LAST_WAVE_LEVEL_REACHED, "Dernière vague atteinte : ");
        FRENCH.put(Text.VICTORY, "victoire");
        FRENCH.put(Text.HOST_DISCONNECTED, "Le joueur hôte a quitté la partie. Vous pouvez quitter cette partie.");
        FRENCH.put(Text.NEARBY_CONNECTIONS_START_DESCRIPTION, "Voulez-vous démarrer la partie avec les joueurs actuels ? Les demandes de connexion suivantes pour cette partie seront automatiquement rejetées.");
        FRENCH.put(Text.HELP, "aide");
        FRENCH.put(Text.SURVIVAL_HELP, "Utilisez l'une de vos 3 attaques avec la bonne couleur pour détruire un ennemi ayant une couleur équivalente.");
        FRENCH.put(Text.NEARBY_CONNECTIONS_HOST_SURVIVAL_HELP, "Utilisez l'une de vos 3 attaques avec la bonne couleur pour détruire un ennemi ayant une couleur équivalente. Appuyez sur le bouton \"démarrer\" pour commencer la partie.");
        FRENCH.put(Text.DEATHMATCH_HELP, "Utilisez l'ensemble de vos attaques afin de détruire les joueurs adverses jusqu'à ce qu'ils n'aient plus de ticket.");
        FRENCH.put(Text.NEARBY_CONNECTIONS_HOST_DEATHMATCH_HELP, "Utilisez l'ensemble de vos attaques afin de détruire les joueurs adverses jusqu'à ce qu'ils n'aient plus de ticket. Appuyez sur le bouton \"démarrer\" pour commencer la partie.");
        FRENCH.put(Text.TEAM_DEATHMATCH_HELP, "Utilisez l'ensemble de vos attaques afin de détruire les joueurs de l'équipe adverse jusqu'à ce qu'elle n'est plus de ticket.");
        FRENCH.put(Text.NEARBY_CONNECTIONS_HOST_TEAM_DEATHMATCH_HELP, "Utilisez l'ensemble de vos attaques afin de détruire les joueurs de l'équipe adverse jusqu'à ce qu'elle n'est plus de ticket. Appuyez sur le bouton \"démarrer\" pour commencer la partie.");
    }

    public static String getTranslation(final Text key){
        if (defaultLanguage == Language.ENGLISH)
            return ENGLISH.get(key);
        return FRENCH.get(key);
    }
}
