package com.stc.game.ui.tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.stc.game.utils.encoder.PNG;
import com.stc.game.solo.SoloContext;
import com.stc.game.ui.enums.ScreenState;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.Callable;

public final class ScreenTools {
  private static final String TAG = "ScreenTools";

  public static ImageButton createImageButton(final Texture texture) {
    return new ImageButton(new TextureRegionDrawable(new TextureRegion(texture)));
  }

  public static byte[] getPixelsFromScreenshot() {
    Pixmap pixmap = getScreenshot(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
    byte[] pixels = null;
    try {
      pixels = PNG.toPNG(pixmap);
    } catch (IOException e) {
      Gdx.app.error(TAG, e.getMessage());
    }
    return pixels;
  }

  private static Pixmap getScreenshot(final int x, final int y, final int w, final int h, final boolean flipY) {
    Gdx.gl.glPixelStorei(GL20.GL_PACK_ALIGNMENT, 1);

    final Pixmap pixmap = new Pixmap(w, h, Pixmap.Format.RGBA8888);
    ByteBuffer pixels = pixmap.getPixels();
    Gdx.gl.glReadPixels(x, y, w, h, GL20.GL_RGBA, GL20.GL_UNSIGNED_BYTE, pixels);

    final int numBytes = w * h * 4;
    byte[] lines = new byte[numBytes];
    if (flipY) {
      final int numBytesPerLine = w * 4;
      for (int i = 0; i < h; i++) {
        pixels.position((h - i - 1) * numBytesPerLine);
        pixels.get(lines, i * numBytesPerLine, numBytesPerLine);
      }
      pixels.clear();
      pixels.put(lines);
    } else {
      pixels.clear();
      pixels.get(lines);
    }

    return pixmap;
  }

  public static ScreenState getScreenToGoBack() {
    ScreenState screenState;
    if (SoloContext.solo) {
      screenState = ScreenState.SOLO_SCREEN;
    } else {
      screenState = ScreenState.MULTIPLAYER_SCREEN;
    }
    return screenState;
  }

  public static InputListener createInputListener(final Callable<Void> func) {
    return new InputListener() {
      @Override
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        try {
          func.call();
        } catch (Exception e) {
          Gdx.app.error(TAG, e.getMessage());
        }
      }

      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        return true;
      }
    };
  }
}
