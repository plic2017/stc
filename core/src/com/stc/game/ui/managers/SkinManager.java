package com.stc.game.ui.managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import lombok.Getter;

public class SkinManager {
  @Getter
  private static Skin skin;

  public static void initSkinManager() {
    skin = new Skin(Gdx.files.internal("skins/uiskin.json"), new TextureAtlas(Gdx.files.internal("skins/uiskin.atlas")));
  }

  public static void dispose() {
    if (skin != null) {
      skin.dispose();
      skin = null;
    }
  }
}
