package com.stc.game.ui.managers;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Timer;
import com.stc.game.services.admob.enums.AdMob;
import com.stc.game.ui.enums.ScreenState;
import com.stc.game.ui.screens.AbstractScreen;

public class ScreenManager {
  private static final float DURATION = 0.2f;

  private static ScreenManager instance;
  private static AbstractScreen currentScreen;

  private Game game;

  private ScreenManager() {
    super();
  }

  public static ScreenManager getInstance() {
    if (instance == null) {
      instance = new ScreenManager();
    }
    return instance;
  }

  public void initialize(final Game game) {
    this.game = game;
  }

  public void showScreen(final ScreenState screenState, final Object... params) {
    Screen oldScreen = game.getScreen();
    currentScreen = screenState.getScreen(params);
    currentScreen.buildStage();
    if (currentScreen.getTable() != null) {
      currentScreen.getTable().getColor().a = 0f;
      currentScreen.getTable().addAction(Actions.fadeIn(DURATION));
    }
    game.setScreen(currentScreen);
    if (oldScreen != null) {
      oldScreen.dispose();
    }
    AdMob.handleScreenParams(params);
  }

  public void switchScreen(final ScreenState screenState, final Object... params) {
    if (currentScreen.getTable() != null) {
      currentScreen.getTable().getColor().a = 1f;
      currentScreen.getTable().addAction(Actions.fadeOut(DURATION));
      Timer.schedule(new Timer.Task() {
        @Override
        public void run() {
          showScreen(screenState, params);
        }
      }, DURATION);
    } else {
      showScreen(screenState, params);
    }
  }
}
