package com.stc.game.ui.enums;

import lombok.Getter;

public enum Language {
    ENGLISH("en"),
    FRENCH("fr");

    @Getter
    private final String languageCode;

    Language(final String languageCode) {
        this.languageCode = languageCode;
    }
}
