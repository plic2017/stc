package com.stc.game.ui.enums;

import com.stc.game.ui.screens.AbstractScreen;
import com.stc.game.ui.screens.AutoMatchPlayersSelectionScreen;
import com.stc.game.ui.screens.GameModeSelectionScreen;
import com.stc.game.ui.screens.GameScreen;
import com.stc.game.ui.screens.HomeScreen;
import com.stc.game.ui.screens.LoadingScreen;
import com.stc.game.ui.screens.MultiplayerScreen;
import com.stc.game.ui.screens.NearbyConnectionsScreen;
import com.stc.game.ui.screens.OptionsScreen;
import com.stc.game.ui.screens.SoloScreen;
import com.stc.game.ui.screens.StatsScreen;

public enum ScreenState {
  HOME_SCREEN {
    public AbstractScreen getScreen(Object... params) {
      return new HomeScreen();
    }
  },

  GAME_MODE_SELECTION_SCREEN {
    public AbstractScreen getScreen(Object... params) {
      return new GameModeSelectionScreen();
    }
  },

  MULTIPLAYER_SCREEN {
    public AbstractScreen getScreen(Object... params) {
      return new MultiplayerScreen();
    }
  },

  LOADING_SCREEN {
    public AbstractScreen getScreen(Object... params) {
      return new LoadingScreen();
    }
  },

  GAME_SCREEN {
    public AbstractScreen getScreen(Object... params) {
      return new GameScreen();
    }
  },

  SOLO_SCREEN {
    public AbstractScreen getScreen(Object... params) {
      return new SoloScreen();
    }
  },

  AUTO_MATCH_PLAYERS_SELECTION_SCREEN {
    public AbstractScreen getScreen(Object... params) {
      return new AutoMatchPlayersSelectionScreen(params);
    }
  },

  OPTIONS_SCREEN {
    public AbstractScreen getScreen(Object... params) {
      return new OptionsScreen();
    }
  },

  NEARBY_CONNECTIONS_SCREEN {
    public AbstractScreen getScreen(Object... params) {
      return new NearbyConnectionsScreen();
    }
  },

  STATS_SCREEN {
    public AbstractScreen getScreen(Object... params) {
      return new StatsScreen();
    }
  };

  public abstract AbstractScreen getScreen(Object... params);
}
