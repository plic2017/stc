package com.stc.game.ui.enums;

public enum GameScreenState {
  RUN,
  PAUSE,
  WIN,
  GAME_OVER,
  RESPAWN_SOLO,
  RESPAWN_MULTIPLAYER
}
