package com.stc.game.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.stc.game.gameplay.GamePlayUtils;
import com.stc.game.gameplay.engine.environment.CustomEnvironment;
import com.stc.game.gameplay.engine.environment.SkyboxEnvironment;
import com.stc.game.gameplay.engine.managers.LabelManager;
import com.stc.game.gameplay.entities.ui.hud.labels.Logger;
import com.stc.game.gameplay.engine.managers.BuilderManager;
import com.stc.game.gameplay.engine.managers.ModelManager;
import com.stc.game.gameplay.engine.managers.SpriteManager;
import com.stc.game.gameplay.engine.managers.input.InputManager;
import com.stc.game.gameplay.engine.physic.Collider;
import com.stc.game.gameplay.engine.pools.MainPool;
import com.stc.game.gameplay.engine.sound.SoundEngine;
import com.stc.game.gameplay.engine.wave.WaveEngine;
import com.stc.game.gameplay.enums.Musics;
import com.stc.game.kryonet.scheduler.PacketScheduler;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.tools.MultiplayerUtils;
import com.stc.game.services.admob.enums.AdMob;
import com.stc.game.solo.SoloContext;
import com.stc.game.ui.enums.GameScreenState;
import com.stc.game.ui.managers.ScreenManager;
import com.stc.game.ui.tools.ScreenTools;
import lombok.Getter;
import lombok.Setter;

public class GameScreen extends AbstractScreen {
  @Getter @Setter
  private static GameScreenState gameScreenState;
  private static long startTimeMillis;

  public GameScreen() {
    super();
    gameScreenState = GameScreenState.RUN;
    startTimeMillis = System.currentTimeMillis();
  }

  @Override
  public void buildStage() {
    SoundEngine.pauseMusic(Musics.MAIN_MUSIC);

    if (SoloContext.solo) {
      SoundEngine.startMusic(Musics.SOLO_GAME_MUSIC);
      GamePlayUtils.startSoloGame();
    } else {
      SoundEngine.startMusic(Musics.MULTIPLAYER_GAME_MUSIC);
    }
  }

  @Override
  public void render(float delta) {
    PacketScheduler.update();
    LabelManager.update();

    if (gameScreenState == GameScreenState.RUN || MultiplayerContext.multiplayer) {
      Collider.update();
      WaveEngine.update();
      MainPool.update();
    }

    InputManager.update(gameScreenState);

    Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

    SkyboxEnvironment.draw();
    MainPool.draw();
    LabelManager.draw();

    if (gameScreenState == GameScreenState.PAUSE) {
      act();
      addActor(PauseScreen.getPauseWindow());
      draw();
    }

    if (gameScreenState == GameScreenState.WIN) {
      act();
      addActor(WinScreen.getWinWindow());
      draw();
    }

    if (gameScreenState == GameScreenState.GAME_OVER) {
      act();
      addActor(GameOverScreen.getGameOverWindow());
      draw();
    }

    if (gameScreenState == GameScreenState.RESPAWN_SOLO) {
      GamePlayUtils.respawnPlayerInSolo();
    }

    if (gameScreenState == GameScreenState.RESPAWN_MULTIPLAYER) {
      GamePlayUtils.respawnPlayerInMultiplayer();
    }
  }

  @Override
  public void dispose() {
    Musics.restoreMainMusic();

    long elapsedTime = System.currentTimeMillis() - startTimeMillis;
    if (SoloContext.solo) {
      SoloContext.dispose(elapsedTime);
    } else {
      MultiplayerUtils.dispose(elapsedTime);
    }

    super.dispose();
    Logger.dispose();
    LabelManager.dispose();
    WaveEngine.dispose();
    MainPool.dispose();
    Collider.dispose();
    SpriteManager.dispose();
    ModelManager.dispose();
    InputManager.dispose();
    BuilderManager.dispose();
    CustomEnvironment.dispose();
    SkyboxEnvironment.dispose();
  }

  @Override
  public boolean keyDown(int keyCode) {
    if (keyCode == Input.Keys.BACK) {
      if (gameScreenState == GameScreenState.PAUSE) {
        PauseScreen.resume();
      } else {
        ScreenManager.getInstance().switchScreen(ScreenTools.getScreenToGoBack(), AdMob.RESTART_AD.getValue());
      }
    }
    return true;
  }

  public static void setGameScreenStateAfterResume(final GameScreenState gameScreenState) {
    if (GameScreen.getGameScreenState() == GameScreenState.PAUSE)
      PauseScreen.resume();
    GameScreen.setGameScreenState(gameScreenState);
  }
}
