package com.stc.game.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.stc.game.gameplay.engine.sound.SoundEngine;
import com.stc.game.services.Services;
import com.stc.game.services.gpgs.achievements.Achievement;
import com.stc.game.ui.enums.Language;
import com.stc.game.ui.enums.ScreenState;
import com.stc.game.ui.managers.ScreenManager;
import com.stc.game.ui.managers.SkinManager;
import com.stc.game.ui.tools.I18N;
import com.stc.game.ui.tools.ScreenTools;

import java.util.concurrent.Callable;

public class OptionsScreen extends AbstractScreen {
  private Texture rateTexture;

  private final TextButton bStats;
  private final TextButton bLanguage;
  private final TextButton bBack;

  public OptionsScreen() {
    super();
    rateTexture = new Texture(Gdx.files.internal("textures/buttons/rate_button.png"));
    bStats = new TextButton(I18N.getTranslation(I18N.Text.STATS), SkinManager.getSkin());
    bLanguage = new TextButton(I18N.getTranslation(I18N.Text.LANGUAGE), SkinManager.getSkin());
    bBack = new TextButton(I18N.getTranslation(I18N.Text.BACK), SkinManager.getSkin());
  }

  @Override
  public void buildStage() {
    ImageButton rateImageButton = ScreenTools.createImageButton(rateTexture);

    rateImageButton.addListener(new InputListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        Services.getGpgs().rateTheSTCGame();
        Services.getGpgs().unlockAchievement(Achievement.THANK_YOU_FOR_YOUR_FEEDBACK.getAchievementId());
        return true;
      }
    });

    bStats.addListener(ScreenTools.createInputListener(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        ScreenManager.getInstance().switchScreen(ScreenState.STATS_SCREEN);
        return null;
      }
    }));

    bLanguage.addListener(ScreenTools.createInputListener(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        if (I18N.getDefaultLanguage() == Language.ENGLISH)
          I18N.setDefaultLanguage(Language.FRENCH);
        else
          I18N.setDefaultLanguage(Language.ENGLISH);
        ScreenManager.getInstance().switchScreen(ScreenState.OPTIONS_SCREEN);
        return null;
      }
    }));

    bBack.addListener(ScreenTools.createInputListener(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        ScreenManager.getInstance().switchScreen(ScreenState.HOME_SCREEN);
        return null;
      }
    }));

    final ImageButton muteButton = new ImageButton(SkinManager.getSkin());
    if (SoundEngine.getVolume() == 0f)
      muteButton.setChecked(true);

    final Slider volume = new Slider(0.0f, 1.0f, 0.1f, false, SkinManager.getSkin());
    volume.setValue(SoundEngine.getVolume());
    volume.addListener(new ChangeListener() {
      public void changed(ChangeEvent event, Actor actor) {
        SoundEngine.setVolume(volume.getValue());
        if (volume.getValue() == 0f) {
          if (!muteButton.isChecked())
            muteButton.setChecked(true);
        } else
          muteButton.setChecked(false);
      }
    });

    InputListener stopTouchDown = new InputListener() {
      public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        event.stop();
        return false;
      }
    };
    volume.addListener(stopTouchDown);

    muteButton.addListener(ScreenTools.createInputListener(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        if (SoundEngine.getVolume() == 0f && volume.getValue() == 0f && SoundEngine.getBeforeMuteVolume() == 0f) {
          muteButton.setChecked(true);
          return null;
        }
        SoundEngine.mute();
        if (volume.getValue() > 0f)
          volume.setValue(0f);
        else
          volume.setValue(SoundEngine.getVolume());
        return null;
      }
    }));

    addActor(AbstractScreen.getBackgroundImage());

    table = new Table();
    table.add(rateImageButton).padLeft(AbstractScreen.SCREEN_WIDTH - (rateImageButton.getWidth() * 4)).row();
    table.add(bStats).padTop(bBack.getHeight()).row();
    table.add(bLanguage).padTop(bBack.getHeight()).row();
    table.add(muteButton).padLeft(-volume.getWidth() -muteButton.getWidth()).padTop(bBack.getHeight());
    table.add(volume).padLeft(-volume.getWidth() * 4 - muteButton.getWidth()).padTop(bBack.getHeight()).row();
    table.add(bBack).padTop(bBack.getHeight()).padBottom(bBack.getHeight()).row();

    ScrollPane scrollPane = new ScrollPane(table, SkinManager.getSkin());
    scrollPane.setBounds(0, 0, AbstractScreen.SCREEN_WIDTH, AbstractScreen.SCREEN_HEIGHT);
    addActor(scrollPane);
  }

  @Override
  public void dispose() {
    super.dispose();
    rateTexture.dispose();
    rateTexture = null;
  }

  @Override
  public boolean keyDown(int keyCode) {
    if (keyCode == Input.Keys.BACK) {
      ScreenManager.getInstance().switchScreen(ScreenState.HOME_SCREEN);
    }
    return true;
  }
}
