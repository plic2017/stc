package com.stc.game.ui.screens;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.stc.game.ui.enums.ScreenState;
import com.stc.game.ui.managers.ScreenManager;
import com.stc.game.ui.managers.SkinManager;
import com.stc.game.ui.tools.I18N;
import com.stc.game.ui.tools.ScreenTools;

import java.util.concurrent.Callable;

public class GameModeSelectionScreen extends AbstractScreen {
  private final TextButton bSolo;
  private final TextButton bMultiplayer;
  private final TextButton bBack;

  public GameModeSelectionScreen() {
    super();
    bSolo = new TextButton(I18N.getTranslation(I18N.Text.SOLO), SkinManager.getSkin());
    bMultiplayer = new TextButton(I18N.getTranslation(I18N.Text.MULTIPLAYER), SkinManager.getSkin());
    bBack = new TextButton(I18N.getTranslation(I18N.Text.BACK), SkinManager.getSkin());
  }

  @Override
  public void buildStage() {
    bSolo.addListener(ScreenTools.createInputListener(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        ScreenManager.getInstance().switchScreen(ScreenState.SOLO_SCREEN);
        return null;
      }
    }));

    bMultiplayer.addListener(ScreenTools.createInputListener(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        ScreenManager.getInstance().switchScreen(ScreenState.MULTIPLAYER_SCREEN);
        return null;
      }
    }));

    bBack.addListener(ScreenTools.createInputListener(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        ScreenManager.getInstance().switchScreen(ScreenState.HOME_SCREEN);
        return null;
      }
    }));

    addActor(AbstractScreen.getBackgroundImage());

    table = new Table();
    table.add(bSolo).row();
    table.add(bMultiplayer).padTop(bBack.getHeight()).row();
    table.add(bBack).padTop(bBack.getHeight()).row();

    ScrollPane scrollPane = new ScrollPane(table, SkinManager.getSkin());
    scrollPane.setBounds(0, 0, AbstractScreen.SCREEN_WIDTH, AbstractScreen.SCREEN_HEIGHT);
    addActor(scrollPane);
  }

  @Override
  public boolean keyDown(int keyCode) {
    if (keyCode == Input.Keys.BACK) {
      ScreenManager.getInstance().switchScreen(ScreenState.HOME_SCREEN);
    }
    return true;
  }
}
