package com.stc.game.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.stc.game.kryonet.tools.KryonetUtils;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.enums.MultiplayerMode;
import com.stc.game.multiplayer.enums.RealtimeMultiplayerStartingType;
import com.stc.game.services.Services;
import com.stc.game.services.admob.enums.AdMob;
import com.stc.game.solo.SoloContext;
import com.stc.game.ui.dialogs.Dialogs;
import com.stc.game.ui.enums.ScreenState;
import com.stc.game.ui.managers.ScreenManager;
import com.stc.game.ui.managers.SkinManager;
import com.stc.game.ui.tools.I18N;
import com.stc.game.ui.tools.ScreenTools;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;

public class AutoMatchPlayersSelectionScreen extends AbstractScreen {
  private static final long TIMEOUT = 1000L * 5L;

  private static int MIN_AUTO_MATCH_PLAYERS = 2;
  private static int MAX_AUTO_MATCH_PLAYERS = 8;

  private Texture lessButtonTexture;
  private Texture moreButtonTexture;

  private final TextButton bGo;
  private final TextButton bBack;

  private Label minAutoMatchPlayersNumberLabel;
  private Label maxAutoMatchPlayersNumberLabel;

  private final RealtimeMultiplayerStartingType realtimeMultiplayerStartingType;

  private boolean canPressGoButton;

  public AutoMatchPlayersSelectionScreen(Object... params) {
    super();
    lessButtonTexture = new Texture(Gdx.files.internal("textures/buttons/less_button.png"));
    moreButtonTexture = new Texture(Gdx.files.internal("textures/buttons/more_button.png"));
    bGo = new TextButton(I18N.getTranslation(I18N.Text.GO), SkinManager.getSkin());
    bBack = new TextButton(I18N.getTranslation(I18N.Text.BACK), SkinManager.getSkin());
    realtimeMultiplayerStartingType = (RealtimeMultiplayerStartingType) params[0];
    canPressGoButton = true;
  }

  @Override
  public void buildStage() {
    ImageButton lessMinAutoMatchPlayers = ScreenTools.createImageButton(lessButtonTexture);
    ImageButton moreMinAutoMatchPlayers = ScreenTools.createImageButton(moreButtonTexture);
    ImageButton lessMaxAutoMatchPlayers = ScreenTools.createImageButton(lessButtonTexture);
    ImageButton moreMaxAutoMatchPlayers = ScreenTools.createImageButton(moreButtonTexture);

    TextButton minAutoMatchPlayersLabel = new TextButton(I18N.getTranslation(I18N.Text.MIN_AUTO_MATCH_PLAYERS), SkinManager.getSkin());
    minAutoMatchPlayersLabel.setTouchable(Touchable.disabled);
    TextButton maxAutoMatchPlayersLabel = new TextButton(I18N.getTranslation(I18N.Text.MAX_AUTO_MATCH_PLAYERS), SkinManager.getSkin());
    maxAutoMatchPlayersLabel.setTouchable(Touchable.disabled);

    minAutoMatchPlayersNumberLabel = new Label(String.valueOf(MIN_AUTO_MATCH_PLAYERS), SkinManager.getSkin());
    maxAutoMatchPlayersNumberLabel = new Label(String.valueOf(MAX_AUTO_MATCH_PLAYERS), SkinManager.getSkin());

    lessMinAutoMatchPlayers.addListener(new InputListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        if (MIN_AUTO_MATCH_PLAYERS > 2) {
          MIN_AUTO_MATCH_PLAYERS--;
          minAutoMatchPlayersNumberLabel.setText(String.valueOf(MIN_AUTO_MATCH_PLAYERS));
        }
        return true;
      }
    });

    moreMinAutoMatchPlayers.addListener(new InputListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        if (MIN_AUTO_MATCH_PLAYERS < MAX_AUTO_MATCH_PLAYERS) {
          MIN_AUTO_MATCH_PLAYERS++;
          minAutoMatchPlayersNumberLabel.setText(String.valueOf(MIN_AUTO_MATCH_PLAYERS));
        }
        return true;
      }
    });

    lessMaxAutoMatchPlayers.addListener(new InputListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        if (MAX_AUTO_MATCH_PLAYERS > MIN_AUTO_MATCH_PLAYERS) {
          MAX_AUTO_MATCH_PLAYERS--;
          maxAutoMatchPlayersNumberLabel.setText(String.valueOf(MAX_AUTO_MATCH_PLAYERS));
        }
        return true;
      }
    });

    moreMaxAutoMatchPlayers.addListener(new InputListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        if (MAX_AUTO_MATCH_PLAYERS < 8) {
          MAX_AUTO_MATCH_PLAYERS++;
          maxAutoMatchPlayersNumberLabel.setText(String.valueOf(MAX_AUTO_MATCH_PLAYERS));
        }
        return true;
      }
    });

    bGo.addListener(ScreenTools.createInputListener(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        if (!canPressGoButton) {
          return null;
        } else {
          activateCooldownOnGoButton();
        }

        if (Services.getGpgs().isSignedIn()) {
          if (realtimeMultiplayerStartingType == RealtimeMultiplayerStartingType.QUICK_GAME) {
            Services.getGpgs().quickMatch(MIN_AUTO_MATCH_PLAYERS - 1, MAX_AUTO_MATCH_PLAYERS - 1, MultiplayerMode.getMultiplayerModeVariantFromMultiplayerContext());
          } else {
            Services.getGpgs().invitePlayers(MIN_AUTO_MATCH_PLAYERS - 1, MAX_AUTO_MATCH_PLAYERS - 1);
          }
        } else if (!Services.getGpgs().isConnecting()) {
          Services.getGpgs().signIn();
        } else {
          String title = realtimeMultiplayerStartingType == RealtimeMultiplayerStartingType.QUICK_GAME ? I18N.getTranslation(I18N.Text.QUICK_GAME) : I18N.getTranslation(I18N.Text.INVITE_PLAYERS);
          Dialogs.showButtonDialog(title, I18N.getTranslation(I18N.Text.GPGS_REQUIRED));
        }
        return null;
      }
    }));

    bBack.addListener(ScreenTools.createInputListener(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        ScreenManager.getInstance().switchScreen(ScreenState.MULTIPLAYER_SCREEN, MultiplayerContext.getMultiplayerMode());
        return null;
      }
    }));

    addActor(AbstractScreen.getBackgroundImage());

    float minMaxSize = maxAutoMatchPlayersLabel.getWidth() - minAutoMatchPlayersLabel.getWidth();

    table = new Table();

    Table minAutoMatchPlayersTables = new Table();
    minAutoMatchPlayersTables.add(minAutoMatchPlayersLabel).padRight(minMaxSize);
    minAutoMatchPlayersTables.add(lessMinAutoMatchPlayers).padLeft(lessMaxAutoMatchPlayers.getWidth());
    minAutoMatchPlayersTables.add(minAutoMatchPlayersNumberLabel).padLeft(lessMaxAutoMatchPlayers.getWidth());
    minAutoMatchPlayersTables.add(moreMinAutoMatchPlayers).padLeft(lessMaxAutoMatchPlayers.getWidth());
    table.add(minAutoMatchPlayersTables).colspan(4).center().padTop(bBack.getHeight());
    table.row();

    Table maxAutoMatchPlayersTable = new Table();
    maxAutoMatchPlayersTable.add(maxAutoMatchPlayersLabel);
    maxAutoMatchPlayersTable.add(lessMaxAutoMatchPlayers).padLeft(lessMaxAutoMatchPlayers.getWidth());
    maxAutoMatchPlayersTable.add(maxAutoMatchPlayersNumberLabel).padLeft(lessMaxAutoMatchPlayers.getWidth());
    maxAutoMatchPlayersTable.add(moreMaxAutoMatchPlayers).padLeft(lessMaxAutoMatchPlayers.getWidth());
    table.add(maxAutoMatchPlayersTable).colspan(4).center().padTop(bBack.getHeight());
    table.row();

    Table goAndBackTable = new Table();
    goAndBackTable.add(bGo).padRight(bBack.getWidth());
    goAndBackTable.add(bBack);
    table.add(goAndBackTable).colspan(4).center().padTop(bBack.getHeight()).padBottom(bBack.getHeight()).row();

    ScrollPane scrollPane = new ScrollPane(table, SkinManager.getSkin());
    scrollPane.setBounds(0, 0, AbstractScreen.SCREEN_WIDTH, AbstractScreen.SCREEN_HEIGHT);
    addActor(scrollPane);
  }


  @Override
  public void render(float delta) {
    super.render(delta);

    if (MultiplayerContext.multiplayer) {
      SoloContext.solo = false;
      KryonetUtils.launchKryonetClient();
      ScreenManager.getInstance().showScreen(ScreenState.LOADING_SCREEN, AdMob.HIDE_AD.getValue());
    }
  }

  @Override
  public void dispose() {
    super.dispose();
    lessButtonTexture.dispose();
    lessButtonTexture = null;
    moreButtonTexture.dispose();
    moreButtonTexture = null;
    minAutoMatchPlayersNumberLabel.clear();
    minAutoMatchPlayersNumberLabel = null;
    maxAutoMatchPlayersNumberLabel.clear();
    maxAutoMatchPlayersNumberLabel = null;
  }

  @Override
  public boolean keyDown(int keyCode) {
    if (keyCode == Input.Keys.BACK) {
      ScreenManager.getInstance().switchScreen(ScreenState.MULTIPLAYER_SCREEN, MultiplayerContext.getMultiplayerMode());
    }
    return true;
  }

  private void activateCooldownOnGoButton() {
    canPressGoButton = false;
    Timer timer = new Timer();
    timer.schedule(new TimerTask() {
      @Override
      public void run() {
        canPressGoButton = true;
      }
    }, TIMEOUT);
  }
}
