package com.stc.game.ui.screens;

import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.enums.MultiplayerType;
import com.stc.game.services.Services;
import com.stc.game.services.admob.enums.AdMob;
import com.stc.game.solo.SoloContext;
import com.stc.game.ui.dialogs.Dialogs;
import com.stc.game.ui.enums.GameScreenState;
import com.stc.game.ui.managers.ScreenManager;
import com.stc.game.ui.managers.SkinManager;
import com.stc.game.ui.tools.I18N;
import com.stc.game.ui.tools.ScreenTools;
import com.stc.game.utils.Help;
import de.tomgrill.gdxdialogs.core.dialogs.GDXProgressDialog;
import de.tomgrill.gdxdialogs.core.dialogs.GDXTextPrompt;
import lombok.Getter;

import java.util.concurrent.Callable;

public class PauseScreen {
  @Getter
  private static Window pauseWindow;
  @Getter
  private static GDXProgressDialog saveProgressDialog;
  private static GDXTextPrompt chatTextPrompt;

  static void initialize(){
    pauseWindow = new Window("", SkinManager.getSkin());

    TextButton bResume = new TextButton(I18N.getTranslation(I18N.Text.RESUME), SkinManager.getSkin());
    TextButton bHelp = new TextButton(I18N.getTranslation(I18N.Text.HELP), SkinManager.getSkin());
    TextButton bExit = new TextButton(I18N.getTranslation(I18N.Text.EXIT), SkinManager.getSkin());

    bResume.addListener(ScreenTools.createInputListener(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        resume();
        return null;
      }
    }));

    bHelp.addListener(ScreenTools.createInputListener(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        Dialogs.showHelpButtonDialog(I18N.getTranslation(I18N.Text.HELP), Help.getHelpMessage());
        return null;
      }
    }));

    bExit.addListener(ScreenTools.createInputListener(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        dispose();
        ScreenManager.getInstance().switchScreen(ScreenTools.getScreenToGoBack(), AdMob.RESTART_AD.getValue());
        return null;
      }
    }));

    pauseWindow.add(bResume).row();

    if (SoloContext.solo) {
      saveProgressDialog = Dialogs.getProgressDialog(I18N.getTranslation(I18N.Text.SAVE), I18N.getTranslation(I18N.Text.SAVING_PROGRESS));
      addSaveButton();
    } else if (MultiplayerContext.getMultiplayerType() == MultiplayerType.REALTIME_MULTIPLAYER) {
      chatTextPrompt = Dialogs.getChatTextPrompt();
      addChatButton();
    }

    pauseWindow.add(bHelp).padTop(bExit.getHeight()).row();
    pauseWindow.add(bExit).padTop(bExit.getHeight()).row();

    pauseWindow.setSize(AbstractScreen.SCREEN_WIDTH, AbstractScreen.SCREEN_HEIGHT);
    pauseWindow.setMovable(false);
  }

  private static void addSaveButton() {
    TextButton bSave = new TextButton(I18N.getTranslation(I18N.Text.SAVE), SkinManager.getSkin());
    bSave.addListener(ScreenTools.createInputListener(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        if (Services.getGpgs().isSignedIn()) {
          byte[] data = Player.getCurrentPlayerStats();
          if (data != null) {
            saveProgressDialog.build().show();
            Services.getGpgs().createOrSaveSnapshot(SoloContext.snapshotTitleToOverwrite, data, ScreenTools.getPixelsFromScreenshot());
          }
        } else if (!Services.getGpgs().isConnecting()) {
          Services.getGpgs().signIn();
        } else {
          Dialogs.showButtonDialog(I18N.getTranslation(I18N.Text.SAVE), I18N.getTranslation(I18N.Text.GPGS_REQUIRED));
        }
        return null;
      }
    }));
    pauseWindow.add(bSave).padTop(bSave.getHeight()).row();
  }

  private static void addChatButton() {
    TextButton bChat = new TextButton("chat", SkinManager.getSkin());
    bChat.addListener(ScreenTools.createInputListener(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        chatTextPrompt.build().show();
        return null;
      }
    }));
    pauseWindow.add(bChat).padTop(bChat.getHeight()).row();
  }

  private static void dispose() {
    pauseWindow = null;
    if (SoloContext.solo) {
      saveProgressDialog = null;
    } else {
      chatTextPrompt = null;
    }
  }

  public static void resume() {
    pauseWindow.remove();
    GameScreen.setGameScreenState(GameScreenState.RUN);
  }
}
