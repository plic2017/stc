package com.stc.game.ui.screens;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.stc.game.services.Services;
import com.stc.game.ui.enums.ScreenState;
import com.stc.game.ui.managers.ScreenManager;
import com.stc.game.ui.managers.SkinManager;
import com.stc.game.ui.tools.I18N;
import com.stc.game.ui.tools.ScreenTools;

import java.util.concurrent.Callable;

public class StatsScreen extends AbstractScreen {
    private final TextButton bLeaderboards;
    private final TextButton bAchievements;
    private final TextButton bBack;

    public StatsScreen() {
        super();
        bLeaderboards = new TextButton(I18N.getTranslation(I18N.Text.LEADERBOARDS), SkinManager.getSkin());
        bAchievements = new TextButton(I18N.getTranslation(I18N.Text.ACHIEVEMENTS), SkinManager.getSkin());
        bBack = new TextButton(I18N.getTranslation(I18N.Text.BACK), SkinManager.getSkin());
    }

    @Override
    public void buildStage() {
        bLeaderboards.addListener(ScreenTools.createInputListener(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                Services.getGpgs().showLeaderboards();
                return null;
            }
        }));

        bAchievements.addListener(ScreenTools.createInputListener(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                Services.getGpgs().showAchievements();
                return null;
            }
        }));

        bBack.addListener(ScreenTools.createInputListener(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                ScreenManager.getInstance().switchScreen(ScreenState.OPTIONS_SCREEN);
                return null;
            }
        }));

        addActor(AbstractScreen.getBackgroundImage());

        table = new Table();
        table.add(bLeaderboards).row();
        table.add(bAchievements).padTop(bBack.getHeight()).row();
        table.add(bBack).padTop(bBack.getHeight()).row();

        ScrollPane scrollPane = new ScrollPane(table, SkinManager.getSkin());
        scrollPane.setBounds(0, 0, AbstractScreen.SCREEN_WIDTH, AbstractScreen.SCREEN_HEIGHT);
        addActor(scrollPane);
    }


    @Override
    public boolean keyDown(int keyCode) {
        if (keyCode == Input.Keys.BACK) {
            ScreenManager.getInstance().switchScreen(ScreenState.OPTIONS_SCREEN);
        }
        return true;
    }
}
