package com.stc.game.ui.screens;

import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.enums.MultiplayerType;
import com.stc.game.services.Services;
import com.stc.game.services.admob.enums.AdMob;
import com.stc.game.solo.SoloContext;
import com.stc.game.ui.dialogs.Dialogs;
import com.stc.game.ui.enums.GameScreenState;
import com.stc.game.ui.enums.ScreenState;
import com.stc.game.ui.managers.ScreenManager;
import com.stc.game.ui.managers.SkinManager;
import com.stc.game.ui.tools.I18N;
import com.stc.game.ui.tools.ScreenTools;
import lombok.Getter;

import java.util.concurrent.Callable;

class GameOverScreen {
  @Getter
  private static Window gameOverWindow;

  static void initialize() {
    gameOverWindow = new Window("", SkinManager.getSkin());

    TextButton bRestart = new TextButton(I18N.getTranslation(I18N.Text.RESTART), SkinManager.getSkin());
    TextButton bExit = new TextButton(I18N.getTranslation(I18N.Text.EXIT), SkinManager.getSkin());

    final Callable<Void> restartDialogCallable = new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        dispose();
        ScreenManager.getInstance().switchScreen(ScreenState.MULTIPLAYER_SCREEN, AdMob.RESTART_AD.getValue());
        return null;
      }
    };
    Callable<Void> restartCallable = new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        if (SoloContext.solo) {
          dispose();
          GameScreen.setGameScreenState(GameScreenState.RESPAWN_SOLO);
        } else {
          if (MultiplayerContext.getMultiplayerType() == MultiplayerType.NEARBY_CONNECTIONS_MULTIPLAYER &&
              Services.getNearbyConnectionsService().isHost()) {
            Dialogs.showYesNoButtonDialog(I18N.getTranslation(I18N.Text.RESTART), I18N.getTranslation(I18N.Text.ARE_YOU_SURE_YOU_WANT_TO_RESTART), restartDialogCallable);
          } else {
            dispose();
            ScreenManager.getInstance().switchScreen(ScreenState.MULTIPLAYER_SCREEN, AdMob.RESTART_AD.getValue());
          }
        }
        return null;
      }
    };
    bRestart.addListener(ScreenTools.createInputListener(restartCallable));


    final Callable<Void> exitDialogCallable = new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        dispose();
        ScreenManager.getInstance().switchScreen(ScreenTools.getScreenToGoBack(), AdMob.RESTART_AD.getValue());
        return null;
      }
    };
    Callable<Void> exitCallable = new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        if (MultiplayerContext.getMultiplayerType() == MultiplayerType.NEARBY_CONNECTIONS_MULTIPLAYER &&
            Services.getNearbyConnectionsService().isHost()) {
          Dialogs.showYesNoButtonDialog(I18N.getTranslation(I18N.Text.EXIT), I18N.getTranslation(I18N.Text.ARE_YOU_SURE_YOU_WANT_TO_EXIT), exitDialogCallable);
        } else {
          dispose();
          ScreenManager.getInstance().switchScreen(ScreenTools.getScreenToGoBack(), AdMob.RESTART_AD.getValue());
        }
        return null;
      }
    };
    bExit.addListener(ScreenTools.createInputListener(exitCallable));

    gameOverWindow.add(bRestart).row();
    gameOverWindow.add(bExit).padTop(bExit.getHeight()).row();

    gameOverWindow.setSize(AbstractScreen.SCREEN_WIDTH, AbstractScreen.SCREEN_HEIGHT);
    gameOverWindow.setMovable(false);
  }

  private static void dispose() {
    gameOverWindow.remove();
  }
}
