package com.stc.game.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.stc.game.kryonet.tools.KryonetUtils;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.enums.MultiplayerMode;
import com.stc.game.multiplayer.enums.MultiplayerType;
import com.stc.game.multiplayer.enums.RealtimeMultiplayerStartingType;
import com.stc.game.services.Services;
import com.stc.game.services.admob.enums.AdMob;
import com.stc.game.solo.SoloContext;
import com.stc.game.ui.dialogs.Dialogs;
import com.stc.game.ui.enums.ScreenState;
import com.stc.game.ui.managers.ScreenManager;
import com.stc.game.ui.managers.SkinManager;
import com.stc.game.ui.tools.I18N;
import com.stc.game.ui.tools.ScreenTools;

import java.util.concurrent.Callable;

public class MultiplayerScreen extends AbstractScreen {
    private final TextButton bMultiplayerMode;
    private final TextButton bMultiplayerType;
    private final TextButton bTeamDeathmatch;
    private final TextButton bNearbyConnection;
    private final TextButton bShowInvitations;
    private final TextButton bGo;
    private final TextButton bBack;

    private MultiplayerType multiplayerType;
    private RealtimeMultiplayerStartingType realtimeMultiplayerStartingType;
    private MultiplayerMode multiplayerMode;

    private Texture lessButtonTexture;
    private Texture moreButtonTexture;

    public MultiplayerScreen() {
        super();
        multiplayerMode = MultiplayerContext.getMultiplayerMode();
        if (multiplayerMode != null) {
            if (multiplayerMode == MultiplayerMode.SURVIVAL)
                bMultiplayerMode = new TextButton(I18N.getTranslation(I18N.Text.SURVIVAL), SkinManager.getSkin());
            /*else if (multiplayerMode == MultiplayerMode.DEATHMATCH)
                bMultiplayerMode = new TextButton(I18N.getTranslation(I18N.Text.DEATHMATCH), SkinManager.getSkin());*/
            else /*if (multiplayerMode == MultiplayerMode.TEAM_DEATHMATCH)*/
                bMultiplayerMode = new TextButton(I18N.getTranslation(I18N.Text.TEAM_DEATHMATCH), SkinManager.getSkin());
        } else {
            multiplayerMode = MultiplayerMode.SURVIVAL;
            bMultiplayerMode = new TextButton(I18N.getTranslation(I18N.Text.SURVIVAL), SkinManager.getSkin());
        }

        multiplayerType = MultiplayerType.REALTIME_MULTIPLAYER;
        realtimeMultiplayerStartingType = RealtimeMultiplayerStartingType.QUICK_GAME;
        bMultiplayerType = new TextButton(I18N.getTranslation(I18N.Text.QUICK_GAME), SkinManager.getSkin());

        // Buttons used to set the table size
        bNearbyConnection = new TextButton(I18N.getTranslation(I18N.Text.NEARBY_CONNECTIONS), SkinManager.getSkin());
        bTeamDeathmatch = new TextButton(I18N.getTranslation(I18N.Text.TEAM_DEATHMATCH), SkinManager.getSkin());

        bShowInvitations = new TextButton(I18N.getTranslation(I18N.Text.SHOW_INVITATIONS), SkinManager.getSkin());

        lessButtonTexture = new Texture(Gdx.files.internal("textures/buttons/less_button.png"));
        moreButtonTexture = new Texture(Gdx.files.internal("textures/buttons/more_button.png"));

        bGo = new TextButton(I18N.getTranslation(I18N.Text.GO), SkinManager.getSkin());
        bBack = new TextButton(I18N.getTranslation(I18N.Text.BACK), SkinManager.getSkin());
    }

    @Override
    public void buildStage() {
        ImageButton previousMultiplayerType = ScreenTools.createImageButton(lessButtonTexture);
        ImageButton nextMultiplayerType = ScreenTools.createImageButton(moreButtonTexture);
        ImageButton previousMultiplayerMode = ScreenTools.createImageButton(lessButtonTexture);
        ImageButton nextMultiplayerMode = ScreenTools.createImageButton(moreButtonTexture);

        bMultiplayerMode.setTouchable(Touchable.disabled);
        bMultiplayerType.setTouchable(Touchable.disabled);

        Callable<Void> multiplayerModeSwitcher = new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                if (multiplayerMode == MultiplayerMode.SURVIVAL) {
                    multiplayerMode = MultiplayerMode.TEAM_DEATHMATCH;
                    bMultiplayerMode.setText(I18N.getTranslation(I18N.Text.TEAM_DEATHMATCH));
                } else {
                    multiplayerMode = MultiplayerMode.SURVIVAL;
                    bMultiplayerMode.setText(I18N.getTranslation(I18N.Text.SURVIVAL));
                }
                return null;
            }
        };

        previousMultiplayerMode.addListener(ScreenTools.createInputListener(multiplayerModeSwitcher/*new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                if (multiplayerMode == MultiplayerMode.DEATHMATCH) {
                    multiplayerMode = MultiplayerMode.SURVIVAL;
                    bMultiplayerMode.setText(I18N.getTranslation(I18N.Text.SURVIVAL));
                } else if (multiplayerMode == MultiplayerMode.TEAM_DEATHMATCH) {
                    multiplayerMode = MultiplayerMode.DEATHMATCH;
                    bMultiplayerMode.setText(I18N.getTranslation(I18N.Text.DEATHMATCH));
                } else if (multiplayerMode == MultiplayerMode.SURVIVAL) {
                    multiplayerMode = MultiplayerMode.TEAM_DEATHMATCH;
                    bMultiplayerMode.setText(I18N.getTranslation(I18N.Text.TEAM_DEATHMATCH));
                }
                return null;
            }
        }*/));

        nextMultiplayerMode.addListener(ScreenTools.createInputListener(multiplayerModeSwitcher/*new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                if (multiplayerMode == MultiplayerMode.DEATHMATCH) {
                    multiplayerMode = MultiplayerMode.TEAM_DEATHMATCH;
                    bMultiplayerMode.setText(I18N.getTranslation(I18N.Text.TEAM_DEATHMATCH));
                } else if (multiplayerMode == MultiplayerMode.SURVIVAL) {
                    multiplayerMode = MultiplayerMode.DEATHMATCH;
                    bMultiplayerMode.setText(I18N.getTranslation(I18N.Text.DEATHMATCH));
                } else if (multiplayerMode == MultiplayerMode.TEAM_DEATHMATCH) {
                    multiplayerMode = MultiplayerMode.SURVIVAL;
                    bMultiplayerMode.setText(I18N.getTranslation(I18N.Text.SURVIVAL));
                }
                return null;
            }
        }*/));

        previousMultiplayerType.addListener(ScreenTools.createInputListener(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                if (multiplayerType == MultiplayerType.REALTIME_MULTIPLAYER && realtimeMultiplayerStartingType == RealtimeMultiplayerStartingType.INVITE_PLAYERS) {
                    realtimeMultiplayerStartingType = RealtimeMultiplayerStartingType.QUICK_GAME;
                    bMultiplayerType.setText(I18N.getTranslation(I18N.Text.QUICK_GAME));
                } else if (multiplayerType == MultiplayerType.NEARBY_CONNECTIONS_MULTIPLAYER) {
                    multiplayerType = MultiplayerType.REALTIME_MULTIPLAYER;
                    realtimeMultiplayerStartingType = RealtimeMultiplayerStartingType.INVITE_PLAYERS;
                    bMultiplayerType.setText(I18N.getTranslation(I18N.Text.INVITE_PLAYERS));
                } else if (multiplayerType == MultiplayerType.REALTIME_MULTIPLAYER && realtimeMultiplayerStartingType == RealtimeMultiplayerStartingType.QUICK_GAME) {
                    multiplayerType = MultiplayerType.NEARBY_CONNECTIONS_MULTIPLAYER;
                    bMultiplayerType.setText(I18N.getTranslation(I18N.Text.NEARBY_CONNECTIONS));
                }
                return null;
            }
        }));

        nextMultiplayerType.addListener(ScreenTools.createInputListener(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                if (multiplayerType == MultiplayerType.REALTIME_MULTIPLAYER && realtimeMultiplayerStartingType == RealtimeMultiplayerStartingType.QUICK_GAME) {
                    realtimeMultiplayerStartingType = RealtimeMultiplayerStartingType.INVITE_PLAYERS;
                    bMultiplayerType.setText(I18N.getTranslation(I18N.Text.INVITE_PLAYERS));
                } else if (multiplayerType == MultiplayerType.REALTIME_MULTIPLAYER && realtimeMultiplayerStartingType == RealtimeMultiplayerStartingType.INVITE_PLAYERS) {
                    multiplayerType = MultiplayerType.NEARBY_CONNECTIONS_MULTIPLAYER;
                    bMultiplayerType.setText(I18N.getTranslation(I18N.Text.NEARBY_CONNECTIONS));
                } else if (multiplayerType == MultiplayerType.NEARBY_CONNECTIONS_MULTIPLAYER) {
                    multiplayerType = MultiplayerType.REALTIME_MULTIPLAYER;
                    realtimeMultiplayerStartingType = RealtimeMultiplayerStartingType.QUICK_GAME;
                    bMultiplayerType.setText(I18N.getTranslation(I18N.Text.QUICK_GAME));
                }
                return null;
            }
        }));

        bShowInvitations.addListener(ScreenTools.createInputListener(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                if (Services.getGpgs().isSignedIn()) {
                    MultiplayerContext.setMultiplayerType(MultiplayerType.REALTIME_MULTIPLAYER);
                    Services.getGpgs().showInvitations();
                } else if (!Services.getGpgs().isConnecting()) {
                    Services.getGpgs().signIn();
                } else {
                    Dialogs.showButtonDialog(I18N.getTranslation(I18N.Text.SHOW_INVITATIONS), I18N.getTranslation(I18N.Text.GPGS_REQUIRED));
                }
                return null;
            }
        }));

        bBack.addListener(ScreenTools.createInputListener(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                MultiplayerContext.reset();
                ScreenManager.getInstance().switchScreen(ScreenState.GAME_MODE_SELECTION_SCREEN);
                return null;
            }
        }));

        bGo.addListener(ScreenTools.createInputListener(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                MultiplayerContext.setMultiplayerMode(multiplayerMode);
                MultiplayerContext.setMultiplayerType(multiplayerType);
                if (multiplayerType == MultiplayerType.REALTIME_MULTIPLAYER)
                    ScreenManager.getInstance().switchScreen(ScreenState.AUTO_MATCH_PLAYERS_SELECTION_SCREEN, realtimeMultiplayerStartingType);
                else
                    ScreenManager.getInstance().switchScreen(ScreenState.NEARBY_CONNECTIONS_SCREEN);
                return null;
            }
        }));

        addActor(AbstractScreen.getBackgroundImage());

        table = new Table();
        float maxSize = Math.max(bTeamDeathmatch.getWidth(), bNearbyConnection.getWidth()) + previousMultiplayerMode.getWidth();
        table.add(previousMultiplayerMode).padTop(bBack.getHeight());
        table.add(bMultiplayerMode).minWidth(maxSize).padTop(bBack.getHeight());
        table.add(nextMultiplayerMode).padTop(bBack.getHeight());
        table.row();
        table.add(previousMultiplayerType).padTop(bBack.getHeight());
        table.add(bMultiplayerType).minWidth(maxSize).padTop(bBack.getHeight());
        table.add(nextMultiplayerType).padTop(bBack.getHeight());
        table.row();
        table.add(bShowInvitations).colspan(3).center().padTop(bBack.getHeight());
        table.row();

        Table goAndBackTable = new Table();
        goAndBackTable.add(bGo).padRight(bBack.getWidth());
        goAndBackTable.add(bBack);
        table.add(goAndBackTable).colspan(3).center().padTop(bBack.getHeight()).padBottom(bBack.getHeight()).row();

        ScrollPane scrollPane = new ScrollPane(table, SkinManager.getSkin());
        scrollPane.setBounds(0, 0, AbstractScreen.SCREEN_WIDTH, AbstractScreen.SCREEN_HEIGHT);
        addActor(scrollPane);
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        if (MultiplayerContext.multiplayer) {
            SoloContext.solo = false;
            KryonetUtils.launchKryonetClient();
            ScreenManager.getInstance().showScreen(ScreenState.LOADING_SCREEN, AdMob.HIDE_AD.getValue());
        }
    }

    @Override
    public boolean keyDown(int keyCode) {
        if (keyCode == Input.Keys.BACK) {
            MultiplayerContext.reset();
            ScreenManager.getInstance().switchScreen(ScreenState.GAME_MODE_SELECTION_SCREEN);
        }
        return true;
    }

    @Override
    public void dispose() {
        super.dispose();
        lessButtonTexture.dispose();
        lessButtonTexture = null;
        moreButtonTexture.dispose();
        moreButtonTexture = null;
    }
}
