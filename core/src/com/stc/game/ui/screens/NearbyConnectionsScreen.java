package com.stc.game.ui.screens;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.stc.game.kryonet.tools.KryonetUtils;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.enums.NearbyConnectionsMultiplayerStartingType;
import com.stc.game.services.Services;
import com.stc.game.services.admob.enums.AdMob;
import com.stc.game.solo.SoloContext;
import com.stc.game.ui.dialogs.Dialogs;
import com.stc.game.ui.enums.ScreenState;
import com.stc.game.ui.managers.ScreenManager;
import com.stc.game.ui.managers.SkinManager;
import com.stc.game.ui.tools.I18N;
import com.stc.game.ui.tools.ScreenTools;

import java.util.concurrent.Callable;

public class NearbyConnectionsScreen extends AbstractScreen {
  private final TextButton bStartAdvertising;
  private final TextButton bStartDiscovering;
  private final TextButton bBack;

  private NearbyConnectionsMultiplayerStartingType nearbyConnectionsMultiplayerStartingType;

  public NearbyConnectionsScreen() {
    super();
    bStartAdvertising = new TextButton(I18N.getTranslation(I18N.Text.START_ADVERTISING), SkinManager.getSkin());
    bStartDiscovering = new TextButton(I18N.getTranslation(I18N.Text.START_DISCOVERING), SkinManager.getSkin());
    bBack = new TextButton(I18N.getTranslation(I18N.Text.BACK), SkinManager.getSkin());
  }

  @Override
  public void buildStage() {
    bStartAdvertising.addListener(ScreenTools.createInputListener(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        if (Services.getNetworkService().isConnectedToWiFiOrEthernet()) {
          stopNearbyConnectionsActivity();
          nearbyConnectionsMultiplayerStartingType = NearbyConnectionsMultiplayerStartingType.START_ADVERTISING;
          Services.getNearbyConnectionsService().startAdvertising();
        } else {
          Dialogs.showButtonDialog(I18N.getTranslation(I18N.Text.START_ADVERTISING), I18N.getTranslation(I18N.Text.WIFI_OR_ETHERNET_REQUIRED));
        }
        return null;
      }
    }));

    bStartDiscovering.addListener(ScreenTools.createInputListener(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        if (Services.getNetworkService().isConnectedToWiFiOrEthernet()) {
          stopNearbyConnectionsActivity();
          nearbyConnectionsMultiplayerStartingType = NearbyConnectionsMultiplayerStartingType.START_DISCOVERING;
          Services.getNearbyConnectionsService().startDiscovering();
        } else {
          Dialogs.showButtonDialog(I18N.getTranslation(I18N.Text.START_DISCOVERING), I18N.getTranslation(I18N.Text.WIFI_OR_ETHERNET_REQUIRED));
        }
        return null;
      }
    }));

    bBack.addListener(ScreenTools.createInputListener(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        stopNearbyConnectionsActivity();
        ScreenManager.getInstance().switchScreen(ScreenState.MULTIPLAYER_SCREEN, MultiplayerContext.getMultiplayerMode());
        return null;
      }
    }));

    addActor(AbstractScreen.getBackgroundImage());

    table = new Table();
    table.add(bStartAdvertising).row();
    table.add(bStartDiscovering).padTop(bBack.getHeight()).row();
    table.add(bBack).padTop(bBack.getHeight()).row();

    ScrollPane scrollPane = new ScrollPane(table, SkinManager.getSkin());
    scrollPane.setBounds(0, 0, AbstractScreen.SCREEN_WIDTH, AbstractScreen.SCREEN_HEIGHT);
    addActor(scrollPane);
  }

  @Override
  public void render(float delta) {
    super.render(delta);

    if (MultiplayerContext.multiplayer) {
      SoloContext.solo = false;
      KryonetUtils.launchKryonetClient();
      ScreenManager.getInstance().showScreen(ScreenState.LOADING_SCREEN, AdMob.HIDE_AD.getValue());
    }
  }

  @Override
  public boolean keyDown(int keyCode) {
    if (keyCode == Input.Keys.BACK) {
      stopNearbyConnectionsActivity();
      ScreenManager.getInstance().switchScreen(ScreenState.MULTIPLAYER_SCREEN, MultiplayerContext.getMultiplayerMode());
    }
    return true;
  }

  private void stopNearbyConnectionsActivity() {
    if (nearbyConnectionsMultiplayerStartingType != null) {
      Services.getNearbyConnectionsService().stopNearbyConnectionsActivity(nearbyConnectionsMultiplayerStartingType);
      nearbyConnectionsMultiplayerStartingType = null;
    }
  }
}
