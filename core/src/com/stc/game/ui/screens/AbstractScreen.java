package com.stc.game.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import lombok.Getter;

public abstract class AbstractScreen extends Stage implements Screen {
  private static final boolean CAMERA_CENTERED = true;
  static final int SCREEN_WIDTH = 800;
  static final int SCREEN_HEIGHT = 480;

  private static Texture backgroundTexture;
  @Getter
  private static Image backgroundImage;

  private boolean shouldBeRendered;

  @Getter
  public Table table;

  AbstractScreen() {
    super(new StretchViewport(SCREEN_WIDTH, SCREEN_HEIGHT, new OrthographicCamera()));
    shouldBeRendered = true;
  }

  public abstract void buildStage();

  @Override
  public void show() {
    Gdx.input.setInputProcessor(this);
  }

  @Override
  public void render(float delta) {
    Gdx.gl.glClearColor(1, 1, 1, 1);
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

    if (shouldBeRendered) {
      super.act(delta);
      super.draw();
    }
  }

  @Override
  public void resize(int width, int height) {
    getViewport().update(width, height, CAMERA_CENTERED);
  }

  @Override
  public void pause() {}

  @Override
  public void resume() {}

  @Override
  public void hide() {}

  @Override
  public void dispose() {
    shouldBeRendered = false;
  }

  public static void initBackground() {
    backgroundTexture = new Texture(Gdx.files.internal("textures/images/background.jpg"));
    backgroundImage = new Image(new TextureRegion(backgroundTexture));
    backgroundImage.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
  }

  public static void disposeBackground() {
    if (backgroundTexture != null) {
      backgroundTexture.dispose();
      backgroundTexture = null;
    }
    backgroundImage = null;
  }
}
