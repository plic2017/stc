package com.stc.game.ui.screens;

import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.enums.MultiplayerMode;
import com.stc.game.multiplayer.enums.MultiplayerType;
import com.stc.game.services.admob.enums.AdMob;
import com.stc.game.services.gpgs.achievements.Achievement;
import com.stc.game.ui.managers.ScreenManager;
import com.stc.game.ui.managers.SkinManager;
import com.stc.game.ui.tools.I18N;
import com.stc.game.ui.tools.ScreenTools;
import lombok.Getter;

import java.util.concurrent.Callable;

class WinScreen {
  @Getter
  private static Window winWindow;

  static void initialize() {
    winWindow = new Window("", SkinManager.getSkin());

    TextButton bVictory = new TextButton(I18N.getTranslation(I18N.Text.VICTORY), SkinManager.getSkin());

    bVictory.addListener(ScreenTools.createInputListener(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        dispose();
        if (MultiplayerContext.getMultiplayerType() == MultiplayerType.REALTIME_MULTIPLAYER) {
          if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.DEATHMATCH) {
            Achievement.incrementDeathmatchAchievements();
          } else if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.TEAM_DEATHMATCH) {
            Achievement.incrementTeamDeathmatchAchievements();
          }
        }
        ScreenManager.getInstance().switchScreen(ScreenTools.getScreenToGoBack(), AdMob.RESTART_AD.getValue());
        return null;
      }
    }));

    winWindow.add(bVictory).row();

    winWindow.setSize(AbstractScreen.SCREEN_WIDTH, AbstractScreen.SCREEN_HEIGHT);
    winWindow.setMovable(false);
  }

  private static void dispose() {
    winWindow.remove();
    winWindow = null;
  }
}
