package com.stc.game.ui.screens;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.stc.game.services.Services;
import com.stc.game.services.admob.enums.AdMob;
import com.stc.game.solo.SoloContext;
import com.stc.game.ui.dialogs.Dialogs;
import com.stc.game.ui.enums.ScreenState;
import com.stc.game.ui.managers.ScreenManager;
import com.stc.game.ui.managers.SkinManager;
import com.stc.game.ui.tools.I18N;
import com.stc.game.ui.tools.ScreenTools;
import com.stc.game.utils.random.FinalRandom;

import java.math.BigInteger;
import java.util.concurrent.Callable;

public class SoloScreen extends AbstractScreen {
  private static final String SNAPSHOT_IDENTIFIER = "snapshot-";
  private static final boolean ALLOW_ADD_BUTTON = false;
  private static final boolean ALLOW_DELETE_BUTTON = true;
  private static final int DISPLAY_LIMIT_NONE = -1;

  private final TextButton bNewGame;
  private final TextButton bLoadGame;
  private final TextButton bBack;

  private boolean bNewGameClicked;

  public SoloScreen() {
    super();
    bNewGame = new TextButton(I18N.getTranslation(I18N.Text.NEW_GAME), SkinManager.getSkin());
    bLoadGame = new TextButton(I18N.getTranslation(I18N.Text.LOAD_GAME), SkinManager.getSkin());
    bBack = new TextButton(I18N.getTranslation(I18N.Text.BACK), SkinManager.getSkin());
    bNewGameClicked = false;
  }

  @Override
  public void buildStage() {
    bNewGame.addListener(ScreenTools.createInputListener(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        if (bNewGameClicked)
          return null;

        bNewGameClicked = true;
        SoloContext.snapshotTitleToOverwrite = SNAPSHOT_IDENTIFIER + new BigInteger(281, FinalRandom.random).toString(13);
        ScreenManager.getInstance().switchScreen(ScreenState.LOADING_SCREEN, AdMob.HIDE_AD.getValue());
        return null;
      }
    }));

    bLoadGame.addListener(ScreenTools.createInputListener(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        if (Services.getGpgs().isSignedIn()) {
          Services.getGpgs().showSnapshots(I18N.getTranslation(I18N.Text.SAVED_GAMES), ALLOW_ADD_BUTTON, ALLOW_DELETE_BUTTON, DISPLAY_LIMIT_NONE);
        } else if (!Services.getGpgs().isConnecting()) {
          Services.getGpgs().signIn();
        } else {
          Dialogs.showButtonDialog(I18N.getTranslation(I18N.Text.LOAD_GAME), I18N.getTranslation(I18N.Text.GPGS_REQUIRED));
        }
        return null;
      }
    }));

    bBack.addListener(ScreenTools.createInputListener(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        ScreenManager.getInstance().switchScreen(ScreenState.GAME_MODE_SELECTION_SCREEN);
        return null;
      }
    }));

    addActor(AbstractScreen.getBackgroundImage());

    table = new Table();
    table.add(bNewGame).row();
    table.add(bLoadGame).padTop(bBack.getHeight()).row();
    table.add(bBack).padTop(bBack.getHeight()).row();

    ScrollPane scrollPane = new ScrollPane(table, SkinManager.getSkin());
    scrollPane.setBounds(0, 0, AbstractScreen.SCREEN_WIDTH, AbstractScreen.SCREEN_HEIGHT);
    addActor(scrollPane);
  }

  @Override
  public void render(float delta) {
    super.render(delta);

    if (SoloContext.loadedSavedGames) {
      ScreenManager.getInstance().showScreen(ScreenState.LOADING_SCREEN, AdMob.HIDE_AD.getValue());
    }
  }

  @Override
  public boolean keyDown(int keyCode) {
    if (keyCode == Input.Keys.BACK) {
      ScreenManager.getInstance().switchScreen(ScreenState.GAME_MODE_SELECTION_SCREEN);
    }
    return true;
  }
}
