package com.stc.game.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.stc.game.services.Services;
import com.stc.game.ui.dialogs.Dialogs;
import com.stc.game.ui.enums.ScreenState;
import com.stc.game.ui.managers.ScreenManager;
import com.stc.game.ui.managers.SkinManager;
import com.stc.game.ui.tools.I18N;
import com.stc.game.ui.tools.ScreenTools;

import java.util.concurrent.Callable;

public class HomeScreen extends AbstractScreen {
  private final TextButton bPlay;
  private final TextButton bOptions;
  private final TextButton bSignOut;

  public HomeScreen() {
    super();
    bPlay = new TextButton(I18N.getTranslation(I18N.Text.PLAY), SkinManager.getSkin());
    bOptions = new TextButton(I18N.getTranslation(I18N.Text.OPTIONS), SkinManager.getSkin());
    bSignOut = new TextButton(I18N.getTranslation(I18N.Text.SIGN_OUT), SkinManager.getSkin());
  }

  @Override
  public void buildStage() {
    bPlay.addListener(ScreenTools.createInputListener(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        ScreenManager.getInstance().switchScreen(ScreenState.GAME_MODE_SELECTION_SCREEN);
        return null;
      }
    }));

    bOptions.addListener(ScreenTools.createInputListener(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        ScreenManager.getInstance().switchScreen(ScreenState.OPTIONS_SCREEN);
        return null;
      }
    }));

    bSignOut.addListener(ScreenTools.createInputListener(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        if (!Services.getGpgs().isSignedIn()) {
          Dialogs.showButtonDialog(I18N.getTranslation(I18N.Text.SIGN_OUT), I18N.getTranslation(I18N.Text.ALREADY_SIGNED_OUT));
          return null;
        }
        Services.getGpgs().signOut();
        return null;
      }
    }));

    addActor(AbstractScreen.getBackgroundImage());

    table = new Table();
    table.add(bPlay).row();
    table.add(bOptions).padTop(bSignOut.getHeight()).row();
    table.add(bSignOut).padTop(bSignOut.getHeight()).row();

    ScrollPane scrollPane = new ScrollPane(table, SkinManager.getSkin());
    scrollPane.setBounds(0, 0, AbstractScreen.SCREEN_WIDTH, AbstractScreen.SCREEN_HEIGHT);
    addActor(scrollPane);
  }


  @Override
  public boolean keyDown(int keyCode) {
    if (keyCode == Input.Keys.BACK) {
      Gdx.app.exit();
    }
    return true;
  }
}
