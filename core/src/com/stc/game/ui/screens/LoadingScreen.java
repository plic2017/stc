package com.stc.game.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Timer;
import com.stc.game.gameplay.GamePlayInitializer;
import com.stc.game.services.admob.enums.AdMob;
import com.stc.game.ui.dialogs.Dialogs;
import com.stc.game.ui.enums.ScreenState;
import com.stc.game.ui.managers.ScreenManager;
import com.stc.game.ui.tools.I18N;
import de.tomgrill.gdxdialogs.core.dialogs.GDXProgressDialog;

public class LoadingScreen extends AbstractScreen {
  private static final long TIMEOUT_SPLASH_SCREEN = 1000L;

  private GDXProgressDialog loadingProgressDialog;

  public LoadingScreen() {
    super();
    loadingProgressDialog = Dialogs.getProgressDialog(I18N.getTranslation(I18N.Text.LOADING), I18N.getTranslation(I18N.Text.LOADING_GAME));
  }

  @Override
  public void buildStage() {
    addActor(AbstractScreen.getBackgroundImage());

    final long startTimeMillis = System.currentTimeMillis();

    new Thread(new Runnable() {
      @Override
      public void run() {
        Gdx.app.postRunnable(new Runnable() {
          @Override
          public void run() {
            loadingProgressDialog.build().show();
            load();
            long elapsedTime = System.currentTimeMillis() - startTimeMillis;
            if (elapsedTime < TIMEOUT_SPLASH_SCREEN) {
              Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                  ScreenManager.getInstance().showScreen(ScreenState.GAME_SCREEN, AdMob.HIDE_AD.getValue());
                }
              }, (TIMEOUT_SPLASH_SCREEN - elapsedTime) / 1000f);
            } else {
              ScreenManager.getInstance().showScreen(ScreenState.GAME_SCREEN, AdMob.HIDE_AD.getValue());
            }
          }
        });
      }
    }).start();
  }

  private void load() {
    GamePlayInitializer.initialize();
    PauseScreen.initialize();
    WinScreen.initialize();
    GameOverScreen.initialize();
  }

  @Override
  public void dispose() {
    super.dispose();
    loadingProgressDialog.dismiss();
    loadingProgressDialog = null;
  }
}
