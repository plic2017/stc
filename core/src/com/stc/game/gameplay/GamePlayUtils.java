package com.stc.game.gameplay;

import com.stc.game.gameplay.engine.managers.LabelManager;
import com.stc.game.gameplay.engine.particle.Particle3DEngine;
import com.stc.game.gameplay.engine.pools.customs.UiPool;
import com.stc.game.gameplay.engine.wave.WaveEngine;
import com.stc.game.gameplay.entities.ui.buttons.attacks.AttackButton;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.gameplay.entities.world.bonus.Bonus;
import com.stc.game.gameplay.entities.world.map.playingArea.Cube;
import com.stc.game.gameplay.enums.Attacks;
import com.stc.game.gameplay.enums.Effects3D;
import com.stc.game.multiplayer.tools.MultiplayerSender;
import com.stc.game.ui.enums.GameScreenState;
import com.stc.game.ui.screens.GameScreen;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

public class GamePlayUtils {
  @Getter @Setter
  private static Cube playerArea;
  @Getter @Setter
  private static Cube enemyMultiplayerArea;

  public static void startSoloGame() {
    WaveEngine.startWaveEngine();
  }

  public static void respawnPlayerInSolo() {
    Player currentPlayer = Player.getCurrent();
    if (currentPlayer != null) {
      currentPlayer.respawnPlayer();
      Particle3DEngine.clearAllEffects();
      Bonus.clearBonus();
      WaveEngine.dispose();
      LabelManager.removeAllDynamicLabels();
      WaveEngine.initWaveEngine();
      startSoloGame();
      resetCooldowns();
      GameScreen.setGameScreenState(GameScreenState.RUN);
    }
  }

  public static void respawnPlayerInMultiplayer() {
    Player currentPlayer = Player.getCurrent();
    if (currentPlayer != null) {
      currentPlayer.setTickets(currentPlayer.getTickets() - 1);
      MultiplayerSender.sendPacketRemoveTeamDeathmatchTicket();
      currentPlayer.respawnPlayer();
      MultiplayerSender.sendPlayer(currentPlayer);
      resetCooldowns();
      GameScreen.setGameScreenState(GameScreenState.RUN);
    }
  }

  public static void resetCooldowns() {
    // Attack cooldowns of the current color used
    UiPool uiPool = UiPool.get();
    if (uiPool != null) {
      for (Attacks attackType : Attacks.values()) {
        AttackButton attackButton = (AttackButton) uiPool.getEntityFromId(attackType + AttackButton.getTAG());
        if (attackButton != null) {
          attackButton.setCooldown(0f);
        }
      }
    }

    // Attack cooldowns of the previous colors used
    Player currentPlayer = Player.getCurrent();
    if (currentPlayer != null) {
      for (Map.Entry<Effects3D, Float> cooldownEntry : currentPlayer.getCooldowns().entrySet()) {
        cooldownEntry.setValue(0f);
      }
    }
  }
}
