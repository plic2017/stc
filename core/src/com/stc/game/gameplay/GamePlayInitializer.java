package com.stc.game.gameplay;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.engine.environment.CustomEnvironment;
import com.stc.game.gameplay.engine.environment.SkyboxEnvironment;
import com.stc.game.gameplay.engine.managers.LabelManager;
import com.stc.game.gameplay.entities.ui.hud.labels.Logger;
import com.stc.game.gameplay.engine.managers.BuilderManager;
import com.stc.game.gameplay.engine.managers.ModelManager;
import com.stc.game.gameplay.engine.managers.SpriteManager;
import com.stc.game.gameplay.engine.managers.input.InputManager;
import com.stc.game.gameplay.engine.particle.Particle2DEngine;
import com.stc.game.gameplay.engine.particle.Particle3DEngine;
import com.stc.game.gameplay.engine.physic.Collider;
import com.stc.game.gameplay.engine.pools.MainPool;
import com.stc.game.gameplay.engine.pools.customs.BackgroundEffectPool;
import com.stc.game.gameplay.engine.pools.customs.BackgroundPool;
import com.stc.game.gameplay.engine.pools.customs.CameraPool;
import com.stc.game.gameplay.engine.pools.customs.Effect3DPool;
import com.stc.game.gameplay.engine.pools.customs.EnemyPool;
import com.stc.game.gameplay.engine.pools.customs.MapPool;
import com.stc.game.gameplay.engine.pools.customs.PlayerPool;
import com.stc.game.gameplay.engine.pools.customs.UiEffectPool;
import com.stc.game.gameplay.engine.pools.customs.UiPool;
import com.stc.game.gameplay.engine.shaders.testShader.TestShader;
import com.stc.game.gameplay.engine.view.Camera;
import com.stc.game.gameplay.engine.wave.WaveEngine;
import com.stc.game.gameplay.entities.ui.buttons.attacks.AttackButton;
import com.stc.game.gameplay.entities.ui.buttons.attacks.AttackColorSelectionButton;
import com.stc.game.gameplay.entities.ui.buttons.chat.IncomingChatMessageButton;
import com.stc.game.gameplay.entities.ui.buttons.classics.NearbyConnectionsStarterButton;
import com.stc.game.gameplay.entities.ui.buttons.classics.OptionsWheel;
import com.stc.game.gameplay.entities.ui.buttons.joysticks.MovementJoystick;
import com.stc.game.gameplay.entities.ui.buttons.joysticks.RotationJoystick;
import com.stc.game.gameplay.entities.ui.buttons.requests.HealthBonusRequestButton;
import com.stc.game.gameplay.entities.ui.buttons.requests.RequestsInboxButton;
import com.stc.game.gameplay.entities.ui.hud.Lifebar;
import com.stc.game.gameplay.entities.ui.hud.Radar;
import com.stc.game.gameplay.entities.ui.hud.labels.customs.DynamicLabel;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.gameplay.entities.world.map.decor.CometSpawner;
import com.stc.game.gameplay.entities.world.map.playingArea.Cube;
import com.stc.game.gameplay.entities.world.map.decor.Planet;
import com.stc.game.gameplay.enums.Attacks;
import com.stc.game.gameplay.enums.Colors;
import com.stc.game.gameplay.enums.EndBehaviour;
import com.stc.game.gameplay.enums.Models;
import com.stc.game.gameplay.tools.Tools;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.enums.MultiplayerMode;
import com.stc.game.multiplayer.tools.MultiplayerSender;
import com.stc.game.services.Services;
import com.stc.game.services.gpgs.savedgames.SavedGames;
import com.stc.game.solo.SoloContext;
import com.stc.game.ui.tools.I18N;

import java.util.UUID;

public class GamePlayInitializer {
  private static final String TAG = "GamePlayInitializer";
  private static final int VIEWPORT_HEIGHT = 700;
  private static final boolean LOGGING = false;
  private static final boolean PRINT_FPS = false;
  private static final String PLAYER_IDENTIFIER = "p_";
  private static final Vector3 CAMERA_DELTA_POSITION = new Vector3(0, 1f, -18f);
  private static final float CUBE_SIZE = 30;

  public static void initialize() {
    initEngines();
    initPools();
    initWorld();
    initPlayer();
    initUI();
    initGameSettings();
  }

  private static void initEngines() {
    CustomEnvironment.initEnvironment();
    SkyboxEnvironment.initSkybox();
    BuilderManager.initBuilderManager();
    InputManager.initInputManager();
    ModelManager.initAssets();
    ModelManager.processLoading();
    SpriteManager.initSpriteManager();
    Collider.initCollider();
    Particle2DEngine.initParticle2DEngine();
    Particle3DEngine.initParticle3DEngine();
    Particle3DEngine.processLoading();
    MainPool.initMainPool();
    WaveEngine.initWaveEngine();
    LabelManager.initLabelManager();
    Logger.initLogger(LOGGING, PRINT_FPS);
  }

  private static void initPools() {
    MainPool.addPool(new MapPool());
    MainPool.addPool(new PlayerPool());
    MainPool.addPool(new EnemyPool());
    MainPool.addPool(new Effect3DPool());
    MainPool.addPool(new UiEffectPool());
    MainPool.addPool(new CameraPool());
    MainPool.addPool(new UiPool());
    MainPool.addPool(new BackgroundEffectPool());
    MainPool.addPool(new BackgroundPool());
  }

  private static void initWorld() {

    Vector3 areaOrigin = new Vector3(50, 0, 50);
    float enemyMultiplayerAreaDelta = 15;

    Cube playerArea = new Cube(areaOrigin, CUBE_SIZE, 0);
    Cube enemyMultiplayerArea = new Cube(areaOrigin, CUBE_SIZE, enemyMultiplayerAreaDelta);

    GamePlayUtils.setPlayerArea(playerArea);
    GamePlayUtils.setEnemyMultiplayerArea(enemyMultiplayerArea);

    int worldDiameter = 15;
    Model customPlanet = BuilderManager.buildSphereModel(worldDiameter, 50, new Material());
    Planet planet1 = new Planet(customPlanet);
    planet1.setPosition(playerArea.getCubeCenter().cpy().add(0, 0, 75));
    Planet planet2 = new Planet(customPlanet);
    planet2.setPosition(playerArea.getCubeCenter().cpy().add(0, 0, -75));
    Planet planet3 = new Planet(customPlanet);
    planet3.setPosition(playerArea.getCubeCenter().cpy().add(75, 0, 0));
    Planet planet4 = new Planet(customPlanet);
    planet4.setPosition(playerArea.getCubeCenter().cpy().add(-75, 0, 0));

    CometSpawner cometSpawner = new CometSpawner();

    MapPool.get().add(planet1);
    MapPool.get().add(planet2);
    MapPool.get().add(planet3);
    MapPool.get().add(planet4);
    MapPool.get().add(playerArea);
    MapPool.get().add(enemyMultiplayerArea);
    MapPool.get().add(cometSpawner);
  }

  private static void initPlayer() {
    String playerName = PLAYER_IDENTIFIER + UUID.randomUUID().toString().replace("-", "");

    if (MultiplayerContext.multiplayer) {
      playerName = MultiplayerContext.playerName;
    } else if (SoloContext.loadedSavedGames) {
      playerName = SavedGames.getPlayerName();
    }

    Cube playingArea = GamePlayUtils.getPlayerArea();
    Player player = new Player(playerName);
    player.setPlayingArea(playingArea);
    player.setMovementSpeed(Player.getPlayerMovementSpeed());
    player.setModelPath(Models.PLAYER_MODEL.getModelPath());
    PlayerPool.get().add(player);
    MultiplayerSender.sendPlayer();
  }

  private static void initUI() {
    MovementJoystick movementJoystick = new MovementJoystick(SpriteManager.getSprite("textures/ui/joystick.png"), new Vector2(128, 128), 192);
    RotationJoystick rotationJoystick = new RotationJoystick(SpriteManager.getSprite("textures/ui/cross.png"), new Vector2(Tools.getScreenWidth() - 384, 128), 192);
    OptionsWheel optionsWheel = new OptionsWheel(SpriteManager.getSprite("textures/buttons/options_wheel.png"), new Vector2(Tools.getScreenWidth() - 48 * 2, Tools.getScreenHeight() - 48 * 2));

    if (SoloContext.solo || MultiplayerContext.getMultiplayerMode() == MultiplayerMode.SURVIVAL) {
      AttackColorSelectionButton blueAttackColorSelectionButton = new AttackColorSelectionButton(SpriteManager.getSprite("textures/buttons/blue_color_attack_selection_button.png"),
        new Vector2(Tools.getScreenWidth() - (256 + (100/2)) + 120, 380), Colors.BLUE);
      AttackColorSelectionButton redAttackColorSelectionButton = new AttackColorSelectionButton(SpriteManager.getSprite("textures/buttons/red_color_attack_selection_button.png"),
        new Vector2(Tools.getScreenWidth() - (256 + (100/2)) + 35, 380), Colors.RED);
      AttackColorSelectionButton greenAttackColorSelectionButton = new AttackColorSelectionButton(SpriteManager.getSprite("textures/buttons/green_color_attack_selection_button.png"),
        new Vector2(Tools.getScreenWidth() - (256 + (100/2)) - 50, 380), Colors.GREEN);
      InputManager.addInputButton(blueAttackColorSelectionButton);
      InputManager.addInputButton(redAttackColorSelectionButton);
      InputManager.addInputButton(greenAttackColorSelectionButton);
      Player currentPlayer = Player.getCurrent();
      if (currentPlayer != null) {
        currentPlayer.setCurrentColorUsed(Colors.BLUE); // Default color used by the current player
        currentPlayer.modifyAttackEffects(currentPlayer.getCurrentColorUsed());
      }
    }

    if (MultiplayerContext.inRealtimeMultiplayer()) {
      if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.SURVIVAL) {
        HealthBonusRequestButton healthBonusRequestButton = new HealthBonusRequestButton(SpriteManager.getSprite("textures/buttons/health_bonus_request_button.png"),
          new Vector2(Tools.getScreenWidth() - 48 * 2, Tools.getScreenHeight() - 48 * 4));
        InputManager.addInputButton(healthBonusRequestButton);
        RequestsInboxButton requestsInboxButton = new RequestsInboxButton(SpriteManager.getSprite("textures/buttons/requests_inbox_button.png"),
          new Vector2(Tools.getScreenWidth() - 48 * 2, Tools.getScreenHeight() - 48 * 6));
        InputManager.addInputButton(requestsInboxButton);
        IncomingChatMessageButton incomingChatMessageButton = new IncomingChatMessageButton(SpriteManager.getSprite("textures/buttons/incoming_chat_message_button.png"),
          new Vector2(Tools.getScreenWidth() - 48 * 2, Tools.getScreenHeight() - 48 * 8));
        InputManager.addInputButton(incomingChatMessageButton);
      } else {
        IncomingChatMessageButton incomingChatMessageButton = new IncomingChatMessageButton(SpriteManager.getSprite("textures/buttons/incoming_chat_message_button.png"),
          new Vector2(Tools.getScreenWidth() - 48 * 2, Tools.getScreenHeight() - 48 * 4));
        InputManager.addInputButton(incomingChatMessageButton);
      }
    } else if (MultiplayerContext.inNearbyConnectionsMultiplayer() && Services.getNearbyConnectionsService().isHost()) {
        NearbyConnectionsStarterButton nearbyConnectionsStarterButton = new NearbyConnectionsStarterButton(SpriteManager.getSprite("textures/buttons/ready_button.png"),
          new Vector2(Tools.getScreenWidth() - 48 * 2, Tools.getScreenHeight() - 48 * 6));
        InputManager.addInputButton(nearbyConnectionsStarterButton);
    }

    AttackButton fireballButton = new AttackButton(SpriteManager.getSprite("textures/buttons/fireball_button.png"),
      new Vector2(Tools.getScreenWidth() - 512, 355), Attacks.FIREBALL);
    AttackButton basicShotButton = new AttackButton(SpriteManager.getSprite("textures/buttons/basic_shot_button.png"),
      new Vector2(Tools.getScreenWidth() - 576, 200), Attacks.BASIC_SHOT);
    AttackButton rocketButton = new AttackButton(SpriteManager.getSprite("textures/buttons/rocket_button.png"),
      new Vector2(Tools.getScreenWidth() - 512, 45), Attacks.ROCKET);

    Lifebar lifebar = new Lifebar(SpriteManager.getSprite("textures/ui/lifebar.png"),
      new Vector2(32, Tools.getScreenHeight() - 64));

    Radar radar = new Radar(Player.getCurrent(), GamePlayUtils.getPlayerArea(), EnemyPool.get());

    movementJoystick.setControlledEntity(Player.getCurrent());
    rotationJoystick.setControlledEntity(Player.getCurrent());

    InputManager.addInputButton(movementJoystick);
    InputManager.addInputButton(rotationJoystick);
    InputManager.addInputButton(optionsWheel);
    InputManager.addInputButton(fireballButton);
    InputManager.addInputButton(basicShotButton);
    InputManager.addInputButton(rocketButton);
    UiPool.get().add(lifebar);
    UiPool.get().add(radar);
  }

  /**
   * This function has to be the last called in the build stage function.
   */
  private static void initGameSettings() {
    Camera cam = new Camera(VIEWPORT_HEIGHT * Tools.getScreenRatio(), VIEWPORT_HEIGHT, CAMERA_DELTA_POSITION.cpy());
    cam.setFollowedEntity(Player.getCurrent());

    TestShader shader = new TestShader("shaders/test.fragment.glsl", "shaders/test.vertex.glsl");
    // Donner le material au modèle suffit à lui même car le material appelle un new du shader
    // ColorShader colorShader = new ColorShader("shaders/color.fragment.glsl", "shaders/color.vertex.glsl");
    // EnemyPool.get().setShader(colorShader);
    // PlayerPool.get().setShader(colorShader);

    MapPool.get().setCamera(cam);
    MapPool.get().setShader(shader);
    MapPool.get().setEnvironment(CustomEnvironment.getEnvironment());

    PlayerPool.get().setCamera(cam);
    PlayerPool.get().setEnvironment(CustomEnvironment.getEnvironment());

    EnemyPool.get().setCamera(cam);
    EnemyPool.get().setEnvironment(CustomEnvironment.getEnvironment());

    CameraPool.get().add(cam);

    Particle3DEngine.setCamera(cam);

    SkyboxEnvironment.setCamera(cam);

    LabelManager.addAdvancedLabel(new DynamicLabel(
      TAG,
      new Vector2(-150, Tools.getScreenHeight() / 1.25f),
      new Vector2(Tools.getScreenWidth() / 2.75f, Tools.getScreenHeight() / 1.25f),
      3.5f,
      2,
      2,
      0,
      Color.RED,
      1,
      EndBehaviour.FADE,
      5,
      3,
      I18N.getTranslation(I18N.Text.SURVIVE)
    ));
  }
}
