package com.stc.game.gameplay.tools;

import com.badlogic.gdx.Gdx;

public final class Tools {
    // Resize problem ?
    private static final float screenWidth = Gdx.graphics.getWidth();
    private static final float screenHeight = Gdx.graphics.getHeight();

    public static float getScreenWidth() {
        return screenWidth;
    }

    public static float getScreenHeight() {
        return screenHeight;
    }

    public static float getScreenRatio() {
        float ratio = screenWidth / screenHeight;

        // 10000 is enough to get a reliable ratio
        float roundRatio = Math.round(ratio * 10000);

        return roundRatio / 10000;
    }
}
