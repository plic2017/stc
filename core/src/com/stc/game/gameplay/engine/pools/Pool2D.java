package com.stc.game.gameplay.engine.pools;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.stc.game.gameplay.entities.ui.Entity2D;
import com.stc.game.gameplay.enums.PoolPriority;
import lombok.Getter;

import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;

public class Pool2D<T extends Entity2D> extends ComparablePool{
  @Getter
  protected ArrayBlockingQueue<T> entities;
  protected SpriteBatch spriteBatch;

  protected Pool2D(PoolPriority poolPriority) {
    this.entities = new ArrayBlockingQueue<>(1024);
    this.spriteBatch = new SpriteBatch();
    this.priority = poolPriority;
  }

  public void add(T entity) {
    entities.add(entity);
  }

  @Override
  protected void update() {
    Iterator<T> iter = entities.iterator();
    while (iter.hasNext()) {
      T obj = iter.next();
      obj.update();
    }
  }

  public Entity2D getEntityFromId(String id) {
    if (id != null) {
      Iterator<T> iter = entities.iterator();
      while (iter.hasNext()) {
        T obj = iter.next();
        if (id.equals(obj.getId()))
          return obj;
      }
    }
    return null;
  }

  public void removeEntityFromUniqueId(String id) {
    Iterator<T> it = entities.iterator();
    while (it.hasNext()) {
      T obj = it.next();
      if (id.equals(obj.getId())) {
        obj.dispose();
        it.remove();
      }
    }
  }

  @Override
  protected void draw() {
    spriteBatch.begin();

    Iterator<T> iter = entities.iterator();
    while (iter.hasNext()) {
      T obj = iter.next();
      obj.draw(spriteBatch);
    }

    spriteBatch.end();

    Iterator<T> iterShape = entities.iterator();
    while (iterShape.hasNext()) {
      T obj = iterShape.next();
      obj.postDraw();
    }
  }

  @Override
  public void dispose() {
    for (Entity2D entity : entities)
      entity.dispose();
    entities.clear();
    entities = null;
    spriteBatch.dispose();
    spriteBatch = null;
  }
}
