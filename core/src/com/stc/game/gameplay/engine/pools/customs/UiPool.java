package com.stc.game.gameplay.engine.pools.customs;

import com.stc.game.gameplay.engine.pools.MainPool;
import com.stc.game.gameplay.engine.pools.Pool2D;
import com.stc.game.gameplay.entities.ui.Entity2D;
import com.stc.game.gameplay.entities.ui.buttons.Button;
import com.stc.game.gameplay.enums.PoolPriority;

import java.util.ArrayList;

public class UiPool extends Pool2D<Entity2D> {
  public UiPool() { super(PoolPriority.UI); }

  public void setBoundingButtons(ArrayList<Button> toFill, int screenX, int screenY) {
    for (Entity2D entity : entities) {
      if (entity instanceof Button) {
        Button button = (Button) entity;
        if (button.isInBoundaries(screenX, screenY))
          toFill.add(button);
      }
    }
  }

  public static UiPool get() {
    return (UiPool) MainPool.getPool(PoolPriority.UI);
  }
}
