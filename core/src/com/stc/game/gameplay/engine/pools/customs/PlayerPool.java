package com.stc.game.gameplay.engine.pools.customs;

import com.stc.game.gameplay.engine.pools.MainPool;
import com.stc.game.gameplay.engine.pools.Pool3D;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.gameplay.enums.Models;
import com.stc.game.gameplay.enums.PoolPriority;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.beans.PlayerBean;
import com.stc.game.multiplayer.enums.MultiplayerTeam;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PlayerPool extends Pool3D<Player> {
  private static final List<PlayerBean> pendingPlayers = Collections.synchronizedList(new ArrayList<PlayerBean>());

  public PlayerPool() {
    super(PoolPriority.PLAYER);

    synchronized (pendingPlayers) {
      Iterator<PlayerBean> it = pendingPlayers.iterator();
      while (it.hasNext()) {
        addPlayerFromBean(this, it.next());
      }
    }
    pendingPlayers.clear();
  }

  public static void addPendingPlayer(final PlayerBean playerBean) {
    PlayerPool playerPool = PlayerPool.get();
    if (playerPool != null)
      addPlayerFromBean(playerPool, playerBean);
    else
      pendingPlayers.add(playerBean);
  }

  private static void addPlayerFromBean(final PlayerPool playerPool, final PlayerBean playerBean) {
    if (playerPool != null && playerBean != null) {
      String playerName = playerBean.getPlayerName();
      if (playerPool.getEntityFromUniqueId(playerName) == null) {
        Player player = new Player(playerName);
        player.setPosition(playerBean.getPosition());
        player.setDirection(playerBean.getDirection());
        player.setMovementSpeed(Player.getPlayerMovementSpeed());
        player.setModelPath(Models.PLAYER_MODEL.getModelPath());
        playerPool.add(player);
      }
    }
  }

  public static PlayerPool get() {
    return (PlayerPool) MainPool.getPool(PoolPriority.PLAYER);
  }

  public static boolean remainsOnlyPlayersOfTeam(final MultiplayerTeam team) {
    PlayerPool playerPool = PlayerPool.get();
    if (playerPool != null) {
      for (Player player : playerPool.getEntities()) {
        if (player.getTeam() == null) {
          return false;
        }
        if (player.getTeam() != team) {
          return false;
        }
      }
      return true;
    }
    return false;
  }

  public static boolean remainsOnlyOnePlayer() {
    PlayerPool playerPool = PlayerPool.get();
    return playerPool != null && playerPool.getEntities().size() == 1;
  }

  @Override
  protected void update() {
    super.update();
    if (MultiplayerContext.multiplayer) {
      if (!MultiplayerContext.allPlayersHaveJoined && getEntities().size() == MultiplayerContext.numberOfParticipants) {
        MultiplayerContext.allPlayersHaveJoined = true;
      }
    }
  }

  @Override
  public void dispose() {
    super.dispose();
    pendingPlayers.clear();
  }
}
