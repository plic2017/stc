package com.stc.game.gameplay.engine.pools.customs;

import com.stc.game.gameplay.engine.pools.MainPool;
import com.stc.game.gameplay.engine.pools.Pool3D;
import com.stc.game.gameplay.entities.world.Entity3D;
import com.stc.game.gameplay.entities.world.alive.enemies.Enemy;
import com.stc.game.gameplay.enums.Attacks;
import com.stc.game.gameplay.enums.PoolPriority;
import com.stc.game.kryonet.packets.PacketEnemyAttack;

public class EnemyPool extends Pool3D<Enemy> {
  public EnemyPool() { super(PoolPriority.ENEMY); }

  public void processPacketEnemyAttack(final PacketEnemyAttack packetAttack) {
    for (Enemy enemy : entities) {
      if (enemy.getUniqueId().equals(packetAttack.entityId)) {
        Attacks attackType = Attacks.values()[packetAttack.attackId];
        if (attackType == Attacks.ROCKET) {
          PlayerPool playerPool = PlayerPool.get();
          if (playerPool == null)
            return;
          Entity3D target = playerPool.getEntityFromUniqueId(enemy.getTargetId());
          if (target == null)
            return;
          enemy.getAttack(attackType).triggerAttack(enemy.getPosition(), target);
        } else {
          enemy.getAttack(attackType).triggerAttack(enemy.getPosition());
        }
        break;
      }
    }
  }

  public static EnemyPool get() {
    return (EnemyPool) MainPool.getPool(PoolPriority.ENEMY);
  }
}
