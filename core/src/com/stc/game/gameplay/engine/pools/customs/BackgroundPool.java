package com.stc.game.gameplay.engine.pools.customs;

import com.stc.game.gameplay.engine.pools.Pool2D;
import com.stc.game.gameplay.entities.ui.Entity2D;
import com.stc.game.gameplay.enums.PoolPriority;

public class BackgroundPool extends Pool2D<Entity2D> {
  public BackgroundPool() {
    super(PoolPriority.BACKGROUND);
    spriteBatch.disableBlending();
  }
}
