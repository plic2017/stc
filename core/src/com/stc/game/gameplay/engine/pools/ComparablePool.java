package com.stc.game.gameplay.engine.pools;

import com.stc.game.gameplay.enums.PoolPriority;

abstract class ComparablePool implements Comparable<ComparablePool> {
  PoolPriority priority;

  protected abstract void update();

  protected abstract void draw();

  protected abstract void dispose();

  @Override
  public int compareTo(ComparablePool other) {
    int otherDrawPriority = other.priority.getPoolPriority();
    int thisDrawPriority = this.priority.getPoolPriority();

    if (thisDrawPriority < otherDrawPriority)
      return 1;
    if (thisDrawPriority > otherDrawPriority)
      return -1;
    return 0;
  }
}
