package com.stc.game.gameplay.engine.pools.customs;

import com.stc.game.gameplay.engine.pools.MainPool;
import com.stc.game.gameplay.engine.pools.Pool3D;
import com.stc.game.gameplay.engine.view.Camera;
import com.stc.game.gameplay.enums.PoolPriority;

public class CameraPool extends Pool3D<Camera> {
  public CameraPool() {
    super(PoolPriority.CAMERA);
  }

  @Override
  protected void draw() {}

  public static CameraPool get() {
    return (CameraPool) MainPool.getPool(PoolPriority.CAMERA);
  }
}
