package com.stc.game.gameplay.engine.pools.customs;

import com.stc.game.gameplay.engine.pools.MainPool;
import com.stc.game.gameplay.engine.pools.Pool3D;
import com.stc.game.gameplay.entities.world.map.MapEntity;
import com.stc.game.gameplay.enums.PoolPriority;

public class MapPool extends Pool3D<MapEntity> {

  public MapPool() { super(PoolPriority.MAP); }

  public static MapPool get() {
    return (MapPool) MainPool.getPool(PoolPriority.MAP);
  }

  @Override
  public void dispose() {
    super.dispose();
  }
}