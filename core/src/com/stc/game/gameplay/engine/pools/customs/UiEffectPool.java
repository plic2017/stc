package com.stc.game.gameplay.engine.pools.customs;

import com.stc.game.gameplay.engine.particle.Particle2DEngine;
import com.stc.game.gameplay.engine.pools.Pool2D;
import com.stc.game.gameplay.entities.ui.effects.Effect2D;
import com.stc.game.gameplay.enums.PoolPriority;

public class UiEffectPool extends Pool2D<Effect2D> {
  public UiEffectPool() { super(PoolPriority.UI_EFFECT); }

  @Override
  public void update() { Particle2DEngine.update(PoolPriority.UI_EFFECT); }

  @Override
  public void draw() { Particle2DEngine.draw(PoolPriority.UI_EFFECT); }

  @Override
  public void dispose() { Particle2DEngine.dispose(); }
}
