package com.stc.game.gameplay.engine.pools.customs;

import com.stc.game.gameplay.engine.particle.Particle3DEngine;
import com.stc.game.gameplay.engine.pools.Pool3D;
import com.stc.game.gameplay.entities.world.Entity3D;
import com.stc.game.gameplay.entities.world.effects.Effect3D;
import com.stc.game.gameplay.enums.PoolPriority;

public class Effect3DPool extends Pool3D<Effect3D> {
  public Effect3DPool() { super(PoolPriority.EFFECT3D); }

  @Override
  public Entity3D getEntityFromCollisionId(int collisionId) { return Particle3DEngine.getEntityFromCollisionId(collisionId); }

  @Override
  public void update() { Particle3DEngine.updateParticleEffects(); }

  @Override
  public void draw() { Particle3DEngine.drawParticleEffects(); }

  @Override
  public void dispose() { Particle3DEngine.dispose(); }
}
