package com.stc.game.gameplay.engine.pools;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.stc.game.gameplay.engine.view.Camera;
import com.stc.game.gameplay.entities.world.Entity3D;
import com.stc.game.gameplay.enums.PoolPriority;
import lombok.Getter;
import lombok.Setter;

import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;

public class Pool3D<T extends Entity3D> extends ComparablePool{
  @Getter
  protected ArrayBlockingQueue<T> entities;
  private Array<ModelInstance> instances;
  private ModelBatch modelBatch;

  @Setter
  private Environment environment;
  @Setter
  protected Shader shader;
  private Camera cam;
  private CameraInputController camController;

  protected Pool3D(PoolPriority poolPriority) {
    this.entities = new ArrayBlockingQueue<>(1024);
    this.instances = new Array<>();
    this.modelBatch = new ModelBatch();
    this.priority = poolPriority;
  }

  public void setCamera(Camera cam) {
    this.cam = cam;
    this.camController = new CameraInputController(cam.getCamera());
  }

  public void add(T entity) {
    entities.add(entity);
  }

  public Entity3D getEntityFromCollisionId(int collisionId) {
    Iterator<T> iter = entities.iterator();
    while (iter.hasNext()) {
      T obj = iter.next();
      if (collisionId == obj.getCollisionId())
        return obj;
    }
    return null;
  }

  public Entity3D getEntityFromUniqueId(String uniqueId) {
    if (uniqueId != null) {
      Iterator<T> iter = entities.iterator();
      while (iter.hasNext()) {
        T obj = iter.next();
        if (uniqueId.equals(obj.getUniqueId()))
          return obj;
      }
    }
    return null;
  }

  public boolean removeEntityFromUniqueId(String uniqueId) {
    Iterator<T> it = entities.iterator();
    while (it.hasNext()) {
      T obj = it.next();
      if (uniqueId.equals(obj.getUniqueId())) {
        obj.dispose();
        it.remove();
        return true;
      }
    }
    return false;
  }

  @Override
  protected void update() {
    if (camController != null)
      camController.update();

    Iterator<T> iter = entities.iterator();
    while (iter.hasNext()) {
      T obj = iter.next();
      obj.update();
    }
  }

  @Override
  protected void draw() {
    if (cam == null)
      return;

    modelBatch.begin(cam.getCamera());

    instances.clear();
    Iterator<T> iter = entities.iterator();
    while (iter.hasNext()) {
      T obj = iter.next();
      ModelInstance modelObj = obj.getModelInstance();
      if (modelObj != null) {
        if (isVisible(obj))
          instances.add(modelObj);
      }
    }

    if (shader != null && environment != null)
      modelBatch.render(instances, environment, shader);
    else if (shader != null)
      modelBatch.render(instances, shader);
    else if (environment != null)
      modelBatch.render(instances, environment);
    else
      modelBatch.render(instances);

    modelBatch.end();
  }

  private Vector3 position = new Vector3();
  private boolean isVisible(Entity3D obj) {
    obj.getModelInstance().transform.getTranslation(position);
    position.add(obj.getCenter());
    return cam.getCamera().frustum.sphereInFrustum(position, obj.getRadius());
  }

  @Override
  protected void dispose() {
    for (Entity3D entity : entities)
      entity.dispose();
    entities.clear();
    entities = null;
    instances.clear();
    instances = null;
    modelBatch.dispose();
    modelBatch = null;
    position = null;
    environment = null;
    if (shader != null) {
      shader.dispose();
    }
    cam = null;
    camController = null;
  }
}
