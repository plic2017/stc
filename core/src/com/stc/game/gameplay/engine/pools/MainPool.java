package com.stc.game.gameplay.engine.pools;

import com.stc.game.gameplay.entities.world.Entity3D;
import com.stc.game.gameplay.enums.PoolPriority;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class MainPool {
  private static ArrayList<ComparablePool> pools;

  private MainPool() {}

  public static void initMainPool() {
    pools = new ArrayList<>();
  }

  public static void addPool(ComparablePool pool) {
    pools.add(pool);
    Collections.sort(pools);
  }

  public static ComparablePool getPool(PoolPriority poolName) {
    if (pools == null)
      return null;

    Iterator<ComparablePool> iter = pools.iterator();
    while (iter.hasNext()) {
      ComparablePool obj = iter.next();
      if (obj.priority.equals(poolName))
        return obj;
    }

    return null;
  }

  public static void update() {
    Iterator<ComparablePool> iter = pools.iterator();
    while (iter.hasNext()) {
      ComparablePool obj = iter.next();
      obj.update();
    }
  }

  public static void draw() {
    Iterator<ComparablePool> iter = pools.iterator();
    while (iter.hasNext()) {
      ComparablePool obj = iter.next();
      obj.draw();
    }
  }

  public static void dispose() {
    Iterator<ComparablePool> iter = pools.iterator();
    while (iter.hasNext()) {
      ComparablePool obj = iter.next();
      obj.dispose();
    }
    pools.clear();
  }

  public static Entity3D getEntityFromCollisionId(int collisionId) {
    Iterator<ComparablePool> iter = pools.iterator();
    while (iter.hasNext()) {
      ComparablePool obj = iter.next();
      if (obj instanceof Pool3D) {
        Entity3D result = ((Pool3D)obj).getEntityFromCollisionId(collisionId);
        if (result != null)
          return result;
      }
    }

    return null;
  }
}
