package com.stc.game.gameplay.engine.environment;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.utils.GdxRuntimeException;

class Skybox {
  private final Pixmap[] data = new Pixmap[6];
  private ShaderProgram shader;

  private int u_worldTrans;
  private Mesh quad;
  private Matrix4 worldTrans;
  private Quaternion viewRotation;

  Skybox(FileHandle positiveX, FileHandle negativeX, FileHandle positiveY, FileHandle negativeY, FileHandle positiveZ, FileHandle negativeZ) {
    this(new Pixmap(positiveX), new Pixmap(negativeX), new Pixmap(positiveY), new Pixmap(negativeY), new Pixmap(positiveZ), new Pixmap(negativeZ));
  }

  private Skybox(Pixmap positiveX, Pixmap negativeX, Pixmap positiveY, Pixmap negativeY, Pixmap positiveZ, Pixmap negativeZ) {
    data[0] = positiveX;
    data[1] = negativeX;

    data[2] = positiveY;
    data[3] = negativeY;

    data[4] = positiveZ;
    data[5] = negativeZ;

    init();
  }

  private void init(){
    shader = new ShaderProgram(Gdx.files.internal("shaders/skybox.vertex.glsl").readString(), Gdx.files.internal("shaders/skybox.fragment.glsl").readString());
    if (!shader.isCompiled())
      throw new GdxRuntimeException(shader.getLog());

    u_worldTrans = shader.getUniformLocation("u_worldTrans");

    worldTrans = new Matrix4();
    viewRotation = new Quaternion();
    quad = createQuad();

    initCubemap();
  }

  private Mesh createQuad(){
    Mesh mesh = new Mesh(true, 4, 6, VertexAttribute.Position(), VertexAttribute.ColorUnpacked(), VertexAttribute.TexCoords(0));
    mesh.setVertices(new float[]
      {-1f, -1f, 0, 1, 1, 1, 1, 0, 1,
        1f, -1f, 0, 1, 1, 1, 1, 1, 1,
        1f, 1f, 0, 1, 1, 1, 1, 1, 0,
        -1f, 1f, 0, 1, 1, 1, 1, 0, 0});
    mesh.setIndices(new short[]{0, 1, 2, 2, 3, 0});
    return mesh;
  }

  private void initCubemap() {
    Gdx.gl20.glBindTexture(GL20.GL_TEXTURE_CUBE_MAP, 0);
    Gdx.gl20.glTexImage2D(GL20.GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL20.GL_RGB, data[0].getWidth(), data[0].getHeight(), 0, GL20.GL_RGB, GL20.GL_UNSIGNED_BYTE, data[0].getPixels());
    Gdx.gl20.glTexImage2D(GL20.GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL20.GL_RGB, data[1].getWidth(), data[1].getHeight(), 0, GL20.GL_RGB, GL20.GL_UNSIGNED_BYTE, data[1].getPixels());

    Gdx.gl20.glTexImage2D(GL20.GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL20.GL_RGB, data[2].getWidth(), data[2].getHeight(), 0, GL20.GL_RGB, GL20.GL_UNSIGNED_BYTE, data[2].getPixels());
    Gdx.gl20.glTexImage2D(GL20.GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL20.GL_RGB, data[3].getWidth(), data[3].getHeight(), 0, GL20.GL_RGB, GL20.GL_UNSIGNED_BYTE, data[3].getPixels());

    Gdx.gl20.glTexImage2D(GL20.GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL20.GL_RGB, data[4].getWidth(), data[4].getHeight(), 0, GL20.GL_RGB, GL20.GL_UNSIGNED_BYTE, data[4].getPixels());
    Gdx.gl20.glTexImage2D(GL20.GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL20.GL_RGB, data[5].getWidth(), data[5].getHeight(), 0, GL20.GL_RGB, GL20.GL_UNSIGNED_BYTE, data[5].getPixels());

    Gdx.gl20.glTexParameteri(GL20.GL_TEXTURE_CUBE_MAP, GL20.GL_TEXTURE_MIN_FILTER, GL20.GL_LINEAR_MIPMAP_LINEAR);
    Gdx.gl20.glTexParameteri(GL20.GL_TEXTURE_CUBE_MAP, GL20.GL_TEXTURE_MAG_FILTER, GL20.GL_LINEAR);
    Gdx.gl20.glTexParameteri(GL20.GL_TEXTURE_CUBE_MAP, GL20.GL_TEXTURE_WRAP_S, GL20.GL_CLAMP_TO_EDGE);
    Gdx.gl20.glTexParameteri(GL20.GL_TEXTURE_CUBE_MAP, GL20.GL_TEXTURE_WRAP_T, GL20.GL_CLAMP_TO_EDGE);

    Gdx.gl20.glGenerateMipmap(GL20.GL_TEXTURE_CUBE_MAP);
  }

  public void draw(Camera camera){

    camera.view.getRotation(viewRotation, true);
    viewRotation.conjugate();

    worldTrans.idt();
    worldTrans.rotate(viewRotation);

    shader.begin();
    shader.setUniformMatrix(u_worldTrans, worldTrans.translate(0, 0, -1));

    quad.render(shader, GL20.GL_TRIANGLES);
    shader.end();
  }

  public void dispose() {
    shader.dispose();
    quad.dispose();
    for (int i = 0; i < 6; i++) {
      data[i].dispose();
    }
    worldTrans = null;
    viewRotation = null;
  }
}
