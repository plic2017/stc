package com.stc.game.gameplay.engine.environment;

import com.badlogic.gdx.Gdx;
import com.stc.game.gameplay.engine.view.Camera;
import lombok.Setter;

public class SkyboxEnvironment {
  @Setter
  private static Camera camera;
  private static Skybox skybox;

  public static void initSkybox() {
    skybox = new Skybox(
      Gdx.files.internal("textures/cubemap/leftImage.png"),
      Gdx.files.internal("textures/cubemap/rightImage.png"),
      Gdx.files.internal("textures/cubemap/upImage.png"),
      Gdx.files.internal("textures/cubemap/downImage.png"),
      Gdx.files.internal("textures/cubemap/frontImage.png"),
      Gdx.files.internal("textures/cubemap/backImage.png"));
  }

  public static void draw() {
    skybox.draw(camera.getCamera());
  }

  public static void dispose() {
    skybox.dispose();
  }
}
