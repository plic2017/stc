package com.stc.game.gameplay.engine.environment;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import lombok.Getter;

public class CustomEnvironment {
  @Getter
  private static Environment environment;
  private static DirectionalLight mainLight;

  private CustomEnvironment () { }

  public static void initEnvironment() {
    environment = new Environment();
    environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.2f, 0.4f, 0.6f, 1f));
    mainLight = new DirectionalLight().set((64f/255f), (198f/255f), (255f/255f), -1f, -1f, -1f);
    environment.add(mainLight);
  }

  public static void dispose() {
    environment.clear();
    environment = null;
    mainLight = null;
  }
}
