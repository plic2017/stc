package com.stc.game.gameplay.engine.managers;

import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;

public class BuilderManager {
  private static ModelBuilder modelBuilder;

  private BuilderManager() { }

  public static void initBuilderManager() {
    modelBuilder = new ModelBuilder();
  }

  public static Model buildSphereModel(float size, int divisions, Material material) {
    if (material == null)
      return null;

    return modelBuilder.createSphere(size, size, size, divisions, divisions, material, Usage.Position | Usage.Normal | Usage.TextureCoordinates);
  }

  public static ModelInstance buildSphereModelInstance(float size, int divisions, Material material, Vector3 position) {
    Model model = buildSphereModel(size, divisions, material);
    if (model == null)
      return null;

    ModelInstance result = new ModelInstance(model);
    result.transform.setToTranslation(position);

    return result;
  }

  public static ModelInstance buildSphereModelInstance(float size, int divisions, Material material) {
    Model model = buildSphereModel(size, divisions, material);
    if (model == null)
      return null;
    return new ModelInstance(model);
  }

  public static void dispose() {
    modelBuilder = null;
  }
}
