package com.stc.game.gameplay.engine.managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public final class SpriteManager {
  private static HashMap<String, Texture> texturesHashMap;

  public static void initSpriteManager() {
    texturesHashMap = new HashMap<>();
  }

  public static Sprite getSprite(String filename) {
    if (!texturesHashMap.containsKey(filename)) {
      Texture texture = new Texture(Gdx.files.internal(filename));
      texturesHashMap.put(filename, texture);
    }

    return new Sprite(texturesHashMap.get(filename));
  }

  public static void dispose() {
    Iterator it = texturesHashMap.entrySet().iterator();
    while (it.hasNext()) {
      Map.Entry pair = (Map.Entry)it.next();
      Texture value = (Texture)pair.getValue();
      if (value != null)
        value.dispose();
    }
    texturesHashMap.clear();
    texturesHashMap = null;
  }
}