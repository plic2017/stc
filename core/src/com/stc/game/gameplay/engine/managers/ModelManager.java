package com.stc.game.gameplay.engine.managers;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g3d.Model;
import com.stc.game.gameplay.enums.Models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class ModelManager {
  private static AssetManager assetManager;
  private static boolean loading;

  private static HashMap<String, Model> modelHashMap;
  private static ArrayList<String> requiredModels;

  private ModelManager() { }

  public static void initAssets() {
    assetManager = new AssetManager();
    modelHashMap = new HashMap<>();
    requiredModels = new ArrayList<>();
    loading = true;

    // Iterates over the Models enum
    for (Models model : Models.values())
      requiredModels.add(model.getModelPath());

    Iterator<String> iter = requiredModels.iterator();
    while (iter.hasNext()) {
      String modelPath = iter.next();
      assetManager.load(modelPath, Model.class);
    }
}

  public static void processLoading() {
    while (loading)
      update();
  }

  public static Model getModel(String modelPath) {
    if (modelPath == null || !modelHashMap.containsKey(modelPath))
      return null;
    return modelHashMap.get(modelPath);
  }

  private static void update() {
    if (assetManager.update())
      initModels();
  }

  private static void initModels() {
    Iterator<String> iter = requiredModels.iterator();
    while (iter.hasNext()) {
      String modelPath = iter.next();
      Model model = assetManager.get(modelPath, Model.class);
      modelHashMap.put(modelPath, model);
    }

    loading = false;
  }

  public static void dispose() {
    assetManager.dispose();
    assetManager = null;
    modelHashMap.clear();
    modelHashMap = null;
    requiredModels.clear();
    requiredModels = null;
  }
}
