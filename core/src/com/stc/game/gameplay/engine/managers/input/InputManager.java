package com.stc.game.gameplay.engine.managers.input;

import com.badlogic.gdx.Gdx;
import com.stc.game.gameplay.engine.pools.customs.UiPool;
import com.stc.game.gameplay.entities.ui.buttons.Button;
import com.stc.game.gameplay.entities.ui.buttons.joysticks.Joystick;
import com.stc.game.ui.enums.GameScreenState;

import java.util.ArrayList;

public class InputManager {
  private static ArrayList<Pointer> pointers;
  private static ArrayList<Button> buttons;
  private static final int MAX_NUMBER_OF_FINGERS_ALLOWED_ON_SCREEN = 2;

  private InputManager() { }

  public static void initInputManager() {
    pointers = new ArrayList<>();
    buttons = new ArrayList<>();
    for (int i = 0; i < MAX_NUMBER_OF_FINGERS_ALLOWED_ON_SCREEN; ++i)
      pointers.add(new Pointer(i));
  }

  public static void addInputButton(Button button) {
    UiPool uiPool = UiPool.get();
    if (uiPool != null)
      uiPool.add(button);
  }

  public static void update(GameScreenState gameScreenState) {
    for (int i = 0; i < MAX_NUMBER_OF_FINGERS_ALLOWED_ON_SCREEN; ++i) {
      int posX = Gdx.input.getX(i);
      int posY = Gdx.input.getY(i);
      Pointer pointer = pointers.get(i);
      UiPool.get().setBoundingButtons(buttons, posX, posY);

      if (Gdx.input.isTouched(i) && gameScreenState.equals(GameScreenState.RUN)) {
        pointer.setPosX(posX);
        pointer.setPosY(posY);

        // TouchDown
        if (!pointer.isTouched()) {
          for (Button button : buttons)
            button.touchDown(posX, posY, i, -1);
          pointer.setTouched(true);
        }
        // TouchDragged
        else {
          for (Button button : buttons)
            button.touchDragged(posX, posY, i);
        }
      }
      // TouchUp
      else if (pointer.isTouched()) {
        for (Button button : buttons)
          button.touchUp(posX, posY, i, -1);
        pointer.setTouched(false);
      }

      for (Button button : buttons)
        if (button instanceof Joystick && pointer.isTouched())
          ((Joystick)button).setTouched(true);

      buttons.clear();
    }
  }

  public static void dispose() {
    pointers.clear();
    pointers = null;
    buttons.clear();
    buttons = null;
  }
}