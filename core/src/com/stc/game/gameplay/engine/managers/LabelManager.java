package com.stc.game.gameplay.engine.managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.stc.game.gameplay.entities.ui.hud.labels.AdvancedLabel;
import com.stc.game.gameplay.entities.ui.hud.labels.customs.DeathmatchTicketsLabel;
import com.stc.game.gameplay.entities.ui.hud.labels.customs.DynamicLabel;
import com.stc.game.gameplay.entities.ui.hud.labels.customs.TeamDeathmatchTicketsLabel;
import com.stc.game.gameplay.entities.ui.hud.labels.customs.WaveEngineLabel;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.enums.MultiplayerMode;
import com.stc.game.solo.SoloContext;

import java.util.ArrayList;
import java.util.Iterator;

public class LabelManager {
  private static SpriteBatch spriteBatch;
  private static ArrayList<AdvancedLabel> advancedLabels;
  private static ArrayList<AdvancedLabel> toRemove;

  private LabelManager() { }

  public static void initLabelManager() {
    spriteBatch = new SpriteBatch();
    advancedLabels = new ArrayList<>();
    toRemove = new ArrayList<>();
    initAdvancedLabels();
  }

  private static void initAdvancedLabels() {
    advancedLabels = new ArrayList<>();

    if (SoloContext.solo || MultiplayerContext.getMultiplayerMode() == MultiplayerMode.SURVIVAL) {
      advancedLabels.add(new WaveEngineLabel(new Vector2(Gdx.graphics.getWidth() / 2f, Gdx.graphics.getHeight() - 2 * 48f), Color.WHITE, 2, 1));
    }
    if (MultiplayerContext.multiplayer) {
      if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.DEATHMATCH) {
        advancedLabels.add(new DeathmatchTicketsLabel(new Vector2(Gdx.graphics.getWidth() / 2f, Gdx.graphics.getHeight() - 2 * 48f), Color.WHITE, 2, 1));
      } else if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.TEAM_DEATHMATCH) {
        advancedLabels.add(new TeamDeathmatchTicketsLabel(new Vector2(Gdx.graphics.getWidth() / 2f, Gdx.graphics.getHeight() - 2 * 48f), Color.WHITE, 2, 1));
      }
    }
  }

  public static void addAdvancedLabel(AdvancedLabel labelToAdd) {
    for (AdvancedLabel label : advancedLabels) {
      if (label.getId().equals(labelToAdd.getId())) {
        if (label instanceof DynamicLabel)
          ((DynamicLabel)label).reset();
        return;
      }
    }
    advancedLabels.add(labelToAdd);
  }

  public static void removeAllDynamicLabels() {
    Iterator<AdvancedLabel> it = advancedLabels.iterator();
    while (it.hasNext()) {
      AdvancedLabel advancedLabel = it.next();
      if (advancedLabel instanceof DynamicLabel) {
        advancedLabel.dispose();
        it.remove();
      }
    }
  }

  public static void update() {
    for (AdvancedLabel advancedLabel : advancedLabels) {
      advancedLabel.update();
      if (advancedLabel.isDisposable())
        toRemove.add(advancedLabel);
    }

    for (AdvancedLabel advancedLabel : toRemove) {
      advancedLabel.dispose();
      advancedLabels.remove(advancedLabel);
    }

    toRemove.clear();
  }

  public static void draw() {
    spriteBatch.begin();
    for (AdvancedLabel advancedLabel : advancedLabels)
      advancedLabel.draw(spriteBatch);
    spriteBatch.end();
  }

  public static void dispose() {
    spriteBatch.dispose();
    spriteBatch = null;
    for (AdvancedLabel advancedLabel : advancedLabels)
      advancedLabel.dispose();
    advancedLabels.clear();
    advancedLabels = null;
    toRemove.clear();
    toRemove = null;
  }
}
