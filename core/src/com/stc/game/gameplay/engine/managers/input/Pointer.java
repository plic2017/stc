package com.stc.game.gameplay.engine.managers.input;

import lombok.Getter;
import lombok.Setter;

class Pointer {
  @Getter
  private int pointerIndex;
  @Getter @Setter
  private int posX;
  @Getter @Setter
  private int posY;
  @Getter @Setter
  private boolean isTouched;

  Pointer(int pointerIndex) {
    this.pointerIndex = pointerIndex;
    posX = 0;
    posY = 0;
    isTouched = false;
  }
}
