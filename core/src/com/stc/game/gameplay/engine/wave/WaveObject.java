package com.stc.game.gameplay.engine.wave;

import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.GamePlayUtils;
import com.stc.game.gameplay.engine.pools.customs.EnemyPool;
import com.stc.game.gameplay.entities.world.alive.enemies.Enemy;
import com.stc.game.gameplay.entities.world.map.playingArea.Cube;
import com.stc.game.gameplay.entities.world.map.playingArea.Face;
import com.stc.game.gameplay.enums.Axis;
import com.stc.game.gameplay.enums.Colors;
import com.stc.game.gameplay.enums.EnemyType;
import com.stc.game.gameplay.enums.Faces;
import com.stc.game.utils.random.FinalRandom;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class WaveObject {
  private static final String ENEMY_IDENTIFIER = "e_";
  @Getter @Setter
  private ArrayList<Enemy> enemies;
  @Getter
  private final List<Enemy> enemiesToClear;
  @Getter @Setter
  private boolean isStarted = false;

  WaveObject() {
    this.enemies = new ArrayList<>();
    this.enemiesToClear = Collections.synchronizedList(new ArrayList<Enemy>());
  }

  public void start() {
    isStarted = true;
    for (Enemy enemy: enemies) {
      EnemyPool.get().add(enemy);
    }
  }

  void addEnemiesToSoloWave(int waveLevel, ArrayList<Faces> unlockedFaces) {
    Cube playerArea = GamePlayUtils.getPlayerArea();
    for (int i = 0; i < waveLevel + 1; i++) {
      int enemyTypeId = FinalRandom.random.nextInt(EnemyType.getMAX_ID());
      Faces faceType = unlockedFaces.get(FinalRandom.random.nextInt(unlockedFaces.size()));
      Colors enemyColor = Colors.values()[FinalRandom.random.nextInt(Colors.values().length - 1)];
      Vector3 position = playerArea.getFaceCenter(faceType).cpy().add(playerArea.getFaceSpawn(faceType));
      modifyPositionToRandom(playerArea, faceType, position);

      String enemyId = ENEMY_IDENTIFIER +
        enemyTypeId + "_" +
        enemyColor.getColorId() + "_" +
        faceType.getFaceId() + "_" +
        UUID.randomUUID().toString().replace("-", "");

      Enemy enemy = EnemyType.buildEnemy(enemyTypeId, enemyId, this, position, enemyColor, faceType);
      addEnemy(faceType, enemy);
    }
  }

  private static void modifyPositionToRandom(Cube playerArea, Faces faceType, Vector3 position) {
    Face face = playerArea.getFace(faceType);

    float xNeg = FinalRandom.random.nextBoolean() ? -1 : 1;
    float yNeg = FinalRandom.random.nextBoolean() ? -1 : 1;
    float xRandom = FinalRandom.random.nextFloat() * playerArea.getSize() / 2 * xNeg;
    float yRandom = FinalRandom.random.nextFloat() * playerArea.getSize() / 2 * yNeg;

    if (face.getLeftAxis() == Axis.X)
      position.x += xRandom;
    else if (face.getLeftAxis() == Axis.Z)
      position.z += xRandom;

    if (face.getTopAxis() == Axis.Y)
      position.y += yRandom;
  }

  void addEnemiesToMultiplayerWave(ArrayList<String> enemiesIds, ArrayList<float[]> enemiesPositions) {
    for (int i = 0; i < enemiesIds.size(); i++) {
      String enemyId = enemiesIds.get(i);

      String[] params = enemyId.split("_");
      int enemyTypeId = Integer.parseInt(params[1]);
      int enemyColorId = Integer.parseInt(params[2]);
      int faceId = Integer.parseInt(params[3]);

      Faces faceType = Faces.values()[faceId];
      Cube playerArea = GamePlayUtils.getPlayerArea();
      Vector3 position = playerArea.getFace(faceType).getBottomRight().cpy().add(playerArea.getFaceSpawn(faceType));
      modifyPositionWithMultiplayerDelta(playerArea, faceType, position, enemiesPositions.get(i)[0], enemiesPositions.get(i)[1]);

      Enemy enemy = EnemyType.buildEnemy(enemyTypeId, enemyId, this, position, Colors.values()[enemyColorId], faceType);
      addEnemy(faceType, enemy);
    }
  }

  private static void modifyPositionWithMultiplayerDelta(Cube playerArea, Faces faceType, Vector3 position, float xDelta, float yDelta) {
    Face face = playerArea.getFace(faceType);

    if (face.getLeftAxis() == Axis.X) {
      if (faceType == Faces.BACK)
        xDelta *= -1;
      position.x += xDelta;
    }
    else if (face.getLeftAxis() == Axis.Z) {
      if (faceType == Faces.LEFT)
        xDelta *= -1;
      position.z += xDelta;
    }

    if (face.getTopAxis() == Axis.Y)
      position.y += yDelta;
  }

  private static void modifyRotationAccordingToFace(Faces faceType, Enemy enemy) {
    if (faceType == Faces.FRONT)
      enemy.setPitchRotation(180);
    if (faceType == Faces.LEFT)
      enemy.setPitchRotation(270);
    if (faceType == Faces.RIGHT)
      enemy.setPitchRotation(90);
    if (faceType == Faces.BACK)
      enemy.setPitchRotation(0);

    enemy.getDirection().setEulerAngles(enemy.getPitchRotation(), 0, 0);
  }

  private void addEnemy(Faces faceType, Enemy enemy) {
    if (enemy != null) {
      modifyRotationAccordingToFace(faceType, enemy);
      enemies.add(enemy);
    }
  }

  public void removeEnemy(Enemy enemy) {
    if (enemies != null && enemies.contains(enemy)) {
      enemiesToClear.add(enemy);
      enemies.remove(enemy);
    }
  }

  public boolean isEmpty() {
    return enemies.isEmpty();
  }

  public void update() {
    synchronized (enemiesToClear) {
      for (Enemy enemy : enemiesToClear)
        EnemyPool.get().removeEntityFromUniqueId(enemy.getUniqueId());
    }
    enemiesToClear.clear();
  }

  private void clearAll() {
    for (Enemy enemy : enemies) {
      EnemyPool.get().removeEntityFromUniqueId(enemy.getUniqueId());
    }
    enemies.clear();
  }

  public void dispose() {
    update();
    clearAll();
  }
}
