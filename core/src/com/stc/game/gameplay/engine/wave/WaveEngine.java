package com.stc.game.gameplay.engine.wave;

import com.stc.game.gameplay.engine.pools.customs.EnemyPool;
import com.stc.game.gameplay.entities.world.alive.enemies.Enemy;
import com.stc.game.gameplay.enums.Faces;
import com.stc.game.kryonet.packets.PacketSendWave;
import com.stc.game.multiplayer.tools.MultiplayerSender;
import com.stc.game.services.Services;
import com.stc.game.services.gpgs.achievements.Achievement;
import com.stc.game.solo.SoloContext;
import com.stc.game.ui.enums.GameScreenState;
import com.stc.game.ui.screens.GameScreen;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

public class WaveEngine {
  private static final int MAX_WAVE_LEVEL = 25;

  private static WaveObject currentWave;
  private static ArrayList<Faces> unlockedFaces;

  @Getter @Setter
  private static int waveLevel = 0;
  private static boolean isStarted = false;
  private static boolean allowedToAskANewWave = false;

  private WaveEngine() { }

  public static void initWaveEngine() {
    currentWave = new WaveObject();
    unlockedFaces = new ArrayList<>();
    unlockedFaces.add(Faces.FRONT);
  }

  public static void startWaveEngine() {
    isStarted = true;
  }

  private static void nextSoloWave() {
    if (waveLevel == 1 || (SoloContext.loadedSavedGames && waveLevel != 0 && unlockedFaces.size() < Faces.values().length)) {
      unlockedFaces.add(Faces.LEFT);
      unlockedFaces.add(Faces.RIGHT);
      unlockedFaces.add(Faces.BACK);
    }

    WaveObject wave = new WaveObject();
    wave.addEnemiesToSoloWave(waveLevel, unlockedFaces);

    currentWave = wave;
    waveLevel++;

    Achievement.checkCurrentWaveLevel();
  }

  public static void nextMultiplayerWave(PacketSendWave packetSendWave) {
    if (!isStarted)
      startWaveEngine();

    if (waveLevel == 0)
      currentWave = new WaveObject();
    currentWave.addEnemiesToMultiplayerWave(packetSendWave.enemiesIds, packetSendWave.enemiesPositions);

    for (Enemy enemy : currentWave.getEnemies()) {
      String enemyId = enemy.getUniqueId();
      if (EnemyPool.get().getEntityFromUniqueId(enemyId) == null &&
          !currentWave.getEnemiesToClear().contains(enemy)) {
        EnemyPool.get().add(enemy);
      }
    }

    if (packetSendWave.complete) {
      allowedToAskANewWave = true;
      waveLevel++;
    }
  }

  public static void update() {
    if (!isStarted)
      return;

    if (SoloContext.solo) {
      if (!currentWave.isStarted())
        currentWave.start();
      if (currentWave.isEmpty()) {
        currentWave.dispose();
        if (waveLevel < MAX_WAVE_LEVEL) {
          nextSoloWave();
        } else {
          Services.getGpgs().unlockAchievement(Achievement.WAVE_25.getAchievementId());
          GameScreen.setGameScreenState(GameScreenState.WIN);
        }
      }
    } else {
      if (allowedToAskANewWave && currentWave.isEmpty()) {
        currentWave.dispose();
        if (waveLevel < MAX_WAVE_LEVEL) {
          allowedToAskANewWave = false;
          MultiplayerSender.sendPacketGetWave();
        } else {
          GameScreen.setGameScreenStateAfterResume(GameScreenState.WIN);
        }
      }
    }

    currentWave.update();
  }

  public static void dispose() {
    waveLevel = 0;
    isStarted = false;
    allowedToAskANewWave = false;
    if (unlockedFaces != null)
      unlockedFaces.clear();
    if (currentWave != null)
      currentWave.dispose();
  }
}
