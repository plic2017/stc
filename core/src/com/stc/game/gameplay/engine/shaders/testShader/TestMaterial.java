package com.stc.game.gameplay.engine.shaders.testShader;

import com.badlogic.gdx.graphics.g3d.Material;

import lombok.Getter;

public class TestMaterial {
  @Getter
  private Material material;

}
