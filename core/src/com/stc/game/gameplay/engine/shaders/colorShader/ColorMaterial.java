package com.stc.game.gameplay.engine.shaders.colorShader;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import lombok.Getter;

public class ColorMaterial {
  @Getter
  private Material material;

  @Getter
  private ColorAttribute attribute;

  public ColorMaterial(Color color) {
    attribute = new ColorShader.TestColorAttribute(ColorShader.TestColorAttribute.Diffuse, color.r, color.g, color.b, color.a);
    material = new Material();
    material.set(attribute);
  }
}
