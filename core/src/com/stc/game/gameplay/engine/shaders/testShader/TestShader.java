package com.stc.game.gameplay.engine.shaders.testShader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.utils.RenderContext;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class TestShader implements Shader {
  private static class TestColorAttribute extends ColorAttribute {
    final static String DiffuseUAlias = "diffuseUColor";
    final static long DiffuseU = register(DiffuseUAlias);

    final static String DiffuseVAlias = "diffuseVColor";
    final static long DiffuseV = register(DiffuseVAlias);

    static {
      Mask = Mask | DiffuseU | DiffuseV;
    }

    TestColorAttribute(long type, float r, float g, float b, float a) {
      super(type, r, g, b, a);
    }
  }

  private String fragmentFile;
  private String vertexFile;

  private ShaderProgram program;
  private int u_projViewTrans;
  private int u_worldTrans;
  private int u_colorU;
  private int u_colorV;

  public TestShader(String fragmentFile, String vertexFile) {
    this.fragmentFile = fragmentFile;
    this.vertexFile = vertexFile;
    init();
  }

  @Override
  public void init() {
    // Charge les shaders dans une string
    String frag = Gdx.files.internal(fragmentFile).readString();
    String vert = Gdx.files.internal(vertexFile).readString();

    // Créé le programme qui lie ces deux shaders
    program = new ShaderProgram(vert, frag);

    // Compile le programme
    if (!program.isCompiled())
      throw new GdxRuntimeException(program.getLog());

    // Récupère les identifiants des Uniforms du program
    u_projViewTrans = program.getUniformLocation("u_projViewTrans");
    u_worldTrans = program.getUniformLocation("u_worldTrans");
    u_colorU = program.getUniformLocation("u_colorU");
    u_colorV = program.getUniformLocation("u_colorV");
  }

  @Override
  public void begin(Camera camera, RenderContext context) {
    program.begin();

    // Set une variable Uniform qui correspond à la matrice de projection de la caméra ?
    program.setUniformMatrix(u_projViewTrans, camera.combined);

    context.setDepthTest(GL20.GL_LEQUAL);
    context.setCullFace(GL20.GL_BACK);
  }

  @Override
  public void render(Renderable renderable) {
    // Set une variable Uniform qui correspond à la matrice du world ?
    program.setUniformMatrix(u_worldTrans, renderable.worldTransform);

    // Récupère la couleur de ce qui va être rendu à l'écran
    Color colorU = ((ColorAttribute)renderable.material.get(TestColorAttribute.DiffuseU)).color;
    Color colorV = ((ColorAttribute)renderable.material.get(TestColorAttribute.DiffuseV)).color;

    // Set deux variables Uniforms qui correspondent aux couleurs utilisées dans le shader pixel
    program.setUniformf(u_colorU, colorU.r, colorU.g, colorU.b);
    program.setUniformf(u_colorV, colorV.r, colorV.g, colorV.b);

    renderable.meshPart.render(program);
  }

  @Override
  public void end() {
    program.end();
  }

  @Override
  public void dispose() {
    fragmentFile = null;
    vertexFile = null;
    program.dispose();
    program = null;
  }

  @Override
  public boolean canRender(Renderable renderable) {
    return renderable.material.has(TestColorAttribute.DiffuseU | TestColorAttribute.DiffuseV);
  }

  @Override
  public int compareTo(Shader other) {
    return 0;
  }
}
