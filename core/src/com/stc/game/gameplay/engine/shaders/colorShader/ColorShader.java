package com.stc.game.gameplay.engine.shaders.colorShader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.GLTexture;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.utils.RenderContext;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.GdxRuntimeException;

class ColorShader implements Shader {
  static class TestColorAttribute extends ColorAttribute {
    final static String DiffuseAlias = "diffuseColor";
    final static long Diffuse = register(DiffuseAlias);

    static {
      Mask = Mask | Diffuse;
    }

    TestColorAttribute(long type, float r, float g, float b, float a) {
      super(type, r, g, b, a);
    }
  }

  private String fragmentFile;
  private String vertexFile;
  private RenderContext renderContext;

  private ShaderProgram program;
  private int u_projViewTrans;
  private int u_worldTrans;
  private int u_sampler2D;
  private int u_color;

  @Override
  public void init() {
    // Charge les shaders dans une string
    String frag = Gdx.files.internal(fragmentFile).readString();
    String vert = Gdx.files.internal(vertexFile).readString();

    // Créé le programme qui lie ces deux shaders
    program = new ShaderProgram(vert, frag);

    // Compile le programme
    if (!program.isCompiled())
      throw new GdxRuntimeException(program.getLog());

    // Récupère les identifiants des Uniforms du program
    u_projViewTrans = program.getUniformLocation("u_projViewTrans");
    u_worldTrans = program.getUniformLocation("u_worldTrans");
    u_color = program.getUniformLocation("u_color");
    u_sampler2D = program.getUniformLocation("u_sampler2D");
  }

  @Override
  public void begin(Camera camera, RenderContext context) {
    renderContext = context;
    program.begin();

    // Set une variable Uniform qui correspond à la matrice de projection de la caméra ?
    program.setUniformMatrix(u_projViewTrans, camera.combined);

    context.setDepthTest(GL20.GL_LEQUAL);
    context.setCullFace(GL20.GL_BACK);
    // Ajoute la transparence mais ne fonctionne pas à cause des textures buguées des modèles
    //context.setBlending(true, GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
  }

  @Override
  public void render(Renderable renderable) {
    // Set une variable Uniform qui correspond à la matrice du monde
    program.setUniformMatrix(u_worldTrans, renderable.worldTransform);

    // Liaison de la Texture
    GLTexture texture = ((TextureAttribute) renderable.material.get(TextureAttribute.Diffuse)).textureDescription.texture;
    program.setUniformi(u_sampler2D, renderContext.textureBinder.bind(texture));

    // Liaison de la Couleur
    Color color = ((ColorAttribute)renderable.material.get(TestColorAttribute.Diffuse)).color;
    program.setUniformf(u_color, color.r, color.g, color.b, color.a);

    renderable.meshPart.render(program);
  }

  @Override
  public void end() {
    program.end();
  }

  @Override
  public void dispose() {
    fragmentFile = null;
    vertexFile = null;
    program.dispose();
    program = null;
    renderContext = null;
  }

  @Override
  public boolean canRender(Renderable renderable) {
    return renderable.material.has(TextureAttribute.Diffuse)
      && renderable.material.has(TestColorAttribute.Diffuse);
  }

  @Override
  public int compareTo(Shader other) {
    return 0;
  }
}
