package com.stc.game.gameplay.engine.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;

public class MaterialUtils {
  public static Material getMaterialWithColor(Color color) {
    Material result = new Material();
    result.set(new ColorAttribute(1, color));
    return result;
  }
}
