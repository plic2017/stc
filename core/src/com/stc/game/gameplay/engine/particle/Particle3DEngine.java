package com.stc.game.gameplay.engine.particle;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.particles.ParticleEffect;
import com.badlogic.gdx.graphics.g3d.particles.ParticleEffectLoader;
import com.badlogic.gdx.graphics.g3d.particles.ParticleSystem;
import com.badlogic.gdx.graphics.g3d.particles.batches.PointSpriteParticleBatch;
import com.stc.game.gameplay.engine.sound.SoundEngine;
import com.stc.game.gameplay.engine.view.Camera;
import com.stc.game.gameplay.entities.world.effects.Effect3D;
import com.stc.game.gameplay.entities.world.effects.EffectAttack;
import com.stc.game.gameplay.enums.Effects3D;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Particle3DEngine {
  private static PerspectiveCamera camera;
  private static AssetManager assetManager;
  private static boolean loading;

  private static ParticleSystem particleSystem;
  private static PointSpriteParticleBatch pointSpriteBatch;
  private static ModelBatch modelBatch;

  private static ArrayList<Effect3D> effect3Ds;
  private static List<Effect3D> effectsToAdd;
  private static ArrayList<Effect3D> effectsToClear;

  private Particle3DEngine() { }

  public static void initParticle3DEngine() {
    loading = true;
    assetManager = new AssetManager();
    particleSystem = new ParticleSystem();
    pointSpriteBatch = new PointSpriteParticleBatch();
    modelBatch = new ModelBatch();
    effect3Ds = new ArrayList<>();
    effectsToAdd = new ArrayList<>();
    effectsToClear = new ArrayList<>();

    particleSystem.add(pointSpriteBatch);

    ParticleEffectLoader.ParticleEffectLoadParameter loadParam = new ParticleEffectLoader.ParticleEffectLoadParameter(particleSystem.getBatches());

    // Iterates over the Effects enum
    for (Effects3D effect : Effects3D.values())
      assetManager.load(effect.getEffectPath(), ParticleEffect.class, loadParam);
  }

  public static Effect3D getEntityFromCollisionId(int collisionId) {
    Iterator<Effect3D> iter = effect3Ds.iterator();
    while (iter.hasNext()) {
      Effect3D obj = iter.next();
      if (collisionId == obj.getCollisionId())
        return obj;
    }
    return null;
  }

  public static void addPendingEffect(Effect3D effect3D) {
    effectsToAdd.add(effect3D);
  }

  private static void startEffect(Effect3D effect3D) {
    if (loading)
      return;

    ParticleEffect originalEffect = assetManager.get(effect3D.getEffectPath());
    ParticleEffect copyEffect = originalEffect.copy();

    effect3D.setCopyEffect(copyEffect);

    if (effect3D instanceof EffectAttack)
      ((EffectAttack) effect3D).initAngles();

    effect3D.getPositionMatrix().setToTranslation(effect3D.getPosition());
    effect3D.getCopyEffect().setTransform(effect3D.getPositionMatrix());
    effect3Ds.add(effect3D);

    copyEffect.init();
    copyEffect.start();
    particleSystem.add(effect3D.getCopyEffect());

    SoundEngine.playSound(effect3D.getSoundPath());
  }

  public static void processLoading() {
    while (loading) {
      if (assetManager.update())
        loading = false;
    }
  }

  public synchronized static void updateParticleEffects() {
    for (Effect3D effect3D : effectsToAdd)
      startEffect(effect3D);

    for (Effect3D effect3D : effect3Ds) {

      // Calculates the motion in order to go to the destination point
      if (effect3D.getTargetPosition() != null)
        effect3D.moveTo(effect3D.getTargetPosition());
      effect3D.update();

      // Applies the calculated position to the matrix of the effect which renders the effect into the world
      effect3D.getPositionMatrix().setToTranslation(effect3D.getPosition());
      effect3D.getCopyEffect().setTransform(effect3D.getPositionMatrix());
      if (effect3D.getLifespan() != Float.MAX_VALUE)
        effect3D.setLifespan(effect3D.getLifespan() - Gdx.graphics.getDeltaTime());

      if (effect3D.getLifespan() <= 0)
        effectsToClear.add(effect3D);
      else if (effect3D.isTargetingEffect() && effect3D.isReadyToBeDisposed())
        effectsToClear.add(effect3D);
    }

    for (Effect3D effect3D : effectsToClear) {
      particleSystem.remove(effect3D.getCopyEffect());
      effect3D.dispose();
      effect3Ds.remove(effect3D);
    }

    effectsToAdd.clear();
    effectsToClear.clear();

    particleSystem.update();
  }

  public static void drawParticleEffects() {
    if (loading && assetManager.update())
      loading = false;

    particleSystem.begin();
    particleSystem.draw();
    particleSystem.end();

    modelBatch.begin(camera);
    modelBatch.render(particleSystem);
    modelBatch.end();
  }

  public static void setCamera(Camera camera) {
    pointSpriteBatch.setCamera(camera.getCamera());
    Particle3DEngine.camera = camera.getCamera();
  }

  public static void clearAllEffects() {
    for (Effect3D effect : effect3Ds)
      effect.dispose();
    effect3Ds.clear();
    for (Effect3D effect : effectsToAdd)
      effect.dispose();
    effectsToAdd.clear();
    for (Effect3D effect : effectsToClear)
      effect.dispose();
    effectsToClear.clear();

    particleSystem.removeAll();
  }

  public static void dispose() {
    clearAllEffects();
    effect3Ds = null;
    effectsToAdd = null;
    effectsToClear = null;
    particleSystem.getBatches().clear();
    particleSystem = null;
    pointSpriteBatch = null;
    modelBatch.dispose();
    modelBatch = null;
    assetManager.dispose();
    assetManager = null;
    camera = null;
  }
}
