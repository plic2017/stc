package com.stc.game.gameplay.engine.particle;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.stc.game.gameplay.entities.ui.effects.Effect2D;
import com.stc.game.gameplay.enums.Effects2D;
import com.stc.game.gameplay.enums.PoolPriority;

import java.util.ArrayList;

public class Particle2DEngine {
  private static SpriteBatch batch;
  private static ArrayList<Effect2D> effects;
  private static ArrayList<Effect2D> effectsToRemove;

  private Particle2DEngine() { }

  public static void initParticle2DEngine () {
    batch = new SpriteBatch();
    effects = new ArrayList<>();
    effectsToRemove = new ArrayList<>();
  }

  public static void addEffect(Effect2D effect) {
    ParticleEffect pe = effect.getCopyEffect();

    pe.load(Gdx.files.internal(effect.getEffectPath()), Gdx.files.internal(Effects2D.getEFFECTS_2D_BASE_PATH()));

    pe.getEmitters().first().setPosition(effect.getPosition().x, effect.getPosition().y);
    pe.start();

    effects.add(effect);
  }

  public static void update(PoolPriority priority) {
    for (Effect2D effect : effects) {
      if (effect.getPriority() != priority)
        continue;

      ParticleEffect pe = effect.getCopyEffect();
      if (pe == null) {
        effectsToRemove.add(effect);
        continue;
      }

      pe.getEmitters().first().setPosition(effect.getPosition().x, effect.getPosition().y);
      pe.update(Gdx.graphics.getDeltaTime());
      if (pe.isComplete())
        pe.reset();
    }

    for (Effect2D effect2D : effectsToRemove)
      effects.remove(effect2D);
    effectsToRemove.clear();
  }

  public static void draw(PoolPriority priority) {
    batch.begin();

    for (Effect2D effect : effects) {
      if (effect.getPriority() != priority)
        continue;

      ParticleEffect pe = effect.getCopyEffect();
      pe.draw(batch);
    }

    batch.end();
  }

  public static void dispose() {
    if (effects != null) {
      for (Effect2D effect : effects)
        effect.dispose();
      effects.clear();
      effects = null;
    }
    if (batch != null) {
      batch.dispose();
      batch = null;
    }
  }
}
