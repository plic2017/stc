package com.stc.game.gameplay.engine.physic;

import com.badlogic.gdx.physics.bullet.collision.btBroadphaseInterface;
import com.badlogic.gdx.physics.bullet.collision.btCollisionDispatcher;
import com.badlogic.gdx.physics.bullet.collision.btCollisionObject;
import com.badlogic.gdx.physics.bullet.collision.btCollisionWorld;
import com.badlogic.gdx.physics.bullet.collision.btDbvtBroadphase;
import com.badlogic.gdx.physics.bullet.collision.btDefaultCollisionConfiguration;

public class Collider {
  private static MyContactListener contactHandler;
  private static btDefaultCollisionConfiguration collisionConfig;
  private static btCollisionDispatcher dispatcher;
  private static btBroadphaseInterface broadphase;
  private static btCollisionWorld collisionWorld;

  private Collider() { }

  public static void initCollider() {
    contactHandler = new MyContactListener();
    collisionConfig = new btDefaultCollisionConfiguration();
    dispatcher = new btCollisionDispatcher(collisionConfig);
    broadphase = new btDbvtBroadphase();
    collisionWorld = new btCollisionWorld(dispatcher, broadphase, collisionConfig);
  }

  public static void update() {
    collisionWorld.performDiscreteCollisionDetection();
  }

  public static void addToCollisionWorld(btCollisionObject object) {
    collisionWorld.addCollisionObject(object);
  }

  public static void removeFromCollisionWorld(btCollisionObject object) {
    collisionWorld.removeCollisionObject(object);
  }

  public static void dispose() {
    contactHandler.dispose();
    contactHandler = null;
    collisionWorld.dispose();
    collisionWorld = null;
    broadphase.dispose();
    broadphase = null;
    dispatcher.dispose();
    dispatcher = null;
    collisionConfig.dispose();
    collisionConfig = null;
  }
}
