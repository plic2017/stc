package com.stc.game.gameplay.engine.physic;

import com.badlogic.gdx.physics.bullet.collision.ContactListener;
import com.stc.game.gameplay.engine.pools.MainPool;
import com.stc.game.gameplay.entities.world.Entity3D;
import com.stc.game.gameplay.entities.world.alive.AliveEntity;
import com.stc.game.gameplay.entities.world.alive.enemies.Enemy;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.gameplay.entities.world.attacks.Attack;
import com.stc.game.gameplay.entities.world.bonus.Bonus;
import com.stc.game.gameplay.entities.world.effects.EffectAttack;
import com.stc.game.gameplay.entities.world.map.MapEntity;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.enums.MultiplayerMode;
import com.stc.game.multiplayer.tools.MultiplayerSender;
import com.stc.game.solo.SoloContext;

import java.util.ArrayList;

class MyContactListener extends ContactListener {
  // Called when something is added to the manifold
  @Override
  public boolean onContactAdded(int userValue0, int partId0, int index0,
                                int userValue1, int partId1, int index1) {

    Entity3D first = MainPool.getEntityFromCollisionId(userValue0);
    Entity3D second = MainPool.getEntityFromCollisionId(userValue1);

    if (first == null || second == null)
      return false;

    if (first instanceof EffectAttack) {
      processAttack((EffectAttack)first, second);
    }
    if (second instanceof EffectAttack) {
      processAttack((EffectAttack)second, first);
    }

    if (first instanceof Bonus && second instanceof Player) {
      processBonus((Bonus)first, (Player)second);
    }
    if (second instanceof Bonus && first instanceof Player) {
      processBonus((Bonus)second, (Player)first);
    }

    return true;
  }

  private static void processBonus(Bonus bonus, Player player){
    bonus.applyEffect(player);
  }

  private static void processAttack(EffectAttack effectAttack, Entity3D attackedEntity) {
    Attack linkedAttack = effectAttack.getLinkedAttack();
    Entity3D attackParent = linkedAttack.getParent();

    if (attackedEntity instanceof EffectAttack) // Ignore collisions between attack effects
      return;
    if (attackedEntity instanceof MapEntity) // Ignore map entities
      return;
    if (attackedEntity instanceof Enemy && attackParent instanceof Enemy) // Ignore friendly fire between enemies
      return;
    if (attackedEntity.equals(attackParent)) // Ignore his own attack
      return;
    ArrayList<String> attackedEntities = effectAttack.getAttackedEntities();
    if ((effectAttack.isSoloTargetAttack() && !attackedEntities.isEmpty()) // Do nothing if the effect is a solo attack and has already attacked an enemy
      || (attackedEntities.contains(attackedEntity.getUniqueId()))) // Do nothing if the effect has already attacked the entity
      return;

    if (SoloContext.solo) {
      if (isUsingTheRightColor(linkedAttack, (AliveEntity) attackedEntity) || attackedEntity instanceof Player) {
        applyDamage(effectAttack, attackedEntity);
      }
    } else {
      if (attackParent instanceof Enemy && attackedEntity instanceof Player) {
        applyDamage(effectAttack, attackedEntity);
      } else {
        handleSurvivalMode(effectAttack, attackedEntity);
        handleDeathMatchMode(effectAttack, attackedEntity);
        handleTeamDeathMatchMode(effectAttack, attackedEntity);
      }
    }
  }

  private static boolean isUsingTheRightColor(Attack attack, AliveEntity aliveEntity) {
    return attack.getAttackColor() == aliveEntity.getColorsType();
  }

  private static void applyDamage(EffectAttack effectAttack, Entity3D attackedEntity) {
    Attack linkedAttack = effectAttack.getLinkedAttack();
    Entity3D attackParent = linkedAttack.getParent();
    attackedEntity.applyDamage(linkedAttack.getDamage(), attackParent);
    effectAttack.getAttackedEntities().add(attackedEntity.getUniqueId());
  }

  private static void handleSurvivalMode(EffectAttack effectAttack, Entity3D attackedEntity) {
    if (MultiplayerContext.getMultiplayerMode() != MultiplayerMode.SURVIVAL) {
      return;
    }
    // Ignore friendly fire
    if (attackedEntity instanceof Player) {
      return;
    }
    // Compare the color of the player attack and the color of the enemy model
    if (!isUsingTheRightColor(effectAttack.getLinkedAttack(), (AliveEntity) attackedEntity)) {
      return;
    }
    processAttackEffectEntityCollision(effectAttack, attackedEntity);
  }

  private static void handleDeathMatchMode(EffectAttack effectAttack, Entity3D attackedEntity) {
    if (MultiplayerContext.getMultiplayerMode() != MultiplayerMode.DEATHMATCH) {
      return;
    }
    processAttackEffectEntityCollision(effectAttack, attackedEntity);
  }

  private static void handleTeamDeathMatchMode(EffectAttack effectAttack, Entity3D attackedEntity) {
    if (MultiplayerContext.getMultiplayerMode() != MultiplayerMode.TEAM_DEATHMATCH) {
      return;
    }
    Attack linkedAttack = effectAttack.getLinkedAttack();
    Entity3D attackParent = linkedAttack.getParent();
    // Ignore friendly fire
    if (((Player) attackParent).getTeam() == ((Player) attackedEntity).getTeam()) {
      return;
    }
    processAttackEffectEntityCollision(effectAttack, attackedEntity);
  }

  private static void processAttackEffectEntityCollision(EffectAttack effectAttack, Entity3D attackedEntity) {
    Attack linkedAttack = effectAttack.getLinkedAttack();
    Entity3D attackParent = linkedAttack.getParent();
    if (attackParent.getUniqueId().equals(MultiplayerContext.playerName)) {
      applyDamage(effectAttack, attackedEntity);
      Player player = (Player) attackParent;
      MultiplayerSender.sendAttackEffectEntityCollision(linkedAttack, player.getDamageCoef(), attackedEntity);
    }
  }
}
