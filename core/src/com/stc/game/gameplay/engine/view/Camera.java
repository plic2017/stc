package com.stc.game.gameplay.engine.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.entities.world.Entity3D;

import lombok.Getter;

public class Camera extends Entity3D {
  private static final String TAG = "Camera";

  @Getter
  private PerspectiveCamera camera;
  private final Vector3 deltaSaved;
  private Vector3 delta;

  private Entity3D followedEntity;

  public Camera(float width, float height, Vector3 delta) {
    float viewportWidth = Math.round(width);
    float viewportHeight = Math.round(height);
    this.delta = delta;
    this.deltaSaved = this.delta.cpy();
    this.direction = new Quaternion(0, 0, 0, 1);

    camera = new PerspectiveCamera(67, viewportWidth, viewportHeight);
    camera.position.set(this.delta);
    camera.far = 200;
    camera.lookAt(0, 0, 0);
    camera.update();
  }

  public void setFollowedEntity(Entity3D followedEntity) {
    this.followedEntity = followedEntity;

    if (followedEntity == null)
      Gdx.app.error(TAG, "Can not set the followed entity of the Camera to null");
    else {
      Vector3 entityPos = followedEntity.getPosition();
      camera.position.set(entityPos.x + delta.x, entityPos.y + delta.y, entityPos.z + delta.z);
      camera.lookAt(entityPos);
      camera.update();
    }
  }

  @Override
  public void update() {
    if (followedEntity != null) {

      Vector3 entityPosition = followedEntity.getPosition().cpy();
      Quaternion entityDirection = followedEntity.getDirection().cpy();

      camera.position.set(entityPosition.add(delta));

      if (!direction.equals(entityDirection)) {

        camera.rotate(direction.conjugate());
        camera.rotate(entityDirection);

        direction = entityDirection;

        delta = deltaSaved.cpy();
        delta.mul(direction);
      }
    }

    camera.update();
  }

  @Override
  public void dispose() {
    super.dispose();
    camera = null;
    delta = null;
    followedEntity = null;
  }
}
