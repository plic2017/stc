package com.stc.game.gameplay.engine.sound;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.stc.game.gameplay.enums.Musics;
import com.stc.game.gameplay.enums.Sounds;
import lombok.Getter;

import java.util.ArrayList;

public class SoundEngine {
  @Getter
  private static float volume;
  @Getter
  private static float beforeMuteVolume;
  private static AssetManager assetManager;
  private static ArrayList<String> requiredSounds;
  private static ArrayList<String> requiredMusics;
  private static Music currentMusic;

  public static void initSoundEngine() {
    requiredSounds = new ArrayList<>();
    requiredMusics = new ArrayList<>();
    assetManager = new AssetManager();
    volume = 0.5f;

    // Iterates over the Models enum
    for (Musics music : Musics.values())
      requiredMusics.add(music.getMusicPath());
    for (Sounds sound : Sounds.values())
      requiredSounds.add(sound.getSoundPath());

    for (String soundPath : requiredSounds) {
      assetManager.load(soundPath, Sound.class);
    }
    for (String musicPath : requiredMusics) {
      assetManager.load(musicPath, Music.class);
    }

    assetManager.finishLoading();
  }

  public static void startMusic(Musics musics) {
    if (assetManager.isLoaded(musics.getMusicPath())) {
      currentMusic = assetManager.get(musics.getMusicPath(), Music.class);
      currentMusic.setVolume(volume);
      currentMusic.play();
      currentMusic.setLooping(true);
    }
  }

  public static void stopMusic(Musics musics){
    if (assetManager.isLoaded(musics.getMusicPath())) {
      Music music = assetManager.get(musics.getMusicPath(), Music.class);
      music.stop();
      currentMusic = null;
    }
  }

  public static void pauseMusic(Musics musics) {
    if (assetManager.isLoaded(musics.getMusicPath())) {
      Music music = assetManager.get(musics.getMusicPath(), Music.class);
      if (music.isPlaying()) {
        music.pause();
      }
    }
  }

  public static void playSound(String soundPath) {
    if (soundPath != null)
      if (assetManager.isLoaded(soundPath)) {
        Sound sound = assetManager.get(soundPath, Sound.class);
        sound.play(volume / 2);
      }
  }

  public static void setVolume(float v) {
    volume = v;
    if (currentMusic != null)
      currentMusic.setVolume(v);
  }

  public static void mute(){
    if (beforeMuteVolume > 0) {
      volume = beforeMuteVolume;
      beforeMuteVolume = 0f;
    } else {
      beforeMuteVolume = volume;
      volume = 0f;
    }
  }

  public static void dispose(){
    requiredSounds.clear();
    requiredSounds = null;
    requiredMusics.clear();
    requiredMusics = null;
    assetManager.dispose();
    assetManager = null;
    currentMusic.dispose();
    currentMusic = null;
  }
}
