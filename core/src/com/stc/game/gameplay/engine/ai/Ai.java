package com.stc.game.gameplay.engine.ai;

import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.engine.pools.customs.EnemyPool;
import com.stc.game.gameplay.engine.pools.customs.PlayerPool;
import com.stc.game.gameplay.entities.world.Entity3D;
import com.stc.game.gameplay.entities.world.alive.AliveEntity;
import com.stc.game.gameplay.entities.world.alive.enemies.Enemy;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.gameplay.entities.world.attacks.Attack;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.enums.MultiplayerMode;
import com.stc.game.solo.SoloContext;
import com.stc.game.utils.maths.MathUtils;

import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;

public class Ai {
  private Ai() { }

  public static String getAutoTarget(final Player shooter) {
    if (SoloContext.solo || MultiplayerContext.getMultiplayerMode() == MultiplayerMode.SURVIVAL) {
      EnemyPool enemyPool = EnemyPool.get();
      if (enemyPool == null)
        return null;
      ArrayBlockingQueue<Enemy> entities = enemyPool.getEntities();
      if (entities == null || entities.isEmpty())
        return null;
      for (Enemy enemy : entities) {
        if (enemy.getColorsType() == shooter.getCurrentColorUsed() && enemy.getCurrentFaces() == shooter.getCurrentFaces()) {
          return enemy.getUniqueId();
        }
      }
      return null;
    } else {
      PlayerPool playerPool = PlayerPool.get();
      if (playerPool == null)
        return null;
      ArrayBlockingQueue<Player> entities = playerPool.getEntities();
      if (entities == null || entities.isEmpty())
        return null;
      for (Player player : entities) {
        if ((MultiplayerContext.getMultiplayerMode() == MultiplayerMode.DEATHMATCH &&
             !player.getUniqueId().equals(shooter.getUniqueId())) ||
            (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.TEAM_DEATHMATCH &&
             player.getTeam() != shooter.getTeam())) {
          return player.getUniqueId();
        }
      }
      return null;
    }
  }

  public static String getTargetId() {
    Player currentPlayer = Player.getCurrent();
    if (currentPlayer != null) {
      return currentPlayer.getUniqueId();
    }
    return null;
  }

  public static String getMultiplayerTargetId(Vector3 enemyPosition) {
    String uniqueId = null;
    PlayerPool playerPool = PlayerPool.get();
    double closestDistance = Double.MAX_VALUE;
    for (Player player : playerPool.getEntities()) {
      double dst = MathUtils.getPositiveDistBetween(enemyPosition, player.getPosition());
      if (dst < closestDistance) {
        uniqueId = player.getUniqueId();
        closestDistance = dst;
      }
    }
    return uniqueId;
  }

  public static Attack getBestAttack(Enemy enemy, ArrayList<Attack> availableAttacks) {
    if (enemy.getGeneralCooldown() > 0)
      return null;

    Attack chosenAttack = null;

    for (Attack attack : availableAttacks) {
      if (attack.getCooldown() <= 0)
        if (chosenAttack == null || chosenAttack.getDamage() < attack.getDamage())
          chosenAttack = attack;
    }

    return chosenAttack;
  }

  public static void useAttack(Enemy enemy, Attack attack) {
    if (enemy == null || attack == null)
      return;

    attack.triggerAttack(enemy.getPosition());
    enemy.setGeneralCooldown(AliveEntity.getGENERAL_COOLDOWN());
  }

  public static boolean isNearOf(Enemy enemy, Entity3D target, float attackRange) {
    return !(enemy == null || target == null) && MathUtils.getPositiveDistBetween(enemy.getPosition(), target.getPosition()) <= attackRange;
  }
}
