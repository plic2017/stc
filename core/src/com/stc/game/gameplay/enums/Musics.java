package com.stc.game.gameplay.enums;

import com.stc.game.gameplay.engine.sound.SoundEngine;
import com.stc.game.solo.SoloContext;

import lombok.Getter;

public enum Musics {
  MAIN_MUSIC("musics/main_music.mp3"),
  SOLO_GAME_MUSIC("musics/solo_game_music.mp3"),
  MULTIPLAYER_GAME_MUSIC("musics/multiplayer_game_music.mp3");

  @Getter
  private final String musicPath;

  Musics(final String musicPath) {
    this.musicPath = musicPath;
  }

  public static void restoreMainMusic() {
    SoundEngine.stopMusic(inGameMusic());
    SoundEngine.startMusic(Musics.MAIN_MUSIC);
  }

  private static Musics inGameMusic() {
    if (SoloContext.solo) {
      return Musics.SOLO_GAME_MUSIC;
    } else {
      return Musics.MULTIPLAYER_GAME_MUSIC;
    }
  }
}
