package com.stc.game.gameplay.enums;

public enum Effects3D {
  BLUE_SUN_EFFECT("bluesun.pfx"),

  BLUE_BASIC_SHOT_EFFECT("bluebasicshot.pfx"),
  GREEN_BASIC_SHOT_EFFECT("greenbasicshot.pfx"),
  RED_BASIC_SHOT_EFFECT("redbasicshot.pfx"),

  BLUE_FIREBALL_EFFECT("bluefireball.pfx"),
  GREEN_FIREBALL_EFFECT("greenfireball.pfx"),
  RED_FIREBALL_EFFECT("redfireball.pfx"),

  BLUE_ROCKET_EFFECT("bluerocket.pfx"),
  GREEN_ROCKET_EFFECT("greenrocket.pfx"),
  RED_ROCKET_EFFECT("redrocket.pfx"),

  COMET_EFFECT("comet.pfx"),
  EXPLOSION_EFFECT("explosion.pfx");

  private static final String EFFECTS_3D_BASE_PATH = "effects/effects3D/";
  private final String effectPath;

  public String getEffectPath() {
    return EFFECTS_3D_BASE_PATH + effectPath;
  }

  Effects3D(final String effectPath) {
    this.effectPath = effectPath;
  }
}
