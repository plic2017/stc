package com.stc.game.gameplay.enums;

import lombok.Getter;

public enum Sounds {
  BASIC_SHOT_SOUND("sounds/basic_shot_sound.mp3"),
  FIREBALL_SOUND("sounds/fireball_sound.mp3"),
  ROCKET_SOUND("sounds/rocket_sound.mp3"),
  EXPLOSION_SOUND("sounds/explosion_sound.mp3"),
  COLOR_ATTACK_SELECTION_SOUND("sounds/color_attack_selection_sound.mp3");

  @Getter
  private final String soundPath;

  Sounds(final String soundPath) {
    this.soundPath = soundPath;
  }
}
