package com.stc.game.gameplay.enums;

import com.badlogic.gdx.graphics.Color;
import lombok.Getter;

public enum Colors {
  BLUE(0, new Color(0, 0, 0.75f, 0)),
  GREEN(1, new Color(0, 0.75f, 0, 0)),
  RED(2, new Color(0.75f, 0, 0, 0)),
  GREY(3, new Color(0.5f, 0.5f, 0.5f, 0.5f));


  @Getter
  private final int colorId;
  @Getter
  private final Color color;

  Colors(final int colorId, final Color color) {
    this.colorId = colorId;
    this.color = color;
  }

  public static int getIdFromColors(final Colors colors) {
    if (colors != null) {
      return colors.getColorId();
    }
    return -1;
  }

  public static Colors getColorFromId(final int colorId) {
    if (colorId == BLUE.getColorId()) {
      return BLUE;
    } else if (colorId == GREEN.getColorId()) {
      return GREEN;
    } else if (colorId == RED.getColorId()) {
      return RED;
    } else if (colorId == GREY.getColorId()) {
      return GREY;
    }
    return null;
  }
}
