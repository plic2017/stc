package com.stc.game.gameplay.enums;

public enum Directions {
  TOP, // Rotation on X or Z
  BOTTOM, // Rotation on X or Z
  LEFT, // Rotation on Y or Z
  RIGHT, // Rotation on Y or Z
  NONE
}
