package com.stc.game.gameplay.enums;

import com.stc.game.gameplay.entities.world.Entity3D;
import com.stc.game.gameplay.entities.world.alive.AliveEntity;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.gameplay.entities.world.attacks.Attack;
import com.stc.game.gameplay.entities.world.attacks.BasicShot;
import com.stc.game.gameplay.entities.world.attacks.Fireball;
import com.stc.game.gameplay.entities.world.attacks.Rocket;
import com.stc.game.utils.Pair;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

public enum Attacks {
  BASIC_SHOT(0),
  FIREBALL(1),
  ROCKET(2);

  @Getter
  private final int attackId;

  @Getter
  private static final Map<Integer, Float> attacksDamages = new HashMap<Integer, Float>() {
    {
      put(BASIC_SHOT.getAttackId(), BasicShot.getBASIC_SHOT_BASE_DAMAGE());
      put(FIREBALL.getAttackId(), Fireball.getFIREBALL_BASE_DAMAGE());
      put(ROCKET.getAttackId(), Rocket.getROCKET_BASE_DAMAGE());
    }
  };

  public static Attack buildAttack(final Entity3D parent, final Attacks attackType) {
    Pair<Effects3D, Colors> result = getEffectAndColor(parent, attackType);
    if (result != null) {
      if (attackType == BASIC_SHOT)
        return new BasicShot(parent, result.getKey(), result.getValue());
      if (attackType == FIREBALL)
        return new Fireball(parent, result.getKey(), result.getValue());
      if (attackType == ROCKET)
        return new Rocket(parent, result.getKey(), result.getValue());
    }
    return null;
  }

  Attacks(final int attackId) {
    this.attackId = attackId;
  }

  private static Pair<Effects3D, Colors> getEffectAndColor(final Entity3D parent, final Attacks attackType) {
    AliveEntity aliveEntity = (AliveEntity) parent;
    if (aliveEntity instanceof Player) {
      if (attackType == BASIC_SHOT)
        return new Pair<>(Effects3D.BLUE_BASIC_SHOT_EFFECT, Colors.BLUE);
      if (attackType == FIREBALL)
        return new Pair<>(Effects3D.RED_FIREBALL_EFFECT, Colors.RED);
      if (attackType == ROCKET)
        return new Pair<>(Effects3D.GREEN_ROCKET_EFFECT, Colors.GREEN);
    } else {
      if (attackType == BASIC_SHOT)
        return aliveEntity.getColorsType() == Colors.BLUE ? new Pair<>(Effects3D.BLUE_BASIC_SHOT_EFFECT, Colors.BLUE) :
          (aliveEntity.getColorsType() == Colors.RED ? new Pair<>(Effects3D.RED_BASIC_SHOT_EFFECT, Colors.RED) : new Pair<>(Effects3D.GREEN_BASIC_SHOT_EFFECT, Colors.GREEN));
      if (attackType == FIREBALL)
        return aliveEntity.getColorsType() == Colors.BLUE ? new Pair<>(Effects3D.BLUE_FIREBALL_EFFECT, Colors.BLUE) :
          (aliveEntity.getColorsType() == Colors.RED ? new Pair<>(Effects3D.RED_FIREBALL_EFFECT, Colors.RED) : new Pair<>(Effects3D.GREEN_FIREBALL_EFFECT, Colors.GREEN));
      if (attackType == ROCKET)
        return aliveEntity.getColorsType() == Colors.BLUE ? new Pair<>(Effects3D.BLUE_ROCKET_EFFECT, Colors.BLUE) :
          (aliveEntity.getColorsType() == Colors.RED ? new Pair<>(Effects3D.RED_ROCKET_EFFECT, Colors.RED) : new Pair<>(Effects3D.GREEN_ROCKET_EFFECT, Colors.GREEN));
    }
    return null;
  }
}
