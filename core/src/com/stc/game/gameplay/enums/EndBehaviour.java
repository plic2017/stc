package com.stc.game.gameplay.enums;

public enum EndBehaviour {
  FADE,
  DISAPPEAR
}
