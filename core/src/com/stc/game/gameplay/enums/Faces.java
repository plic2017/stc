package com.stc.game.gameplay.enums;

import lombok.Getter;

public enum Faces {
  FRONT(0), // +Z
  BACK(1), // -Z
  LEFT(2), // -X
  RIGHT(3); // +X

  @Getter
  private final int faceId;

  Faces(final int faceId) {
    this.faceId = faceId;
  }
}
