package com.stc.game.gameplay.enums;

import lombok.Getter;

public enum Models {
  PLAYER_MODEL("models/player/player_spaceship/player_spaceship.g3db"),
  ENEMY_SMALL_SHIP_MODEL("models/enemies/enemy_small_spaceship_1/enemy_small_spaceship_1.g3db"),
  ENEMY_MEDIUM_SHIP_MODEL("models/enemies/enemy_medium_spaceship_1/enemy_medium_spaceship_1.g3db"),
  ENEMY_BIG_SHIP_MODEL("models/enemies/enemy_big_spaceship_1/enemy_big_spaceship_1.g3db");

  @Getter
  private final String modelPath;

  Models(final String modelPath) {
    this.modelPath = modelPath;
  }
}
