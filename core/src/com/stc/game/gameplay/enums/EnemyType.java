package com.stc.game.gameplay.enums;

import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.engine.wave.WaveObject;
import com.stc.game.gameplay.entities.world.alive.enemies.Enemy;
import com.stc.game.gameplay.entities.world.alive.enemies.EnemyBigShip;
import com.stc.game.gameplay.entities.world.alive.enemies.EnemyMediumShip;
import com.stc.game.gameplay.entities.world.alive.enemies.EnemySmallShip;
import lombok.Getter;

public enum EnemyType {
  ENEMY_SMALL_SHIP(0),
  ENEMY_MEDIUM_SHIP(1),
  ENEMY_BIG_SHIP(2);

  @Getter
  private static final int MAX_ID = EnemyType.values().length;

  @Getter
  private final int enemyTypeId;

  public static Enemy buildEnemy(int enemyTypeId, String enemyId, WaveObject parentWave, Vector3 enemyPosition, Colors enemyColor, Faces enemyFace) {
    Enemy enemy = null;
    if (enemyTypeId == ENEMY_SMALL_SHIP.getEnemyTypeId())
      enemy = new EnemySmallShip(enemyId, parentWave, enemyPosition, enemyColor);
    if (enemyTypeId == ENEMY_MEDIUM_SHIP.getEnemyTypeId())
      enemy = new EnemyMediumShip(enemyId, parentWave, enemyPosition, enemyColor);
    if (enemyTypeId == ENEMY_BIG_SHIP.getEnemyTypeId())
      enemy = new EnemyBigShip(enemyId, parentWave, enemyPosition, enemyColor);
    if (enemy != null) {
      enemy.setCurrentFaces(enemyFace);
      enemy.buildAvailableAttacks();
    }
    return enemy;
  }

  EnemyType(final int enemyTypeId) {
    this.enemyTypeId = enemyTypeId;
  }
}
