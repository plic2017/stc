package com.stc.game.gameplay.enums;

import lombok.Getter;

// Priority 0 will be the last updated/drawn
public enum PoolPriority {
  CAMERA(0),
  UI_EFFECT(1),
  UI(2),
  EFFECT3D(3),
  PLAYER(4),
  ENEMY(5),
  MAP(6),
  BACKGROUND_EFFECT(7),
  BACKGROUND(8);

  @Getter
  private final int poolPriority;

  PoolPriority(final int priority) {
    this.poolPriority = priority;
  }
}
