package com.stc.game.gameplay.enums;

import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.GamePlayUtils;
import com.stc.game.gameplay.entities.world.bonus.CooldownBonus;
import com.stc.game.gameplay.entities.world.bonus.InvincibleBonus;
import com.stc.game.gameplay.entities.world.bonus.LifeBonus;
import com.stc.game.gameplay.entities.world.bonus.PowerBonus;
import com.stc.game.kryonet.packets.PacketBonus;
import lombok.Getter;

import java.util.UUID;

public enum Bonus {
  COOLDOWN_BONUS(0),
  INVINCIBLE_BONUS(1),
  LIFE_BONUS(2),
  POWER_BONUS(3);

  @Getter
  private final int bonusId;

  Bonus(final int bonusId) {
    this.bonusId = bonusId;
  }

  public static com.stc.game.gameplay.entities.world.bonus.Bonus getBonus(final PacketBonus packetBonus) {
    Faces faceType = Faces.values()[packetBonus.faceId];
    Vector3 position = GamePlayUtils.getPlayerArea()
      .getFace(faceType)
      .getCenter();
    if (packetBonus.bonusId == COOLDOWN_BONUS.getBonusId()) {
      return new CooldownBonus(com.stc.game.gameplay.entities.world.bonus.Bonus.getBONUS_IDENTIFIER() + "cooldown" + "_" + UUID.randomUUID().toString().replace("-", ""), position);
    } else if (packetBonus.bonusId == INVINCIBLE_BONUS.getBonusId()) {
      return new InvincibleBonus(com.stc.game.gameplay.entities.world.bonus.Bonus.getBONUS_IDENTIFIER() + "invincible" + "_" + UUID.randomUUID().toString().replace("-", ""), position);
    } else if (packetBonus.bonusId == LIFE_BONUS.getBonusId()) {
      return new LifeBonus(com.stc.game.gameplay.entities.world.bonus.Bonus.getBONUS_IDENTIFIER() + "life" + "_" + UUID.randomUUID().toString().replace("-", ""), position);
    } else if (packetBonus.bonusId == POWER_BONUS.getBonusId()) {
      return new PowerBonus(com.stc.game.gameplay.entities.world.bonus.Bonus.getBONUS_IDENTIFIER() + "power" + "_" + UUID.randomUUID().toString().replace("-", ""), position);
    }
    return null;
  }
}
