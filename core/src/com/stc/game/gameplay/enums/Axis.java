package com.stc.game.gameplay.enums;

public enum Axis {
  X,
  Y,
  Z
}
