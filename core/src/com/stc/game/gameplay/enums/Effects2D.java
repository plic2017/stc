package com.stc.game.gameplay.enums;

import lombok.Getter;

public enum Effects2D {
  LIFEBAR_EFFECT("life.pfx2d");

  @Getter
  private static final String EFFECTS_2D_BASE_PATH = "effects/effects2D/";
  private final String effect2DPath;

  public String getEffect2DPath() {
    return EFFECTS_2D_BASE_PATH + effect2DPath;
  }

  Effects2D(final String effect2DPath) {
    this.effect2DPath = effect2DPath;
  }
}
