package com.stc.game.gameplay.entities.world.bonus;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.engine.managers.BuilderManager;
import com.stc.game.gameplay.engine.utils.MaterialUtils;
import com.stc.game.gameplay.entities.ui.hud.bonus.BonusEffect;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.gameplay.entities.ui.hud.bonus.BonusEffectLife;

public class LifeBonus extends Bonus {
  public LifeBonus(String uniqueId, Vector3 position) {
    super(uniqueId, position);
    this.modelInstance = BuilderManager.buildSphereModelInstance(1.5f, 10, MaterialUtils.getMaterialWithColor(new Color(0.8f, 0, 0, 0)), position);
    if (this.modelInstance != null) {
      this.modelInstance.transform.setToTranslation(position);
    }
  }

  @Override
  public BonusEffect getEffect(final Player player) {
    return new BonusEffectLife(uniqueId, player, new Vector2(50,50));
  }
}
