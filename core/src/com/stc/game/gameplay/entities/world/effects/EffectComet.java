package com.stc.game.gameplay.entities.world.effects;

import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.engine.managers.BuilderManager;
import com.stc.game.gameplay.enums.Effects3D;

public class EffectComet extends Effect3D {
  public EffectComet(Vector3 startPosition, Vector3 target) {
    super(Effects3D.COMET_EFFECT);
    this.lifespan = 20;
    this.movementSpeed = 20;
    this.modelInstance = BuilderManager.buildSphereModelInstance(1, 1, new Material());
    this.position = startPosition;
    this.targetPosition = target;
  }
}
