package com.stc.game.gameplay.entities.world.attacks;

import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.entities.world.Entity3D;
import com.stc.game.gameplay.entities.world.effects.EffectAttack;
import com.stc.game.gameplay.enums.Attacks;
import com.stc.game.gameplay.enums.Colors;
import com.stc.game.gameplay.enums.Effects3D;
import com.stc.game.utils.Pair;
import lombok.Getter;
import lombok.Setter;

public abstract class Attack extends Entity3D {
  @Getter
  protected Entity3D parent;
  @Getter
  protected Attacks type;
  @Getter @Setter
  protected Colors attackColor;

  @Getter @Setter
  Effects3D effectType;
  EffectAttack associatedEffect;
  @Getter @Setter
  protected float cooldown;
  @Getter
  protected float damage;
  @Getter
  protected float range;

  Attack(Entity3D parent) {
    this.parent = parent;
    this.direction = parent.getDirection();
  }

  public abstract Pair<Boolean, Entity3D> triggerAttack(Vector3 startPosition);

  public abstract void triggerAttack(Vector3 startPosition, Entity3D target);

  @Override
  public void dispose() {
    super.dispose();
    parent = null;
    type = null;
    attackColor = null;
    effectType = null;
    associatedEffect = null;
  }
}
