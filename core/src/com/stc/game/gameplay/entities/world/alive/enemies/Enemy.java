package com.stc.game.gameplay.entities.world.alive.enemies;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.GamePlayUtils;
import com.stc.game.gameplay.engine.ai.Ai;
import com.stc.game.gameplay.engine.pools.customs.EnemyPool;
import com.stc.game.gameplay.engine.pools.customs.PlayerPool;
import com.stc.game.gameplay.engine.wave.WaveObject;
import com.stc.game.gameplay.entities.world.Entity3D;
import com.stc.game.gameplay.entities.world.alive.AliveEntity;
import com.stc.game.gameplay.entities.world.attacks.Attack;
import com.stc.game.gameplay.entities.world.bonus.Bonus;
import com.stc.game.gameplay.entities.world.effects.EffectExplosion;
import com.stc.game.gameplay.entities.world.map.playingArea.Face;
import com.stc.game.gameplay.enums.Faces;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.solo.SoloContext;
import com.stc.game.utils.maths.MathUtils;
import lombok.Getter;

public abstract class Enemy extends AliveEntity {
  private static final float ATTACK_RANGE = 10.0f;

  @Getter
  protected WaveObject parentWave;
  @Getter
  protected float powerLevel;
  private Entity3D target;
  private Vector3 targetPosition;

  Enemy(String uniqueId, WaveObject parentWave) {
    super(uniqueId);
    this.currentAcceleration = 0;
    this.parentWave = parentWave;
    this.targetPosition = new Vector3();
  }

  @Override
  public void update() {
    super.update();

    if (target == null || !target.getUniqueId().equals(targetId))
      target = PlayerPool.get().getEntityFromUniqueId(targetId);
    if (target != null)
      moveToTarget();
    moveToFace();

    if (SoloContext.solo) {
      if (currentLife <= 0)
        kill();
      else {
        if (targetId == null)
          targetId = Ai.getTargetId();
        if (targetId != null) {
          Attack attack = Ai.getBestAttack(this, availableAttacks);
          if (attack != null)
            if (Ai.isNearOf(this, target, attack.getRange())) {
              Ai.useAttack(this, attack);
            }
        }
      }
      updateCooldowns();
    }

    if (MultiplayerContext.multiplayer) {
      targetId = Ai.getMultiplayerTargetId(position);
    }
  }

  @Override
  public void moveTo(Vector3 targetPosition) {
    double distance = MathUtils.getPositiveDistBetween(position, targetPosition);

    if (distance < ATTACK_RANGE)
      decelerate();
    else
      accelerate();

    if (currentAcceleration != 0)
      super.moveTo(targetPosition);
  }

  @Override
  public void dispose() {
    super.dispose();
    parentWave = null;
    target = null;
    targetPosition = null;
  }

  private void moveToFace() {
    Face faceToMoveTo = getCurrentFace();
    if (faceToMoveTo == null)
      return;

    if (currentFaces == Faces.FRONT) {
      targetPosition.set(position.x, position.y, faceToMoveTo.getCenter().z + 1);
    }
    if (currentFaces == Faces.BACK) {
      targetPosition.set(position.x, position.y, faceToMoveTo.getCenter().z - 1);
    }
    if (currentFaces == Faces.LEFT) {
      targetPosition.set(faceToMoveTo.getCenter().x + 1, position.y, position.z);
    }
    if (currentFaces == Faces.RIGHT) {
      targetPosition.set(faceToMoveTo.getCenter().x - 1, position.y, position.z);
    }

    moveTo(targetPosition);
  }

  private void moveToTarget() {
    Faces targetFaces = target.getCurrentFaces();
    if (targetFaces != currentFaces) {
      if (!enemiesPresentOnFaces(targetFaces) && isWeakerEnemyType()) {
        move();
      }
    } else {
      if (isWeakerEnemyType()) {
        move();
      }
    }
  }

  private void move() {
    Vector3 targetPlayerPosition = target.getPosition();
    float speed = movementSpeed / 4;

    if (currentFaces == Faces.FRONT || currentFaces == Faces.BACK) {
      float diffX = targetPlayerPosition.x - position.x;
      if (diffX < 0) diffX *= -1;
      if (diffX > 0.5f) {
        if (targetPlayerPosition.x < position.x)
          position.x -= speed * Gdx.graphics.getDeltaTime();
        else if (targetPlayerPosition.x > position.x)
          position.x += speed * Gdx.graphics.getDeltaTime();
      }
    }
    if (currentFaces == Faces.LEFT || currentFaces == Faces.RIGHT) {
      float diffZ = targetPlayerPosition.z - position.z;
      if (diffZ < 0) diffZ *= -1;
      if (diffZ > 0.5f) {
        if (targetPlayerPosition.z < position.z)
          position.z -= speed * Gdx.graphics.getDeltaTime();
        else if (targetPlayerPosition.z > position.z)
          position.z += speed * Gdx.graphics.getDeltaTime();
      }
    }

    float diffY = targetPlayerPosition.y - position.y;
    if (diffY < 0) diffY *= -1;
    if (diffY > 0.5f) {
      if (targetPlayerPosition.y < position.y)
        position.y -= speed * Gdx.graphics.getDeltaTime();
      else if (targetPlayerPosition.y > position.y)
        position.y += speed * Gdx.graphics.getDeltaTime();
    }
  }

  private boolean isWeakerEnemyType() {
    EnemyPool enemyPool = EnemyPool.get();
    for (Enemy enemy : enemyPool.getEntities()) {
      if (enemy.getCurrentFaces() == currentFaces) {
        if (this instanceof EnemyBigShip) {
          if (enemy instanceof EnemyMediumShip || enemy instanceof EnemySmallShip)
            return false;
        }
        if (this instanceof EnemyMediumShip) {
          if (enemy instanceof EnemySmallShip)
            return false;
        }
      }
    }
    return true;
  }

  private boolean enemiesPresentOnFaces(Faces faces) {
    EnemyPool enemyPool = EnemyPool.get();
    for (Enemy enemy : enemyPool.getEntities()) {
      if (enemy.getCurrentFaces() == faces) {
        return true;
      }
    }
    return false;
  }

  private void accelerate() {
    if (currentAcceleration < MAX_ACCELERATION)
      currentAcceleration += ACCELERATION_SPEED;
    if (currentAcceleration > MAX_ACCELERATION)
      currentAcceleration = MAX_ACCELERATION;
  }

  private void decelerate() {
    if (currentAcceleration > 0)
      currentAcceleration -= DECELERATION_SPEED;
    if (currentAcceleration <= 0) {
      currentAcceleration = 0;
      setPositionMotion(Vector3.Zero);
    }
  }

  public void kill() {
    if (parentWave != null)
      parentWave.removeEnemy(this);
    else
      EnemyPool.get().removeEntityFromUniqueId(uniqueId);

    EffectExplosion effectExplosion = new EffectExplosion(this, position, position);
    effectExplosion.triggerEffect();

    if (SoloContext.solo)
      Bonus.pop(GamePlayUtils.getPlayerArea().getRandomPointOnFace(getCurrentFace()));
  }

  private void updateCooldowns() {
    if (generalCooldown > 0) {
      generalCooldown -= Gdx.graphics.getDeltaTime();
      if (generalCooldown < 0)
        generalCooldown = 0;
    }

    for (Attack attack : availableAttacks) {
      if (attack.getCooldown() > 0) {
        attack.setCooldown(attack.getCooldown() - Gdx.graphics.getDeltaTime());
        if (attack.getCooldown() <= 0)
          attack.setCooldown(0);
      }
    }
  }
}
