package com.stc.game.gameplay.entities.world.bonus;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.engine.managers.BuilderManager;
import com.stc.game.gameplay.engine.utils.MaterialUtils;
import com.stc.game.gameplay.entities.ui.hud.bonus.BonusEffect;
import com.stc.game.gameplay.entities.ui.hud.bonus.BonusEffectCooldown;
import com.stc.game.gameplay.entities.world.alive.player.Player;

public class CooldownBonus extends Bonus {
  public CooldownBonus(String uniqueId, Vector3 position) {
    super(uniqueId, position);
    this.modelInstance = BuilderManager.buildSphereModelInstance(1.5f, 10, MaterialUtils.getMaterialWithColor(new Color(0.8f, 0, 0.8f, 0)), position);
  }

  @Override
  protected BonusEffect getEffect(Player player) {
    return new BonusEffectCooldown(uniqueId, player, new Vector2(0, 0));
  }
}
