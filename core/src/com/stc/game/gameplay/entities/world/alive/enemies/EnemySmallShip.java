package com.stc.game.gameplay.entities.world.alive.enemies;

import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.engine.wave.WaveObject;
import com.stc.game.gameplay.enums.Attacks;
import com.stc.game.gameplay.enums.Colors;
import com.stc.game.gameplay.enums.Models;
import com.stc.game.utils.random.FinalRandom;

import java.util.ArrayList;

public class EnemySmallShip extends Enemy {
  public EnemySmallShip(String uniqueId, WaveObject parentWave, Vector3 position, Colors color) {
    super(uniqueId, parentWave);
    this.setPosition(position);
    this.maxLife = 100;
    this.currentLife = maxLife;
    this.modelPath = Models.ENEMY_SMALL_SHIP_MODEL.getModelPath();
    this.movementSpeed = 4;
    this.movementSpeed += FinalRandom.getValueBetween(1, 5);
    this.powerLevel = 1;
    this.colorsType = color;
  }

  @Override
  protected ArrayList<Attacks> buildAvailableAttackTypes() {
    return new ArrayList<Attacks>() {
      {
        add(Attacks.BASIC_SHOT);
      }
    };
  }
}
