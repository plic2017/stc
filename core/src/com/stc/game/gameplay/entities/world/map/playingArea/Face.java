package com.stc.game.gameplay.entities.world.map.playingArea;

import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.enums.Axis;
import com.stc.game.gameplay.enums.Faces;
import lombok.Getter;

public class Face {
  @Getter
  private Faces faceType;

  @Getter
  private Vector3 bottomLeft;
  @Getter
  private Vector3 topLeft;
  @Getter
  private Vector3 topRight;
  @Getter
  private Vector3 bottomRight;
  @Getter
  private Vector3 center;
  @Getter
  private Faces leftFace;
  @Getter
  private Faces rightFace;
  @Getter
  private Axis leftAxis;
  @Getter
  private Axis topAxis;
  @Getter
  private Vector3 facedDirection;

  public Face(Faces faceType, Vector3 bottomRight, Vector3 topRight, Vector3 topLeft, Vector3 bottomLeft,
              Faces leftFace, Faces rightFace, Axis leftAxis, Axis topAxis, Vector3 facedDirection) {
    this.faceType = faceType;
    this.bottomRight = bottomRight;
    this.topRight = topRight;
    this.topLeft = topLeft;
    this.bottomLeft = bottomLeft;
    this.leftFace = leftFace;
    this.rightFace = rightFace;
    this.center = bottomRight.cpy().lerp(this.topLeft, 0.5f);
    this.leftAxis = leftAxis;
    this.topAxis = topAxis;
    this.facedDirection = facedDirection;
  }

  public float getTop() {
    return topRight.y;
  }

  public float getBottom() {
    return bottomRight.y;
  }

  public float getLeft() {
    if (leftAxis == Axis.Z)
      return bottomLeft.z;
    return bottomLeft.x;
  }

  public float getRight() {
    if (leftAxis == Axis.Z)
      return bottomRight.z;
    return bottomRight.x;
  }

  public void addDeltaOnX(float delta) {
    bottomRight.add(delta, 0, 0);
    topRight.add(delta, 0, 0);
    topLeft.add(delta, 0, 0);
    bottomLeft.add(delta, 0, 0);
    center = bottomRight.cpy().lerp(topLeft, 0.5f);
  }

  public void addDeltaOnZ(float delta) {
    bottomRight.add(0, 0, delta);
    topRight.add(0, 0, delta);
    topLeft.add(0, 0, delta);
    bottomLeft.add(0, 0, delta);
    center = bottomRight.cpy().lerp(topLeft, 0.5f);
  }
}
