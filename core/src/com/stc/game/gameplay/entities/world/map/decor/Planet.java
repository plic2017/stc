package com.stc.game.gameplay.entities.world.map.decor;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.stc.game.gameplay.entities.world.map.MapEntity;

public class Planet extends MapEntity {
  public Planet(Model model)  {
    super("planet_01");
    this.modelInstance = new ModelInstance(model);
  }
}