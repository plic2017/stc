package com.stc.game.gameplay.entities.world.bonus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.engine.pools.customs.MapPool;
import com.stc.game.gameplay.engine.pools.customs.UiPool;
import com.stc.game.gameplay.entities.ui.Entity2D;
import com.stc.game.gameplay.entities.ui.hud.bonus.BonusEffect;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.gameplay.entities.world.map.MapEntity;
import com.stc.game.utils.random.FinalRandom;
import lombok.Getter;

import java.util.ArrayList;
import java.util.UUID;

public abstract class Bonus extends MapEntity {
  @Getter
  private static final String BONUS_IDENTIFIER = "b_";
  private static final float LIFE_SPAN = 5;

  private float cooldown;

  Bonus(String uniqueId, Vector3 position){
    super(uniqueId);
    this.position = position;
    this.cooldown = LIFE_SPAN;
  }

  @Override
  public void update() {
    super.update();
    cooldown -= Gdx.graphics.getDeltaTime();
    if (cooldown <= 0) {
      removeBonus();
    }
  }

  public void applyEffect(Player player) {
    UiPool.get().add(getEffect(player));
    removeBonus();
  }

  protected abstract BonusEffect getEffect(Player player);

  public static void pop(Vector3 position) {
    float rand = FinalRandom.getValueBetween(0, 1);
    if (rand <= 0.80f) {
      Bonus bonus;
      if (rand <= 0.30f) {
        bonus = new LifeBonus(BONUS_IDENTIFIER + "life" + "_" + UUID.randomUUID().toString().replace("-", ""), position);
      } else if (rand <= 0.50f) {
        bonus = new CooldownBonus(BONUS_IDENTIFIER + "cooldown" + "_" + UUID.randomUUID().toString().replace("-", ""), position);
      } else if (rand <= 0.60f) {
        bonus = new PowerBonus(BONUS_IDENTIFIER + "power" + "_" + UUID.randomUUID().toString().replace("-", ""), position);
      } else {
        bonus = new InvincibleBonus(BONUS_IDENTIFIER + "invincible" + "_" + UUID.randomUUID().toString().replace("-", ""), position);
      }
      MapPool.get().add(bonus);
    }
  }

  public static void clearBonus() {
    ArrayList<Entity2D> toRemoveInUi = new ArrayList<>();
    UiPool uiPool = UiPool.get();
    for (Entity2D entity : uiPool.getEntities())
      if (entity instanceof BonusEffect)
        toRemoveInUi.add(entity);

    for (Entity2D entity : toRemoveInUi)
      uiPool.getEntities().remove(entity);
    toRemoveInUi.clear();

    ArrayList<MapEntity> toRemoveInMap = new ArrayList<>();
    MapPool mapPool = MapPool.get();
    for (MapEntity entity : mapPool.getEntities())
      if (entity instanceof Bonus)
        toRemoveInMap.add(entity);

    for (MapEntity entity : toRemoveInMap)
      mapPool.getEntities().remove(entity);
    toRemoveInMap.clear();
  }

  private void removeBonus(){
    if (MapPool.get() != null)
      MapPool.get().removeEntityFromUniqueId(uniqueId);
  }
}
