package com.stc.game.gameplay.entities.world.map.playingArea;

import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.entities.world.map.MapEntity;
import com.stc.game.gameplay.enums.Axis;
import com.stc.game.gameplay.enums.Faces;
import com.stc.game.utils.random.FinalRandom;
import lombok.Getter;

import java.util.ArrayList;

public class Cube extends MapEntity {
  private static final float SPAWN_DISTANCE = 50;

  // The origin is the bottom right point of the front face
  @Getter
  private final Vector3 cubeOrigin;
  @Getter
  private final Vector3 cubeCenter;
  @Getter
  private final Vector3 spawnPosition;
  @Getter
  private final float size;
  private final float delta; // Multiplayer attribute which push the faces
  @Getter
  private ArrayList<Face> faces;

  public Cube(Vector3 origin, float size, float delta) {
    super("cube");
    this.cubeOrigin = origin;
    this.cubeCenter = cubeOrigin.cpy();
    this.spawnPosition = new Vector3();
    this.size = size;
    this.delta = delta;
    initFaces();
  }

  private void initFaces() {
    /*
    2--1
    |  |
    3--0
    */
    Face frontFace = new Face(Faces.FRONT,
      cubeOrigin.cpy(),
      cubeOrigin.cpy().add(0, size, 0),
      cubeOrigin.cpy().add(size, size, 0),
      cubeOrigin.cpy().add(size, 0, 0),
      Faces.LEFT,
      Faces.RIGHT,
      Axis.X,
      Axis.Y,
      new Vector3(0, 0, 1)
    );
    Face backFace = new Face(Faces.BACK,
      frontFace.getBottomLeft().cpy().add(0, 0, -size),
      frontFace.getTopLeft().cpy().add(0, 0, -size),
      frontFace.getTopRight().cpy().add(0, 0, -size),
      frontFace.getBottomRight().cpy().add(0, 0, -size),
      Faces.RIGHT,
      Faces.LEFT,
      Axis.X,
      Axis.Y,
      new Vector3(0, 0, -1)
    );
    Face leftFace = new Face(Faces.LEFT,
      frontFace.getBottomLeft().cpy(),
      frontFace.getTopLeft().cpy(),
      backFace.getTopRight().cpy(),
      backFace.getBottomRight().cpy(),
      Faces.BACK,
      Faces.FRONT,
      Axis.Z,
      Axis.Y,
      new Vector3(1, 0, 0)
    );
    Face rightFace = new Face(Faces.RIGHT,
      backFace.getBottomLeft().cpy(),
      backFace.getTopLeft().cpy(),
      frontFace.getTopRight().cpy(),
      frontFace.getBottomRight().cpy(),
      Faces.FRONT,
      Faces.BACK,
      Axis.Z,
      Axis.Y,
      new Vector3(-1, 0, 0)
    );

    faces = new ArrayList<>();
    frontFace.addDeltaOnZ(delta);
    faces.add(frontFace);
    backFace.addDeltaOnZ(-delta);
    faces.add(backFace);
    leftFace.addDeltaOnX(delta);
    faces.add(leftFace);
    rightFace.addDeltaOnX(-delta);
    faces.add(rightFace);

    float centerX = (frontFace.getBottomRight().x + frontFace.getBottomLeft().x) / 2;
    float centerY = (frontFace.getBottomRight().y + frontFace.getTopRight().y) / 2;
    float centerZ = (frontFace.getBottomRight().z + backFace.getBottomRight().z) / 2;
    cubeCenter.set(centerX, centerY, centerZ);
    spawnPosition.set(frontFace.getCenter());
  }

  public Face getFace(Faces faceType) {
    for (Face face: faces)
      if (face.getFaceType().equals(faceType))
        return face;

    return null;
  }

  public Vector3 getFaceSpawn(Faces faceType) {
    Face face = getFace(faceType);
    return
      new Vector3(
        face.getFacedDirection().x * SPAWN_DISTANCE,
        face.getFacedDirection().y * SPAWN_DISTANCE,
        face.getFacedDirection().z * SPAWN_DISTANCE);
  }

  public Vector3 getFaceCenter(Faces faceType) {
    Face face = getFace(faceType);
    return face.getCenter();
  }

  public Vector3 getRandomPointOnFace(Face face) {
    float yMin = face.getBottomRight().y;
    float yMax = face.getTopRight().y;
    if (yMin > yMax) {
      float tmp = yMin;
      yMin = yMax;
      yMax = tmp;
    }
    float deltaY = FinalRandom.getValueBetween(0, yMax - yMin);

    float deltaX = 0;
    if (face.getLeftAxis() == Axis.X) {
      float xMin = face.getBottomRight().x;
      float xMax = face.getBottomLeft().x;
      if (xMin > xMax) {
        float tmp = xMin;
        xMin = xMax;
        xMax = tmp;
      }
      deltaX = FinalRandom.getValueBetween(0, xMax - xMin);
    } else if (face.getLeftAxis() == Axis.Z) {
      float zMin = face.getBottomRight().z;
      float zMax = face.getBottomLeft().z;
      if (zMin > zMax) {
        float tmp = zMin;
        zMin = zMax;
        zMax = tmp;
      }
      deltaX = FinalRandom.getValueBetween(0, zMax - zMin);
    }

    return getPointOnFace(face, deltaX, deltaY);
  }

  private Vector3 getPointOnFace(Face face, float deltaX, float deltaY) {
    Vector3 position = new Vector3();

    if (face.getLeftAxis() == Axis.X) {
      if (face.getFaceType() == Faces.FRONT)
        position.x = face.getBottomRight().x + deltaX;
      else if (face.getFaceType() == Faces.BACK)
        position.x = face.getBottomRight().x - deltaX;
      position.z = face.getBottomRight().z;
    } else if (face.getLeftAxis() == Axis.Z) {
      if (face.getFaceType() == Faces.RIGHT)
        position.z = face.getBottomRight().z + deltaX;
      else if (face.getFaceType() == Faces.LEFT)
        position.z = face.getBottomRight().z - deltaX;
      position.x = face.getBottomRight().x;
    }

    position.y = face.getBottom() + deltaY;

    return position;
  }

  @Override
  public void dispose() {
    super.dispose();
    if (faces != null) {
      faces.clear();
      faces = null;
    }
  }
}
