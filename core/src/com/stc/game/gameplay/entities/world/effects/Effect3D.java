package com.stc.game.gameplay.entities.world.effects;

import com.badlogic.gdx.graphics.g3d.particles.ParticleEffect;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.engine.particle.Particle3DEngine;
import com.stc.game.gameplay.entities.world.Entity3D;
import com.stc.game.gameplay.enums.Effects3D;
import com.stc.game.gameplay.enums.Sounds;
import com.stc.game.utils.random.FinalRandom;
import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;

public class Effect3D extends Entity3D {
  private static final String EFFECT_3D_IDENTIFIER = "fx_";

  @Getter @Setter
  private ParticleEffect copyEffect;
  @Getter
  private String soundPath;
  @Getter
  private String effectPath;
  @Getter
  private Matrix4 positionMatrix = new Matrix4();
  @Getter @Setter
  protected boolean isTargetingEffect;
  @Getter
  protected boolean isReadyToBeDisposed;
  @Getter @Setter
  protected Vector3 targetPosition;
  @Getter @Setter
  protected double lifespan;

  Effect3D(Effects3D effectPath) {
    super(EFFECT_3D_IDENTIFIER + new BigInteger(281, FinalRandom.random).toString(13));
    this.effectPath = effectPath.getEffectPath();
  }

  Effect3D(Entity3D parent, Effects3D effectPath, Sounds soundPath) {
    super(EFFECT_3D_IDENTIFIER + new BigInteger(281, FinalRandom.random).toString(13));
    this.parent = parent;
    this.direction = parent.getDirection();
    this.effectPath = effectPath.getEffectPath();
    this.soundPath = soundPath.getSoundPath();
  }

  public void triggerEffect() {
    Particle3DEngine.addPendingEffect(this);
  }

  @Override
  public void dispose() {
    super.dispose();
    if (copyEffect != null) {
      copyEffect.dispose();
      copyEffect = null;
    }
    soundPath = null;
    effectPath = null;
    positionMatrix = null;
    targetPosition = null;
  }
}
