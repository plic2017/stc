package com.stc.game.gameplay.entities.world.alive.player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.GamePlayUtils;
import com.stc.game.gameplay.engine.pools.customs.PlayerPool;
import com.stc.game.gameplay.engine.pools.customs.UiPool;
import com.stc.game.gameplay.engine.wave.WaveEngine;
import com.stc.game.gameplay.entities.ui.buttons.attacks.AttackButton;
import com.stc.game.gameplay.entities.world.alive.AliveEntity;
import com.stc.game.gameplay.entities.world.attacks.Attack;
import com.stc.game.gameplay.entities.world.map.playingArea.Cube;
import com.stc.game.gameplay.enums.Attacks;
import com.stc.game.gameplay.enums.Colors;
import com.stc.game.gameplay.enums.Directions;
import com.stc.game.gameplay.enums.Effects3D;
import com.stc.game.gameplay.enums.Faces;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.enums.MultiplayerMode;
import com.stc.game.multiplayer.enums.MultiplayerTeam;
import com.stc.game.multiplayer.tools.MultiplayerSender;
import com.stc.game.solo.SoloContext;
import com.stc.game.ui.enums.GameScreenState;
import com.stc.game.ui.screens.GameScreen;
import lombok.Getter;
import lombok.Setter;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Player extends AliveEntity {
  private static final int INT = Integer.SIZE / Byte.SIZE;

  @Getter
  private static final Quaternion identityQuaternion = new Quaternion(0, 0, 0, 1);
  @Getter
  private static final float playerMovementSpeed = 20f;

  @Getter
  private Vector3 playerSpawn;
  @Getter
  private Cube playingArea;

  @Getter @Setter
  private MultiplayerTeam team;
  private boolean spawned;
  private TeamAffectationRequest teamAffectationRequest;
  @Getter @Setter
  private int tickets;

  @Getter @Setter
  private Colors currentColorUsed;
  @Getter
  private Map<Effects3D, Float> cooldowns;

  public Player(String uniqueId) {
    super(uniqueId);
    this.maxLife = 500;
    this.currentLife = maxLife;
    this.colorsType = Colors.GREY;
    this.tickets = SoloContext.solo ? -1 : 5;
    this.cooldowns = new HashMap<>();
    this.spawned = false;
    buildAvailableAttacks();
    sendTeamAffectationRequest();
  }

  @Override
  public void update() {
    super.update();

    MultiplayerSender.sendPlayerIfMoving(this);

    checkPlayerLife();
    checkMultiplayerContext();
    updateCooldowns();
  }

  @Override
  protected ArrayList<Attacks> buildAvailableAttackTypes() {
    return new ArrayList<Attacks>() {
      {
        add(Attacks.BASIC_SHOT);
        add(Attacks.FIREBALL);
        add(Attacks.ROCKET);
      }
    };
  }

  @Override
  public void dispose() {
    super.dispose();
    playerSpawn = null;
    playingArea = null;
    team = null;
    if (teamAffectationRequest != null) {
      teamAffectationRequest.dispose();
      teamAffectationRequest = null;
    }
    currentColorUsed = null;
    cooldowns.clear();
    cooldowns = null;
  }

  public void setPlayingArea(Cube playingArea) {
    this.playingArea = playingArea;
    this.playerSpawn = playingArea.getSpawnPosition().cpy();
    this.position = playerSpawn.cpy();
  }

  public void modifyAttackEffects(final Colors colors) {
    for (Attack attack : availableAttacks) {
      attack.setAttackColor(colors);
      attack.setEffectType(
        attack.getType() == Attacks.BASIC_SHOT ?
          (colors == Colors.BLUE ? Effects3D.BLUE_BASIC_SHOT_EFFECT : (colors == Colors.RED ? Effects3D.RED_BASIC_SHOT_EFFECT : Effects3D.GREEN_BASIC_SHOT_EFFECT)) :
          ((attack.getType() == Attacks.FIREBALL ?
            (colors == Colors.BLUE ? Effects3D.BLUE_FIREBALL_EFFECT : (colors == Colors.RED ? Effects3D.RED_FIREBALL_EFFECT : Effects3D.GREEN_FIREBALL_EFFECT)) :
            (colors == Colors.BLUE ? Effects3D.BLUE_ROCKET_EFFECT : (colors == Colors.RED ? Effects3D.RED_ROCKET_EFFECT : Effects3D.GREEN_ROCKET_EFFECT))))
      );
    }
  }

  public void modifyAttackEffects(Effects3D basicShotEffect, Effects3D fireballEffect, Effects3D rocketEffect) {
    Colors attackColor = colorsType == Colors.BLUE ? Colors.BLUE : Colors.RED;
    for (Attack attack : availableAttacks) {
      attack.setAttackColor(attackColor);
      attack.setEffectType(attack.getType() == Attacks.BASIC_SHOT ? basicShotEffect : (attack.getType() == Attacks.FIREBALL ? fireballEffect : rocketEffect));
    }
  }

  public void respawnPlayer() {
    currentAcceleration = 1;
    currentFaces = Faces.FRONT;
    targetFaces = Faces.FRONT;
    position = playerSpawn.cpy();
    positionMotion.set(0, 0, 0);

    if (playingArea == GamePlayUtils.getPlayerArea()) {
      pitchRotation = 0;
    } else if (playingArea == GamePlayUtils.getEnemyMultiplayerArea()) {
      pitchRotation = 180;
    }

    direction.setEulerAngles(pitchRotation, 0, 0);
    directionMotion = Directions.NONE;
    moved = 0;
    currentLife = maxLife;
    damageCoef = 1;
    invincible = false;
  }

  private void checkPlayerLife() {
    if (SoloContext.solo) {
      if (currentLife <= 0f) {
        GameScreen.setGameScreenState(GameScreenState.GAME_OVER);
      }
    } else {
      Player currentPlayer = getCurrent();
      if (currentPlayer != null && currentPlayer.getCurrentLife() <= 0f) {
        if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.SURVIVAL) {
          MultiplayerSender.sendPlayer(currentPlayer);
          GameScreen.setGameScreenStateAfterResume(GameScreenState.GAME_OVER);
        } else if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.DEATHMATCH ||
                   MultiplayerContext.getMultiplayerMode() == MultiplayerMode.TEAM_DEATHMATCH) {
          GameScreen.setGameScreenState(GameScreenState.RESPAWN_MULTIPLAYER);
        }
      }
    }
  }

  private void checkMultiplayerContext() {
    if (MultiplayerContext.multiplayer) {
      if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.DEATHMATCH) {
        if (MultiplayerContext.allPlayersHaveJoined && PlayerPool.remainsOnlyOnePlayer()) {
          GameScreen.setGameScreenStateAfterResume(GameScreenState.WIN);
        }
      } else if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.TEAM_DEATHMATCH) {
        if (team == MultiplayerTeam.RED_TEAM && !spawned) {
          setPlayingArea(GamePlayUtils.getEnemyMultiplayerArea());
          respawnPlayer();
          spawned = true;
        }
        if (MultiplayerContext.allPlayersHaveJoined && PlayerPool.remainsOnlyPlayersOfTeam(team)) {
          GameScreen.setGameScreenStateAfterResume(GameScreenState.WIN);
        }
      }
      if (tickets == 0) {
        GameScreen.setGameScreenStateAfterResume(GameScreenState.GAME_OVER);
        MultiplayerSender.sendPacketGameOver(team);
      }
    }
  }

  public static Player getCurrent() {
    PlayerPool playerPool = PlayerPool.get();
    if (playerPool == null)
      return null;

    return SoloContext.solo ?
      playerPool.getEntities().peek() :
      (Player) playerPool.getEntityFromUniqueId(MultiplayerContext.playerName);
  }

  public static byte[] getCurrentPlayerStats() {
    Player currentPlayer = Player.getCurrent();
    if (currentPlayer != null && currentPlayer.getUniqueId() != null && currentPlayer.getPosition() != null) {
      int capacity = 2 * INT + currentPlayer.getUniqueId().getBytes().length;
      return ByteBuffer.allocate(capacity)
        .putInt(currentPlayer.getUniqueId().length())
        .put(currentPlayer.getUniqueId().getBytes())
        .putInt(WaveEngine.getWaveLevel() - 1) // The current wave is not finished
        .array();
    }
    return null;
  }

  private void updateCooldowns() {
    for (Map.Entry<Effects3D, Float> cooldownEntry : cooldowns.entrySet()) {
      if (cooldownEntry.getValue() > 0f) {
        cooldownEntry.setValue(cooldownEntry.getValue() - Gdx.graphics.getDeltaTime());
        if (cooldownEntry.getValue() <= 0f) {
          cooldownEntry.setValue(0f);
        }
      }
    }
  }

  public void saveCooldownsOfCurrentColorAndSwitchColor(final Colors newColor) {
    UiPool uiPool = UiPool.get();
    if (uiPool != null) {
      for (Attacks attackType : Attacks.values()) {
        AttackButton attackButton = (AttackButton) uiPool.getEntityFromId(attackType + AttackButton.getTAG());
        if (attackButton != null) {
          Effects3D key = getKeyOfPreviousAttackCooldown(attackType.name(), newColor.name());
          Effects3D effects3D =
            attackType == Attacks.BASIC_SHOT ?
              (currentColorUsed == Colors.BLUE ? Effects3D.BLUE_BASIC_SHOT_EFFECT : (currentColorUsed == Colors.RED ? Effects3D.RED_BASIC_SHOT_EFFECT : Effects3D.GREEN_BASIC_SHOT_EFFECT)) :
              ((attackType == Attacks.FIREBALL ?
               (currentColorUsed == Colors.BLUE ? Effects3D.BLUE_FIREBALL_EFFECT : (currentColorUsed == Colors.RED ? Effects3D.RED_FIREBALL_EFFECT : Effects3D.GREEN_FIREBALL_EFFECT)) :
               (currentColorUsed == Colors.BLUE ? Effects3D.BLUE_ROCKET_EFFECT : (currentColorUsed == Colors.RED ? Effects3D.RED_ROCKET_EFFECT : Effects3D.GREEN_ROCKET_EFFECT))));
          if (cooldowns.get(key) != null) {
            cooldowns.put(effects3D, attackButton.getCooldown());
            attackButton.setCooldown(cooldowns.get(key));
            cooldowns.remove(key);
          } else {
            cooldowns.put(effects3D, attackButton.getCooldown());
            attackButton.setCooldown(0f);
          }
        }
      }
    }
  }

  private Effects3D getKeyOfPreviousAttackCooldown(final String attackTypeName, final String colorName) {
    for (Map.Entry<Effects3D, Float> cooldownEntry : cooldowns.entrySet()) {
      String keyName = cooldownEntry.getKey().name();
      if (keyName.contains(attackTypeName) && keyName.contains(colorName)) {
        return cooldownEntry.getKey();
      }
    }
    return null;
  }

  private void sendTeamAffectationRequest() {
    if (MultiplayerContext.multiplayer) {
      if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.TEAM_DEATHMATCH) {
        teamAffectationRequest = new TeamAffectationRequest();
        teamAffectationRequest.scheduleTeamAffectationRequest(this);
      }
    }
  }
}
