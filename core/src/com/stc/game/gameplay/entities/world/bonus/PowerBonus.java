package com.stc.game.gameplay.entities.world.bonus;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.engine.managers.BuilderManager;
import com.stc.game.gameplay.engine.utils.MaterialUtils;
import com.stc.game.gameplay.entities.ui.hud.bonus.BonusEffect;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.gameplay.entities.ui.hud.bonus.BonusEffectPower;

public class PowerBonus extends Bonus {
  public PowerBonus(String uniqueId, Vector3 position) {
    super(uniqueId, position);
    this.modelInstance = BuilderManager.buildSphereModelInstance(1.5f, 10, MaterialUtils.getMaterialWithColor(new Color(0, 0, 0.8f, 0)), position);
  }

  @Override
  public BonusEffect getEffect(final Player player) {
    return new BonusEffectPower(uniqueId, player, new Vector2(150, 300));
  }
}
