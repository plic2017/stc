package com.stc.game.gameplay.entities.world.effects;

import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.entities.world.Entity3D;
import com.stc.game.gameplay.enums.Effects3D;
import com.stc.game.gameplay.enums.Sounds;

public class EffectExplosion extends Effect3D {
  public EffectExplosion(Entity3D parent, Vector3 startPosition, Vector3 targetPosition) {
    super(parent, Effects3D.EXPLOSION_EFFECT, Sounds.EXPLOSION_SOUND);
    this.lifespan = 1;
    this.position = startPosition;
    this.targetPosition = targetPosition;
  }
}
