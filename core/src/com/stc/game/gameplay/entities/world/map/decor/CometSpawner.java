package com.stc.game.gameplay.entities.world.map.decor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.GamePlayUtils;
import com.stc.game.gameplay.entities.world.effects.EffectComet;
import com.stc.game.gameplay.entities.world.map.MapEntity;
import com.stc.game.utils.random.FinalRandom;

public class CometSpawner extends MapEntity {
  private final Vector3 playerAreaCenter;
  private final float playerAreaSize;
  private float timeToNextComet = 0;

  public CometSpawner() {
    super("comet_01");
    this.playerAreaCenter = GamePlayUtils.getPlayerArea().getCubeCenter().cpy();
    this.playerAreaSize = GamePlayUtils.getPlayerArea().getSize();
  }

  @Override
  public void update() {
    timeToNextComet -= Gdx.graphics.getDeltaTime();

    if (timeToNextComet <= 0) {
      float depthZ = FinalRandom.getValueBetween(playerAreaSize, 100) * (FinalRandom.getValueBetween(0, 1) > 0.5 ? 1 : -1);
      float depthX = FinalRandom.getValueBetween(playerAreaSize, 100) * (FinalRandom.getValueBetween(0, 1) > 0.5 ? 1 : -1);
      float incline = FinalRandom.getValueBetween(35, 55);
      float up = 120;
      float down = -120;

      EffectComet effect = new EffectComet(
        new Vector3(depthX, up, depthZ).add(playerAreaCenter),
        new Vector3(depthX - incline, down, depthZ).add(playerAreaCenter));
      effect.triggerEffect();
      timeToNextComet = FinalRandom.getValueBetween(2f, 5f);
    }
  }
}
