package com.stc.game.gameplay.entities.world.effects;

import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.engine.managers.BuilderManager;
import com.stc.game.gameplay.entities.world.attacks.Attack;
import com.stc.game.gameplay.enums.Sounds;

public class EffectRocket extends EffectAttack {
  public EffectRocket(Attack linkedAttack, Vector3 startPosition, Vector3 target) {
    super(linkedAttack, Sounds.ROCKET_SOUND);
    this.lifespan = 2;
    this.movementSpeed = 25;
    this.modelInstance = BuilderManager.buildSphereModelInstance(1, 1, new Material());
    this.position = startPosition;
    this.targetPosition = target;
    this.isTargetingEffect = true;
    this.isSoloTargetAttack = true;
  }
}
