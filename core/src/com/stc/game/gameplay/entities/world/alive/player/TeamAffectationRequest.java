package com.stc.game.gameplay.entities.world.alive.player;

import com.stc.game.multiplayer.tools.MultiplayerSender;

import java.util.Timer;
import java.util.TimerTask;

class TeamAffectationRequest {
  private static final long NOW = 0L;
  private static final long ONE_SECOND = 1000L;

  private Timer timer;

  TeamAffectationRequest() {
    this.timer = new Timer();
  }

  void scheduleTeamAffectationRequest(final Player player) {
    timer.schedule(new TimerTask() {
      @Override
      public void run() {
        if (player.getTeam() == null) {
          MultiplayerSender.sendTeamAffectationRequest(player.getUniqueId());
        } else {
          cancel();
        }
      }
    }, NOW, ONE_SECOND);
  }

  public void dispose() {
    if (timer != null) {
      timer.cancel();
      timer.purge();
      timer = null;
    }
  }
}
