package com.stc.game.gameplay.entities.world.effects;

import com.badlogic.gdx.graphics.g3d.particles.ParticleEffect;
import com.badlogic.gdx.graphics.g3d.particles.influencers.DynamicsInfluencer;
import com.badlogic.gdx.graphics.g3d.particles.influencers.DynamicsModifier;
import com.badlogic.gdx.math.Quaternion;
import com.stc.game.gameplay.entities.world.attacks.Attack;
import com.stc.game.gameplay.enums.Effects3D;
import com.stc.game.gameplay.enums.Sounds;
import lombok.Getter;

import java.util.ArrayList;

public class EffectAttack extends Effect3D {
  @Getter
  protected ArrayList<String> attackedEntities = new ArrayList<>();
  @Getter
  protected Attack linkedAttack;
  @Getter
  protected boolean isSoloTargetAttack;

  EffectAttack(Attack linkedAttack, Sounds soundPath) {
    super(linkedAttack, linkedAttack.getEffectType(), soundPath);
    this.linkedAttack = linkedAttack;
  }

  EffectAttack(Attack linkedAttack, Effects3D effectPath, Sounds soundPath) {
    super(linkedAttack, effectPath, soundPath);
    this.linkedAttack = linkedAttack;
  }

  // TODO : Faire le même type de fix pour la roquette car celle-ci a une direction propre, pas celle de son parent
  public void initAngles() {
    ParticleEffect particleEffect = getCopyEffect();

    Quaternion direction = getDirection();

    for (int i = 0; i < particleEffect.getControllers().size; i++) {
      DynamicsInfluencer di = particleEffect.getControllers().get(i).findInfluencer(DynamicsInfluencer.class);
      DynamicsModifier.Angular dm;

      for (int j = 0; j < di.velocities.size; j++) {
        dm = (DynamicsModifier.Angular) di.velocities.get(j);
        dm.thetaValue.setHigh(dm.thetaValue.getHighMin() + direction.getYaw()); // Polar in the 3D editor
        dm.phiValue.setHigh(dm.phiValue.getHighMin() + direction.getPitch()); // Azimuth in the 3D editor
      }
    }
  }

  @Override
  public void update() {
    super.update();
    if (isSoloTargetAttack && !attackedEntities.isEmpty())
      isReadyToBeDisposed = true;
    // TODO : Fixer les effets des attaquent qui restent dans les airs (la roquette)
    else if (targetPosition != null && targetPosition.equals(position))
      isReadyToBeDisposed = true;
  }
  @Override
  public void dispose() {
    super.dispose();
    attackedEntities.clear();
    attackedEntities = null;
    linkedAttack = null;
  }
}
