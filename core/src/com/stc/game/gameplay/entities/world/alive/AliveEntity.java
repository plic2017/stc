package com.stc.game.gameplay.entities.world.alive;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.stc.game.gameplay.engine.shaders.colorShader.ColorMaterial;
import com.stc.game.gameplay.entities.world.Entity3D;
import com.stc.game.gameplay.entities.world.attacks.Attack;
import com.stc.game.gameplay.enums.Attacks;
import com.stc.game.gameplay.enums.Colors;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

public abstract class AliveEntity extends Entity3D {
  @Getter
  private static final float GENERAL_COOLDOWN = 0.3f;

  @Getter @Setter
  protected Colors colorsType;

  @Getter
  protected ArrayList<Attacks> availableAttackTypes;
  @Getter @Setter
  protected ArrayList<Attack> availableAttacks;
  @Getter @Setter
  protected float generalCooldown;
  @Getter @Setter
  protected int damageCoef;
  @Setter
  protected boolean invincible;

  protected abstract ArrayList<Attacks> buildAvailableAttackTypes();

  protected AliveEntity(String uniqueId) {
    super(uniqueId);
    this.availableAttackTypes = buildAvailableAttackTypes();
    this.availableAttacks = new ArrayList<>();
    this.damageCoef = 1;
    this.invincible = false;
  }

  public Attack getAttack(Attacks attackType) {
    for (Attack attack : availableAttacks) {
      if (attack.getType() == attackType)
        return attack;
    }
    return null;
  }

  public void modifyModelInstanceWithColor(Color color) {
    ColorMaterial colorMaterial = new ColorMaterial(color);
    for (Material material : modelInstance.materials) {
      if (material.has(colorMaterial.getAttribute().type))
        material.set(colorMaterial.getAttribute());
    }
  }

  @Override
  public void applyDamage(float damage, Entity3D damageSource) {
    if (invincible)
      damage = 0;

    super.applyDamage(damage, damageSource);
  }

  @Override
  protected void preModelInstanceCreation(Model model) {
    ColorMaterial colorMaterial = new ColorMaterial(colorsType.getColor());
    for (Material material : model.materials) {
      if (material.has(colorMaterial.getAttribute().type))
        material.set(colorMaterial.getAttribute());
    }
  }

  @Override
  public void dispose() {
    super.dispose();
    availableAttackTypes.clear();
    availableAttackTypes = null;
    availableAttacks.clear();
    availableAttacks = null;
    colorsType = null;
  }

  public void buildAvailableAttacks() {
    for (Attacks attackType : availableAttackTypes) {
      Attack newAttack = Attacks.buildAttack(this, attackType);
      availableAttacks.add(newAttack);
    }
  }
}
