package com.stc.game.gameplay.entities.world.attacks;

import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.entities.world.Entity3D;
import com.stc.game.gameplay.entities.world.alive.enemies.Enemy;
import com.stc.game.gameplay.entities.world.effects.EffectBasicShot;
import com.stc.game.gameplay.enums.Attacks;
import com.stc.game.gameplay.enums.Colors;
import com.stc.game.gameplay.enums.Effects3D;
import com.stc.game.utils.Pair;
import com.stc.game.utils.random.FinalRandom;
import lombok.Getter;

public class BasicShot extends Attack {
  private static final int BASIC_SHOT_DISTANCE = 50;
  @Getter
  private static final float BASIC_SHOT_BASE_DAMAGE = 20f;
  private static final float BASIC_SHOT_COOLDOWN = 0.1f;

  public BasicShot(Entity3D parent, Effects3D effects3D, Colors colors) {
    super(parent);
    this.type = Attacks.BASIC_SHOT;
    this.damage = BASIC_SHOT_BASE_DAMAGE;
    this.range = 30;
    this.effectType = effects3D;
    this.attackColor = colors;
  }

  @Override
  public Pair<Boolean, Entity3D> triggerAttack(Vector3 startPosition) {
    Vector3 targetPosition = startPosition.cpy().add(new Vector3(0, 0, BASIC_SHOT_DISTANCE).mul(parent.getDirection()));

    float deltaCooldown = 0;
    if (parent instanceof Enemy)
      deltaCooldown += FinalRandom.getValueBetween(0.5f, 1.0f);

    cooldown = BASIC_SHOT_COOLDOWN + deltaCooldown;
    associatedEffect = new EffectBasicShot(this, startPosition.cpy(), targetPosition);
    associatedEffect.triggerEffect();
    return new Pair<>(true, null);
  }

  @Override
  public void triggerAttack(Vector3 startPosition, Entity3D target) {}
}
