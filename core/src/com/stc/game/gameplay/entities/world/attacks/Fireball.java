package com.stc.game.gameplay.entities.world.attacks;

import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.entities.world.Entity3D;
import com.stc.game.gameplay.entities.world.effects.EffectFireball;
import com.stc.game.gameplay.enums.Attacks;
import com.stc.game.gameplay.enums.Colors;
import com.stc.game.gameplay.enums.Effects3D;

import com.stc.game.utils.Pair;
import lombok.Getter;

public class Fireball extends Attack {
  private static final int FIREBALL_DISTANCE = 50;
  @Getter
  private static final float FIREBALL_BASE_DAMAGE = 50f;
  private static final float FIREBALL_COOLDOWN = 3f;

  public Fireball(Entity3D parent, Effects3D effects3D, Colors colors) {
    super(parent);
    this.type = Attacks.FIREBALL;
    this.damage = FIREBALL_BASE_DAMAGE;
    this.range = 45;
    this.effectType = effects3D;
    this.attackColor = colors;
  }

  @Override
  public Pair<Boolean, Entity3D> triggerAttack(Vector3 startPosition) {
    Vector3 targetPosition = startPosition.cpy().add(new Vector3(0, 0, FIREBALL_DISTANCE).mul(parent.getDirection()));
    cooldown = FIREBALL_COOLDOWN;
    associatedEffect = new EffectFireball(this, effectType, startPosition.cpy(), targetPosition);
    associatedEffect.triggerEffect();
    return new Pair<>(true, null);
  }

  @Override
  public void triggerAttack(Vector3 startPosition, Entity3D target) {}
}
