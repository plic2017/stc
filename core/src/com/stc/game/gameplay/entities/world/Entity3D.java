package com.stc.game.gameplay.entities.world;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.physics.bullet.collision.btCollisionObject;
import com.badlogic.gdx.physics.bullet.collision.btCollisionShape;
import com.badlogic.gdx.physics.bullet.collision.btSphereShape;
import com.stc.game.gameplay.GamePlayUtils;
import com.stc.game.gameplay.engine.managers.ModelManager;
import com.stc.game.gameplay.engine.physic.Collider;
import com.stc.game.gameplay.entities.world.alive.AliveEntity;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.gameplay.entities.world.map.playingArea.Face;
import com.stc.game.gameplay.enums.Axis;
import com.stc.game.gameplay.enums.Directions;
import com.stc.game.gameplay.enums.Faces;
import com.stc.game.utils.maths.MathUtils;
import lombok.Getter;
import lombok.Setter;

public class Entity3D {
  // TODO : Les changer selon les entités ?
  protected static final float MAX_ACCELERATION = 1.0f;
  protected static final float ACCELERATION_SPEED = 0.025f;
  protected static final float DECELERATION_SPEED = 0.025f;
  private static final float SECONDS_TO_CHANGE_FACE = 0.5f;

  private static final BoundingBox bounds = new BoundingBox();

  @Getter
  protected Entity3D parent;

  // Ids
  @Getter
  protected String uniqueId;
  @Getter
  protected int collisionId;
  @Getter @Setter
  protected String targetId;

  // Model
  @Setter
  protected String modelPath;
  @Getter
  private Vector3 center;
  private Vector3 dimension;
  @Getter
  private float radius;
  @Getter
  protected ModelInstance modelInstance;
  @Getter
  private btCollisionObject entityCollider;
  private btCollisionShape entityShape;

  // Translation
  @Getter @Setter
  protected Vector3 position; // X - Left/Right ; Y - Top/Bottom ; Z - Front/Back
  @Getter @Setter
  protected Vector3 positionMotion;
  @Getter @Setter
  protected float movementSpeed;
  @Getter @Setter
  protected float currentAcceleration;

  // Rotation
  @Getter @Setter
  protected Quaternion direction;
  @Getter @Setter
  protected Directions directionMotion;
  protected float moved;
  @Getter @Setter
  // Je me suis rendu compte que les deux étaient inversés dans le jeu. Comprendre pourquoi ?
  protected float pitchRotation = 0;

  // Game related
  @Getter
  protected float maxLife;
  @Getter @Setter
  protected float currentLife;
  @Getter @Setter
  protected Faces currentFaces;
  protected Faces targetFaces;

  protected Entity3D() {}

  protected Entity3D(String uniqueId) {
    this.uniqueId = uniqueId;
    this.collisionId = MathUtils.uniqueIdToCollisionId(uniqueId);
    this.position = new Vector3(0, 0, 0);
    this.center = new Vector3(0, 0, 0);
    this.dimension = new Vector3(0, 0, 0);
    this.direction = new Quaternion(0, 0, 0, 1);
    this.directionMotion = Directions.NONE;
    this.positionMotion = new Vector3(0, 0, 0);
    this.movementSpeed = 0;
    this.currentAcceleration = 1;
    this.currentFaces = Faces.FRONT;
    this.targetFaces = Faces.FRONT;

    buildCollider();
  }

  private void buildCollider() {
    entityShape = new btSphereShape(0.75f); // TODO : À override selon le type d'unité

    entityCollider = new btCollisionObject();
    entityCollider.setCollisionShape(entityShape);
    entityCollider.setUserValue(collisionId);
    entityCollider.setCollisionFlags(entityCollider.getCollisionFlags() | btCollisionObject.CollisionFlags.CF_CUSTOM_MATERIAL_CALLBACK);

    Collider.addToCollisionWorld(entityCollider);
  }

  private void onDoneModelLoading() {
    if (modelPath != null) {
      Model model = ModelManager.getModel(modelPath);
      if (model != null) {
        preModelInstanceCreation(model);
        modelInstance = new ModelInstance(model);
        modelInstance.transform.setToTranslation(position);
        modelInstance.transform.rotate(direction);
        modelInstance.calculateBoundingBox(bounds);
        bounds.getCenter(center);
        bounds.getDimensions(dimension);
        radius = dimension.len() / 2;
      }
    }
  }

  protected void preModelInstanceCreation(Model model) {  }

  public void update() {
    updatePosition();
    updateRotation();
    updateModel();
  }

  private void updatePosition() {
    if (position != null && positionMotion != null) {

      Vector3 positionMot = positionMotion.cpy();
      if (this instanceof Player && targetFaces == currentFaces)
        positionMot.mul(direction);

      position.add(positionMot);
    }
  }

  private void updateDirection() {

    // Rotation angle for each frame
    float epsilonRotation = 90 / SECONDS_TO_CHANGE_FACE * Gdx.graphics.getDeltaTime();
    moved += epsilonRotation;
    if (moved > 90)
      epsilonRotation -= (moved - 90);

    boolean changeDirection = ((Player)this).getPlayingArea() == GamePlayUtils.getEnemyMultiplayerArea();

    if (directionMotion.equals(Directions.RIGHT) && !changeDirection
      || directionMotion.equals(Directions.LEFT) && changeDirection) {
      targetFaces = getCurrentFace().getRightFace();
      if (pitchRotation == 0)
        pitchRotation += 360;
      pitchRotation -= epsilonRotation;
    }

    if (directionMotion.equals(Directions.LEFT) && !changeDirection
      || directionMotion.equals(Directions.RIGHT) && changeDirection) {
      targetFaces = getCurrentFace().getLeftFace();
      pitchRotation += epsilonRotation;
    }

    if (targetFaces != currentFaces) {
      moveTo(((Player)this).getPlayingArea().getFace(targetFaces).getCenter(), movementSpeed * 5);
    }

    direction.setEulerAngles(pitchRotation, 0, 0);

    if (moved >= 90) {
      pitchRotation %= 360;
      moved = 0;
      directionMotion = Directions.NONE;
      currentFaces = targetFaces;
      fixesPrecisionLoss();
      fixesAxisMoved();
    }
  }

  private void fixesPrecisionLoss() {
    if (pitchRotation < 0.5 || pitchRotation > 359.5)
      pitchRotation = 0;
    if (pitchRotation < 90.5 && pitchRotation > 89.5)
      pitchRotation = 90;
    if (pitchRotation < 180.5 && pitchRotation > 179.5)
      pitchRotation = 180;
    if (pitchRotation < 270.5 && pitchRotation > 269.5)
      pitchRotation = 270;
  }

  private void fixesAxisMoved() {
    Face face = getCurrentFace();

    if (face.getLeftAxis() == Axis.X && face.getTopAxis() == Axis.Y)
      position.z = face.getBottomRight().z; // Cela aurait pu être n'importe quel z
    if (face.getLeftAxis() == Axis.Z && face.getTopAxis() == Axis.Y)
      position.x = face.getBottomRight().x; // Cela aurait pu être n'importe quel x
  }

  private void updateRotation() {
    if (directionMotion != Directions.NONE)
      updateDirection();
  }

  private void updateModel() {
    if (modelInstance != null) {
      if (position != null)
        modelInstance.transform.setToTranslation(position);
      if (direction != null)
        modelInstance.transform.rotate(direction);
      if (entityCollider != null)
        entityCollider.setWorldTransform(modelInstance.transform);
    } else
      onDoneModelLoading();
  }

  public void applyDamage(float damage, Entity3D damageSource) {
    float modifier = 1;
    if (damageSource instanceof AliveEntity)
      modifier = ((AliveEntity) damageSource).getDamageCoef();
    applyDamage(damage, modifier);
  }

  public void applyDamage(float damage, float damageCoef) {
    currentLife -= damage * damageCoef;
    if (currentLife < 0)
      currentLife = 0;
  }

  private void moveTo(Vector3 targetPosition, float speed) {
    Vector3 dir = MathUtils.getDirection(position, targetPosition);

    float moveX = dir.x * (speed * currentAcceleration) * Gdx.graphics.getDeltaTime();
    float moveY = dir.y * (speed * currentAcceleration) * Gdx.graphics.getDeltaTime();
    float moveZ = dir.z * (speed * currentAcceleration) * Gdx.graphics.getDeltaTime();

    getPositionMotion().set(
      clamp(position.x, targetPosition.x, moveX),
      clamp(position.y, targetPosition.y, moveY),
      clamp(position.z, targetPosition.z, moveZ)
    );
  }

  public void moveTo(Vector3 targetPosition) {
    moveTo(targetPosition, movementSpeed);
  }

  private float clamp(float positionElement, float targetPositionElement, float moveElement) {
    float result = moveElement;

    float dst = Math.abs(positionElement - targetPositionElement);

    if (dst < moveElement) {
      float neg = moveElement < 0 ? -1 : 1;
      result = dst * neg;
    }

    return result;
  }

  protected Face getCurrentFace() {
    if (this instanceof Player)
      return ((Player)this).getPlayingArea().getFace(currentFaces);

    return GamePlayUtils.getPlayerArea().getFace(currentFaces);
  }

  public void dispose() {
    uniqueId = null;
    modelPath = null;
    modelInstance = null;

    if (entityCollider != null) {
      Collider.removeFromCollisionWorld(entityCollider);
      entityCollider.dispose();
      entityCollider = null;
    }
    if (entityShape != null) {
      entityShape.dispose();
      entityShape = null;
    }

    parent = null;
    targetId = null;
    position = null;
    positionMotion = null;
    direction = null;
    directionMotion = null;
    center = null;
    dimension = null;
    currentFaces = null;
    targetFaces = null;
  }
}
