package com.stc.game.gameplay.entities.world.attacks;

import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.engine.ai.Ai;
import com.stc.game.gameplay.engine.pools.customs.EnemyPool;
import com.stc.game.gameplay.engine.pools.customs.PlayerPool;
import com.stc.game.gameplay.entities.world.Entity3D;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.gameplay.entities.world.effects.EffectRocket;
import com.stc.game.gameplay.enums.Attacks;
import com.stc.game.gameplay.enums.Colors;
import com.stc.game.gameplay.enums.Effects3D;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.enums.MultiplayerMode;
import com.stc.game.solo.SoloContext;
import com.stc.game.utils.Pair;
import lombok.Getter;

public class Rocket extends Attack {
  @Getter
  private static final float ROCKET_BASE_DAMAGE = 150f;
  private static final float ROCKET_COOLDOWN = 8f;

  public Rocket(Entity3D parent, Effects3D effects3D, Colors colors) {
    super(parent);
    this.type = Attacks.ROCKET;
    this.damage = ROCKET_BASE_DAMAGE;
    this.range = 45;
    this.effectType = effects3D;
    this.attackColor = colors;
  }

  @Override
  public Pair<Boolean, Entity3D> triggerAttack(Vector3 startPosition) {
    Entity3D target;

    if (parent instanceof Player) {
      parent.setTargetId(Ai.getAutoTarget((Player) parent));

      if (SoloContext.solo || MultiplayerContext.getMultiplayerMode() == MultiplayerMode.SURVIVAL)
        target = EnemyPool.get().getEntityFromUniqueId(parent.getTargetId());
      else
        target = PlayerPool.get().getEntityFromUniqueId(parent.getTargetId());
    } else {
      target = PlayerPool.get().getEntityFromUniqueId(parent.getTargetId());
    }

    if (target == null)
      return new Pair<>(false, null);

    triggerAttack(startPosition, target);
    return new Pair<>(true, target);
  }

  @Override
  public void triggerAttack(Vector3 startPosition, Entity3D target) {
    cooldown = ROCKET_COOLDOWN;
    Vector3 targetPosition = target.getPosition().cpy();
    associatedEffect = new EffectRocket(this, startPosition.cpy(), targetPosition);
    associatedEffect.triggerEffect();
  }
}
