package com.stc.game.gameplay.entities.world.alive.enemies;

import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.engine.wave.WaveObject;
import com.stc.game.gameplay.enums.Attacks;
import com.stc.game.gameplay.enums.Colors;
import com.stc.game.gameplay.enums.Models;
import com.stc.game.utils.random.FinalRandom;

import java.util.ArrayList;

public class EnemyMediumShip extends Enemy {
  public EnemyMediumShip(String uniqueId, WaveObject parentWave, Vector3 position, Colors color) {
    super(uniqueId, parentWave);
    this.setPosition(position);
    this.maxLife = 150;
    this.currentLife = maxLife;
    this.modelPath = Models.ENEMY_MEDIUM_SHIP_MODEL.getModelPath();
    this.movementSpeed = 3;
    this.movementSpeed += FinalRandom.getValueBetween(1, 5);
    this.powerLevel = 2;
    this.colorsType = color;
  }

  @Override
  protected ArrayList<Attacks> buildAvailableAttackTypes() {
    return new ArrayList<Attacks>() {
      {
        add(Attacks.BASIC_SHOT);
        add(Attacks.FIREBALL);
      }
    };
  }
}
