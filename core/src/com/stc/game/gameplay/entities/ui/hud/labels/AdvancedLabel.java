package com.stc.game.gameplay.entities.ui.hud.labels;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.stc.game.ui.managers.SkinManager;
import lombok.Getter;

abstract public class AdvancedLabel {
  @Getter
  protected boolean isDisposable;

  private StringBuilder stringBuilder;
  private Label.LabelStyle labelStyle;
  protected Label label;
  private final float alpha;
  @Getter
  private String id;

  protected AdvancedLabel(String id, Vector2 position, Color fontColor, float fontSize, float alpha) {
    this.id = id;
    this.isDisposable = false;
    this.stringBuilder = new StringBuilder();
    this.alpha = alpha;
    this.label = new Label("", SkinManager.getSkin());
    this.labelStyle = new Label.LabelStyle(this.label.getStyle());
    this.labelStyle.fontColor = fontColor;
    this.label.setStyle(labelStyle);
    this.label.setPosition(position.x, position.y);
    this.label.setFontScale(fontSize);
  }

  public void update() {}

  protected void updateLabelPosition(float deltaX, float deltaY) {
    label.setPosition(label.getX() + deltaX, label.getY() + deltaY);
  }

  protected void updateLabelFontSize(float deltaFontSize) {
    label.setFontScale(label.getFontScaleX() + deltaFontSize);
  }

  public void draw(SpriteBatch spriteBatch) {
    label.draw(spriteBatch, alpha);
  }

  protected void clearText() {
    stringBuilder.setLength(0);
    label.setText(stringBuilder);
  }

  protected void setText(String text) {
    stringBuilder.setLength(0);
    stringBuilder.append(text);
    label.setText(stringBuilder);
  }

  public void appendText(String text) {
    stringBuilder.append(text);
    label.setText(stringBuilder);
  }

  protected void setLabelPosition(float x, float y) {
    label.setPosition(x, y);
  }

  protected void setLabelColor(Color color) {
    labelStyle.fontColor = color;
    label.setStyle(labelStyle);
  }

  protected Color getLabelColor() {
    return labelStyle.fontColor;
  }

  protected void updateLabelAlpha(float deltaAlpha) {
    this.label.getColor().a += deltaAlpha;
  }

  protected float getLabelWidth() {
    return label.getWidth();
  }

  public void dispose() {
    stringBuilder.setLength(0);
    stringBuilder = null;
    label.clear();
    label = null;
    labelStyle = null;
    id = null;
  }
}
