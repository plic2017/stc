package com.stc.game.gameplay.entities.ui.buttons.joysticks;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.stc.game.gameplay.enums.Directions;
import com.stc.game.ui.enums.GameScreenState;
import com.stc.game.ui.screens.GameScreen;

public class RotationJoystick extends Joystick {
  private static final String TAG = "RotationJoystick";

  public RotationJoystick(Sprite sprite, Vector2 position, int radius) {
    super(sprite, position, radius);
    this.id = TAG;
  }

  private static Directions getDirection(Vector2 joystickTouch) {
    Directions dir = Directions.NONE;

    boolean xNeg = (joystickTouch.x < 0);
    boolean yNeg = (joystickTouch.y < 0);
    float finalX = joystickTouch.x * (xNeg ? -1 : 1);
    float finalY = joystickTouch.y * (yNeg ? -1 : 1);

    if (finalY < finalX) {
      if (joystickTouch.x <= 0)
        dir = Directions.LEFT;
      else
        dir = Directions.RIGHT;
    }

    return dir;
  }

  @Override
  public boolean touchDown(int screenX, int screenY, int pointer, int button) {
    super.touchDown(screenX, screenY, pointer, button);

    if (GameScreen.getGameScreenState() == GameScreenState.PAUSE)
      return false;
    if (controlledEntity == null)
      return false;
    if (controlledEntity.getDirectionMotion() != null && controlledEntity.getDirectionMotion() != Directions.NONE)
      return false;
    if (!isInBoundaries(screenX, screenY))
      return false;

    Directions direction = getDirection(getJoystickTouchCoordinates(screenX, screenY));
    controlledEntity.setDirectionMotion(direction);
    return true;
  }

  @Override
  public boolean touchDragged(int screenX, int screenY, int pointer) {
    super.touchDragged(screenX, screenY, pointer);

    if (!touchDown(screenX, screenY, 0, 0))
      return false;

    touchDown(screenX, screenY, pointer, 0);
    return true;
  }

  @Override
  public void postDraw() {
    if (controlledEntity.getDirectionMotion() != Directions.NONE)
      sprite.setColor(0.5f, 0.5f, 0.5f, 0.75f);
    else
      sprite.setColor(1, 1, 1, 1);
  }
}
