package com.stc.game.gameplay.entities.ui.buttons.classics;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.stc.game.gameplay.entities.ui.buttons.Button;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.enums.MultiplayerMode;
import com.stc.game.multiplayer.tools.MultiplayerSender;
import com.stc.game.multiplayer.tools.MultiplayerUtils;
import com.stc.game.services.Services;
import com.stc.game.ui.dialogs.Dialogs;
import com.stc.game.ui.tools.I18N;

import java.util.concurrent.Callable;

public class NearbyConnectionsStarterButton extends Button {
    private static final String TAG = "NearbyConnectionsStarterButton";

    private boolean enable;

    public NearbyConnectionsStarterButton(Sprite sprite, Vector2 position) {
        super(sprite, position);
        id = TAG;
        enable();
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (!isInBoundaries(screenX, screenY))
            return false;

        if (enable) {
            Dialogs.showYesNoButtonDialog(I18N.getTranslation(I18N.Text.NEARBY_CONNECTIONS), I18N.getTranslation(I18N.Text.NEARBY_CONNECTIONS_START_DESCRIPTION), new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.SURVIVAL) {
                        if (MultiplayerUtils.isHost()) {
                            // Rejects the next connection requests and inform the others players of numberOfParticipants
                            Services.getNearbyConnectionsService().rejectConnectionRequests();
                            // Informs the server of the numberOfParticipants to wait before sending the first wave
                            MultiplayerSender.sendNumberOfParticipants();
                            disable();
                        }
                    } else if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.DEATHMATCH ||
                               MultiplayerContext.getMultiplayerMode() == MultiplayerMode.TEAM_DEATHMATCH){
                        Services.getNearbyConnectionsService().rejectConnectionRequests();
                        disable();
                    }
                    return null;
                }
            });
        }

        return true;
    }

    private void enable() {
        enable = true;
        sprite.setAlpha(1);
    }

    private void disable() {
        enable = false;
        sprite.setAlpha(0);
    }
}
