package com.stc.game.gameplay.entities.ui.buttons.attacks;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.stc.game.gameplay.engine.sound.SoundEngine;
import com.stc.game.gameplay.entities.ui.buttons.Button;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.gameplay.enums.Colors;
import com.stc.game.gameplay.enums.Sounds;

public class AttackColorSelectionButton extends Button {
  private static final String TAG = "AttackColorSelectionButton";

  private final float circleX;
  private final float circleY;
  private final float circleRadius;

  private Colors colors;
  private ShapeRenderer shapeRenderer;

  public AttackColorSelectionButton(Sprite sprite, Vector2 position, Colors colors) {
    super(sprite, position);
    this.id = colors + TAG;
    this.colors = colors;
    this.shapeRenderer = new ShapeRenderer();
    this.shapeRenderer.setColor(new Color(0.5f, 0.5f, 0.5f, 0.75f));
    this.circleX = sprite.getX() + sprite.getHeight() / 2;
    this.circleY = sprite.getY() + sprite.getWidth() / 2;
    this.circleRadius = sprite.getWidth() / 2 - 4;
  }

  @Override
  public boolean touchDown(int screenX, int screenY, int pointer, int button) {
    if (!isInBoundaries(screenX, screenY))
      return false;

    Player currentPlayer = Player.getCurrent();
    if (currentPlayer != null && currentPlayer.getCurrentColorUsed() != colors) {
      SoundEngine.playSound(Sounds.COLOR_ATTACK_SELECTION_SOUND.getSoundPath());
      currentPlayer.saveCooldownsOfCurrentColorAndSwitchColor(colors);
      currentPlayer.setCurrentColorUsed(colors);
      currentPlayer.modifyAttackEffects(colors);
    }

    return true;
  }

  @Override
  public void postDraw() {
    Gdx.graphics.getGL20().glEnable(GL20.GL_BLEND);
    Player currentPlayer = Player.getCurrent();
    if (currentPlayer != null && currentPlayer.getCurrentColorUsed() == colors) {
      shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
      shapeRenderer.circle(circleX, circleY, circleRadius);
      shapeRenderer.end();
    }
    Gdx.graphics.getGL20().glDisable(GL20.GL_BLEND);
  }

  @Override
  public void dispose() {
    super.dispose();
    shapeRenderer.dispose();
    shapeRenderer = null;
    colors = null;
  }
}
