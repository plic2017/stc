package com.stc.game.gameplay.entities.ui.buttons.attacks;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.stc.game.gameplay.entities.ui.buttons.Button;
import com.stc.game.gameplay.entities.world.Entity3D;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.gameplay.entities.world.attacks.Attack;
import com.stc.game.gameplay.enums.Attacks;
import com.stc.game.gameplay.enums.Colors;
import com.stc.game.multiplayer.tools.MultiplayerSender;
import com.stc.game.ui.enums.GameScreenState;
import com.stc.game.ui.screens.GameScreen;
import com.stc.game.utils.Pair;
import lombok.Getter;
import lombok.Setter;

public class AttackButton extends Button {
  @Getter
  private static final String TAG = "AttackButton";

  private final float circleX;
  private final float circleY;
  private final float circleRadius;
  @Getter
  private Attacks attackType;

  @Getter @Setter
  private float cooldown;
  private ShapeRenderer shapeRenderer;

  public AttackButton(Sprite sprite, Vector2 position, Attacks attackType) {
    super(sprite, position);
    this.id = attackType + TAG;
    this.attackType = attackType;
    this.cooldown = 0;
    this.shapeRenderer = new ShapeRenderer();
    this.shapeRenderer.setColor(new Color(0.3f, 0.3f, 0.3f, 0.75f));
    this.circleX = sprite.getX() + sprite.getHeight() / 2;
    this.circleY = sprite.getY() + sprite.getWidth() / 2;
    this.circleRadius = sprite.getWidth() / 2 - 4;
  }

  @Override
  public boolean touchDown(int screenX, int screenY, int pointer, int button) {
    if (GameScreen.getGameScreenState() == GameScreenState.PAUSE)
      return false;
    if (!isInBoundaries(screenX, screenY))
      return false;

    Player currentPlayer = Player.getCurrent();
    if (currentPlayer == null)
      return false;

    if (cooldown == 0) {
      Attack attack = currentPlayer.getAttack(attackType);
      if (attack != null) {
        Pair<Boolean, Entity3D> result = attack.triggerAttack(currentPlayer.getPosition());
        if (result.getKey()) {
          String targetId = result.getValue() != null ? result.getValue().getUniqueId() : null;
          Colors colors = currentPlayer.getCurrentColorUsed();
          MultiplayerSender.sendPlayerAttack(attackType, targetId, colors);
          cooldown = attack.getCooldown();
        }
      }
    }

    return true;
  }

  @Override
  public void update() {
    super.update();

    if (cooldown > 0) {
      cooldown -= Gdx.graphics.getDeltaTime();
      if (cooldown <= 0)
        cooldown = 0;
    }
  }

  @Override
  public void draw(SpriteBatch spriteBatch) {
    Player player = Player.getCurrent();
    if (player != null) {
      float alpha = 1;
      float auxiliaryComposant = 0.8f;
      float mainComposant = 1f;
      if (player.getCurrentColorUsed() == Colors.RED)
        sprite.setColor(mainComposant, auxiliaryComposant, auxiliaryComposant, alpha);
      else if (player.getCurrentColorUsed() == Colors.GREEN)
        sprite.setColor(auxiliaryComposant, mainComposant, auxiliaryComposant, alpha);
      else if (player.getCurrentColorUsed() == Colors.BLUE)
        sprite.setColor(auxiliaryComposant, auxiliaryComposant, mainComposant, alpha);
    }

    super.draw(spriteBatch);
  }

  @Override
  public void postDraw() {
    Gdx.graphics.getGL20().glEnable(GL20.GL_BLEND);

    if (cooldown > 0) {
      shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
      shapeRenderer.circle(circleX, circleY, circleRadius);
      shapeRenderer.end();
    }

    Gdx.graphics.getGL20().glDisable(GL20.GL_BLEND);
  }

  @Override
  public void dispose() {
    super.dispose();
    shapeRenderer.dispose();
    shapeRenderer = null;
    attackType = null;
  }
}
