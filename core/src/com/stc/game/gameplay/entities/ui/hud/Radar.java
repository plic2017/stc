package com.stc.game.gameplay.entities.ui.hud;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.stc.game.gameplay.engine.managers.SpriteManager;
import com.stc.game.gameplay.engine.pools.customs.EnemyPool;
import com.stc.game.gameplay.entities.ui.Entity2D;
import com.stc.game.gameplay.entities.world.alive.enemies.Enemy;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.gameplay.entities.world.map.playingArea.Cube;
import com.stc.game.gameplay.entities.world.map.playingArea.Face;
import com.stc.game.gameplay.enums.Faces;
import com.stc.game.gameplay.tools.Tools;

public class Radar extends Entity2D {
  private final Player watchedPlayer;
  private final Cube playerArea;
  private final EnemyPool enemyPool;

  private final Sprite leftSprite;
  private boolean leftDetected;

  private final Sprite rightSprite;
  private boolean rightDetected;

  private final Sprite backSprite;
  private boolean backDetected;

  public Radar(Player watchedPlayer, Cube playerArea, EnemyPool enemyPool) {
    this.watchedPlayer = watchedPlayer;
    this.playerArea = playerArea;
    this.enemyPool = enemyPool;

    this.leftSprite = SpriteManager.getSprite("textures/ui/arrow_left.png");
    this.leftSprite.setPosition(leftSprite.getWidth(), Tools.getScreenHeight() / 2 - leftSprite.getHeight() / 2);

    this.rightSprite = SpriteManager.getSprite("textures/ui/arrow_right.png");
    this.rightSprite.setPosition(Tools.getScreenWidth() - rightSprite.getWidth() * 2, Tools.getScreenHeight() / 2 - rightSprite.getHeight() / 2);

    this.backSprite = SpriteManager.getSprite("textures/ui/arrow_back.png");
    this.backSprite.setPosition(Tools.getScreenWidth() / 2 - backSprite.getWidth() / 2, backSprite.getHeight());
  }

  @Override
  public void update() {
    leftDetected = false;
    rightDetected = false;
    backDetected = false;

    Faces playerFaces = watchedPlayer.getCurrentFaces();
    Face playerFace = playerArea.getFace(playerFaces);
    Faces playerLeftFaces = playerFace.getLeftFace();
    Faces playerRightFaces = playerFace.getRightFace();

    for (Enemy enemy : enemyPool.getEntities()) {
      Faces enemyFace = enemy.getCurrentFaces();

      if (enemyFace == playerLeftFaces && !leftDetected) {
        leftDetected = true;
      }

      if (enemyFace == playerRightFaces && !rightDetected) {
        rightDetected = true;
      }

      if (enemyFace != playerLeftFaces
        && enemyFace != playerRightFaces
        && enemyFace != playerFaces
        && !backDetected) {
        backDetected = true;
      }
    }
  }

  @Override
  public void draw(SpriteBatch spriteBatch) {
    if (leftDetected)
      leftSprite.draw(spriteBatch);
    if (rightDetected)
      rightSprite.draw(spriteBatch);
    if (backDetected)
      backSprite.draw(spriteBatch);
  }
}
