package com.stc.game.gameplay.entities.ui.buttons.chat;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.stc.game.gameplay.entities.ui.buttons.Button;
import com.stc.game.gameplay.entities.ui.buttons.chat.utils.ChatMessageEntry;
import com.stc.game.ui.dialogs.Dialogs;
import lombok.Getter;

import java.util.LinkedList;

public class IncomingChatMessageButton extends Button {
    private static final String TAG = "IncomingChatMessageButton";

    @Getter
    private static LinkedList<ChatMessageEntry> chatMessagesInbox;

    private boolean enable;

    public IncomingChatMessageButton(Sprite sprite, Vector2 position) {
        super(sprite, position);
        id = TAG;
        chatMessagesInbox = new LinkedList<>();
        disable();
    }

    @Override
    public void update() {
        super.update();

        if (chatMessagesInbox != null && !chatMessagesInbox.isEmpty()) {
            enable();
        }
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (!isInBoundaries(screenX, screenY))
            return false;

        if (enable) {
            ChatMessageEntry chatMessageEntry = getChatMessageEntry();
            if (chatMessageEntry != null) {
                Dialogs.showChatButtonDialog(chatMessageEntry.getPlayerName(), chatMessageEntry.getMessage());
            }
            disable();
        }

        return true;
    }

    @Override
    public void dispose() {
        super.dispose();
        chatMessagesInbox.clear();
        chatMessagesInbox = null;
    }

    private void enable() {
        enable = true;
        sprite.setAlpha(1);
    }

    private void disable() {
        enable = false;
        sprite.setAlpha(0);
    }

    private static ChatMessageEntry getChatMessageEntry() {
        if (chatMessagesInbox == null || chatMessagesInbox.isEmpty())
            return null;
        ChatMessageEntry chatMessageEntry = chatMessagesInbox.getFirst();
        chatMessagesInbox.removeFirst();
        return chatMessageEntry;
    }
}
