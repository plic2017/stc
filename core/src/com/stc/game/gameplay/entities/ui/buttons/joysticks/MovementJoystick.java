package com.stc.game.gameplay.entities.ui.buttons.joysticks;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.stc.game.gameplay.GamePlayUtils;
import com.stc.game.gameplay.engine.managers.SpriteManager;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.gameplay.entities.world.map.playingArea.Cube;
import com.stc.game.gameplay.entities.world.map.playingArea.Face;
import com.stc.game.gameplay.enums.Axis;
import com.stc.game.gameplay.enums.Directions;
import com.stc.game.gameplay.tools.Tools;
import com.stc.game.ui.enums.GameScreenState;
import com.stc.game.ui.screens.GameScreen;

public class MovementJoystick extends Joystick {
  private static final String TAG = "MovementJoystick";

  private Sprite knob;
  private Vector2 knobBasePosition;

  public MovementJoystick(Sprite sprite, Vector2 position, int radius) {
    super(sprite, position, radius);
    this.id = TAG;

    this.knob = SpriteManager.getSprite("textures/ui/stick.png");
    this.knobBasePosition = new Vector2(
      position.x + this.sprite.getWidth() / 2 - this.knob.getWidth() / 2,
      position.y + this.sprite.getHeight() / 2 - this.knob.getHeight() / 2);
    this.knob.setPosition(knobBasePosition.x, knobBasePosition.y);
  }

  private Vector2 getMovement(int screenX, int screenY) {
    Vector2 motion = getJoystickTouchCoordinates(screenX, screenY);
    if (motion.equals(Vector2.Zero))
      return Vector2.Zero;

    double movementSpeed = controlledEntity.getMovementSpeed();
    motion.set((float) (-motion.x * movementSpeed * Gdx.graphics.getDeltaTime()), (float) (motion.y * movementSpeed * Gdx.graphics.getDeltaTime()));
    return motion;
  }

  private float getMovementOnX(float movementX) {
    Cube playingArea = ((Player)controlledEntity).getPlayingArea();
    Face face = playingArea.getFace(controlledEntity.getCurrentFaces());

    // Movement
    float moveX = movementX;

    // Current Position
    float currentPosX = 0;
    if (face.getLeftAxis() == Axis.X)
      currentPosX = controlledEntity.getPosition().x;
    if (face.getLeftAxis() == Axis.Z)
      currentPosX = controlledEntity.getPosition().z;

    // Boundaries
    float maxX = face.getLeft();
    float minX = face.getRight();

    // Fixes a negative referential (Genius)
    if (maxX < minX) {
      maxX *= -1;
      minX *= -1;
      currentPosX *= -1;
    }

    float inversed = 1.0f;
    if (playingArea == GamePlayUtils.getEnemyMultiplayerArea())
      inversed *= -1;

    if (maxX <= currentPosX + (moveX * inversed)) {
      moveX = (maxX - currentPosX) * inversed;
    } else if (minX >= currentPosX + (moveX * inversed)) {
      moveX = (minX - currentPosX) * inversed;
    }

    return moveX;
  }

  private float getMovementOnY(float movementY) {
    Face face = ((Player)controlledEntity).getPlayingArea().getFace(controlledEntity.getCurrentFaces());
    float moveY = movementY;

    float currentPosY = 0;
    if (face.getTopAxis() == Axis.Y)
      currentPosY = controlledEntity.getPosition().y;

    float maxY = face.getTop();
    float minY = face.getBottom();

    if (currentPosY + movementY >= maxY)
      moveY = maxY - currentPosY;
    if (currentPosY + movementY <= minY)
      moveY = minY - currentPosY;

    return moveY;
  }

  @Override
  public boolean touchDown(int screenX, int screenY, int pointer, int button) {
    super.touchDown(screenX, screenY, pointer, button);
    float fixedY = Tools.getScreenHeight() - screenY;
    knob.setPosition(screenX - knob.getWidth() / 2, fixedY - knob.getWidth() / 2);

    if (GameScreen.getGameScreenState() == GameScreenState.PAUSE)
      return false;
    if (controlledEntity == null)
      return false;
    if (controlledEntity.getDirectionMotion() != null && controlledEntity.getDirectionMotion() != Directions.NONE)
      return false;
    if (!isInBoundaries(screenX, screenY))
      return false;

    Vector2 movement = getMovement(screenX, screenY);
    if (movement.equals(Vector2.Zero))
      return false;

    controlledEntity.getPositionMotion().set(getMovementOnX(movement.x), getMovementOnY(movement.y), 0);
    return true;
  }

  @Override
  public boolean touchDragged (int screenX, int screenY, int pointer) {
    super.touchDragged(screenX, screenY, pointer);
    float fixedY = Tools.getScreenHeight() - screenY;
    knob.setPosition(screenX - knob.getWidth() / 2, fixedY - knob.getWidth() / 2);

    if (!isInBoundaries(screenX, screenY) && controlledEntity != null) {
      controlledEntity.getPositionMotion().set(0, 0, 0);
      return true;
    }
    touchDown(screenX, screenY, pointer, 0);
    return false;
  }

  @Override
  public boolean touchUp(int screenX, int screenY, int pointer, int button) {
    super.touchUp(screenX, screenY, pointer, button);
    knob.setPosition(knobBasePosition.x, knobBasePosition.y);

    if (controlledEntity != null && controlledEntity.getDirectionMotion() == Directions.NONE)
      controlledEntity.getPositionMotion().set(0, 0, 0);
    return false;
  }

  @Override
  public void draw(SpriteBatch spriteBatch) {
    super.draw(spriteBatch);
    knob.draw(spriteBatch);
  }

  @Override
  public void dispose() {
    super.dispose();
    knob = null;
    knobBasePosition = null;
  }
}
