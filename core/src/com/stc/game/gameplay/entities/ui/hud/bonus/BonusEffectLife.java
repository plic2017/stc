package com.stc.game.gameplay.entities.ui.hud.bonus;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.stc.game.gameplay.engine.managers.LabelManager;
import com.stc.game.gameplay.entities.ui.hud.labels.customs.DynamicLabel;
import com.stc.game.gameplay.entities.world.alive.AliveEntity;
import com.stc.game.gameplay.enums.EndBehaviour;
import com.stc.game.gameplay.tools.Tools;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.solo.SoloContext;

public class BonusEffectLife extends BonusEffect {
  private static final String TAG = "BonusEffectLife";

  private final float lifeRegen = 50;

  public BonusEffectLife(String uniqueId, AliveEntity aliveEntity, Vector2 position) {
    super(uniqueId, aliveEntity, position);
    if (SoloContext.solo || aliveEntity.getUniqueId().equals(MultiplayerContext.playerName)) {
      LabelManager.addAdvancedLabel(new DynamicLabel(
        TAG,
        new Vector2(-150, Tools.getScreenHeight() / 1.450f),
        new Vector2(Tools.getScreenWidth() / 2.75f, Tools.getScreenHeight() / 1.450f),
        0.5f,
        2,
        2,
        0,
        Color.SCARLET,
        1,
        EndBehaviour.FADE,
        0.5f,
        2,
        "Life +" + (int) lifeRegen
      ));
    }
  }

  @Override
  public void update(){
    if (aliveEntity.getCurrentLife() != aliveEntity.getMaxLife())
      aliveEntity.setCurrentLife(Math.min(aliveEntity.getCurrentLife() + lifeRegen, aliveEntity.getMaxLife()));
    removeBonusEffect();
  }
}
