package com.stc.game.gameplay.entities.ui.hud.labels.customs;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.stc.game.gameplay.entities.ui.hud.labels.AdvancedLabel;

public class DebugLabel extends AdvancedLabel {
  private static final String TAG = "DebugLabel";

  public DebugLabel(Vector2 position, Color fontColor, float fontSize, float alpha) {
    super(TAG, position, fontColor, fontSize, alpha);
  }

  @Override
  public void update() {
    clearText();
  }
}
