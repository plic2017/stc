package com.stc.game.gameplay.entities.ui.hud.labels.customs;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.stc.game.gameplay.engine.wave.WaveEngine;
import com.stc.game.gameplay.entities.ui.hud.labels.AdvancedLabel;

public class WaveEngineLabel extends AdvancedLabel {
  private static final String TAG = "WaveEngineLabel";

  public WaveEngineLabel(Vector2 position, Color fontColor, float fontSize, float alpha) {
    super(TAG, position, fontColor, fontSize, alpha);
  }

  @Override
  public void update() {
    super.update();
    setText(String.valueOf(WaveEngine.getWaveLevel()));
    setLabelPosition((Gdx.graphics.getWidth() / 2) - (getLabelWidth() / 2), Gdx.graphics.getHeight() - 2 * 48f);
  }
}
