package com.stc.game.gameplay.entities.ui.buttons.requests;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.stc.game.gameplay.entities.ui.buttons.Button;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.services.Services;
import com.stc.game.services.gpgs.gamegifts.GameGift;
import com.stc.game.services.gpgs.gamegifts.RequestType;
import lombok.Getter;
import lombok.Setter;

public class HealthBonusRequestButton extends Button {
  @Getter
  private static final String TAG = "HealthBonusRequestButton";
  private static final float HEALTH_BONUS_REQUEST_BUTTON_COOLDOWN = 15f;

  private boolean enable;
  @Setter
  private float cooldown;

  public HealthBonusRequestButton(Sprite sprite, Vector2 position) {
    super(sprite, position);
    id = TAG;
    cooldown = 0;
    disable();
  }

  @Override
  public void update() {
    super.update();

    Player currentPlayer = Player.getCurrent();
    if (currentPlayer != null) {
      if (currentPlayer.getCurrentLife() <= currentPlayer.getMaxLife() / 2) {
        if (!enable && cooldown == 0) {
          enable();
        }

        if (cooldown > 0) {
          cooldown -= Gdx.graphics.getDeltaTime();
          if (cooldown <= 0)
            cooldown = 0;
        }
      }
    }
  }

  @Override
  public boolean touchDown(int screenX, int screenY, int pointer, int button) {
    if (!isInBoundaries(screenX, screenY))
      return false;

    if (enable) {
      Services.getGpgs().sendGameGiftsRequest(RequestType.TYPE_WISH.getRequestTypeId(), GameGift.HEALTH_BONUS_GIFT.getGameGiftId());
      cooldown = HEALTH_BONUS_REQUEST_BUTTON_COOLDOWN;
      disable();
    }

    return true;
  }

  private void enable() {
    enable = true;
    sprite.setAlpha(1);
  }

  private void disable() {
    enable = false;
    sprite.setAlpha(0);
  }
}
