package com.stc.game.gameplay.entities.ui.buttons.classics;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.stc.game.gameplay.entities.ui.buttons.Button;
import com.stc.game.ui.enums.GameScreenState;
import com.stc.game.ui.screens.GameScreen;

public class OptionsWheel extends Button {
  private static final String TAG = "OptionsWheel";

  public OptionsWheel(Sprite sprite, Vector2 position) {
    super(sprite, position);
    id = TAG;
  }

  @Override
  public boolean touchDown(int screenX, int screenY, int pointer, int button) {
    if (!isInBoundaries(screenX, screenY))
      return false;

    GameScreen.setGameScreenState(GameScreenState.PAUSE);

    return true;
  }
}
