package com.stc.game.gameplay.entities.ui.buttons.joysticks;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.stc.game.gameplay.entities.ui.buttons.Button;
import com.stc.game.gameplay.tools.Tools;
import lombok.Setter;

public class Joystick extends Button {
  private static final String TAG = "Joystick";

  @Setter
  private boolean isTouched;
  private final float radius;
  private final Vector2 center;

  Joystick(Sprite sprite, Vector2 position, int radius) {
    super(sprite, position);
    this.id = TAG;
    this.isTouched = false;
    this.radius = radius;
    this.center = getCenter();
  }

  Vector2 getJoystickTouchCoordinates(int screenX, int screenY) {
    // Fix the reversed view
    float fixedY = Tools.getScreenHeight() - screenY;

    Circle deadZone = new Circle(center.x, center.y, 16);
    if (deadZone.contains(screenX, fixedY))
      return new Vector2(0, 0);

    float centerX = position.x + sprite.getWidth() / 2;
    float centerY = position.y + sprite.getHeight() / 2;

    // Clamping to stay in the sprite boundaries
    float distX = Math.max(-sprite.getWidth() / 2, Math.min(sprite.getWidth() / 2, screenX - centerX));
    float distY = Math.max(-sprite.getHeight() / 2, Math.min(sprite.getHeight() / 2, fixedY - centerY));

    return new Vector2(distX, distY).nor();
  }

  @Override
  public boolean isInBoundaries(int screenX, int screenY) {
    Circle boundaries = new Circle(center.x, center.y, radius);
    float fixedY = Tools.getScreenHeight() - screenY;
    return  boundaries.contains(screenX, fixedY);
  }

  @Override
  public boolean touchDown(int screenX, int screenY, int pointer, int button) {
    return false;
  }

  @Override
  public boolean touchDragged (int screenX, int screenY, int pointer) {
    return false;
  }


  @Override
  public boolean touchUp(int screenX, int screenY, int pointer, int button) {
    return false;
  }

  @Override
  public void update() {
    super.update();

    if (!isTouched)
      touchUp(0, 0, 0, 0);
    isTouched = false;
  }
}
