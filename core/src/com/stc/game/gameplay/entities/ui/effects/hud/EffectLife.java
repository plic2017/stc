package com.stc.game.gameplay.entities.ui.effects.hud;

import com.badlogic.gdx.math.Vector2;
import com.stc.game.gameplay.entities.ui.effects.Effect2D;
import com.stc.game.gameplay.enums.Effects2D;
import com.stc.game.gameplay.enums.PoolPriority;

public class EffectLife extends Effect2D {
  private static final String TAG = "EffectLife";

  public EffectLife(Vector2 position) {
    super(Effects2D.LIFEBAR_EFFECT, position, PoolPriority.UI_EFFECT);
    id = TAG;
  }
}
