package com.stc.game.gameplay.entities.ui.hud.labels.customs;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.stc.game.gameplay.entities.ui.hud.labels.AdvancedLabel;
import com.stc.game.gameplay.enums.EndBehaviour;

public class DynamicLabel extends AdvancedLabel {
  private final float secondsToReachPosition;
  private float secondsLeftToReachPosition;
  private final float secondsToReachFontSize;
  private float secondsLeftToReachFontSize;
  private final float fadeTime;
  private float fadeTimeLeft;
  private final float originalAlpha;
  private final float delayBeforeFading;
  private float delayLeftBeforeFading;

  private final EndBehaviour endBehaviour;

  private final float distX;
  private final float distY;
  private final float fontDiff;

  public DynamicLabel(String id, Vector2 position,
                      Vector2 targetPostion,
                      float secondsToReachPosition,

                      float fontSize,
                      float targetFontSize,
                      float secondsToReachFontSize,

                      Color fontColor,

                      float alpha,
                      EndBehaviour endBehaviour,
                      float fadeTime,

                      float delayBeforeFading,

                      String content) {

    super(id, position, fontColor, fontSize, alpha);

    this.secondsToReachPosition = secondsToReachPosition;
    this.secondsLeftToReachPosition = secondsToReachPosition;
    this.secondsToReachFontSize = secondsToReachFontSize;
    this.secondsLeftToReachFontSize = secondsToReachFontSize;
    this.fadeTime = fadeTime;
    this.fadeTimeLeft = fadeTime;
    this.originalAlpha = this.label.getColor().a;
    this.delayBeforeFading = delayBeforeFading;
    this.delayLeftBeforeFading = delayBeforeFading;

    this.endBehaviour = endBehaviour;

    this.distX = targetPostion.x - position.x;
    this.distY = targetPostion.y - position.y;
    this.fontDiff = targetFontSize - fontSize;

    this.setText(content);
  }

  public void reset() {
    fadeTimeLeft = fadeTime;
    delayLeftBeforeFading = delayBeforeFading;
    label.getColor().a = originalAlpha;
    isDisposable = false;
  }

  @Override
  public void update() {
    if (secondsLeftToReachPosition <= 0)
      updateAlpha();
    else
      updatePosition();

    if (secondsLeftToReachFontSize > 0)
      updateFontSize();
  }

  private void updateAlpha() {
    if (endBehaviour == EndBehaviour.DISAPPEAR) {
      isDisposable = true;
    } else if (endBehaviour == EndBehaviour.FADE) {
      if (fadeTimeLeft <= 0)
        isDisposable = true;
      else {
        if (delayLeftBeforeFading > 0) {
          delayLeftBeforeFading -= Gdx.graphics.getDeltaTime();
          return;
        }

        fadeTimeLeft -= Gdx.graphics.getDeltaTime();

        float deltaFadeTime = Gdx.graphics.getDeltaTime() / fadeTime;
        float deltaAlpha = 1.0f * deltaFadeTime;

        updateLabelAlpha(-deltaAlpha);
      }
    }
  }

  private void updatePosition() {
    secondsLeftToReachPosition -= Gdx.graphics.getDeltaTime();

    float deltaPosTime = Gdx.graphics.getDeltaTime() / secondsToReachPosition;
    float deltaX = distX * deltaPosTime;
    float deltaY = distY * deltaPosTime;

    updateLabelPosition(deltaX, deltaY);
  }

  private void updateFontSize() {
    if (fontDiff == 0)
      return;

    secondsLeftToReachFontSize -= Gdx.graphics.getDeltaTime();

    float deltaFontTime = Gdx.graphics.getDeltaTime() / secondsToReachFontSize;
    float deltaFont = fontDiff * deltaFontTime;

    updateLabelFontSize(deltaFont);
  }
}
