package com.stc.game.gameplay.entities.ui.effects;

import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.math.Vector2;
import com.stc.game.gameplay.entities.ui.Entity2D;
import com.stc.game.gameplay.enums.Effects2D;
import com.stc.game.gameplay.enums.PoolPriority;

import lombok.Getter;
import lombok.Setter;

public class Effect2D extends Entity2D {
  private static final String TAG = "Effect2D";

  @Getter
  private String effectPath;
  @Getter @Setter
  private ParticleEffect copyEffect;
  @Getter
  private PoolPriority priority;

  protected Effect2D(Effects2D effectPath, Vector2 position, PoolPriority priority) {
    this.id = TAG;
    this.effectPath = effectPath.getEffect2DPath();
    this.position = position;
    this.copyEffect = new ParticleEffect();
    this.priority = priority;
  }

  @Override
  public void dispose() {
    super.dispose();
    effectPath = null;
    position = null;
    if (copyEffect != null) {
      copyEffect.dispose();
      copyEffect = null;
    }
    priority = null;
  }
}
