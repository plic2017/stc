package com.stc.game.gameplay.entities.ui.buttons.requests;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.stc.game.gameplay.entities.ui.buttons.Button;
import com.stc.game.services.Services;

public class RequestsInboxButton extends Button {
    private static final String TAG = "RequestsInboxButton";

    private boolean enable;

    public RequestsInboxButton(Sprite sprite, Vector2 position) {
        super(sprite, position);
        id = TAG;
        disable();
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (!isInBoundaries(screenX, screenY))
            return false;

        if (enable) {
            Services.getGpgs().showRequestsInbox();
            disable();
        }

        return true;
    }

    public static void enable() {
        RequestsInboxButton button = (RequestsInboxButton) getButtonFromId(TAG);
        if (button != null) {
            button.enable = true;
            button.sprite.setAlpha(1);
        }
    }

    private void disable() {
        enable = false;
        sprite.setAlpha(0);
    }
}
