package com.stc.game.gameplay.entities.ui.hud.bonus;

import com.badlogic.gdx.math.Vector2;
import com.stc.game.gameplay.engine.pools.customs.UiPool;
import com.stc.game.gameplay.entities.ui.Entity2D;
import com.stc.game.gameplay.entities.world.alive.AliveEntity;
import lombok.Setter;

public abstract class BonusEffect extends Entity2D {
  @Setter
  protected AliveEntity aliveEntity;

  BonusEffect(String uniqueId, AliveEntity aliveEntity, Vector2 position){
    super(position);
    this.id = uniqueId;
    this.aliveEntity = aliveEntity;
  }

  void removeBonusEffect(){
    if (UiPool.get() != null)
      UiPool.get().removeEntityFromUniqueId(id);
  }

  @Override
  public void dispose() {
    super.dispose();
    aliveEntity = null;
  }
}
