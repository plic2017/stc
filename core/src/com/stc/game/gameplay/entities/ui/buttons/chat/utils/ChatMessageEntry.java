package com.stc.game.gameplay.entities.ui.buttons.chat.utils;

import lombok.Getter;

public class ChatMessageEntry {
    @Getter
    private final String playerName;
    @Getter
    private final String message;

    public ChatMessageEntry(final String playerName, final String message) {
        this.playerName = playerName;
        this.message = message;
    }
}
