package com.stc.game.gameplay.entities.ui;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import lombok.Getter;
import lombok.Setter;

public class Entity2D {
  @Getter @Setter
  protected String id;
  @Getter
  protected Vector2 position = new Vector2(0, 0);
  @Getter
  protected Sprite sprite;

  protected Entity2D() {}

  protected Entity2D(Vector2 position) {
    this.position = position;
  }

  protected Entity2D(Sprite sprite, Vector2 position) {
    this.sprite = sprite;
    this.position = position;
    this.sprite.setPosition(this.position.x, this.position.y);
  }

  public void update() {}

  public void draw(SpriteBatch spriteBatch) {
    if (sprite != null)
      sprite.draw(spriteBatch);
  }

  public void postDraw() { }

  public void setPosition(float x, float y) {
    position.set(x, y);
    if (sprite != null)
      sprite.setPosition(x, y);
  }

  protected Vector2 getCenter() {
    if (sprite == null || position == null)
      return null;

    Vector2 center = new Vector2();
    center.x = position.x + sprite.getWidth() / 2;
    center.y = position.y + sprite.getHeight() / 2;
    return center;
  }

  protected Vector2 getRightBorderCenterPosition() {
    if (sprite == null || position == null)
      return null;

    Vector2 rightBorderCenterPosition = new Vector2();

    rightBorderCenterPosition.x = sprite.getX() + sprite.getWidth();
    rightBorderCenterPosition.y = sprite.getY() + sprite.getHeight() / 2;

    return rightBorderCenterPosition;
  }

  public void dispose() {
    id = null;
    position = null;
    sprite = null;
  }
}
