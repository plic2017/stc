package com.stc.game.gameplay.entities.ui.hud.bonus;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.stc.game.gameplay.GamePlayUtils;
import com.stc.game.gameplay.engine.managers.LabelManager;
import com.stc.game.gameplay.entities.ui.hud.labels.customs.DynamicLabel;
import com.stc.game.gameplay.entities.world.alive.AliveEntity;
import com.stc.game.gameplay.enums.EndBehaviour;
import com.stc.game.gameplay.tools.Tools;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.solo.SoloContext;

public class BonusEffectCooldown extends BonusEffect {
  private static final String TAG = "BonusEffectCooldown";

  public BonusEffectCooldown(String uniqueId, AliveEntity aliveEntity, Vector2 position) {
    super(uniqueId, aliveEntity, position);
    if (SoloContext.solo || aliveEntity.getUniqueId().equals(MultiplayerContext.playerName)) {
      GamePlayUtils.resetCooldowns();
      LabelManager.addAdvancedLabel(new DynamicLabel(
        TAG,
        new Vector2(-150, Tools.getScreenHeight() / 1.450f),
        new Vector2(Tools.getScreenWidth() / 2.75f, Tools.getScreenHeight() / 1.575f),
        0.5f,
        2,
        2,
        0,
        Color.PURPLE,
        1,
        EndBehaviour.FADE,
        0.5f,
        2,
        "Reloaded!"
      ));
    }
  }

  @Override
  public void update(){
    removeBonusEffect();
  }
}
