package com.stc.game.gameplay.entities.ui.hud.labels.customs;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.stc.game.gameplay.entities.ui.hud.labels.AdvancedLabel;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.multiplayer.enums.MultiplayerTeam;
import com.stc.game.ui.managers.SkinManager;

public class TeamDeathmatchTicketsLabel extends AdvancedLabel {
    private static final String TAG = "TeamDeathmatchTicketsLabel";

    public TeamDeathmatchTicketsLabel(Vector2 position, Color fontColor, float fontSize, float alpha) {
        super(TAG, position, fontColor, fontSize, alpha);
    }

    @Override
    public void update() {
        super.update();
        Player currentPlayer = Player.getCurrent();
        if (getLabelColor().equals(Color.WHITE)) {
            if (currentPlayer != null) {
                if (currentPlayer.getTeam() == MultiplayerTeam.BLUE_TEAM) {
                    setLabelColor(SkinManager.getSkin().getColor("lightblue"));
                } else if (currentPlayer.getTeam() == MultiplayerTeam.RED_TEAM) {
                    setLabelColor(SkinManager.getSkin().getColor("red"));
                }
            }
        }
        if (currentPlayer != null) {
            setText(String.valueOf(currentPlayer.getTickets()));
            setLabelPosition((Gdx.graphics.getWidth() / 2) - (getLabelWidth() / 2), Gdx.graphics.getHeight() - 2 * 48f);
        }
    }
}
