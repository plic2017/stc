package com.stc.game.gameplay.entities.ui.hud;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.stc.game.gameplay.engine.managers.SpriteManager;
import com.stc.game.gameplay.engine.particle.Particle2DEngine;
import com.stc.game.gameplay.entities.ui.Entity2D;
import com.stc.game.gameplay.entities.ui.effects.hud.EffectLife;
import com.stc.game.gameplay.entities.world.Entity3D;
import com.stc.game.gameplay.entities.world.alive.player.Player;

public class Lifebar extends Entity2D {
  private static final String TAG = "Lifebar";

  private Entity3D watchedEntity;
  private EffectLife lifeEffect;
  private Sprite lifePixel;

  public Lifebar(Sprite sprite, Vector2 position) {
    super(sprite, position);

    this.id = TAG;
    this.lifeEffect = new EffectLife(getRightBorderCenterPosition());
    this.lifePixel = SpriteManager.getSprite("textures/ui/lifepixel.png");

    Player currentPlayer = Player.getCurrent();
    if (currentPlayer != null)
      this.watchedEntity = currentPlayer;

    Particle2DEngine.addEffect(this.lifeEffect);
  }

  @Override
  public void draw(SpriteBatch spriteBatch) {
    super.draw(spriteBatch);

    if (watchedEntity == null || watchedEntity.getMaxLife() == 0)
      return;

    float factorOfLifeRemaining = watchedEntity.getCurrentLife() / watchedEntity.getMaxLife();
    float numberOfPixelToWrite = sprite.getWidth() * factorOfLifeRemaining;

    for (int i = 0; i < numberOfPixelToWrite; i++) {
        lifePixel.setPosition(sprite.getX() + i, sprite.getY());
        lifePixel.draw(spriteBatch);
    }

    float posX = sprite.getX() + sprite.getWidth() * factorOfLifeRemaining;
    float posY = sprite.getY() + sprite.getHeight() / 2;
    lifeEffect.setPosition(posX, posY);
  }

  @Override
  public void dispose() {
    super.dispose();
    watchedEntity = null;
    if (lifeEffect != null) {
      lifeEffect.dispose();
      lifeEffect = null;
    }
    lifePixel = null;
  }
}
