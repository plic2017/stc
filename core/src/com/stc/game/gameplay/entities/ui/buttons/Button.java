package com.stc.game.gameplay.entities.ui.buttons;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.stc.game.gameplay.engine.pools.customs.UiPool;
import com.stc.game.gameplay.entities.world.Entity3D;
import com.stc.game.gameplay.tools.Tools;

import lombok.Setter;

public class Button extends InputButton {
  private static final String TAG = "Button";

  @Setter
  protected Entity3D controlledEntity;

  protected Button() {}

  protected Button(Sprite sprite, Vector2 position) {
    this.id = TAG;
    this.sprite = sprite;
    this.position = position;
    this.sprite.setPosition(this.position.x, this.position.y);
  }

  public boolean isInBoundaries(int screenX, int screenY) {
    if (sprite == null)
      return false;

    float fixedY = Tools.getScreenHeight() - screenY;
    return sprite.getBoundingRectangle().contains(screenX, fixedY);
  }

  protected static Button getButtonFromId(final String id) {
    Button button = null;
    UiPool uiPool = UiPool.get();
    if (uiPool != null) {
      button = (Button) uiPool.getEntityFromId(id);
    }
    return button;
  }

  @Override
  public void dispose() {
    super.dispose();
    controlledEntity = null;
  }
}
