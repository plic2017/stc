package com.stc.game.gameplay.entities.ui.hud.labels;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.stc.game.gameplay.engine.managers.LabelManager;
import com.stc.game.gameplay.entities.ui.hud.labels.customs.DebugLabel;
import com.stc.game.gameplay.entities.ui.hud.labels.customs.FpsLabel;

public class Logger {
  private static DebugLabel debugLabel;

  private Logger() {}

  public static void initLogger(boolean logging, boolean printFps) {
    if (printFps)
      addFpsLabel();
    if (logging)
      addDebugLabel();
  }

  private static void addFpsLabel() {
    LabelManager.addAdvancedLabel(new FpsLabel(new Vector2(Gdx.graphics.getWidth() / 1.2f, Gdx.graphics.getHeight() / 1.2f)));
  }

  private static void addDebugLabel() {
    debugLabel = new DebugLabel(new Vector2(Gdx.graphics.getWidth() / 1.5f, Gdx.graphics.getHeight() - 2 * 48f), Color.WHITE, 1, 1);
    LabelManager.addAdvancedLabel(debugLabel);
  }

  public static void logDebug(String logText) {
    if (debugLabel != null) {
      debugLabel.appendText(logText + "\n");
    }
  }

  public static void dispose() {
    debugLabel = null;
  }
}
