package com.stc.game.gameplay.entities.ui.hud.labels.customs;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.stc.game.gameplay.entities.ui.hud.labels.AdvancedLabel;

public class FpsLabel extends AdvancedLabel {
  private static final String TAG = "FpsLabel";

  private static float elapsedSeconds;
  private static double fps;

  public FpsLabel(Vector2 position) {
    super(TAG, position, Color.RED, 1, 1);
  }

  @Override
  public void update() {
    logFps();
  }

  private void logFps() {
    elapsedSeconds -= Gdx.graphics.getDeltaTime();

    if (elapsedSeconds <= 0) {
      elapsedSeconds = 0;
      elapsedSeconds += 1;
      fps = Math.floor(1 / Gdx.graphics.getDeltaTime());
    }

    setText("FPS : " + fps);
  }
}
