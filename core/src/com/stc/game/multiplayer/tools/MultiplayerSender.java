package com.stc.game.multiplayer.tools;

import com.stc.game.gameplay.entities.world.Entity3D;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.gameplay.entities.world.attacks.Attack;
import com.stc.game.gameplay.enums.Attacks;
import com.stc.game.gameplay.enums.Colors;
import com.stc.game.gameplay.enums.Directions;
import com.stc.game.kryonet.client.KryonetClient;
import com.stc.game.kryonet.packets.Packet;
import com.stc.game.kryonet.packets.PacketAttackEffectEntityCollision;
import com.stc.game.kryonet.packets.PacketPlayer;
import com.stc.game.kryonet.packets.PacketPlayerAttack;
import com.stc.game.kryonet.packets.PacketRemoveTeamDeathmatchTicket;
import com.stc.game.kryonet.tools.KryonetPacketBuilder;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.enums.MultiplayerMode;
import com.stc.game.multiplayer.enums.MultiplayerTeam;
import com.stc.game.multiplayer.enums.MultiplayerType;
import com.stc.game.services.Services;
import com.stc.game.services.gpgs.achievements.Achievement;

public class MultiplayerSender {
  public static void sendPlayerIfMoving(final Player player) {
    if (MultiplayerContext.multiplayer) {
      if (!player.getUniqueId().equals(MultiplayerContext.playerName))
        return;
      if (!player.getPositionMotion().isZero() || player.getDirectionMotion() != Directions.NONE)
        sendPlayer(player);
    }
  }

  public static void sendPlayer(final Player player) {
    PacketPlayer packetPlayer = KryonetPacketBuilder.getPacketPlayer(player);
    if (MultiplayerContext.getMultiplayerType() == MultiplayerType.REALTIME_MULTIPLAYER) {
      if (!MultiplayerContext.hasBeenDisconnected) {
        Services.getGpgs().sendPlayer(player);
        if (MultiplayerContext.atLeastOnePlayerDisconnected()) {
          sendPacket(packetPlayer);
        }
      } else {
        sendPacket(packetPlayer);
      }
    } else {
      Services.getNearbyConnectionsService().sendPlayer(player);
    }
  }

  public static void sendPlayer() {
    if (MultiplayerContext.multiplayer) {
      Player currentPlayer = Player.getCurrent();
      if (currentPlayer != null) {
        if (MultiplayerContext.getMultiplayerType() == MultiplayerType.REALTIME_MULTIPLAYER) {
          sendPlayer(currentPlayer);
          Achievement.checkCurrentNumberOfParticipants();
        } else if (!Services.getNearbyConnectionsService().isHost()) {
          sendPlayer(currentPlayer);
        }
      }
    }
  }

  public static void sendChatMessage(final String recipient, final String message) {
    if (MultiplayerContext.numberOfParticipants > 1 && message != null && !(message.length() == 0) && !(message.trim().length() == 0)) {
      if (!MultiplayerContext.hasBeenDisconnected) {
        Services.getGpgs().sendChatMessage(recipient, message);
        if (MultiplayerContext.atLeastOnePlayerDisconnected() &&(recipient == null || MultiplayerContext.haveBeenDisconnected.contains(recipient))) {
          Services.getChatService().sendChatMessage(recipient, message);
        }
      } else {
        Services.getChatService().sendChatMessage(recipient, message);
      }
      Achievement.checkHelloWorld(message);
    }
  }

  public static void sendAttackEffectEntityCollision(final Attack attack, final float damageCoef, final Entity3D attackedEntity) {
    int attackId = attack.getType().getAttackId();
    String attackedEntityId = attackedEntity.getUniqueId();
    PacketAttackEffectEntityCollision packetAttackEffectEntityCollision = KryonetPacketBuilder.getPacketAttackEffectEntityCollision(attackId, damageCoef, attackedEntityId);
    if (MultiplayerContext.getMultiplayerType() == MultiplayerType.REALTIME_MULTIPLAYER) {
      if (!MultiplayerContext.hasBeenDisconnected) {
        Services.getGpgs().sendAttackEffectEntityCollision(attackId, damageCoef, attackedEntityId);
        if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.SURVIVAL || MultiplayerContext.atLeastOnePlayerDisconnected()) {
          sendPacket(packetAttackEffectEntityCollision);
        }
      } else {
        sendPacket(packetAttackEffectEntityCollision);
      }
    } else {
      Services.getNearbyConnectionsService().sendAttackEffectEntityCollision(attackId, damageCoef, attackedEntityId);
      if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.SURVIVAL) {
        sendPacket(packetAttackEffectEntityCollision);
      }
    }
  }

  public static void sendPlayerAttack(final Attacks attackType, final String targetId, final Colors colors) {
    if (MultiplayerContext.multiplayer) {
      int colorId = Colors.getIdFromColors(colors);
      if (MultiplayerContext.getMultiplayerType() == MultiplayerType.REALTIME_MULTIPLAYER) {
        PacketPlayerAttack packetPlayerAttack = KryonetPacketBuilder.getPacketPlayerAttack(attackType.getAttackId(), targetId, colorId);
        if (!MultiplayerContext.hasBeenDisconnected) {
          Services.getGpgs().sendPlayerAttack(attackType.getAttackId(), targetId, colorId);
          if (MultiplayerContext.atLeastOnePlayerDisconnected()) {
            sendPacket(packetPlayerAttack);
          }
        } else {
          sendPacket(packetPlayerAttack);
        }
      } else {
        Services.getNearbyConnectionsService().sendPlayerAttack(attackType.getAttackId(), targetId, colorId);
      }
    }
  }

  public static void sendPacketRemoveTeamDeathmatchTicket() {
    if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.TEAM_DEATHMATCH) {
      PacketRemoveTeamDeathmatchTicket packetRemoveTeamDeathmatchTicket = KryonetPacketBuilder.getPacketRemoveTeamDeathmatchTicket();
      if (MultiplayerContext.getMultiplayerType() == MultiplayerType.REALTIME_MULTIPLAYER) {
        if (!MultiplayerContext.hasBeenDisconnected) {
          Services.getGpgs().sendRemoveTeamDeathmatchTicket();
          if (MultiplayerContext.atLeastOnePlayerDisconnected()) {
            sendPacket(packetRemoveTeamDeathmatchTicket);
          }
        } else {
          sendPacket(packetRemoveTeamDeathmatchTicket);
        }
      } else {
        Services.getNearbyConnectionsService().sendRemoveTeamDeathmatchTicket();
      }
    }
  }

  public static void sendNumberOfParticipants() {
    sendPacket(KryonetPacketBuilder.getPacketNumberOfParticipants());
  }

  static void sendPacketPlayerReady() {
    if (!MultiplayerUtils.isHost()) {
      sendPacket(KryonetPacketBuilder.getPacketPlayerReady());
    }
  }

  public static void sendPacketGetWave() {
    if (MultiplayerUtils.isHost()) {
      sendPacket(KryonetPacketBuilder.getPacketGetWave());
    }
  }

  public static void sendTeamAffectationRequest(final String playerName) {
    sendPacket(KryonetPacketBuilder.getPacketTeamAffectation(playerName));
  }

  public static void sendPacketGameOver(final MultiplayerTeam team) {
    sendPacket(KryonetPacketBuilder.getPacketGameOver(team.getMultiplayerTeamId()));
  }

  public static void sendPacket(final Packet packet) {
    if (KryonetClient.client != null && KryonetClient.connected) {
      KryonetClient.client.sendUDP(packet);
    }
  }
}
