package com.stc.game.multiplayer.tools;

import com.stc.game.gameplay.engine.pools.customs.PlayerPool;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.kryonet.client.KryonetClient;
import com.stc.game.kryonet.tools.KryonetUtils;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.beans.PlayerBean;
import com.stc.game.multiplayer.enums.MultiplayerType;
import com.stc.game.services.Services;
import com.stc.game.services.gpgs.leaderboards.Leaderboard;
import com.stc.game.solo.SoloContext;
import com.stc.game.ui.dialogs.Dialogs;
import com.stc.game.ui.tools.I18N;

public final class MultiplayerUtils {
  private static final int MAX_CONNECTION_ATTEMPTS = 5;

  private static int connectionAttempts = 0;

  static void updateMultiplayer(final PlayerBean playerBean) {
    if (isAlreadyInGame(playerBean.getPlayerName())) {
      updateAlreadyInGamePlayer(playerBean);
    } else {
      PlayerPool.addPendingPlayer(playerBean);
    }
  }

  private static boolean isAlreadyInGame(final String playerName) {
    PlayerPool currentPlayerPool = PlayerPool.get();
    return currentPlayerPool != null && currentPlayerPool.getEntityFromUniqueId(playerName) != null;
  }

  private static void updateAlreadyInGamePlayer(final PlayerBean playerBean) {
    PlayerPool currentPlayerPool = PlayerPool.get();
    if (currentPlayerPool != null) {
      Player player = (Player) currentPlayerPool.getEntityFromUniqueId(playerBean.getPlayerName());
      if (player != null) {
        player.setPosition(playerBean.getPosition());
        player.setDirection(playerBean.getDirection());
      }
    }
  }

  public static boolean bothPlayersInSameTeam(final String playerName1, final String playerName2) {
    PlayerPool playerPool = PlayerPool.get();
    if (playerPool != null) {
      Player player1 = (Player) playerPool.getEntityFromUniqueId(playerName1);
      Player player2 = (Player) playerPool.getEntityFromUniqueId(playerName2);
      return player1 != null && player2 != null && player1.getTeam() == player2.getTeam();
    }
    return false;
  }

  public static boolean isHost() {
    if (MultiplayerContext.hostName == null) {
      if (connectionAttempts++ < MAX_CONNECTION_ATTEMPTS) {
        KryonetUtils.disposeKryonetClient(true);
        return false;
      } else {
        Dialogs.showButtonDialog(I18N.getTranslation(I18N.Text.SURVIVAL_MODE_ERROR), I18N.getTranslation(I18N.Text.SURVIVAL_MODE_ERROR_DESCRIPTION));
        return false;
      }
    }

    return MultiplayerContext.hostName.equals(MultiplayerContext.playerName);
  }

  public static void dispose(final long elapsedTime) {
    if (KryonetClient.connected) {
      KryonetUtils.disposeKryonetClient(false);
    }
    if (MultiplayerContext.getMultiplayerType() == MultiplayerType.REALTIME_MULTIPLAYER) {
      Services.getGpgs().submitScore(Leaderboard.LONGEST_MULTIPLAYER_SESSION.getLeaderboardId(), elapsedTime);
      Services.getGpgs().leaveRoom();
    } else {
      Services.getNearbyConnectionsService().stopNearbyConnectionsActivity(null);
    }
    connectionAttempts = 0;
    MultiplayerContext.reset();
    SoloContext.solo = true;
  }
}
