package com.stc.game.multiplayer.tools;

import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.stc.game.gameplay.engine.pools.customs.EnemyPool;
import com.stc.game.gameplay.engine.pools.customs.MapPool;
import com.stc.game.gameplay.engine.pools.customs.PlayerPool;
import com.stc.game.gameplay.engine.wave.WaveEngine;
import com.stc.game.gameplay.entities.ui.buttons.chat.IncomingChatMessageButton;
import com.stc.game.gameplay.entities.ui.buttons.chat.utils.ChatMessageEntry;
import com.stc.game.gameplay.entities.world.Entity3D;
import com.stc.game.gameplay.entities.world.alive.enemies.Enemy;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.gameplay.entities.world.bonus.Bonus;
import com.stc.game.gameplay.enums.Attacks;
import com.stc.game.gameplay.enums.Colors;
import com.stc.game.gameplay.enums.Effects3D;
import com.stc.game.kryonet.packets.PacketAttackEffectEntityCollision;
import com.stc.game.kryonet.packets.PacketBonus;
import com.stc.game.kryonet.packets.PacketPlayer;
import com.stc.game.kryonet.packets.PacketPlayerAttack;
import com.stc.game.kryonet.packets.PacketPlayerMessage;
import com.stc.game.kryonet.packets.PacketSendHost;
import com.stc.game.kryonet.packets.PacketSendWave;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.beans.PlayerBean;
import com.stc.game.multiplayer.enums.MultiplayerMode;
import com.stc.game.multiplayer.enums.MultiplayerTeam;
import com.stc.game.multiplayer.enums.MultiplayerType;
import com.stc.game.ui.dialogs.Dialogs;
import com.stc.game.ui.tools.I18N;

import java.util.LinkedList;

public class MultiplayerReceiver {
  public static void updateMultiplayer(final PacketPlayer packetPlayer) {
    PlayerBean playerBean = new PlayerBean();
    playerBean.setPlayerName(packetPlayer.entityId);
    playerBean.setPosition(new Vector3(packetPlayer.xPosition, packetPlayer.yPosition, packetPlayer.zPosition));
    playerBean.setDirection(new Quaternion(packetPlayer.xDirection, packetPlayer.yDirection, packetPlayer.zDirection, packetPlayer.wDirection));
    MultiplayerUtils.updateMultiplayer(playerBean);
  }

  public static void synchronizePlayersAttacks(final PacketPlayerAttack packetPlayerAttack) {
    PlayerPool currentPlayerPool = PlayerPool.get();
    if (currentPlayerPool != null) {
      Player player = (Player) currentPlayerPool.getEntityFromUniqueId(packetPlayerAttack.entityId);
      if (player != null) {
        if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.SURVIVAL) {
          Colors colors = Colors.getColorFromId(packetPlayerAttack.colorId);
          if (colors != null && player.getCurrentColorUsed() != colors) {
            player.setCurrentColorUsed(colors);
            player.modifyAttackEffects(colors);
          }
        }
        Attacks attackType = Attacks.values()[packetPlayerAttack.attackId];
        if (packetPlayerAttack.targetId != null) {
          if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.SURVIVAL) {
            EnemyPool currentEnemyPool = EnemyPool.get();
            if (currentEnemyPool != null) {
              Entity3D target = currentEnemyPool.getEntityFromUniqueId(packetPlayerAttack.targetId);
              if (target != null)
                player.getAttack(attackType).triggerAttack(player.getPosition(), target);
            }
          } else {
            Entity3D target = currentPlayerPool.getEntityFromUniqueId(packetPlayerAttack.targetId);
            if (target != null)
              player.getAttack(attackType).triggerAttack(player.getPosition(), target);
          }
        } else {
          player.getAttack(attackType).triggerAttack(player.getPosition());
        }
      }
    }
  }

  public static void synchronizeAttackEffectEntityCollision(final PacketAttackEffectEntityCollision packetAttackEffectEntityCollision) {
    if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.SURVIVAL) {
      EnemyPool currentEnemyPool = EnemyPool.get();
      if (currentEnemyPool != null) {
        Enemy enemy = (Enemy) currentEnemyPool.getEntityFromUniqueId(packetAttackEffectEntityCollision.attackedEntityId);
        if (enemy != null)
          enemy.applyDamage(Attacks.getAttacksDamages().get(packetAttackEffectEntityCollision.attackId), packetAttackEffectEntityCollision.damageCoef);
      }
    } else if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.DEATHMATCH ||
               MultiplayerContext.getMultiplayerMode() == MultiplayerMode.TEAM_DEATHMATCH) {
      if (MultiplayerContext.inNearbyConnectionsMultiplayer() && MultiplayerContext.numberOfParticipants == -1) {
        return;
      }
      PlayerPool currentPlayerPool = PlayerPool.get();
      if (currentPlayerPool != null) {
        Player player = (Player) currentPlayerPool.getEntityFromUniqueId(packetAttackEffectEntityCollision.attackedEntityId);
        if (player != null)
          player.applyDamage(Attacks.getAttacksDamages().get(packetAttackEffectEntityCollision.attackId), packetAttackEffectEntityCollision.damageCoef);
      }
    }
  }

  public static void removePlayerFromPlayerPool(final String playerName) {
    PlayerPool currentPlayerPool = PlayerPool.get();
    if (currentPlayerPool != null && currentPlayerPool.removeEntityFromUniqueId(playerName)) {
      MultiplayerContext.numberOfParticipants--;
    }
  }

  public static void removeEnemyFromEnemyPool(final String enemyId) {
    EnemyPool enemyPool = EnemyPool.get();
    if (enemyPool != null) {
      Enemy enemy = (Enemy) enemyPool.getEntityFromUniqueId(enemyId);
      if (enemy != null) {
        enemy.kill();
      }
    }
  }

  public static void displayChatMessageNotification(final PacketPlayerMessage packetPlayerMessage) {
    String playerName = packetPlayerMessage.entityId;
    if (MultiplayerContext.blockedPlayers != null && MultiplayerContext.blockedPlayers.contains(playerName)) {
      return;
    }

    String message = I18N.getTranslation(I18N.Text.FROM) + " " + playerName +  " : " + packetPlayerMessage.message;
    LinkedList<ChatMessageEntry> chatMessagesInbox = IncomingChatMessageButton.getChatMessagesInbox();
    if (chatMessagesInbox != null) {
      chatMessagesInbox.addLast(new ChatMessageEntry(playerName, message));
    }
  }

  public static void displayHostDisconnectionInformation() {
    Dialogs.showButtonDialog(I18N.getTranslation(I18N.Text.NEARBY_CONNECTIONS), I18N.getTranslation(I18N.Text.HOST_DISCONNECTED));
  }

  public static void setHostName(final PacketSendHost packetSendHost) {
    MultiplayerContext.hostName = packetSendHost.hostName;
    if (MultiplayerContext.getMultiplayerType() == MultiplayerType.REALTIME_MULTIPLAYER && MultiplayerUtils.isHost() && !MultiplayerContext.hasSentNumberOfParticipants) {
      MultiplayerSender.sendNumberOfParticipants();
    }
    MultiplayerContext.hasSentNumberOfParticipants = true;
  }

  public static void addEnemiesToCurrentWave(final PacketSendWave packetSendWave) {
    WaveEngine.nextMultiplayerWave(packetSendWave);
    if (packetSendWave.complete) {
      MultiplayerSender.sendPacketPlayerReady();
    }
  }

  public static void affectPlayerToATeam(final String playerName, final int teamId) {
    PlayerPool currentPlayerPool = PlayerPool.get();
    if (currentPlayerPool != null) {
      Player player = (Player) currentPlayerPool.getEntityFromUniqueId(playerName);
      if (player != null) {
        player.setTeam(MultiplayerTeam.getMultiplayerTeamFromId(teamId));
        if (player.getTeam() != null) {
          Colors colors = player.getTeam() == MultiplayerTeam.BLUE_TEAM ? Colors.BLUE : Colors.RED;
          player.setCurrentColorUsed(colors);
          player.setColorsType(colors);
          if (player.getModelInstance() != null) {
            player.modifyModelInstanceWithColor(colors.getColor());
          }
          Effects3D basicShotEffect = player.getTeam() == MultiplayerTeam.RED_TEAM ?
            Effects3D.RED_BASIC_SHOT_EFFECT : Effects3D.BLUE_BASIC_SHOT_EFFECT;
          Effects3D fireballEffect = player.getTeam() == MultiplayerTeam.RED_TEAM ?
            Effects3D.RED_FIREBALL_EFFECT : Effects3D.BLUE_FIREBALL_EFFECT;
          Effects3D rocketEffect = player.getTeam() == MultiplayerTeam.RED_TEAM ?
            Effects3D.RED_ROCKET_EFFECT : Effects3D.BLUE_ROCKET_EFFECT;
          player.modifyAttackEffects(basicShotEffect, fireballEffect, rocketEffect);
        }
      }
    }
  }

  public static void removeTeamDeathmatchTicket() {
    Player currentPlayer = Player.getCurrent();
    if (currentPlayer != null) {
      currentPlayer.setTickets(currentPlayer.getTickets() - 1);
    }
  }

  public static void addBonusObject(final PacketBonus packetBonus) {
    Bonus bonus = com.stc.game.gameplay.enums.Bonus.getBonus(packetBonus);
    if (bonus != null) {
      MapPool.get().add(bonus);
    }
  }
}
