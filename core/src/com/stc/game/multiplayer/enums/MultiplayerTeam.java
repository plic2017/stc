package com.stc.game.multiplayer.enums;

import lombok.Getter;

public enum MultiplayerTeam {
  BLUE_TEAM(0),
  RED_TEAM(1);

  @Getter
  private final int multiplayerTeamId;

  MultiplayerTeam(final int multiplayerTeamId) {
    this.multiplayerTeamId = multiplayerTeamId;
  }

  public static MultiplayerTeam getMultiplayerTeamFromId(final int teamId) {
    if (teamId == BLUE_TEAM.getMultiplayerTeamId()) {
      return BLUE_TEAM;
    } else if (teamId == RED_TEAM.getMultiplayerTeamId()) {
      return RED_TEAM;
    }
    return null;
  }
}
