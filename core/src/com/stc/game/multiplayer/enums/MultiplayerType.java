package com.stc.game.multiplayer.enums;

import com.stc.game.multiplayer.beans.MultiplayerContext;
import lombok.Getter;

public enum MultiplayerType {
    REALTIME_MULTIPLAYER(0),
    NEARBY_CONNECTIONS_MULTIPLAYER(1);

    @Getter
    private final int multiplayerTypeId;

    MultiplayerType(final int multiplayerTypeId) {
        this.multiplayerTypeId = multiplayerTypeId;
    }

    public static int getMultiplayerTypeIdFromMultiplayerContext() {
        if (MultiplayerContext.getMultiplayerType() != null) {
            return MultiplayerContext.getMultiplayerType().getMultiplayerTypeId();
        }
        return -1;
    }
}
