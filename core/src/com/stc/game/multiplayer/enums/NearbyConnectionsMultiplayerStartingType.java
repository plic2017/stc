package com.stc.game.multiplayer.enums;

public enum NearbyConnectionsMultiplayerStartingType {
  START_ADVERTISING,
  START_DISCOVERING
}
