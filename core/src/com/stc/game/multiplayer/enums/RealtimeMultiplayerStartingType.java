package com.stc.game.multiplayer.enums;

public enum RealtimeMultiplayerStartingType {
    QUICK_GAME,
    INVITE_PLAYERS
}
