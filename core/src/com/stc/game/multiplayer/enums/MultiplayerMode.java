package com.stc.game.multiplayer.enums;

import com.stc.game.multiplayer.beans.MultiplayerContext;

import lombok.Getter;

public enum MultiplayerMode {
    SURVIVAL(0, "Survival"),
    DEATHMATCH(1, "Deathmatch"),
    TEAM_DEATHMATCH(2, "Team Deathmatch");

    @Getter
    private final int multiplayerModeId;
    @Getter
    private final String name;

    MultiplayerMode(final int multiplayerModeId, final String name) {
        this.multiplayerModeId = multiplayerModeId;
        this.name = name;
    }

    public static int getMultiplayerModeIdFromMultiplayerContext() {
        if (MultiplayerContext.getMultiplayerMode() != null) {
            return MultiplayerContext.getMultiplayerMode().getMultiplayerModeId();
        }
        return -1;
    }

    public static int getMultiplayerModeVariantFromMultiplayerContext() {
        return getMultiplayerModeIdFromMultiplayerContext() + 1;
    }

    public static void setMultiplayerModeFromVariant(final int variant) {
        int multiplayerModeId = variant - 1;
        MultiplayerContext.setMultiplayerMode(getMultiplayerModeFromId(multiplayerModeId));
    }

    private static MultiplayerMode getMultiplayerModeFromId(final int multiplayerModeId) {
        if (multiplayerModeId == SURVIVAL.getMultiplayerModeId()) {
            return SURVIVAL;
        } else if (multiplayerModeId == DEATHMATCH.getMultiplayerModeId()) {
            return DEATHMATCH;
        } else if (multiplayerModeId == TEAM_DEATHMATCH.getMultiplayerModeId()) {
            return TEAM_DEATHMATCH;
        }
        return null;
    }
}
