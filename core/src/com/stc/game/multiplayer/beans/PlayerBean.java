package com.stc.game.multiplayer.beans;

import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PlayerBean {
  private String playerName;
  private Vector3 position;
  private Quaternion direction;
}
