package com.stc.game.multiplayer.beans;

import com.stc.game.multiplayer.enums.MultiplayerMode;
import com.stc.game.multiplayer.enums.MultiplayerType;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

public class MultiplayerContext {
    public static boolean multiplayer = false;
    public static boolean hasBeenDisconnected = false;
    public static boolean hasSentNumberOfParticipants = false;
    public static boolean allPlayersHaveJoined = false;

    public static String playerName = null;
    public static String roomId = null;
    public static String hostName = null;

    public static ArrayList<String> haveBeenDisconnected = null;
    public static ArrayList<String> blockedPlayers = null;

    public static int numberOfParticipants = -1;

    @Getter @Setter
    private static MultiplayerType multiplayerType = null;
    @Getter @Setter
    private static MultiplayerMode multiplayerMode = null;

    public static boolean inRealtimeMultiplayer() {
        return multiplayer && multiplayerType == MultiplayerType.REALTIME_MULTIPLAYER;
    }

    public static boolean inNearbyConnectionsMultiplayer() {
        return multiplayer && multiplayerType == MultiplayerType.NEARBY_CONNECTIONS_MULTIPLAYER;
    }

    public static boolean atLeastOnePlayerDisconnected() {
        return haveBeenDisconnected != null && !haveBeenDisconnected.isEmpty();
    }

    public static void reset() {
        multiplayer = false;
        hasBeenDisconnected = false;
        hasSentNumberOfParticipants = false;
        allPlayersHaveJoined = false;

        playerName = null;
        roomId = null;
        hostName = null;

        if (haveBeenDisconnected != null) {
            haveBeenDisconnected.clear();
            haveBeenDisconnected = null;
        }
        if (blockedPlayers != null) {
            blockedPlayers.clear();
            blockedPlayers = null;
        }

        numberOfParticipants = -1;

        multiplayerType = null;
        multiplayerMode = null;
    }
}
