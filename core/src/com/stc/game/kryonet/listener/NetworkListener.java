package com.stc.game.kryonet.listener;

import com.badlogic.gdx.Gdx;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.stc.game.kryonet.client.KryonetClient;
import com.stc.game.kryonet.scheduler.PacketScheduler;
import com.stc.game.kryonet.tools.KryonetSender;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.enums.MultiplayerMode;
import com.stc.game.services.Services;
import com.stc.game.ui.dialogs.Dialogs;
import com.stc.game.ui.tools.I18N;
import de.tomgrill.gdxdialogs.core.dialogs.GDXProgressDialog;
import lombok.Setter;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class NetworkListener extends Listener {
  private static final String TAG = "NetworkListener";
  private static final long NOW = 0L;
  private static final long ONE_SECOND = 1000L;
  private static final long RECONNECTION_TIMEOUT = 1000L * 20L;

  @Setter
  private static boolean shouldReconnect;

  private Timer timer;
  private GDXProgressDialog reconnectionProgressDialog;

  public NetworkListener() {
    super();
    shouldReconnect = MultiplayerContext.inRealtimeMultiplayer();
  }

  @Override
  public void connected(Connection connection) {
    KryonetClient.connected = true;
    KryonetSender.registerCurrentRoom();
    KryonetSender.registerCurrentPlayer();
    if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.SURVIVAL) {
      KryonetSender.getHostName();
    }

    if (MultiplayerContext.hasBeenDisconnected) {
      KryonetSender.sendReconnectionInformation();
      reconnectionProgressDialog.dismiss();
      dispose();
    }
  }

  @Override
  public void disconnected(Connection connection) {
    KryonetClient.connected = false;

    if (shouldReconnect) {
      MultiplayerContext.hasBeenDisconnected = true;
      Services.getGpgs().leaveRoom();
      reconnectionProgressDialog = Dialogs.getProgressDialog(I18N.getTranslation(I18N.Text.DISCONNECTED), I18N.getTranslation(I18N.Text.RECONNECTION_IN_PROGRESS));
      reconnectionProgressDialog.build().show();
      timer = new Timer();
      final long startTimeMillis = System.currentTimeMillis();
      timer.schedule(new TimerTask() {
        @Override
        public void run() {
          if (Services.getNetworkService().isConnectedToWifiOrMobile()) {
            try {
              KryonetClient.client.reconnect();
              cancel();
            } catch (IOException e) {
              Gdx.app.error(TAG, e.getMessage());
            }
          } else if (System.currentTimeMillis() - startTimeMillis > RECONNECTION_TIMEOUT) {
            reconnectionProgressDialog.dismiss();
            dispose();
            Dialogs.showButtonDialog(I18N.getTranslation(I18N.Text.DISCONNECTED), I18N.getTranslation(I18N.Text.RECONNECTION_TIMEOUT));
          }
        }
      }, NOW, ONE_SECOND);
    }
  }

  @Override
  public void received(Connection connection, Object object) {
    if (object == null)
      return;

    PacketScheduler.addPacket(object);
  }

  private void dispose() {
    if (timer != null) {
      timer.cancel();
      timer.purge();
      timer = null;
    }
    reconnectionProgressDialog = null;
  }
}
