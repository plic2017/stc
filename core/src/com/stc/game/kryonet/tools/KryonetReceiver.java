package com.stc.game.kryonet.tools;

import com.stc.game.gameplay.engine.pools.customs.EnemyPool;
import com.stc.game.kryonet.packets.PacketAttackEffectEntityCollision;
import com.stc.game.kryonet.packets.PacketBonus;
import com.stc.game.kryonet.packets.PacketEnemyAttack;
import com.stc.game.kryonet.packets.PacketPlayer;
import com.stc.game.kryonet.packets.PacketPlayerAttack;
import com.stc.game.kryonet.packets.PacketPlayerMessage;
import com.stc.game.kryonet.packets.PacketReconnection;
import com.stc.game.kryonet.packets.PacketRemoveEnemy;
import com.stc.game.kryonet.packets.PacketRemovePlayer;
import com.stc.game.kryonet.packets.PacketSendHost;
import com.stc.game.kryonet.packets.PacketSendWave;
import com.stc.game.kryonet.packets.PacketTeamAffectation;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.tools.MultiplayerReceiver;
import com.stc.game.ui.enums.GameScreenState;
import com.stc.game.ui.screens.GameScreen;

import java.util.ArrayList;

public class KryonetReceiver {
  public static void handlePacketTeamAffectation(final PacketTeamAffectation packetTeamAffectation) {
    String playerName = packetTeamAffectation.entityId;
    int teamId = packetTeamAffectation.teamId;
    MultiplayerReceiver.affectPlayerToATeam(playerName, teamId);
  }

  public static void handlePacketSendHost(final PacketSendHost packetSendHost) {
    MultiplayerReceiver.setHostName(packetSendHost);
  }

  public static void handlePacketHostDisconnection() {
    MultiplayerReceiver.displayHostDisconnectionInformation();
  }

  public static void handlePacketSendWave(final PacketSendWave packetSendWave) {
    MultiplayerReceiver.addEnemiesToCurrentWave(packetSendWave);
  }

  public static void handlePacketPlayer(final PacketPlayer packetPlayer) {
    MultiplayerReceiver.updateMultiplayer(packetPlayer);
  }

  public static void handlePacketPlayerAttack(final PacketPlayerAttack packetPlayerAttack) {
    MultiplayerReceiver.synchronizePlayersAttacks(packetPlayerAttack);
  }

  public static void handlePacketEnemyAttack(final PacketEnemyAttack packetEnemyAttack) {
    EnemyPool currentEnemyPool = EnemyPool.get();
    if (currentEnemyPool != null)
      currentEnemyPool.processPacketEnemyAttack(packetEnemyAttack);
  }

  public static void handlePacketCollisionEffectEntity(final PacketAttackEffectEntityCollision packetEffectEntityCollision) {
    MultiplayerReceiver.synchronizeAttackEffectEntityCollision(packetEffectEntityCollision);
  }

  public static void handlePacketRemovePlayer(final PacketRemovePlayer packetRemovePlayer) {
    MultiplayerReceiver.removePlayerFromPlayerPool(packetRemovePlayer.entityId);
  }

  public static void handlePacketRemoveEnemy(final PacketRemoveEnemy packetRemoveEnemy) {
    MultiplayerReceiver.removeEnemyFromEnemyPool(packetRemoveEnemy.entityId);
  }

  public static void handlePacketPlayerMessage(final PacketPlayerMessage packetPlayerMessage) {
    MultiplayerReceiver.displayChatMessageNotification(packetPlayerMessage);
  }

  public static void handlePacketReconnection(final PacketReconnection packetReconnection) {
    MultiplayerContext.numberOfParticipants++;
    if (MultiplayerContext.haveBeenDisconnected == null) {
      MultiplayerContext.haveBeenDisconnected = new ArrayList<>();
    }
    if (!MultiplayerContext.haveBeenDisconnected.contains(packetReconnection.entityId)) {
      MultiplayerContext.haveBeenDisconnected.add(packetReconnection.entityId);
    }
  }

  public static void handlePacketRemoveTeamDeathmatchTicket() {
    MultiplayerReceiver.removeTeamDeathmatchTicket();
  }

  public static void handlePacketBonus(final PacketBonus packetBonus) {
    MultiplayerReceiver.addBonusObject(packetBonus);
  }

  public static void handlePacketWin() {
    GameScreen.setGameScreenStateAfterResume(GameScreenState.WIN);
  }
}
