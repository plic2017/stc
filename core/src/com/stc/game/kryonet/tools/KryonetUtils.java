package com.stc.game.kryonet.tools;

import com.stc.game.kryonet.client.KryonetClient;
import com.stc.game.kryonet.threads.KryonetClientDisconnection;

public final class KryonetUtils {
  public static void launchKryonetClient() {
    new KryonetClient();
  }

  public static void disposeKryonetClient(final boolean relaunchKryonetClient) {
    // Use of a thread to not block the UI
    new Thread(new KryonetClientDisconnection(relaunchKryonetClient)).start();
  }
}
