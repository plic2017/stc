package com.stc.game.kryonet.tools;

import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.kryonet.packets.PacketAttackEffectEntityCollision;
import com.stc.game.kryonet.packets.PacketGameOver;
import com.stc.game.kryonet.packets.PacketGetHost;
import com.stc.game.kryonet.packets.PacketGetWave;
import com.stc.game.kryonet.packets.PacketNumberOfParticipants;
import com.stc.game.kryonet.packets.PacketOnConnectedToRoom;
import com.stc.game.kryonet.packets.PacketPlayer;
import com.stc.game.kryonet.packets.PacketPlayerAttack;
import com.stc.game.kryonet.packets.PacketPlayerMessage;
import com.stc.game.kryonet.packets.PacketPlayerReady;
import com.stc.game.kryonet.packets.PacketReconnection;
import com.stc.game.kryonet.packets.PacketRemoveTeamDeathmatchTicket;
import com.stc.game.kryonet.packets.PacketTeamAffectation;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.enums.MultiplayerMode;
import com.stc.game.multiplayer.enums.MultiplayerType;

public final class KryonetPacketBuilder {
  public static PacketAttackEffectEntityCollision getPacketAttackEffectEntityCollision(final int attackId, final float damageCoef, final String attackedEntityId) {
    PacketAttackEffectEntityCollision packetCollisionEffectEntity = new PacketAttackEffectEntityCollision();
    packetCollisionEffectEntity.entityId = MultiplayerContext.playerName;
    packetCollisionEffectEntity.roomId = MultiplayerContext.roomId;
    packetCollisionEffectEntity.attackId = attackId;
    packetCollisionEffectEntity.damageCoef = damageCoef;
    packetCollisionEffectEntity.attackedEntityId = attackedEntityId;
    packetCollisionEffectEntity.hasBeenDisconnected = MultiplayerContext.hasBeenDisconnected;
    packetCollisionEffectEntity.haveBeenDisconnected = MultiplayerContext.haveBeenDisconnected;
    return packetCollisionEffectEntity;
  }

  static PacketGetHost getPacketGetHost() {
    PacketGetHost packetGetHost = new PacketGetHost();
    packetGetHost.entityId = MultiplayerContext.playerName;
    packetGetHost.roomId = MultiplayerContext.roomId;
    return packetGetHost;
  }

  public static PacketGetWave getPacketGetWave() {
    PacketGetWave packetGetWave = new PacketGetWave();
    packetGetWave.entityId = MultiplayerContext.playerName;
    packetGetWave.roomId = MultiplayerContext.roomId;
    return packetGetWave;
  }

  public static PacketNumberOfParticipants getPacketNumberOfParticipants() {
    PacketNumberOfParticipants packetNumberOfParticipants = new PacketNumberOfParticipants();
    packetNumberOfParticipants.entityId = MultiplayerContext.playerName;
    packetNumberOfParticipants.roomId = MultiplayerContext.roomId;
    packetNumberOfParticipants.numberOfParticipants = MultiplayerContext.numberOfParticipants;
    return packetNumberOfParticipants;
  }

  static PacketOnConnectedToRoom getPacketOnConnectedToRoom() {
    PacketOnConnectedToRoom packetOnRoomConnected = new PacketOnConnectedToRoom();
    packetOnRoomConnected.entityId = MultiplayerContext.playerName;
    packetOnRoomConnected.roomId = MultiplayerContext.roomId;
    packetOnRoomConnected.multiplayerTypeId = MultiplayerType.getMultiplayerTypeIdFromMultiplayerContext();
    packetOnRoomConnected.multiplayerModeId = MultiplayerMode.getMultiplayerModeIdFromMultiplayerContext();
    return packetOnRoomConnected;
  }

  public static PacketPlayer getPacketPlayer(final Player player) {
    PacketPlayer packetPlayer = new PacketPlayer();
    packetPlayer.entityId = MultiplayerContext.playerName;
    packetPlayer.roomId = MultiplayerContext.roomId;
    packetPlayer.xPosition = player.getPosition().x;
    packetPlayer.yPosition = player.getPosition().y;
    packetPlayer.zPosition = player.getPosition().z;
    packetPlayer.xDirection = player.getDirection().x;
    packetPlayer.yDirection = player.getDirection().y;
    packetPlayer.zDirection = player.getDirection().z;
    packetPlayer.wDirection = player.getDirection().w;
    packetPlayer.hasBeenDisconnected = MultiplayerContext.hasBeenDisconnected;
    packetPlayer.haveBeenDisconnected = MultiplayerContext.haveBeenDisconnected;
    return packetPlayer;
  }

  public static PacketPlayerAttack getPacketPlayerAttack(final int attackId, final String targetId, final int colorId) {
    PacketPlayerAttack packetPlayerAttack = new PacketPlayerAttack();
    packetPlayerAttack.entityId = MultiplayerContext.playerName;
    packetPlayerAttack.roomId = MultiplayerContext.roomId;
    packetPlayerAttack.attackId = attackId;
    packetPlayerAttack.colorId = colorId;
    packetPlayerAttack.targetId = targetId;
    packetPlayerAttack.hasBeenDisconnected = MultiplayerContext.hasBeenDisconnected;
    packetPlayerAttack.haveBeenDisconnected = MultiplayerContext.haveBeenDisconnected;
    return packetPlayerAttack;
  }

  public static PacketPlayerReady getPacketPlayerReady() {
    PacketPlayerReady packetPlayerReady = new PacketPlayerReady();
    packetPlayerReady.entityId = MultiplayerContext.playerName;
    packetPlayerReady.roomId = MultiplayerContext.roomId;
    return packetPlayerReady;
  }

  public static PacketPlayerMessage getPacketPlayerMessage(final String recipient, final String message) {
    PacketPlayerMessage packetPlayerMessage = new PacketPlayerMessage();
    packetPlayerMessage.entityId = MultiplayerContext.playerName;
    packetPlayerMessage.roomId = MultiplayerContext.roomId;
    packetPlayerMessage.recipient = recipient;
    packetPlayerMessage.message = message;
    packetPlayerMessage.hasBeenDisconnected = MultiplayerContext.hasBeenDisconnected;
    packetPlayerMessage.haveBeenDisconnected = MultiplayerContext.haveBeenDisconnected;
    return packetPlayerMessage;
  }

  static PacketReconnection getPacketReconnection() {
    PacketReconnection packetReconnection = new PacketReconnection();
    packetReconnection.entityId = MultiplayerContext.playerName;
    packetReconnection.roomId = MultiplayerContext.roomId;
    return packetReconnection;
  }

  public static PacketTeamAffectation getPacketTeamAffectation(final String playerName) {
    PacketTeamAffectation packetTeamAffectation = new PacketTeamAffectation();
    packetTeamAffectation.entityId = playerName;
    packetTeamAffectation.roomId = MultiplayerContext.roomId;
    packetTeamAffectation.teamId = -1;
    return packetTeamAffectation;
  }

  public static PacketRemoveTeamDeathmatchTicket getPacketRemoveTeamDeathmatchTicket() {
    PacketRemoveTeamDeathmatchTicket packetRemoveTeamDeathmatchTicket = new PacketRemoveTeamDeathmatchTicket();
    packetRemoveTeamDeathmatchTicket.entityId = MultiplayerContext.playerName;
    packetRemoveTeamDeathmatchTicket.roomId = MultiplayerContext.roomId;
    packetRemoveTeamDeathmatchTicket.hasBeenDisconnected = MultiplayerContext.hasBeenDisconnected;
    packetRemoveTeamDeathmatchTicket.haveBeenDisconnected = MultiplayerContext.haveBeenDisconnected;
    return packetRemoveTeamDeathmatchTicket;
  }

  public static PacketGameOver getPacketGameOver(final int teamId) {
    PacketGameOver packetGameOver = new PacketGameOver();
    packetGameOver.entityId = MultiplayerContext.playerName;
    packetGameOver.roomId = MultiplayerContext.roomId;
    packetGameOver.loserTeamId = teamId;
    return packetGameOver;
  }
}
