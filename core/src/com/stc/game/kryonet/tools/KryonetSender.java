package com.stc.game.kryonet.tools;

import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.multiplayer.tools.MultiplayerSender;

public class KryonetSender {
  public static void registerCurrentRoom() {
    MultiplayerSender.sendPacket(KryonetPacketBuilder.getPacketOnConnectedToRoom());
  }

  public static void registerCurrentPlayer() {
    Player currentPlayer = Player.getCurrent();
    if (currentPlayer != null)
      MultiplayerSender.sendPacket(KryonetPacketBuilder.getPacketPlayer(currentPlayer));
  }

  public static void getHostName() {
    MultiplayerSender.sendPacket(KryonetPacketBuilder.getPacketGetHost());
  }

  public static void sendReconnectionInformation() {
    MultiplayerSender.sendPacket(KryonetPacketBuilder.getPacketReconnection());
  }
}
