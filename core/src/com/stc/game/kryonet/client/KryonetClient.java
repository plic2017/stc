package com.stc.game.kryonet.client;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Listener;
import com.stc.game.kryonet.listener.NetworkListener;
import com.stc.game.kryonet.packets.PacketBonus;
import com.stc.game.kryonet.packets.Packet;
import com.stc.game.kryonet.packets.PacketAttackEffectEntityCollision;
import com.stc.game.kryonet.packets.PacketEnemyAttack;
import com.stc.game.kryonet.packets.PacketGetHost;
import com.stc.game.kryonet.packets.PacketGetWave;
import com.stc.game.kryonet.packets.PacketHostDisconnection;
import com.stc.game.kryonet.packets.PacketNumberOfParticipants;
import com.stc.game.kryonet.packets.PacketOnConnectedToRoom;
import com.stc.game.kryonet.packets.PacketPlayer;
import com.stc.game.kryonet.packets.PacketPlayerAttack;
import com.stc.game.kryonet.packets.PacketPlayerMessage;
import com.stc.game.kryonet.packets.PacketPlayerReady;
import com.stc.game.kryonet.packets.PacketReconnection;
import com.stc.game.kryonet.packets.PacketRemoveEnemy;
import com.stc.game.kryonet.packets.PacketRemovePlayer;
import com.stc.game.kryonet.packets.PacketRemoveTeamDeathmatchTicket;
import com.stc.game.kryonet.packets.PacketSendHost;
import com.stc.game.kryonet.packets.PacketSendWave;
import com.stc.game.kryonet.packets.PacketTeamAffectation;
import com.stc.game.kryonet.packets.PacketGameOver;
import com.stc.game.kryonet.packets.PacketWin;
import com.stc.game.kryonet.tools.KryonetUtils;
import com.stc.game.utils.random.FinalRandom;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class KryonetClient {
  private static final int TIMEOUT = 5000;
  private static final String HOST = getHost();
  private static final int TCP_PORT = 54555;
  private static final int UDP_PORT = 54777;

  public static boolean connected = false;
  public static Client client;

  public KryonetClient() {
    client = new Client();
    registerPackets();
    NetworkListener networkListener = new NetworkListener();
    client.addListener(new Listener.ThreadedListener(networkListener));
    client.start();

    try {
      client.connect(TIMEOUT, HOST, TCP_PORT, UDP_PORT);
    } catch (IOException e) {
      KryonetUtils.disposeKryonetClient(false);
    }
  }

  private void registerPackets() {
    Kryo kryo = client.getKryo();

    kryo.register(float[].class);
    kryo.register(ArrayList.class);

    kryo.register(Packet.class);
    kryo.register(PacketPlayer.class);
    kryo.register(PacketEnemyAttack.class);
    kryo.register(PacketGetHost.class);
    kryo.register(PacketSendHost.class);
    kryo.register(PacketOnConnectedToRoom.class);
    kryo.register(PacketRemovePlayer.class);
    kryo.register(PacketPlayerAttack.class);
    kryo.register(PacketRemoveEnemy.class);
    kryo.register(PacketAttackEffectEntityCollision.class);
    kryo.register(PacketGetWave.class);
    kryo.register(PacketSendWave.class);
    kryo.register(PacketNumberOfParticipants.class);
    kryo.register(PacketPlayerReady.class);
    kryo.register(PacketPlayerMessage.class);
    kryo.register(PacketReconnection.class);
    kryo.register(PacketTeamAffectation.class);
    kryo.register(PacketRemoveTeamDeathmatchTicket.class);
    kryo.register(PacketHostDisconnection.class);
    kryo.register(PacketBonus.class);
    kryo.register(PacketGameOver.class);
    kryo.register(PacketWin.class);
  }

  private static String getHost() {
    int i = FinalRandom.random.nextInt(51);
    int diff = 51 - i;
    Map<Integer, Integer> map = new LinkedHashMap<>();
    map.put(i + diff, i + diff + 75);
    map.put(203 + i + diff, 150 + i + diff);
    map.put(150 + i + diff, 203 + i + diff);
    map.put(i + diff + 75, i + diff);
    String host = "";
    i = 0;

    for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
      host += map.get(entry.getValue()) + (i++ < 3 ? "." : "");
    }

    return host;
  }
}
