package com.stc.game.kryonet.packets;

import java.util.ArrayList;

public class PacketPlayer extends Packet {
  public float xPosition;
  public float yPosition;
  public float zPosition;
  public float xDirection;
  public float yDirection;
  public float zDirection;
  public float wDirection;
  public boolean hasBeenDisconnected;
  public ArrayList<String> haveBeenDisconnected;
}
