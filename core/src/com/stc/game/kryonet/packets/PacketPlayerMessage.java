package com.stc.game.kryonet.packets;

import java.util.ArrayList;

public class PacketPlayerMessage extends Packet {
  public String recipient;
  public String message;
  public boolean hasBeenDisconnected;
  public ArrayList<String> haveBeenDisconnected;
}
