package com.stc.game.kryonet.packets;

import java.util.ArrayList;

public class PacketAttackEffectEntityCollision extends Packet {
  public int attackId;
  public float damageCoef;
  public String attackedEntityId;
  public boolean hasBeenDisconnected;
  public ArrayList<String> haveBeenDisconnected;
}
