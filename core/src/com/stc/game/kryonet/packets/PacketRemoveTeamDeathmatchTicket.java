package com.stc.game.kryonet.packets;

import java.util.ArrayList;

public class PacketRemoveTeamDeathmatchTicket extends Packet {
  public boolean hasBeenDisconnected;
  public ArrayList<String> haveBeenDisconnected;
}
