package com.stc.game.kryonet.packets;

import java.util.ArrayList;

public class PacketSendWave {
  public ArrayList<String> enemiesIds;
  public ArrayList<float[]> enemiesPositions;
  public boolean complete;
}
