package com.stc.game.kryonet.packets;

import java.util.ArrayList;

public class PacketPlayerAttack extends Packet {
  public int attackId;
  public int colorId;
  public String targetId;
  public boolean hasBeenDisconnected;
  public ArrayList<String> haveBeenDisconnected;
}
