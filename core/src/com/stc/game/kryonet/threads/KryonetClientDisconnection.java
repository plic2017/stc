package com.stc.game.kryonet.threads;

import com.stc.game.kryonet.client.KryonetClient;
import com.stc.game.kryonet.listener.NetworkListener;
import com.stc.game.kryonet.tools.KryonetUtils;

public class KryonetClientDisconnection implements Runnable {
    private final boolean relaunchKryonetClient;

    public KryonetClientDisconnection(final boolean relaunchKryonetClient) {
        this.relaunchKryonetClient = relaunchKryonetClient;
    }

    @Override
    public void run() {
        NetworkListener.setShouldReconnect(false);
        if (KryonetClient.client != null) {
            KryonetClient.client.close();
            KryonetClient.client = null;
        }
        if (relaunchKryonetClient) {
            KryonetUtils.launchKryonetClient();
        }
    }
}
