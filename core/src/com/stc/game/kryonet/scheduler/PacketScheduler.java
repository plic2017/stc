package com.stc.game.kryonet.scheduler;

import com.stc.game.kryonet.packets.Packet;
import com.stc.game.kryonet.packets.PacketAttackEffectEntityCollision;
import com.stc.game.kryonet.packets.PacketBonus;
import com.stc.game.kryonet.packets.PacketEnemyAttack;
import com.stc.game.kryonet.packets.PacketHostDisconnection;
import com.stc.game.kryonet.packets.PacketPlayer;
import com.stc.game.kryonet.packets.PacketPlayerAttack;
import com.stc.game.kryonet.packets.PacketPlayerMessage;
import com.stc.game.kryonet.packets.PacketReconnection;
import com.stc.game.kryonet.packets.PacketRemoveEnemy;
import com.stc.game.kryonet.packets.PacketRemovePlayer;
import com.stc.game.kryonet.packets.PacketRemoveTeamDeathmatchTicket;
import com.stc.game.kryonet.packets.PacketSendHost;
import com.stc.game.kryonet.packets.PacketSendWave;
import com.stc.game.kryonet.packets.PacketTeamAffectation;
import com.stc.game.kryonet.packets.PacketWin;
import com.stc.game.kryonet.tools.KryonetReceiver;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class PacketScheduler {
  private static final Queue<Object> packetsQueue = new ConcurrentLinkedQueue<>();

  private PacketScheduler() { }

  public static void addPacket(Object packet) {
    packetsQueue.add(packet);
  }

  public static void update() {
    Object object = packetsQueue.poll();
    while (object != null) {
      if (object instanceof PacketSendHost) {
        KryonetReceiver.handlePacketSendHost((PacketSendHost) object);
        return;
      }

      if (object instanceof PacketHostDisconnection) {
        KryonetReceiver.handlePacketHostDisconnection();
        return;
      }

      if (object instanceof PacketSendWave) {
        KryonetReceiver.handlePacketSendWave((PacketSendWave) object);
        return;
      }

      if (object instanceof PacketBonus) {
        KryonetReceiver.handlePacketBonus((PacketBonus) object);
        return;
      }

      if (object instanceof PacketWin) {
        KryonetReceiver.handlePacketWin();
        return;
      }

      if (object instanceof Packet) {
        if (object instanceof PacketPlayer) {
          KryonetReceiver.handlePacketPlayer((PacketPlayer) object);
          return;
        }
        if (object instanceof PacketPlayerAttack) {
          KryonetReceiver.handlePacketPlayerAttack((PacketPlayerAttack) object);
          return;
        }
        if (object instanceof PacketEnemyAttack) {
          KryonetReceiver.handlePacketEnemyAttack((PacketEnemyAttack) object);
          return;
        }
        if (object instanceof PacketAttackEffectEntityCollision) {
          KryonetReceiver.handlePacketCollisionEffectEntity((PacketAttackEffectEntityCollision) object);
          return;
        }
        if (object instanceof PacketRemovePlayer) {
          KryonetReceiver.handlePacketRemovePlayer((PacketRemovePlayer) object);
          return;
        }
        if (object instanceof PacketRemoveEnemy) {
          KryonetReceiver.handlePacketRemoveEnemy((PacketRemoveEnemy) object);
          return;
        }
        if (object instanceof PacketPlayerMessage) {
          KryonetReceiver.handlePacketPlayerMessage((PacketPlayerMessage) object);
          return;
        }
        if (object instanceof PacketTeamAffectation) {
          KryonetReceiver.handlePacketTeamAffectation((PacketTeamAffectation) object);
          return;
        }
        if (object instanceof PacketRemoveTeamDeathmatchTicket) {
          KryonetReceiver.handlePacketRemoveTeamDeathmatchTicket();
          return;
        }
        if (object instanceof PacketReconnection) {
          KryonetReceiver.handlePacketReconnection((PacketReconnection) object);
          return;
        }
      }

      object = packetsQueue.poll();
    }
  }
}
