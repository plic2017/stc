package com.stc.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.physics.bullet.Bullet;
import com.badlogic.gdx.utils.Timer;
import com.stc.game.gameplay.engine.sound.SoundEngine;
import com.stc.game.gameplay.enums.Musics;
import com.stc.game.services.admob.enums.AdMob;
import com.stc.game.ui.enums.ScreenState;
import com.stc.game.ui.managers.ScreenManager;
import com.stc.game.ui.managers.SkinManager;
import com.stc.game.ui.screens.AbstractScreen;
import com.stc.game.ui.screens.SplashScreen;

class Main extends Game {
  private static final long TIMEOUT_SPLASH_SCREEN = 1000L;

  Main() {}

  @Override
  public void create() {
    Gdx.input.setCatchBackKey(true);
    initMain();
  }

  @Override
  public void dispose() {
    super.dispose();
    AbstractScreen.disposeBackground();
    SkinManager.dispose();
    SoundEngine.dispose();
  }

  private void initMain() {
    setScreen(new SplashScreen());
    final long startTimeMillis = System.currentTimeMillis();
    new Thread(new Runnable() {
      @Override
      public void run() {
        Gdx.app.postRunnable(new Runnable() {
          @Override
          public void run() {
            init();
            SoundEngine.startMusic(Musics.MAIN_MUSIC);
            long elapsedTime = System.currentTimeMillis() - startTimeMillis;
            if (elapsedTime < TIMEOUT_SPLASH_SCREEN) {
              Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                  ScreenManager.getInstance().showScreen(ScreenState.HOME_SCREEN, AdMob.SHOW_AD.getValue());
                }
              }, (TIMEOUT_SPLASH_SCREEN - elapsedTime) / 1000f);
            } else {
              ScreenManager.getInstance().showScreen(ScreenState.HOME_SCREEN, AdMob.SHOW_AD.getValue());
            }
          }
        });
      }
    }).start();
  }

  private void init() {
    Bullet.init();
    AbstractScreen.initBackground();
    SkinManager.initSkinManager();
    SoundEngine.initSoundEngine();
    ScreenManager.getInstance().initialize(this);
  }
}
