package com.stc.game.utils.random;

import java.util.Random;

public final class FinalRandom {
  public static final Random random = new Random();

  private FinalRandom() {}

  public static float getValueBetween(float minBound, float maxBound) {
    if (minBound > maxBound)
      return -1;
    if (minBound == maxBound)
      return minBound;

    return minBound + random.nextFloat() * (maxBound - minBound);
  }
}
