package com.stc.game.utils.maths;

import com.badlogic.gdx.math.Vector3;

import java.util.Arrays;

public final class MathUtils {
  public static Vector3 getDirection(final Vector3 ownPosition, final Vector3 targetedPosition) {
    if (ownPosition == null || targetedPosition == null)
      return null;

    Vector3 result = new Vector3();

    float distX = targetedPosition.x - ownPosition.x;
    float distY = targetedPosition.y - ownPosition.y;
    float distZ = targetedPosition.z - ownPosition.z;

    result.set(distX, distY, distZ);
    result = result.nor();

    return result;
  }

  public static int uniqueIdToCollisionId(final String uniqueId) {
    return Arrays.hashCode(uniqueId.toCharArray());
  }

  public static double getPositiveDistBetween(final Vector3 source, final Vector3 dest) {
    if (source == null || dest == null)
      return -1;

    double distance = Math.sqrt(Math.pow((dest.x - source.x), 2) + Math.pow((dest.y - source.y), 2) + Math.pow((dest.z - source.z), 2));

    if (distance < 0)
      distance *= 1;

    return distance;
  }
}
