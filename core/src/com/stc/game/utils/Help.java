package com.stc.game.utils;

import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.enums.MultiplayerMode;
import com.stc.game.services.Services;
import com.stc.game.solo.SoloContext;
import com.stc.game.ui.tools.I18N;

public class Help {
  public static String getHelpMessage() {
    if (SoloContext.solo) {
      return I18N.getTranslation(I18N.Text.SURVIVAL_HELP);
    } else {
      if (MultiplayerContext.inRealtimeMultiplayer()) {
        if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.SURVIVAL) {
          return I18N.getTranslation(I18N.Text.SURVIVAL_HELP);
        } else if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.DEATHMATCH) {
          return I18N.getTranslation(I18N.Text.DEATHMATCH_HELP);
        } else {
          return I18N.getTranslation(I18N.Text.TEAM_DEATHMATCH_HELP);
        }
      } else {
        if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.SURVIVAL) {
          if (Services.getNearbyConnectionsService().isHost()) {
            return I18N.getTranslation(I18N.Text.NEARBY_CONNECTIONS_HOST_SURVIVAL_HELP);
          } else {
            return I18N.getTranslation(I18N.Text.SURVIVAL_HELP);
          }
        } else if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.DEATHMATCH) {
          if (Services.getNearbyConnectionsService().isHost()) {
            return I18N.getTranslation(I18N.Text.NEARBY_CONNECTIONS_HOST_DEATHMATCH_HELP);
          } else {
            return I18N.getTranslation(I18N.Text.DEATHMATCH_HELP);
          }
        } else {
          if (Services.getNearbyConnectionsService().isHost()) {
            return I18N.getTranslation(I18N.Text.NEARBY_CONNECTIONS_HOST_TEAM_DEATHMATCH_HELP);
          } else {
            return I18N.getTranslation(I18N.Text.TEAM_DEATHMATCH_HELP);
          }
        }
      }
    }
  }
}
