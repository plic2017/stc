attribute vec3 a_position;
attribute vec3 a_normal;
attribute vec2 a_texCoord0;

uniform mat4 u_worldTrans;

varying vec3 v_cubeMapUV;
varying vec2 v_texCoord0;

void main() {
    vec4 g_position = u_worldTrans * vec4(a_position, 1.0);
    v_cubeMapUV = normalize(g_position.xyz);
    v_texCoord0 = a_texCoord0;
    gl_Position = vec4(a_position, 1.0);
}

