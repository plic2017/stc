/*
Les variables d'entrée

gl_Color            vec4    Couleur du pixel
gl_FragCoord        vec2    Coordonnées écran du pixel
gl_SecondaryColor   bool    Couleur secondaire
gl_TexCoord[n]      vec4[]  Coordonnées de l'unité de texturage n
gl_FogFragCoord     float   Coordonnée de fog

Les variables de sortie

gl_FragColor    vec4   Couleur finale du pixel
gl_FragDepth    float  Profondeur du pixel dans le depth buffer
gl_FragData[n]  vec4[] En rapport avec glDrawBuffers()

*/

#ifdef GL_ES
precision mediump float;
#endif

uniform vec3 u_colorU;
uniform vec3 u_colorV;

varying vec2 v_texCoord0;

void main() {
  gl_FragColor = vec4(v_texCoord0.x * u_colorU + v_texCoord0.y * u_colorV, 0.5);
}
