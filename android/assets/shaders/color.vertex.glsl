/*
Les variables d'entrée

gl_Vertex           vec4    Position du sommet
gl_Color            vec4    Couleur du sommet
gl_Normal           vec3    Normale du sommet.
gl_MultiTexCoordn   vec4    Coordonnées de l'unité de texture n
gl_SecondaryColor   vec4    Couleur secondaire du sommet
gl_FogCoord         float   Coordonnées de brouillard

Les variables de sortie

gl_Position             vec4    Position en coordonnées écran du sommet
gl_FrontColor           vec4    Couleur du côté "avant" de la face à laquelle est rattaché le sommet
gl_BackColor            vec4    Couleur du côté "arrière" de la face à laquelle est rattaché le sommet
gl_FrontSecondaryColor  vec4    Couleur secondaire du côté "avant" de la face à laquelle est rattaché le sommet
gl_BackSecondaryColor   vec4    Couleur secondaire du côté "arrière" de la face à laquelle est rattaché le sommet
gl_TexCoord[n]          vec4[]  Coordonnées de l'unité de texture n
gl_FogFragCoord         float   Coordonnée de fog
gl_PointSize            float   Taille du point du sommet
gl_ClipVertex           vec4    Vecteur utilisé pour les plans de clipping

Et d'autres avec des matrices de partout

Attribute : Type de variable de sommet qui représente une donnée supplémentaire différente pour chaque sommet
Uniform : Type de variable qui revient à dire "je veux que la valeur de cette variable soit indiquée par mon application"
Varying : Type de variable que le Fragment shader et le Texture shader partagent si ils ont le même nom

Liste d'attributs présents dans LibGDX pour facilement attacher des shaders aux SpriteBatchs et autres :

a_position
a_normal
a_color
a_texCoord, avec un numéro à la fin, i.e. a_texCoord0, a_texCoord1, etc...
a_tangent
a_binormal

Remarques :

Un vertex shader doit toujours placer une valeur dans la variable de sortie gl_Position

*/

attribute vec4 a_color;
attribute vec3 a_position;
attribute vec2 a_texCoord0;

uniform mat4 u_worldTrans;
uniform mat4 u_projViewTrans;

varying vec4 v_color;
varying vec2 v_texCoord0;

void main() {
    v_color = a_color;
    v_texCoord0 = a_texCoord0;

    gl_Position = u_projViewTrans * u_worldTrans * vec4(a_position, 1.0); // avec 0.5 taille *2
}