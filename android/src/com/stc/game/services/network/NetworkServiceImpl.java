package com.stc.game.services.network;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;

public class NetworkServiceImpl implements NetworkService {
    private Activity activity;

    public NetworkServiceImpl(Activity activity) {
        this.activity = activity;
    }

    @Override
    public boolean isConnectedToWiFi() {
        int[] networkTypes = { ConnectivityManager.TYPE_WIFI };
        return isConnected(networkTypes);
    }

    @Override
    public boolean isConnectedToWiFiOrEthernet() {
        int[] networkTypes;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            networkTypes = new int[2];
            networkTypes[0] = ConnectivityManager.TYPE_WIFI;
            networkTypes[1] = ConnectivityManager.TYPE_ETHERNET;
        } else {
            networkTypes = new int[1];
            networkTypes[0] = ConnectivityManager.TYPE_WIFI;
        }
        return isConnected(networkTypes);
    }

    @Override
    public boolean isConnectedToWifiOrMobile() {
        int[] networkTypes = { ConnectivityManager.TYPE_WIFI, ConnectivityManager.TYPE_MOBILE };
        return isConnected(networkTypes);
    }

    public void onStop() {
        activity = null;
    }

    private boolean isConnected(int[] networkTypes) {
        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
            if (activeNetwork != null && activeNetwork.isConnectedOrConnecting()) {
                for (int networkType : networkTypes) {
                    if (activeNetwork.getType() == networkType) {
                        return true;
                    }
                }
            }
            return false;
        }
        return false;
    }
}
