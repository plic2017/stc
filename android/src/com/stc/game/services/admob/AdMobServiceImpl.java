package com.stc.game.services.admob;

import android.app.Activity;
import android.view.View;

import com.badlogic.gdx.Gdx;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import lombok.Getter;

public class AdMobServiceImpl implements AdMobService {
    private static final String TAG = "AdMobServiceImpl";
    private static final String MAIN_BANNER_AD_UNIT_ID = "ca-app-pub-2412006968237932/9827340805";

    private final Activity activity;

    @Getter
    private AdView mainBannerAd;

    public AdMobServiceImpl(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void showMainBannerAd() {
        try {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mainBannerAd.setVisibility(View.VISIBLE);
                    AdRequest.Builder builder = new AdRequest.Builder();
                    AdRequest ad = builder.build();
                    mainBannerAd.loadAd(ad);
                }
            });
        } catch (Exception e) {
            Gdx.app.error(TAG, "Failed to show the main banner ad. " + e.getMessage());
        }
    }

    @Override
    public void hideMainBannerAd() {
        try {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mainBannerAd.setVisibility(View.GONE);
                }
            });
        } catch (Exception e) {
            Gdx.app.error(TAG, "Failed to hide the main banner ad. " + e.getMessage());
        }
    }

    public void initAds() {
        mainBannerAd = new AdView(activity);
        mainBannerAd.setVisibility(View.VISIBLE);
        mainBannerAd.setAdUnitId(MAIN_BANNER_AD_UNIT_ID);
        mainBannerAd.setAdSize(AdSize.SMART_BANNER);
        mainBannerAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                int visibility = mainBannerAd.getVisibility();
                mainBannerAd.setVisibility(View.GONE);
                mainBannerAd.setVisibility(visibility);
            }
        });
    }
}
