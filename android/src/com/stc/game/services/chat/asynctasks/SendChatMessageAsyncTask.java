package com.stc.game.services.chat.asynctasks;

import android.os.AsyncTask;
import com.stc.game.kryonet.tools.KryonetPacketBuilder;
import com.stc.game.multiplayer.tools.MultiplayerSender;
import com.stc.game.utils.SwearWords;

public class SendChatMessageAsyncTask extends AsyncTask<Void, Void, Void> {
  private final String recipient;
  private final String message;

  public SendChatMessageAsyncTask(final String recipient, final String message) {
    this.recipient = recipient;
    this.message = message;
  }

  @Override
  protected Void doInBackground(Void... voids) {
    if (SwearWords.getSwearWords().isEmpty())
      SwearWords.initSwearWords();
    MultiplayerSender.sendPacket(KryonetPacketBuilder.getPacketPlayerMessage(recipient, SwearWords.cleanMessage(message)));
    return null;
  }
}
