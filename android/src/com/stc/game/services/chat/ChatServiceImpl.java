package com.stc.game.services.chat;

import com.stc.game.services.chat.asynctasks.SendChatMessageAsyncTask;

public class ChatServiceImpl implements ChatService {
    @Override
    public void sendChatMessage(final String recipient, final String message) {
        SendChatMessageAsyncTask task = new SendChatMessageAsyncTask(recipient, message);
        task.execute();
    }
}
