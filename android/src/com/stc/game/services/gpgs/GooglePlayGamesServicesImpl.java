package com.stc.game.services.gpgs;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.WindowManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.StringBuilder;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesActivityResultCodes;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.Multiplayer;
import com.google.android.gms.games.multiplayer.OnInvitationReceivedListener;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessageReceivedListener;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.RoomStatusUpdateListener;
import com.google.android.gms.games.multiplayer.realtime.RoomUpdateListener;
import com.google.android.gms.games.request.GameRequest;
import com.google.android.gms.games.request.OnRequestReceivedListener;
import com.google.android.gms.games.request.Requests;
import com.google.android.gms.games.snapshot.SnapshotMetadata;
import com.google.android.gms.games.snapshot.Snapshots;
import com.google.example.games.basegameutils.BaseGameUtils;
import com.google.example.games.basegameutils.GameHelper;
import com.stc.game.R;
import com.stc.game.gameplay.entities.ui.buttons.requests.RequestsInboxButton;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.kryonet.packets.PacketRemovePlayer;
import com.stc.game.kryonet.scheduler.PacketScheduler;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.enums.MultiplayerMode;
import com.stc.game.multiplayer.tools.GoogleMultiplayerUtils;
import com.stc.game.multiplayer.tools.MultiplayerUtils;
import com.stc.game.services.gpgs.asynctasks.CreateOrSaveSnapshotAsyncTask;
import com.stc.game.services.gpgs.asynctasks.LoadSnapshotAsyncTask;
import com.stc.game.services.gpgs.gamegifts.GameGift;
import com.stc.game.services.gpgs.gamegifts.RequestType;
import com.stc.game.solo.SoloContext;
import com.stc.game.ui.dialogs.Dialogs;
import com.stc.game.ui.tools.I18N;
import com.stc.game.utils.SwearWords;
import de.tomgrill.gdxdialogs.core.dialogs.GDXProgressDialog;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

public class GooglePlayGamesServicesImpl
  extends GameHelper
  implements GoogleApiClient.ConnectionCallbacks,
             GoogleApiClient.OnConnectionFailedListener,
             RealTimeMessageReceivedListener,
             RoomStatusUpdateListener,
             RoomUpdateListener,
             OnInvitationReceivedListener,
             OnRequestReceivedListener {
  private static final String TAG = "GooglePlayGamesServicesImpl";
  private static final char ROOM_IDENTIFIER = 'r';
  private static final int RC_SELECT_PLAYERS = 10000;
  private static final int RC_INVITATION_INBOX = 10001;
  private static final int RC_WAITING_ROOM = 10002;
  private static final int RC_SIGN_IN = 9001;
  private static final int RC_LIST_SAVED_GAMES = 9002;
  private static final int RC_REQUESTS_INBOX = 0;
  private static final int RC_WISH_REQUEST = 1;
  private static final int RC_GIFT_REQUEST = 2;
  private static final int DEFAULT_LIFETIME = 1;
  private static final int BYTE = 1;

  private String myParticipantId = null;
  private String myIncomingInvitationId = null;
  private String roomId = null;
  private List<Participant> participants = null;

  private GDXProgressDialog savedGamesProgressDialog = null;

  private Activity activity = null;

  public GooglePlayGamesServicesImpl(Activity activity) {
    super(activity, GameHelper.CLIENT_ALL);
    this.activity = activity;
  }

  @Override
  public void onConnected(Bundle bundle) {
    Games.Invitations.registerInvitationListener(getApiClient(), this);
    Games.Requests.registerRequestListener(getApiClient(), this);

    if (bundle != null) {
      Invitation invitation = bundle.getParcelable(Multiplayer.EXTRA_INVITATION);
      if (invitation != null && invitation.getInvitationId() != null) {
        acceptInviteToRoom(invitation.getInvitationId());
      } else {
        handleRequests(Games.Requests.getGameRequestsFromBundle(bundle));
      }
    }
  }

  @Override
  public void onConnectionSuspended(int cause) {
    getApiClient().connect();
  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    BaseGameUtils.resolveConnectionFailure(activity, getApiClient(), connectionResult, RC_SIGN_IN, activity.getString(R.string.signin_other_error));
  }

  @Override
  public void onInvitationReceived(Invitation invitation) {
    myIncomingInvitationId = invitation.getInvitationId();
  }

  @Override
  public void onInvitationRemoved(String invitationId) {
    if (myIncomingInvitationId != null && myIncomingInvitationId.equals(invitationId)) {
      myIncomingInvitationId = null;
    }
  }

  @Override
  public void onRealTimeMessageReceived(RealTimeMessage realTimeMessage) {
    byte[] data = realTimeMessage.getMessageData();
    GoogleMultiplayerUtils.processData(data);
  }

  @Override
  public void onRoomConnecting(Room room) {}

  @Override
  public void onRoomAutoMatching(Room room) {}

  @Override
  public void onPeerInvitedToRoom(Room room, List<String> participantIds) {}

  @Override
  public void onPeerDeclined(Room room, List<String> participantIds) {}

  @Override
  public void onPeerJoined(Room room, List<String> participantIds) {}

  @Override
  public void onPeerLeft(Room room, List<String> participantIds) {
    removePlayersFromPlayerPool(room, participantIds);
    updateParticipantsList(participantIds);
  }

  @Override
  public void onConnectedToRoom(Room room) {
    myParticipantId = room.getParticipantId(Games.Players.getCurrentPlayerId(getApiClient()));
    if (roomId == null) {
      roomId = room.getRoomId();
    }
    participants = room.getParticipants();
    MultiplayerContext.playerName = room.getParticipant(myParticipantId).getDisplayName();
    MultiplayerContext.roomId = getCustomRoomId();
    MultiplayerContext.numberOfParticipants = participants.size();
  }

  @Override
  public void onDisconnectedFromRoom(Room room) {
    List<String> participantIds = new ArrayList<>();
    for (Participant participant : room.getParticipants()) {
      if (!participant.getParticipantId().equals(myParticipantId) && !participant.isConnectedToRoom()) {
        participantIds.add(participant.getParticipantId());
      }
    }
    removePlayersFromPlayerPool(room, participantIds);
    updateParticipantsList(participantIds);
  }

  @Override
  public void onPeersConnected(Room room, List<String> participantIds) {}

  @Override
  public void onPeersDisconnected(Room room, List<String> participantIds) {
    removePlayersFromPlayerPool(room, participantIds);
    updateParticipantsList(participantIds);
  }

  @Override
  public void onP2PConnected(String participantId) {}

  @Override
  public void onP2PDisconnected(String participantId) {}

  @Override
  public void onRoomCreated(int statusCode, Room room) {
    if (statusCode != GamesStatusCodes.STATUS_OK) {
      stopKeepingScreenOn();
      showGameError();
      return;
    }
    roomId = room.getRoomId();
    showWaitingRoom(room);
  }

  @Override
  public void onJoinedRoom(int statusCode, Room room) {
    if (statusCode != GamesStatusCodes.STATUS_OK) {
      stopKeepingScreenOn();
      showGameError();
      return;
    }
    showWaitingRoom(room);
  }

  @Override
  public void onLeftRoom(int statusCode, String roomId) {}

  @Override
  public void onRoomConnected(int statusCode, Room room) {
    if (statusCode != GamesStatusCodes.STATUS_OK) {
      stopKeepingScreenOn();
      showGameError();
    }
  }

  @Override
  public void onRequestReceived(GameRequest request) {
    switch (request.getType()) {
      case GameRequest.TYPE_GIFT :
      case GameRequest.TYPE_WISH :
        RequestsInboxButton.enable();
        break;
      default :
        break;
    }
  }

  @Override
  public void onRequestRemoved(String requestId) {}

  @Override
  public void onStop() {
    leaveRoom();
    if (participants != null) {
      participants.clear();
      participants = null;
    }
    savedGamesProgressDialog = null;
    activity = null;
    super.onStop();
  }

  private void handleSelectPlayersResult(final int response, final Intent intent) {
    if (response != Activity.RESULT_OK) {
      return;
    }
    ArrayList<String> invitees = intent.getStringArrayListExtra(Games.EXTRA_PLAYER_IDS);
    Bundle autoMatchCriteria = null;
    int minAutoMatchPlayers = intent.getIntExtra(Multiplayer.EXTRA_MIN_AUTOMATCH_PLAYERS, 0);
    int maxAutoMatchPlayers = intent.getIntExtra(Multiplayer.EXTRA_MAX_AUTOMATCH_PLAYERS, 0);
    if (minAutoMatchPlayers > 0 || maxAutoMatchPlayers > 0) {
      autoMatchCriteria = RoomConfig.createAutoMatchCriteria(minAutoMatchPlayers, maxAutoMatchPlayers, 0);
    }
    RoomConfig.Builder roomConfigBuilder = RoomConfig.builder(this);
    roomConfigBuilder.addPlayersToInvite(invitees);
    roomConfigBuilder.setMessageReceivedListener(this);
    roomConfigBuilder.setRoomStatusUpdateListener(this);
    roomConfigBuilder.setVariant(MultiplayerMode.getMultiplayerModeVariantFromMultiplayerContext());
    if (autoMatchCriteria != null) {
      roomConfigBuilder.setAutoMatchCriteria(autoMatchCriteria);
    }
    Games.RealTimeMultiplayer.create(getApiClient(), roomConfigBuilder.build());
    keepScreenOn();
  }

  private void handleInvitationInboxResult(final int response, final Intent intent) {
    if (response != Activity.RESULT_OK) {
      return;
    }
    Invitation invitation = intent.getExtras().getParcelable(Multiplayer.EXTRA_INVITATION);
    if (invitation != null) {
      MultiplayerMode.setMultiplayerModeFromVariant(invitation.getVariant());
      acceptInviteToRoom(invitation.getInvitationId());
    }
  }

  private void acceptInviteToRoom(final String invitationId) {
    RoomConfig.Builder roomConfigBuilder = RoomConfig.builder(this);
    roomConfigBuilder.setInvitationIdToAccept(invitationId)
            .setMessageReceivedListener(this)
            .setRoomStatusUpdateListener(this);
    Games.RealTimeMultiplayer.join(getApiClient(), roomConfigBuilder.build());
    keepScreenOn();
  }

  private void handleWaitingRoomResult(final int resultCode) {
    if (resultCode == Activity.RESULT_OK) {
      MultiplayerContext.multiplayer = true;
    } else if (resultCode == GamesActivityResultCodes.RESULT_LEFT_ROOM || resultCode == Activity.RESULT_CANCELED) {
      leaveRoom();
    }
  }

  private void handleListSavedGamesResult(final Intent intent) {
    if (intent != null) {
      if (intent.hasExtra(Snapshots.EXTRA_SNAPSHOT_METADATA)) {
        SnapshotMetadata snapshotMetadata = intent.getParcelableExtra(Snapshots.EXTRA_SNAPSHOT_METADATA);
        SoloContext.snapshotTitleToOverwrite = snapshotMetadata.getUniqueName();
        loadFromSnapshot(snapshotMetadata);
      }
    }
  }

  private void loadFromSnapshot(final SnapshotMetadata snapshotMetadata) {
    if (savedGamesProgressDialog == null) {
      savedGamesProgressDialog = Dialogs.getProgressDialog(I18N.getTranslation(I18N.Text.SAVED_GAME), I18N.getTranslation(I18N.Text.LOADING_SAVED_GAMES));
    }
    savedGamesProgressDialog.build().show();
    LoadSnapshotAsyncTask task = new LoadSnapshotAsyncTask(getApiClient(), savedGamesProgressDialog, snapshotMetadata);
    task.execute();
  }

  private void handleGiftsInboxResult(final int resultCode, final Intent intent) {
    if (resultCode == Activity.RESULT_OK && intent != null) {
      handleRequests(Games.Requests.getGameRequestsFromInboxResponse(intent));
    }
  }

  private void handleRequests(final ArrayList<GameRequest> requests) {
    if (requests == null) {
      return;
    }

    for (GameRequest gameRequest : requests) {
      ByteBuffer bf = ByteBuffer.wrap(gameRequest.getData());
      byte gameGiftId = bf.get();
      String description = GameGift.getGiftsDescriptions().get((int) gameGiftId);
      Dialogs.showYesNoButtonDialog(I18N.getTranslation(I18N.Text.DO_YOU_WANT_TO_ACCEPT_THE_REQUEST), description, new Callable<Void>() {
        @Override
        public Void call() throws Exception {
          acceptRequests(requests);
          return null;
        }
      });
    }
  }

  private void acceptRequests(ArrayList<GameRequest> requests) {
    List<String> requestIds = new ArrayList<>();
    final Map<String, GameRequest> gameRequestMap = new HashMap<>();
    for (GameRequest request : requests) {
      String requestId = request.getRequestId();
      requestIds.add(requestId);
      gameRequestMap.put(requestId, request);
    }

    Games.Requests.acceptRequests(getApiClient(), requestIds).setResultCallback(
            new ResultCallback<Requests.UpdateRequestsResult>() {
              @Override
              public void onResult(@NonNull Requests.UpdateRequestsResult result) {
                for (String requestId : result.getRequestIds()) {
                  if (!gameRequestMap.containsKey(requestId) || result.getRequestOutcome(requestId) != Requests.REQUEST_UPDATE_OUTCOME_SUCCESS) {
                    continue;
                  }

                  switch (gameRequestMap.get(requestId).getType()) {
                    case GameRequest.TYPE_GIFT :
                      GameGift.acceptGameGift(gameRequestMap.get(requestId).getData());
                      break;
                    case GameRequest.TYPE_WISH :
                    default :
                      break;
                  }
                }
              }
            });
  }

  private void handleSignInResult(final int requestCode, final int resultCode) {
    if (resultCode == Activity.RESULT_OK) {
      getApiClient().connect();
    } else {
      BaseGameUtils.showActivityResultError(activity, requestCode, resultCode, R.string.signin_other_error);
    }
  }

  private void showGameError() {
    BaseGameUtils.makeSimpleDialog(activity, activity.getString(R.string.game_problem));
  }

  private void showWaitingRoom(final Room room) {
    Intent intent = Games.RealTimeMultiplayer.getWaitingRoomIntent(getApiClient(), room, Integer.MAX_VALUE);
    activity.startActivityForResult(intent, RC_WAITING_ROOM);
  }

  private void removePlayersFromPlayerPool(final Room room, final List<String> participantIds) {
    for (String participantId : participantIds) {
      String playerName = room.getParticipant(participantId).getDisplayName();
      PacketRemovePlayer packetRemovePlayer = new PacketRemovePlayer();
      packetRemovePlayer.entityId = playerName;
      PacketScheduler.addPacket(packetRemovePlayer);
    }
  }

  private void updateParticipantsList(final List<String> participantIds) {
    if (participants == null)
      return;

    for (String participantId : participantIds) {
      Iterator<Participant> it = participants.iterator();
      while (it.hasNext()) {
        Participant participant = it.next();
        if (participant.getParticipantId().equals(participantId)) {
          it.remove();
          break;
        }
      }
    }
  }

  private void keepScreenOn() {
    activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
  }

  private void stopKeepingScreenOn() {
    try {
      activity.runOnUiThread(new Runnable() {
        public void run() {
          activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
      });
    } catch (Exception e) {
      Gdx.app.error(TAG, "Failed to clear the flag FLAG_KEEP_SCREEN_ON. " + e.getMessage());
    }
  }

  private String getCustomRoomId() {
    if (participants != null) {
      int numberOfParticipants = participants.size();
      String[] participantsIds = new String[numberOfParticipants];
      for (int i = 0; i < numberOfParticipants; i++) {
        participantsIds[i] = participants.get(i).getParticipantId();
      }
      Arrays.sort(participantsIds);
      StringBuilder builder = new StringBuilder(participantsIds[0]);
      builder.setCharAt(0, ROOM_IDENTIFIER);
      return builder.toString();
    }
    return null;
  }

  private void sendData(final byte[] data) {
    for (Participant p : participants) {
      if (!p.getParticipantId().equals(myParticipantId) && p.getStatus() == Participant.STATUS_JOINED) {
        Games.RealTimeMultiplayer.sendUnreliableMessage(getApiClient(), data, roomId, p.getParticipantId());
      }
    }
  }

  private void sendDataTo(final String recipient, final byte[] data) {
    for (Participant p : participants) {
      if (!p.getParticipantId().equals(myParticipantId) && p.getStatus() == Participant.STATUS_JOINED) {
        if (p.getDisplayName().equals(recipient)) {
          Games.RealTimeMultiplayer.sendUnreliableMessage(getApiClient(), data, roomId, p.getParticipantId());
          return;
        }
      }
    }
  }

  public void leaveRoom() {
    if (roomId != null) {
      Games.RealTimeMultiplayer.leave(getApiClient(), this, roomId);
      roomId = null;
    }
    stopKeepingScreenOn();
  }

  public void onActivityResult(final int requestCode, final int resultCode, final Intent intent) {
    switch (requestCode) {
      case RC_SELECT_PLAYERS :
        handleSelectPlayersResult(resultCode, intent);
        break;
      case RC_INVITATION_INBOX :
        handleInvitationInboxResult(resultCode, intent);
        break;
      case RC_WAITING_ROOM :
        handleWaitingRoomResult(resultCode);
        break;
      case RC_LIST_SAVED_GAMES :
        handleListSavedGamesResult(intent);
        break;
      case RC_REQUESTS_INBOX:
        handleGiftsInboxResult(resultCode, intent);
        break;
      case RC_WISH_REQUEST :
      case RC_GIFT_REQUEST :
        break;
      case RC_SIGN_IN :
        handleSignInResult(requestCode, resultCode);
        break;
      default :
        super.onActivityResult(requestCode, resultCode, intent);
    }
  }

  public void startQuickGame(final int minAutoMatchPlayers, final int maxAutoMathPlayers, final int variant) {
    Bundle autoMatchCriteria = RoomConfig.createAutoMatchCriteria(minAutoMatchPlayers, maxAutoMathPlayers, 0);
    RoomConfig.Builder roomConfigBuilder = RoomConfig.builder(this);
    roomConfigBuilder.setMessageReceivedListener(this);
    roomConfigBuilder.setRoomStatusUpdateListener(this);
    roomConfigBuilder.setAutoMatchCriteria(autoMatchCriteria);
    roomConfigBuilder.setVariant(variant);
    RoomConfig roomConfig = roomConfigBuilder.build();
    Games.RealTimeMultiplayer.create(getApiClient(), roomConfig);
    keepScreenOn();
  }

  public void invitePlayers(final int minAutoMatchPlayers, final int maxAutoMathPlayers){
    Intent intent = Games.RealTimeMultiplayer.getSelectOpponentsIntent(getApiClient(), minAutoMatchPlayers, maxAutoMathPlayers);
    activity.startActivityForResult(intent, RC_SELECT_PLAYERS);
  }

  public void showInvitations() {
    Intent intent = Games.Invitations.getInvitationInboxIntent(getApiClient());
    activity.startActivityForResult(intent, RC_INVITATION_INBOX);
  }

  public void sendPlayer(final Player player) {
    try {
      byte[] data = GoogleMultiplayerUtils.getPacketPlayer(player);
      sendData(data);
    } catch (Exception e) {
      Gdx.app.error(TAG, "Failed to send a position. " + e.getMessage());
    }
  }

  public void sendPlayerAttack(final int attackId, final String targetId, final int colorId) {
    try {
      byte[] data = GoogleMultiplayerUtils.getPacketPlayerAttack(attackId, targetId, colorId);
      sendData(data);
    } catch (Exception e) {
      Gdx.app.error(TAG, "Failed to send a player attack. " + e.getMessage());
    }
  }

  public void sendAttackEffectEntityCollision(final int attackId, final float damageCoef, final String attackedEntityId) {
    try {
      byte[] data = GoogleMultiplayerUtils.getPacketAttackEffectEntityCollision(attackId, damageCoef, attackedEntityId);
      sendData(data);
    } catch (Exception e) {
      Gdx.app.error(TAG, "Failed to send an effect/entity collision. " + e.getMessage());
    }
  }

  public void sendRemoveTeamDeathmatchTicket() {
    try {
      byte[] data = GoogleMultiplayerUtils.getPacketPlayerName();
      for (Participant p : participants) {
        if (!p.getParticipantId().equals(myParticipantId) && p.getStatus() == Participant.STATUS_JOINED) {
          if (MultiplayerUtils.bothPlayersInSameTeam(MultiplayerContext.playerName, p.getDisplayName())) {
            Games.RealTimeMultiplayer.sendUnreliableMessage(getApiClient(), data, roomId, p.getParticipantId());
          }
        }
      }
    } catch (Exception e) {
      Gdx.app.error(TAG, "Failed to send a remove team deathmatch ticket instruction. " + e.getMessage());
    }
  }

  public void sendChatMessage(final String recipient, final String message) {
    try {
      if (SwearWords.getSwearWords().isEmpty())
        SwearWords.initSwearWords();
      byte[] data = GoogleMultiplayerUtils.getPacketPlayerMessage(SwearWords.cleanMessage(message));
      if (recipient == null) {
        for (Participant p : participants) {
          if (!p.getParticipantId().equals(myParticipantId) && p.getStatus() == Participant.STATUS_JOINED) {
            if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.TEAM_DEATHMATCH) {
              if (MultiplayerUtils.bothPlayersInSameTeam(MultiplayerContext.playerName, p.getDisplayName())) {
                Games.RealTimeMultiplayer.sendUnreliableMessage(getApiClient(), data, roomId, p.getParticipantId());
              }
            } else {
              Games.RealTimeMultiplayer.sendUnreliableMessage(getApiClient(), data, roomId, p.getParticipantId());
            }
          }
        }
      } else {
        sendDataTo(recipient, data);
      }
    } catch (Exception e) {
      Gdx.app.error(TAG, "Failed to send a chat message. " + e.getMessage());
    }
  }

  public void showSnapshots(final String snapshotsListTitle, final boolean allowAddButton, final boolean allowDeleteButton, final int maxSnapshots) {
    Intent intent = Games.Snapshots.getSelectSnapshotIntent(getApiClient(), snapshotsListTitle, allowAddButton, allowDeleteButton, maxSnapshots);
    activity.startActivityForResult(intent, RC_LIST_SAVED_GAMES);
  }

  public void createOrSaveSnapshot(final String snapshotTitleToOverwrite, final byte[] data, final byte[] pixels) {
    CreateOrSaveSnapshotAsyncTask task = new CreateOrSaveSnapshotAsyncTask(getApiClient(), snapshotTitleToOverwrite, data, pixels);
    task.execute();
  }

  public void sendGameGiftsRequest(final int requestTypeId, final byte gameGiftId) {
    int request = requestTypeId == RequestType.TYPE_GIFT.getRequestTypeId() ? GameRequest.TYPE_GIFT : GameRequest.TYPE_WISH;
    byte[] data = ByteBuffer.allocate(BYTE).put(gameGiftId).array();
    Bitmap icon = BitmapFactory.decodeResource(activity.getResources(), R.drawable.health_bonus_request);
    String description = GameGift.getGiftsDescriptions().get((int) gameGiftId);
    Intent intent = Games.Requests.getSendIntent(getApiClient(), request, data, DEFAULT_LIFETIME, icon, description);
    activity.startActivityForResult(intent, RC_WISH_REQUEST);
  }

  public void showRequestsInbox() {
    activity.startActivityForResult(Games.Requests.getInboxIntent(getApiClient()), RC_REQUESTS_INBOX);
  }
}
