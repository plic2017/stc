package com.stc.game.services.gpgs.asynctasks;

import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.SnapshotMetadataChange;
import com.google.android.gms.games.snapshot.Snapshots;
import com.stc.game.gameplay.engine.wave.WaveEngine;
import com.stc.game.ui.screens.PauseScreen;
import com.stc.game.ui.tools.I18N;

public class CreateOrSaveSnapshotAsyncTask extends AsyncTask<Void, Void, Void> {
  private final GoogleApiClient googleApiClient;
  private final String snapshotTitle;
  private final byte[] data;
  private final byte[] pixels;

  public CreateOrSaveSnapshotAsyncTask(final GoogleApiClient googleApiClient, final String snapshotTitle, final byte[] data, final byte[] pixels) {
    this.googleApiClient = googleApiClient;
    this.snapshotTitle = snapshotTitle;
    this.data = data;
    this.pixels = pixels;
  }

  @Override
  protected Void doInBackground(Void... voids) {
    Snapshots.OpenSnapshotResult result = Games.Snapshots.open(googleApiClient, snapshotTitle, true).await();
    if (result.getStatus().isSuccess()) {
      Snapshot snapshot = result.getSnapshot();
      snapshot.getSnapshotContents().writeBytes(data);
      SnapshotMetadataChange metadataChange = new SnapshotMetadataChange.Builder()
        .setCoverImage(BitmapFactory.decodeByteArray(pixels, 0, pixels.length))
        .setDescription(I18N.getTranslation(I18N.Text.LAST_WAVE_LEVEL_REACHED) + WaveEngine.getWaveLevel())
        .build();
      Games.Snapshots.commitAndClose(googleApiClient, snapshot, metadataChange);
    }
    return null;
  }

  @Override
  protected void onPostExecute(Void aVoid) {
    PauseScreen.getSaveProgressDialog().dismiss();
  }
}
