package com.stc.game.services.gpgs.asynctasks;

import android.os.AsyncTask;

import com.badlogic.gdx.Gdx;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.SnapshotMetadata;
import com.google.android.gms.games.snapshot.Snapshots;
import com.stc.game.services.gpgs.savedgames.SavedGames;
import com.stc.game.solo.SoloContext;

import java.io.IOException;

import de.tomgrill.gdxdialogs.core.dialogs.GDXProgressDialog;

public class LoadSnapshotAsyncTask extends AsyncTask<Void, Void, Integer> {
  private static final String TAG = "LoadSnapshotAsyncTask";
  private static final int MAX_SNAPSHOT_RESOLVE_RETRIES = 3;

  private final GoogleApiClient googleApiClient;
  private GDXProgressDialog savedGamesProgressDialog;
  private final SnapshotMetadata snapshotMetadata;

  private SavedGames savedGames;

  public LoadSnapshotAsyncTask(final GoogleApiClient googleApiClient, GDXProgressDialog savedGamesProgressDialog, final SnapshotMetadata snapshotMetadata) {
    this.googleApiClient = googleApiClient;
    this.savedGamesProgressDialog = savedGamesProgressDialog;
    this.snapshotMetadata = snapshotMetadata;
  }

  @Override
  protected Integer doInBackground(Void... voids) {
    Snapshots.OpenSnapshotResult result = Games.Snapshots.open(googleApiClient, snapshotMetadata).await();

    int status = result.getStatus().getStatusCode();

    Snapshot snapshot = null;
    if (status == GamesStatusCodes.STATUS_OK) {
      snapshot = result.getSnapshot();
    } else if (status == GamesStatusCodes.STATUS_SNAPSHOT_CONFLICT) {
      snapshot = processSnapshotOpenResult(result, 0);
      if (snapshot != null) {
        status = GamesStatusCodes.STATUS_OK;
      } else {
        Gdx.app.error(TAG, "Conflict was not resolved automatically");
      }
    } else {
      Gdx.app.error(TAG, "Error while loading : " + status);
    }

    if (snapshot != null) {
      try {
        savedGames = new SavedGames(snapshot.getSnapshotContents().readFully());
      } catch (IOException e) {
        Gdx.app.error(TAG, "Error while reading snapshot content. " + e.getMessage());
      }
    }

    return status;
  }

  @Override
  protected void onPostExecute(Integer status) {
    savedGamesProgressDialog.dismiss();
    savedGamesProgressDialog = null;
    savedGames.loadData();
    SoloContext.loadedSavedGames = true;
  }

  private Snapshot processSnapshotOpenResult(Snapshots.OpenSnapshotResult result, int retryCount) {
    Snapshot resolvedSnapshot;
    retryCount++;

    int status = result.getStatus().getStatusCode();

    if (status == GamesStatusCodes.STATUS_OK) {
      return result.getSnapshot();
    } else if (status == GamesStatusCodes.STATUS_SNAPSHOT_CONTENTS_UNAVAILABLE) {
      return result.getSnapshot();
    } else if (status == GamesStatusCodes.STATUS_SNAPSHOT_CONFLICT) {
      Snapshot snapshot = result.getSnapshot();
      Snapshot conflictSnapshot = result.getConflictingSnapshot();
      resolvedSnapshot = snapshot;
      if (snapshot.getMetadata().getLastModifiedTimestamp() < conflictSnapshot.getMetadata().getLastModifiedTimestamp()) {
        resolvedSnapshot = conflictSnapshot;
      }
      Snapshots.OpenSnapshotResult resolveResult = Games.Snapshots.resolveConflict(googleApiClient, result.getConflictId(), resolvedSnapshot).await();
      if (retryCount < MAX_SNAPSHOT_RESOLVE_RETRIES) {
        return processSnapshotOpenResult(resolveResult, retryCount);
      }
    }

    return null;
  }
}
