package com.stc.game.services.nearbyconnections;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.WindowManager;
import com.badlogic.gdx.Gdx;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.connection.AppIdentifier;
import com.google.android.gms.nearby.connection.AppMetadata;
import com.google.android.gms.nearby.connection.Connections;
import com.stc.game.R;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.enums.GoogleMultiplayerPacket;
import com.stc.game.multiplayer.enums.MultiplayerMode;
import com.stc.game.multiplayer.enums.NearbyConnectionsMultiplayerStartingType;
import com.stc.game.multiplayer.tools.GoogleMultiplayerUtils;
import com.stc.game.multiplayer.tools.MultiplayerUtils;
import com.stc.game.services.nearbyconnections.tools.MyListDialog;
import com.stc.game.services.nearbyconnections.tools.NearbyConnectionsUtils;
import com.stc.game.ui.enums.Language;
import com.stc.game.ui.tools.I18N;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NearbyConnectionsServiceImpl
  implements NearbyConnectionsService,
             GoogleApiClient.ConnectionCallbacks,
             GoogleApiClient.OnConnectionFailedListener,
             Connections.ConnectionRequestListener,
             Connections.MessageListener,
             Connections.EndpointDiscoveryListener {
  private static final String TAG = "NearbyConnectionsServiceImpl";

  private boolean isHost = false;
  private boolean acceptConnectionRequests = true;

  private List<String> remoteEndpointIds = null;
  private Map<String, String> remoteEndpointIdsMap = null;

  private MyListDialog listDialog = null;

  private GoogleApiClient googleApiClient = null;

  private Activity activity = null;

  public NearbyConnectionsServiceImpl(Activity activity) {
    this.activity = activity;
    remoteEndpointIds = new ArrayList<>();
    remoteEndpointIdsMap = new HashMap<>();
    googleApiClient = new GoogleApiClient.Builder(this.activity)
      .addConnectionCallbacks(this)
      .addOnConnectionFailedListener(this)
      .addApi(Nearby.CONNECTIONS_API)
      .build();
  }

  @Override
  public void onConnected(Bundle bundle) {}

  @Override
  public void onConnectionSuspended(int cause) {
    googleApiClient.connect();
  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    googleApiClient.reconnect();
  }

  @Override
  public void onConnectionRequest(final String remoteEndpointId, final String remoteDeviceId, String remoteEndpointName, byte[] data) {
    if (isHost) {
      if (!acceptConnectionRequests) {
        Nearby.Connections.rejectConnectionRequest(googleApiClient, remoteEndpointId);
        return;
      }

      String questionMark = I18N.getDefaultLanguage() == Language.ENGLISH ? "?" : " ?";

      AlertDialog connectionRequestDialog = new AlertDialog.Builder(activity)
        .setTitle(I18N.getTranslation(I18N.Text.CONNECTION_REQUEST))
        .setMessage(I18N.getTranslation(I18N.Text.DO_YOU_WANT_TO_CONNECT_TO) + remoteEndpointName + questionMark)
        .setCancelable(false)
        .setPositiveButton(I18N.getTranslation(I18N.Text.CONNECT), new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            Nearby.Connections
              .acceptConnectionRequest(googleApiClient, remoteEndpointId, null, NearbyConnectionsServiceImpl.this)
              .setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(@NonNull Status status) {
                  if (status.isSuccess()) {
                    remoteEndpointIds.add(remoteEndpointId);
                    sendRoomIdTo(remoteEndpointId);
                  } else {
                    NearbyConnectionsUtils.handleConnectionsStatusCode(status);
                  }
                }
              });
          }
        })
        .setNegativeButton(I18N.getTranslation(I18N.Text.NO), new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            Nearby.Connections.rejectConnectionRequest(googleApiClient, remoteEndpointId);
          }
        }).create();

      connectionRequestDialog.show();
    } else {
      Nearby.Connections.rejectConnectionRequest(googleApiClient, remoteEndpointId);
    }
  }

  @Override
  public void onEndpointFound(String remoteEndpointId, String remoteDeviceId, String serviceId, String remoteEndpointName) {
    if (isHost || !remoteEndpointName.endsWith(MultiplayerContext.getMultiplayerMode().getName())) {
      return;
    }

    if (listDialog == null) {
      AlertDialog.Builder builder = new AlertDialog.Builder(activity)
        .setTitle(I18N.getTranslation(I18N.Text.ENDPOINTS_FOUND))
        .setCancelable(true)
        .setNegativeButton(I18N.getTranslation(I18N.Text.CANCEL), new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            listDialog.dismiss();
          }
        });

      listDialog = new MyListDialog(activity, builder, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
          String selectedRemoteEndpointId = listDialog.getItemValue(which);
          connectTo(selectedRemoteEndpointId);
          listDialog.dismiss();
        }
      });
    }

    listDialog.addItem(remoteEndpointName, remoteEndpointId);
    listDialog.show();
  }

  @Override
  public void onEndpointLost(String remoteEndpointId) {}

  @Override
  public void onMessageReceived(String remoteEndpointId, byte[] data, boolean isReliable) {
    boolean canProcessData = true;

    if (isHost) {
      ByteBuffer bf = ByteBuffer.wrap(data);
      byte packetId = bf.get();
      if (packetId == GoogleMultiplayerPacket.PACKET_REMOVE_TEAM_DEATHMATCH_TICKET.getPacketId()) {
        String playerName = GoogleMultiplayerUtils.getPlayerNameFromGoogleMultiplayerPacket(bf);
        canProcessData = MultiplayerUtils.bothPlayersInSameTeam(MultiplayerContext.playerName, playerName);
        for (Map.Entry<String, String> remoteEndpointIdsEntry : remoteEndpointIdsMap.entrySet()) {
          String endpointId = remoteEndpointIdsEntry.getKey();
          if (!endpointId.equals(remoteEndpointId)) {
            String otherPlayerName = remoteEndpointIdsEntry.getValue();
            if (MultiplayerUtils.bothPlayersInSameTeam(playerName, otherPlayerName)) {
              Nearby.Connections.sendUnreliableMessage(googleApiClient, endpointId, data);
            }
          }
        }
      } else if (packetId == GoogleMultiplayerPacket.PACKET_PLAYER_NAME.getPacketId()) {
        String playerName = GoogleMultiplayerUtils.getPlayerNameFromGoogleMultiplayerPacket(bf);
        remoteEndpointIdsMap.put(remoteEndpointId, playerName);
      } else {
        for (String endpointId : remoteEndpointIds) {
          if (!endpointId.equals(remoteEndpointId)) {
            Nearby.Connections.sendUnreliableMessage(googleApiClient, endpointId, data);
          }
        }
      }
    }

    if (canProcessData) {
      GoogleMultiplayerUtils.processData(data);
    }
  }

  @Override
  public void onDisconnected(String remoteEndpointId) {}

  @Override
  public void startAdvertising() {
    isHost = true;
    NearbyConnectionsUtils.setCustomPlayerName(true);
    NearbyConnectionsUtils.setCustomRoomId();

    List<AppIdentifier> appIdentifierList = new ArrayList<>();
    appIdentifierList.add(new AppIdentifier(activity.getPackageName()));
    AppMetadata appMetadata = new AppMetadata(appIdentifierList);

    String hostDescription = activity.getString(R.string.app_name) + " - " + MultiplayerContext.getMultiplayerMode().getName();

    Nearby.Connections
      .startAdvertising(googleApiClient, hostDescription, appMetadata, Connections.DURATION_INDEFINITE, this)
      .setResultCallback(new ResultCallback<Connections.StartAdvertisingResult>() {
        @Override
        public void onResult(@NonNull Connections.StartAdvertisingResult result) {
          if (result.getStatus().isSuccess()) {
            MultiplayerContext.multiplayer = true;
          } else {
            NearbyConnectionsUtils.handleConnectionsStatusCode(result.getStatus());
          }
        }
      });
    keepScreenOn();
  }

  @Override
  public void startDiscovering() {
    isHost = false;
    NearbyConnectionsUtils.setCustomPlayerName(false);

    String serviceId = activity.getString(R.string.nearby_connections_service_id);

    Nearby.Connections
      .startDiscovery(googleApiClient, serviceId, Connections.DURATION_INDEFINITE, this)
      .setResultCallback(new ResultCallback<Status>() {
        @Override
        public void onResult(@NonNull Status status) {
          if (!status.isSuccess()) {
            NearbyConnectionsUtils.handleConnectionsStatusCode(status);
          }
        }
      });
    keepScreenOn();
  }

  @Override
  public void stopNearbyConnectionsActivity(final NearbyConnectionsMultiplayerStartingType startingType) {
    if (startingType == NearbyConnectionsMultiplayerStartingType.START_ADVERTISING) {
      Nearby.Connections.stopAdvertising(googleApiClient);
      Nearby.Connections.stopAllEndpoints(googleApiClient);
      reset();
    } else if (startingType == NearbyConnectionsMultiplayerStartingType.START_DISCOVERING) {
      String serviceId = activity.getString(R.string.nearby_connections_service_id);
      Nearby.Connections.stopDiscovery(googleApiClient, serviceId);
      if (!remoteEndpointIds.isEmpty()) {
        String hostId = remoteEndpointIds.get(0);
        Nearby.Connections.disconnectFromEndpoint(googleApiClient, hostId);
      }
      reset();
    } else {
      if (isHost) {
        stopNearbyConnectionsActivity(NearbyConnectionsMultiplayerStartingType.START_ADVERTISING);
      } else {
        stopNearbyConnectionsActivity(NearbyConnectionsMultiplayerStartingType.START_DISCOVERING);
      }
    }
  }

  @Override
  public void sendPlayer(final Player player) {
    try {
      byte[] data = GoogleMultiplayerUtils.getPacketPlayer(player);
      sendData(data);
    } catch(Exception e){
      Gdx.app.error(TAG, "Failed to send a position. " + e.getMessage());
    }
  }

  @Override
  public void sendPlayerAttack(final int attackId, final String targetId, final int colorId) {
    try {
      byte[] data = GoogleMultiplayerUtils.getPacketPlayerAttack(attackId, targetId, colorId);
      sendData(data);
    } catch (Exception e) {
      Gdx.app.error(TAG, "Failed to send a player attack. " + e.getMessage());
    }
  }

  @Override
  public void sendAttackEffectEntityCollision(final int attackId, final float damageCoef, final String attackedEntityId) {
    try {
      byte[] data = GoogleMultiplayerUtils.getPacketAttackEffectEntityCollision(attackId, damageCoef, attackedEntityId);
      sendData(data);
    } catch (Exception e) {
      Gdx.app.error(TAG, "Failed to send an effect/entity collision. " + e.getMessage());
    }
  }

  @Override
  public void sendRemoveTeamDeathmatchTicket() {
    try {
      byte[] data = GoogleMultiplayerUtils.getPacketPlayerName();
      if (isHost) {
        for (Map.Entry<String, String> remoteEndpointIdsEntry : remoteEndpointIdsMap.entrySet()) {
          String playerName = remoteEndpointIdsEntry.getValue();
          if (MultiplayerUtils.bothPlayersInSameTeam(MultiplayerContext.playerName, playerName)) {
            String remoteEndpointId = remoteEndpointIdsEntry.getKey();
            Nearby.Connections.sendUnreliableMessage(googleApiClient, remoteEndpointId, data);
          }
        }
      } else {
        if (!remoteEndpointIds.isEmpty()) {
          String hostId = remoteEndpointIds.get(0);
          Nearby.Connections.sendUnreliableMessage(googleApiClient, hostId, data);
        }
      }
    } catch (Exception e) {
      Gdx.app.error(TAG, "Failed to send a remove team deathmatch ticket instruction. " + e.getMessage());
    }
  }

  @Override
  public void rejectConnectionRequests() {
    acceptConnectionRequests = false;
    int me = 1;
    MultiplayerContext.numberOfParticipants = remoteEndpointIds.size() + me;
    if (MultiplayerContext.numberOfParticipants > 1) {
      byte[] data = GoogleMultiplayerUtils.getPacketNumberOfParticipants();
      sendData(data);
    }
  }

  @Override
  public boolean isHost() {
    return isHost;
  }

  public void onStart() {
    googleApiClient.connect();
  }

  public void onStop() {
    if (googleApiClient != null && googleApiClient.isConnected()) {
      googleApiClient.disconnect();
      googleApiClient = null;
    }
    if (remoteEndpointIds != null) {
      remoteEndpointIds.clear();
      remoteEndpointIds = null;
    }
    if (remoteEndpointIdsMap != null) {
      remoteEndpointIdsMap.clear();
      remoteEndpointIdsMap = null;
    }
    if (listDialog != null) {
      listDialog.dispose();
      listDialog = null;
    }
    activity = null;
  }

  private void connectTo(final String remoteEndpointId) {
    Nearby.Connections.sendConnectionRequest(googleApiClient, null, remoteEndpointId, null, new Connections.ConnectionResponseCallback() {
      @Override
      public void onConnectionResponse(String endpointId, Status status, byte[] data) {
        if (status.isSuccess()) {
          remoteEndpointIds.add(endpointId);
          if (MultiplayerContext.getMultiplayerMode() == MultiplayerMode.TEAM_DEATHMATCH) {
            byte[] packetPlayerName = GoogleMultiplayerUtils.getPacketPlayerName();
            sendData(packetPlayerName);
          }
        } else {
          NearbyConnectionsUtils.handleConnectionsStatusCode(status);
        }
      }
    }, this);
  }

  private void sendData(final byte[] data) {
    if (isHost) {
      for (String remoteEndpointId : remoteEndpointIds) {
        Nearby.Connections.sendUnreliableMessage(googleApiClient, remoteEndpointId, data);
      }
    } else {
      if (!remoteEndpointIds.isEmpty()) {
        String hostId = remoteEndpointIds.get(0);
        Nearby.Connections.sendUnreliableMessage(googleApiClient, hostId, data);
      }
    }
  }

  private void sendRoomIdTo(final String remoteEndpointId) {
    try {
      byte[] data = GoogleMultiplayerUtils.getPacketRoomId();
      Nearby.Connections.sendUnreliableMessage(googleApiClient, remoteEndpointId, data);
    } catch (Exception e) {
      Gdx.app.error(TAG, "Failed to send the room id. " + e.getMessage());
    }
  }

  private void keepScreenOn() {
    activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
  }

  private void stopKeepingScreenOn() {
    try {
      activity.runOnUiThread(new Runnable() {
        public void run() {
          activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
      });
    } catch (Exception e) {
      Gdx.app.error(TAG, "Failed to clear the flag FLAG_KEEP_SCREEN_ON. " + e.getMessage());
    }
  }

  private void reset() {
    isHost = false;
    acceptConnectionRequests = true;
    remoteEndpointIds.clear();
    remoteEndpointIdsMap.clear();
    if (listDialog != null) {
      listDialog.clear();
    }
    stopKeepingScreenOn();
  }
}
