package com.stc.game.services.nearbyconnections.tools;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.nearby.connection.ConnectionsStatusCodes;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.ui.dialogs.Dialogs;
import com.stc.game.ui.tools.I18N;

import java.util.UUID;

public final class NearbyConnectionsUtils {
  private static final String PLAYER_IDENTIFIER = "p_";
  private static final String HOST_IDENTIFIER = "h_";
  private static final String ROOM_IDENTIFIER = "r_";

  public static void setCustomPlayerName(final boolean isHost) {
    MultiplayerContext.playerName = (isHost ? HOST_IDENTIFIER : PLAYER_IDENTIFIER) + UUID.randomUUID().toString().replace("-", "");
  }

  public static void setCustomRoomId() {
    MultiplayerContext.roomId = ROOM_IDENTIFIER + UUID.randomUUID().toString().replace("-", "");
  }

  public static void handleConnectionsStatusCode(final Status status) {
    int statusCode = status.getStatusCode();
    switch (statusCode) {
      case ConnectionsStatusCodes.STATUS_ERROR :
        Dialogs.showButtonDialog(I18N.getTranslation(I18N.Text.NEARBY_CONNECTIONS), I18N.getTranslation(I18N.Text.STATUS_ERROR));
        break;
      case ConnectionsStatusCodes.STATUS_ALREADY_ADVERTISING :
        Dialogs.showButtonDialog(I18N.getTranslation(I18N.Text.START_ADVERTISING), I18N.getTranslation(I18N.Text.STATUS_ALREADY_ADVERTISING));
        break;
      case ConnectionsStatusCodes.STATUS_ALREADY_DISCOVERING :
        Dialogs.showButtonDialog(I18N.getTranslation(I18N.Text.START_DISCOVERING), I18N.getTranslation(I18N.Text.STATUS_ALREADY_DISCOVERING));
        break;
      case ConnectionsStatusCodes.STATUS_ALREADY_CONNECTED_TO_ENDPOINT :
        Dialogs.showButtonDialog(I18N.getTranslation(I18N.Text.NEARBY_CONNECTIONS), I18N.getTranslation(I18N.Text.STATUS_ALREADY_CONNECTED_TO_ENDPOINT));
        break;
      case ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED :
        Dialogs.showButtonDialog(I18N.getTranslation(I18N.Text.NEARBY_CONNECTIONS), I18N.getTranslation(I18N.Text.STATUS_CONNECTION_REJECTED));
        break;
      case ConnectionsStatusCodes.STATUS_NOT_CONNECTED_TO_ENDPOINT :
        Dialogs.showButtonDialog(I18N.getTranslation(I18N.Text.NEARBY_CONNECTIONS), I18N.getTranslation(I18N.Text.STATUS_NOT_CONNECTED_TO_ENDPOINT));
        break;
      default :
        break;
    }
  }
}