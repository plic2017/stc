package com.stc.game;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.games.Games;
import com.google.example.games.basegameutils.GameHelper.GameHelperListener;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.services.Services;
import com.stc.game.services.admob.AdMobServiceImpl;
import com.stc.game.services.chat.ChatServiceImpl;
import com.stc.game.services.gpgs.GooglePlayGamesServices;
import com.stc.game.services.gpgs.GooglePlayGamesServicesImpl;
import com.stc.game.services.nearbyconnections.NearbyConnectionsServiceImpl;
import com.stc.game.services.network.NetworkServiceImpl;
import com.stc.game.ui.dialogs.Dialogs;
import com.stc.game.ui.enums.Language;
import com.stc.game.ui.tools.I18N;

import java.util.Locale;

public class AndroidLauncher extends AndroidApplication implements GooglePlayGamesServices {
  private static final String TAG = "AndroidLauncher";
  private static final int RC_ACHIEVEMENTS = 100;
  private static final int RC_LEADERBOARDS = 101;

  private GooglePlayGamesServicesImpl gameHelper;
  private NearbyConnectionsServiceImpl nearbyConnections;
  private AdMobServiceImpl adMob;
  private ChatServiceImpl chat;
  private NetworkServiceImpl network;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setDefaultLanguage();
    initGPGS();
    initNearbyConnectionsService();
    initAdMobService();
    initChatService();
    initNetworkService();
    startMain();
  }

  @Override
  protected void onStart() {
    super.onStart();
    initGPGS();
    gameHelper.onStart(this);
    initNearbyConnectionsService();
    nearbyConnections.onStart();
    initAdMobService();
    initChatService();
    initNetworkService();
  }

  @Override
  protected void onStop() {
    super.onStop();
    if (!MultiplayerContext.inRealtimeMultiplayer() && !MultiplayerContext.inNearbyConnectionsMultiplayer()) {
      gameHelper.onStop();
      gameHelper = null;

      nearbyConnections.onStop();
      nearbyConnections = null;

      chat = null;

      network.onStop();
      network = null;
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
    super.onActivityResult(requestCode, resultCode, intent);
    gameHelper.onActivityResult(requestCode, resultCode, intent);
  }

  @Override
  public void signIn() {
    try {
      runOnUiThread(new Runnable() {
        @Override
        public void run() {
          gameHelper.beginUserInitiatedSignIn();
        }
      });
    } catch (Exception e) {
      Gdx.app.error(TAG, "Failed to sign in. " + e.getMessage());
    }
  }

  @Override
  public boolean isSignedIn() {
    return gameHelper.isSignedIn();
  }

  @Override
  public boolean isConnecting() {
    return gameHelper.isConnecting();
  }

  @Override
  public void signOut() {
    try {
      runOnUiThread(new Runnable() {
        @Override
        public void run() {
          gameHelper.signOut();
          if (!gameHelper.getApiClient().isConnected()) {
            Dialogs.showButtonDialog(I18N.getTranslation(I18N.Text.SIGN_OUT), I18N.getTranslation(I18N.Text.SIGNED_OUT));
          }
        }
      });
    } catch (Exception e) {
      Gdx.app.error(TAG, "Failed to sign out. " + e.getMessage());
    }
  }

  @Override
  public void rateTheSTCGame() {
    Uri uri = Uri.parse("market://details?id=" + getPackageName());
    Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
    }
    try {
      startActivity(goToMarket);
    } catch (ActivityNotFoundException e) {
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
    }
  }

  @Override
  public void unlockAchievement(final String achievementId) {
    if (isSignedIn()) {
      Games.Achievements.unlock(gameHelper.getApiClient(), achievementId);
    } else if (!gameHelper.isConnecting()){
      signIn();
    }
  }

  @Override
  public void incrementAchievement(final String achievementId, final int increment) {
    if (isSignedIn()) {
      Games.Achievements.increment(gameHelper.getApiClient(), achievementId, increment);
    } else if (!gameHelper.isConnecting()) {
      signIn();
    }
  }

  @Override
  public void showAchievements() {
    if (isSignedIn()) {
      startActivityForResult(Games.Achievements.getAchievementsIntent(gameHelper.getApiClient()), RC_ACHIEVEMENTS);
    } else if (!gameHelper.isConnecting()) {
      signIn();
    } else {
      Dialogs.showButtonDialog(I18N.getTranslation(I18N.Text.ACHIEVEMENTS), I18N.getTranslation(I18N.Text.GPGS_REQUIRED));
    }
  }

  @Override
  public void submitScore(final String leaderboardId, final long score) {
    if (isSignedIn()) {
      Games.Leaderboards.submitScore(gameHelper.getApiClient(), leaderboardId, score);
    } else if (!gameHelper.isConnecting()) {
      signIn();
    }
  }

  @Override
  public void showLeaderboards() {
    if (isSignedIn()) {
      startActivityForResult(Games.Leaderboards.getAllLeaderboardsIntent(gameHelper.getApiClient()), RC_LEADERBOARDS);
    } else if (!gameHelper.isConnecting()) {
      signIn();
    } else {
      Dialogs.showButtonDialog(I18N.getTranslation(I18N.Text.LEADERBOARDS), I18N.getTranslation(I18N.Text.GPGS_REQUIRED));
    }
  }

  @Override
  public void quickMatch(final int minAutoMatchPlayers, final int maxAutoMathPlayers, final int variant) {
    try {
      runOnUiThread(new Runnable() {
        public void run() {
          gameHelper.startQuickGame(minAutoMatchPlayers, maxAutoMathPlayers, variant);
        }
      });
    } catch (Exception e) {
      Gdx.app.error(TAG, "Failed to launch a quick match. " + e.getMessage());
    }
  }

  @Override
  public void invitePlayers(final int minAutoMatchPlayers, final int maxAutoMathPlayers) {
    gameHelper.invitePlayers(minAutoMatchPlayers, maxAutoMathPlayers);
  }

  @Override
  public void showInvitations() {
    gameHelper.showInvitations();
  }

  @Override
  public void sendPlayer(final Player player) {
    gameHelper.sendPlayer(player);
  }

  @Override
  public void sendPlayerAttack(final int attackId, final String targetId, final int colorId) {
    gameHelper.sendPlayerAttack(attackId, targetId, colorId);
  }

  @Override
  public void sendAttackEffectEntityCollision(final int attackId, final float damageCoef, final String attackedEntityId) {
    gameHelper.sendAttackEffectEntityCollision(attackId, damageCoef, attackedEntityId);
  }

  @Override
  public void sendRemoveTeamDeathmatchTicket() {
    gameHelper.sendRemoveTeamDeathmatchTicket();
  }

  @Override
  public void sendChatMessage(String recipient, String message) {
    gameHelper.sendChatMessage(recipient, message);
  }

  @Override
  public void leaveRoom() {
    gameHelper.leaveRoom();
  }

  @Override
  public void showSnapshots(final String snapshotsListTitle, final boolean allowAddButton, final boolean allowDeleteButton, final int maxSnapshots) {
    gameHelper.showSnapshots(snapshotsListTitle, allowAddButton, allowDeleteButton, maxSnapshots);
  }

  @Override
  public void createOrSaveSnapshot(final String snapshotTitleToOverwrite, final byte[] data, final byte[] pixels) {
    gameHelper.createOrSaveSnapshot(snapshotTitleToOverwrite, data, pixels);
  }

  @Override
  public void sendGameGiftsRequest(final int requestTypeId, final byte gameGiftId) {
    gameHelper.sendGameGiftsRequest(requestTypeId, gameGiftId);
  }

  @Override
  public void showRequestsInbox() {
    gameHelper.showRequestsInbox();
  }

  private void startMain() {
    AndroidApplicationConfiguration configuration = new AndroidApplicationConfiguration();
    configuration.useAccelerometer = false;
    configuration.useCompass = false;
    View gameView = initializeForView(new Main(), configuration);

    RelativeLayout relativeLayout = new RelativeLayout(this);
    relativeLayout.addView(gameView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
    relativeLayout.addView(adMob.getMainBannerAd(), params);

    setContentView(relativeLayout);
  }

  private void initGPGS() {
    if (gameHelper == null) {
      gameHelper = new GooglePlayGamesServicesImpl(this);
      gameHelper.enableDebugLog(false);
      gameHelper.setup(new GameHelperListener() {
        @Override
        public void onSignInFailed() {}

        @Override
        public void onSignInSucceeded() {}
      });
      Services.setGpgs(this);
    }
  }

  private void initNearbyConnectionsService() {
    if (nearbyConnections == null) {
      nearbyConnections = new NearbyConnectionsServiceImpl(this);
      Services.setNearbyConnectionsService(nearbyConnections);
    }
  }

  private void initChatService() {
    if (chat == null) {
      chat = new ChatServiceImpl();
      Services.setChatService(chat);
    }
  }

  private void initAdMobService() {
    if (adMob == null) {
      adMob = new AdMobServiceImpl(this);
      adMob.initAds();
      Services.setAdMobService(adMob);
    }
  }

  private void initNetworkService() {
    if (network == null) {
      network = new NetworkServiceImpl(this);
      Services.setNetworkService(network);
    }
  }

  private static void setDefaultLanguage() {
    if (Locale.getDefault().getLanguage().equals(Language.FRENCH.getLanguageCode())) {
      I18N.initialize(Language.FRENCH);
    } else {
      I18N.initialize(Language.ENGLISH);
    }
  }
}
