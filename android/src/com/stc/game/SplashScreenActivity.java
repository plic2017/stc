package com.stc.game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashScreenActivity extends Activity {
  private static final long TIMEOUT_SPLASH_SCREEN = 2000L;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.splash_screen);
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        Intent intent = new Intent(SplashScreenActivity.this, AndroidLauncher.class);
        startActivity(intent);
        finish();
      }
    }, TIMEOUT_SPLASH_SCREEN);
  }
}
