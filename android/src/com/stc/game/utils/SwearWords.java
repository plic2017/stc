package com.stc.game.utils;

import lombok.Getter;
import lombok.Setter;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class SwearWords {
  private static final String SWEAR_WORD = "?#@!";

  @Getter @Setter
  private static Set<String> swearWords = new HashSet<>();

  public static void initSwearWords() {
    String frenchContent = "baiser\n" +
      "bander\n" +
      "bigornette\n" +
      "bite\n" +
      "bitte\n" +
      "bloblos\n" +
      "bordel\n" +
      "bosser\n" +
      "bourré\n" +
      "bourrée\n" +
      "brackmard\n" +
      "branlage\n" +
      "branler\n" +
      "branlette\n" +
      "branleur\n" +
      "branleuse\n" +
      "brouter le cresson\n" +
      "caca\n" +
      "cailler\n" +
      "chatte\n" +
      "chiasse\n" +
      "chier\n" +
      "chiottes\n" +
      "clito\n" +
      "clitoris\n" +
      "con\n" +
      "connard\n" +
      "connasse\n" +
      "conne\n" +
      "couilles\n" +
      "cramouille\n" +
      "cul\n" +
      "déconne\n" +
      "déconner\n" +
      "drague\n" +
      "emmerdant\n" +
      "emmerder\n" +
      "emmerdeur\n" +
      "emmerdeuse\n" +
      "enculé\n" +
      "enculée\n" +
      "enculeur\n" +
      "enculeurs\n" +
      "enfoiré\n" +
      "enfoirée\n" +
      "étron\n" +
      "fille de pute\n" +
      "fils de pute\n" +
      "folle\n" +
      "foutre\n" +
      "gerbe\n" +
      "gerber\n" +
      "gouine\n" +
      "grande folle\n" +
      "grogniasse\n" +
      "gueule\n" +
      "jouir\n" +
      "la putain de ta mère\n" +
      "MALPT\n" +
      "ménage à trois\n" +
      "merde\n" +
      "merdeuse\n" +
      "merdeux\n" +
      "meuf\n" +
      "nègre\n" +
      "nique ta mère\n" +
      "nique ta race\n" +
      "palucher\n" +
      "pédale\n" +
      "pédé\n" +
      "péter\n" +
      "pipi\n" +
      "pisser\n" +
      "pouffiasse\n" +
      "pousse-crotte\n" +
      "putain\n" +
      "pute\n" +
      "ramoner\n" +
      "sac à merde\n" +
      "salaud\n" +
      "salope\n" +
      "suce\n" +
      "tapette\n" +
      "teuf\n" +
      "tringler\n" +
      "trique\n" +
      "trou du cul\n" +
      "turlute\n" +
      "veuve\n" +
      "zigounette\n" +
      "zizi";
    String englishContent = "2g1c\n" +
      "2 girls 1 cup\n" +
      "acrotomophilia\n" +
      "alabama hot pocket\n" +
      "alaskan pipeline\n" +
      "anal\n" +
      "anilingus\n" +
      "anus\n" +
      "apeshit\n" +
      "arsehole\n" +
      "ass\n" +
      "asshole\n" +
      "assmunch\n" +
      "auto erotic\n" +
      "autoerotic\n" +
      "babeland\n" +
      "baby batter\n" +
      "baby juice\n" +
      "ball gag\n" +
      "ball gravy\n" +
      "ball kicking\n" +
      "ball licking\n" +
      "ball sack\n" +
      "ball sucking\n" +
      "bangbros\n" +
      "bareback\n" +
      "barely legal\n" +
      "barenaked\n" +
      "bastard\n" +
      "bastardo\n" +
      "bastinado\n" +
      "bbw\n" +
      "bdsm\n" +
      "beaner\n" +
      "beaners\n" +
      "beaver cleaver\n" +
      "beaver lips\n" +
      "bestiality\n" +
      "big black\n" +
      "big breasts\n" +
      "big knockers\n" +
      "big tits\n" +
      "bimbos\n" +
      "birdlock\n" +
      "bitch\n" +
      "bitches\n" +
      "black cock\n" +
      "blonde action\n" +
      "blonde on blonde action\n" +
      "blowjob\n" +
      "blow job\n" +
      "blow your load\n" +
      "blue waffle\n" +
      "blumpkin\n" +
      "bollocks\n" +
      "bondage\n" +
      "boner\n" +
      "boob\n" +
      "boobs\n" +
      "booty call\n" +
      "brown showers\n" +
      "brunette action\n" +
      "bukkake\n" +
      "bulldyke\n" +
      "bullet vibe\n" +
      "bullshit\n" +
      "bung hole\n" +
      "bunghole\n" +
      "busty\n" +
      "butt\n" +
      "buttcheeks\n" +
      "butthole\n" +
      "camel toe\n" +
      "camgirl\n" +
      "camslut\n" +
      "camwhore\n" +
      "carpet muncher\n" +
      "carpetmuncher\n" +
      "chocolate rosebuds\n" +
      "circlejerk\n" +
      "cleveland steamer\n" +
      "clit\n" +
      "clitoris\n" +
      "clover clamps\n" +
      "clusterfuck\n" +
      "cock\n" +
      "cocks\n" +
      "coprolagnia\n" +
      "coprophilia\n" +
      "cornhole\n" +
      "coon\n" +
      "coons\n" +
      "creampie\n" +
      "cum\n" +
      "cumming\n" +
      "cunnilingus\n" +
      "cunt\n" +
      "darkie\n" +
      "date rape\n" +
      "daterape\n" +
      "deep throat\n" +
      "deepthroat\n" +
      "dendrophilia\n" +
      "dick\n" +
      "dildo\n" +
      "dingleberry\n" +
      "dingleberries\n" +
      "dirty pillows\n" +
      "dirty sanchez\n" +
      "doggie style\n" +
      "doggiestyle\n" +
      "doggy style\n" +
      "doggystyle\n" +
      "dog style\n" +
      "dolcett\n" +
      "domination\n" +
      "dominatrix\n" +
      "dommes\n" +
      "donkey punch\n" +
      "double dong\n" +
      "double penetration\n" +
      "dp action\n" +
      "dry hump\n" +
      "dvda\n" +
      "eat my ass\n" +
      "ecchi\n" +
      "ejaculation\n" +
      "erotic\n" +
      "erotism\n" +
      "escort\n" +
      "eunuch\n" +
      "faggot\n" +
      "fecal\n" +
      "felch\n" +
      "fellatio\n" +
      "feltch\n" +
      "female squirting\n" +
      "femdom\n" +
      "figging\n" +
      "fingerbang\n" +
      "fingering\n" +
      "fisting\n" +
      "foot fetish\n" +
      "footjob\n" +
      "frotting\n" +
      "fuck\n" +
      "fuck buttons\n" +
      "fuckin\n" +
      "fucking\n" +
      "fucktards\n" +
      "fudge packer\n" +
      "fudgepacker\n" +
      "futanari\n" +
      "gang bang\n" +
      "gay sex\n" +
      "genitals\n" +
      "giant cock\n" +
      "girl on\n" +
      "girl on top\n" +
      "girls gone wild\n" +
      "goatcx\n" +
      "goatse\n" +
      "god damn\n" +
      "gokkun\n" +
      "golden shower\n" +
      "goodpoop\n" +
      "goo girl\n" +
      "goregasm\n" +
      "grope\n" +
      "group sex\n" +
      "g-spot\n" +
      "guro\n" +
      "hand job\n" +
      "handjob\n" +
      "hard core\n" +
      "hardcore\n" +
      "hentai\n" +
      "homoerotic\n" +
      "honkey\n" +
      "hooker\n" +
      "hot carl\n" +
      "hot chick\n" +
      "how to kill\n" +
      "how to murder\n" +
      "huge fat\n" +
      "humping\n" +
      "incest\n" +
      "intercourse\n" +
      "jack off\n" +
      "jail bait\n" +
      "jailbait\n" +
      "jelly donut\n" +
      "jerk off\n" +
      "jigaboo\n" +
      "jiggaboo\n" +
      "jiggerboo\n" +
      "jizz\n" +
      "juggs\n" +
      "kike\n" +
      "kinbaku\n" +
      "kinkster\n" +
      "kinky\n" +
      "knobbing\n" +
      "leather restraint\n" +
      "leather straight jacket\n" +
      "lemon party\n" +
      "lolita\n" +
      "lovemaking\n" +
      "make me come\n" +
      "male squirting\n" +
      "masturbate\n" +
      "menage a trois\n" +
      "milf\n" +
      "missionary position\n" +
      "motherfucker\n" +
      "mound of venus\n" +
      "mr hands\n" +
      "muff diver\n" +
      "muffdiving\n" +
      "nambla\n" +
      "nawashi\n" +
      "negro\n" +
      "neonazi\n" +
      "nigga\n" +
      "nigger\n" +
      "nig nog\n" +
      "nimphomania\n" +
      "nipple\n" +
      "nipples\n" +
      "nsfw images\n" +
      "nude\n" +
      "nudity\n" +
      "nympho\n" +
      "nymphomania\n" +
      "octopussy\n" +
      "omorashi\n" +
      "one cup two girls\n" +
      "one guy one jar\n" +
      "orgasm\n" +
      "orgy\n" +
      "paedophile\n" +
      "paki\n" +
      "panties\n" +
      "panty\n" +
      "pedobear\n" +
      "pedophile\n" +
      "pegging\n" +
      "penis\n" +
      "phone sex\n" +
      "piece of shit\n" +
      "pissing\n" +
      "piss pig\n" +
      "pisspig\n" +
      "playboy\n" +
      "pleasure chest\n" +
      "pole smoker\n" +
      "ponyplay\n" +
      "poof\n" +
      "poon\n" +
      "poontang\n" +
      "punany\n" +
      "poop chute\n" +
      "poopchute\n" +
      "porn\n" +
      "porno\n" +
      "pornography\n" +
      "prince albert piercing\n" +
      "pthc\n" +
      "pubes\n" +
      "pussy\n" +
      "queaf\n" +
      "queef\n" +
      "quim\n" +
      "raghead\n" +
      "raging boner\n" +
      "rape\n" +
      "raping\n" +
      "rapist\n" +
      "rectum\n" +
      "reverse cowgirl\n" +
      "rimjob\n" +
      "rimming\n" +
      "rosy palm\n" +
      "rosy palm and her 5 sisters\n" +
      "rusty trombone\n" +
      "sadism\n" +
      "santorum\n" +
      "scat\n" +
      "schlong\n" +
      "scissoring\n" +
      "semen\n" +
      "sex\n" +
      "sexo\n" +
      "sexy\n" +
      "shaved beaver\n" +
      "shaved pussy\n" +
      "shemale\n" +
      "shibari\n" +
      "shit\n" +
      "shitblimp\n" +
      "shitty\n" +
      "shota\n" +
      "shrimping\n" +
      "skeet\n" +
      "slanteye\n" +
      "slut\n" +
      "s&m\n" +
      "smut\n" +
      "snatch\n" +
      "snowballing\n" +
      "sodomize\n" +
      "sodomy\n" +
      "spic\n" +
      "splooge\n" +
      "splooge moose\n" +
      "spooge\n" +
      "spread legs\n" +
      "spunk\n" +
      "strap on\n" +
      "strapon\n" +
      "strappado\n" +
      "strip club\n" +
      "style doggy\n" +
      "suck\n" +
      "sucks\n" +
      "suicide girls\n" +
      "sultry women\n" +
      "swastika\n" +
      "swinger\n" +
      "tainted love\n" +
      "taste my\n" +
      "tea bagging\n" +
      "threesome\n" +
      "throating\n" +
      "tied up\n" +
      "tight white\n" +
      "tit\n" +
      "tits\n" +
      "titties\n" +
      "titty\n" +
      "tongue in a\n" +
      "topless\n" +
      "tosser\n" +
      "towelhead\n" +
      "tranny\n" +
      "tribadism\n" +
      "tub girl\n" +
      "tubgirl\n" +
      "tushy\n" +
      "twat\n" +
      "twink\n" +
      "twinkie\n" +
      "two girls one cup\n" +
      "undressing\n" +
      "upskirt\n" +
      "urethra play\n" +
      "urophilia\n" +
      "vagina\n" +
      "venus mound\n" +
      "vibrator\n" +
      "violet wand\n" +
      "vorarephilia\n" +
      "voyeur\n" +
      "vulva\n" +
      "wank\n" +
      "wetback\n" +
      "wet dream\n" +
      "white power\n" +
      "wrapping men\n" +
      "wrinkled starfish\n" +
      "xx\n" +
      "xxx\n" +
      "yaoi\n" +
      "yellow showers\n" +
      "yiffy\n" +
      "zoophilia\n" +
      "\uD83D\uDD95";
    String[] frenchSwearWords = frenchContent.split(String.valueOf('\n'));
    String[] englishSwearWords = englishContent.split(String.valueOf('\n'));
    Collections.addAll(swearWords, frenchSwearWords);
    Collections.addAll(swearWords, englishSwearWords);
  }

  public static String cleanMessage(String message) {
    for (String word : swearWords) {
      message = message.replaceAll("(?i)" + word, SWEAR_WORD);
    }
    message = message.replaceAll("\\w*\\*{4}", SWEAR_WORD);
    return message;
  }
}
