package com.stc.game.multiplayer.tools;

import com.badlogic.gdx.Gdx;
import com.stc.game.gameplay.entities.world.alive.player.Player;
import com.stc.game.kryonet.packets.PacketAttackEffectEntityCollision;
import com.stc.game.kryonet.packets.PacketPlayer;
import com.stc.game.kryonet.packets.PacketPlayerAttack;
import com.stc.game.kryonet.packets.PacketPlayerMessage;
import com.stc.game.kryonet.scheduler.PacketScheduler;
import com.stc.game.kryonet.tools.KryonetPacketBuilder;
import com.stc.game.multiplayer.beans.MultiplayerContext;
import com.stc.game.multiplayer.enums.GoogleMultiplayerPacket;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Arrays;

public final class GoogleMultiplayerUtils {
  private static final String TAG = "GoogleMultiplayerUtils";
  private static final int BYTE = 1;
  private static final int INT = Integer.SIZE / Byte.SIZE;
  private static final int FLOAT = Float.SIZE / Byte.SIZE;
  private static final int CONTINUE = 1;
  private static final int STOP = 0;

  public static byte[] getPacketRoomId() {
    int roomIdLength = MultiplayerContext.roomId.length();
    byte[] roomIdAsBytes = MultiplayerContext.roomId.getBytes();
    int capacity = BYTE + INT + roomIdAsBytes.length;
    return ByteBuffer.allocate(capacity)
      .put(GoogleMultiplayerPacket.PACKET_ROOM_ID.getPacketId())
      .putInt(roomIdLength)
      .put(roomIdAsBytes)
      .array();
  }

  public static byte[] getPacketPlayer(final Player player) {
    int playerNameLength = MultiplayerContext.playerName.length();
    byte[] playerNameAsBytes = MultiplayerContext.playerName.getBytes();
    int capacity = BYTE + INT + playerNameAsBytes.length + 7 * FLOAT;
    return ByteBuffer.allocate(capacity)
      .put(GoogleMultiplayerPacket.PACKET_PLAYER.getPacketId())
      .putInt(playerNameLength)
      .put(playerNameAsBytes)
      .putFloat(player.getPosition().x)
      .putFloat(player.getPosition().y)
      .putFloat(player.getPosition().z)
      .putFloat(player.getDirection().x)
      .putFloat(player.getDirection().y)
      .putFloat(player.getDirection().z)
      .putFloat(player.getDirection().w)
      .array();
  }

  public static byte[] getPacketPlayerAttack(final int attackId, final String targetId, final int colorId) {
    int playerNameLength = MultiplayerContext.playerName.length();
    byte[] playerNameAsBytes = MultiplayerContext.playerName.getBytes();
    int targetIdLength = targetId != null ? targetId.length() : 0;
    byte[] targetIdAsBytes = targetId != null ? targetId.getBytes() : null;
    int capacity = 2 * BYTE +
                   4 * INT +
                   playerNameAsBytes.length +
                   (targetIdAsBytes != null ? targetIdAsBytes.length : 0);

    ByteBuffer byteBuffer =
      ByteBuffer.allocate(capacity)
        .put(GoogleMultiplayerPacket.PACKET_PLAYER_ATTACK.getPacketId())
        .putInt(playerNameLength)
        .put(playerNameAsBytes)
        .putInt(attackId)
        .putInt(colorId);

    if (targetId == null) {
      byteBuffer.put((byte) STOP);
    } else {
      byteBuffer.put((byte) CONTINUE).putInt(targetIdLength).put(targetIdAsBytes);
    }

    return byteBuffer.array();
  }

  public static byte[] getPacketAttackEffectEntityCollision(final int attackId, final float damageCoef, final String attackedEntityId) {
    int attackedEntityIdLength = attackedEntityId.length();
    byte[] attackedEntityIdAsBytes = attackedEntityId.getBytes();
    int capacity = BYTE + 2 * INT + FLOAT + attackedEntityIdAsBytes.length;
    return ByteBuffer.allocate(capacity)
      .put(GoogleMultiplayerPacket.PACKET_ATTACK_EFFECT_ENTITY_COLLISION.getPacketId())
      .putInt(attackId)
      .putFloat(damageCoef)
      .putInt(attackedEntityIdLength)
      .put(attackedEntityIdAsBytes)
      .array();
  }

  public static byte[] getPacketPlayerMessage(final String message) {
    int playerNameLength = MultiplayerContext.playerName.length();
    byte[] playerNameAsBytes = MultiplayerContext.playerName.getBytes();
    byte[] messageAsBytes = message.getBytes();
    int capacity = BYTE + INT + playerNameAsBytes.length + messageAsBytes.length;
    return ByteBuffer.allocate(capacity)
      .put(GoogleMultiplayerPacket.PACKET_PLAYER_MESSAGE.getPacketId())
      .putInt(playerNameLength)
      .put(playerNameAsBytes)
      .put(messageAsBytes)
      .array();
  }

  public static byte[] getPacketNumberOfParticipants() {
    int capacity = BYTE + INT;
    return  ByteBuffer.allocate(capacity)
      .put(GoogleMultiplayerPacket.PACKET_NUMBER_OF_PARTICIPANTS.getPacketId())
      .putInt(MultiplayerContext.numberOfParticipants)
      .array();
  }

  public static byte[] getPacketPlayerName() {
    int playerNameLength = MultiplayerContext.playerName.length();
    byte[] playerNameAsBytes = MultiplayerContext.playerName.getBytes();
    int capacity = BYTE + INT + playerNameAsBytes.length;
    return ByteBuffer.allocate(capacity)
      .put(GoogleMultiplayerPacket.PACKET_PLAYER_NAME.getPacketId())
      .putInt(playerNameLength)
      .put(playerNameAsBytes)
      .array();
  }

  public static String getPlayerNameFromGoogleMultiplayerPacket(final ByteBuffer bf) {
    int playerNameLength = bf.getInt();
    String playerName = "";
    for (int i = 0; i < playerNameLength; i++) {
      playerName += (char) bf.get();
    }
    return  playerName;
  }

  public static void processData(final byte[] data) {
    ByteBuffer bf = ByteBuffer.wrap(data);
    byte packetId = bf.get();

    if (packetId == GoogleMultiplayerPacket.PACKET_ROOM_ID.getPacketId()) {
      handlePacketRoomId(bf);
    } else if (packetId == GoogleMultiplayerPacket.PACKET_PLAYER.getPacketId()) {
      handlePacketPlayer(bf);
    } else if (packetId == GoogleMultiplayerPacket.PACKET_PLAYER_ATTACK.getPacketId()) {
      handlePacketPlayerAttack(bf);
    } else if (packetId == GoogleMultiplayerPacket.PACKET_ATTACK_EFFECT_ENTITY_COLLISION.getPacketId()) {
      handlePacketAttackEffectEntityCollision(bf);
    } else if (packetId == GoogleMultiplayerPacket.PACKET_PLAYER_MESSAGE.getPacketId()) {
      handlePacketPlayerMessage(bf);
    } else if (packetId == GoogleMultiplayerPacket.PACKET_REMOVE_TEAM_DEATHMATCH_TICKET.getPacketId()) {
      handlePacketRemoveTeamDeathmatchTicket();
    } else if (packetId == GoogleMultiplayerPacket.PACKET_NUMBER_OF_PARTICIPANTS.getPacketId()) {
      handlePacketNumberOfParticipants(bf);
    }
  }

  private static void handlePacketRoomId(final ByteBuffer bf) {
    int roomIdLength = bf.getInt();
    String roomId = "";
    for (int i = 0; i < roomIdLength; i++) {
      roomId += (char) bf.get();
    }

    MultiplayerContext.roomId = roomId;
    MultiplayerContext.multiplayer = true;
  }

  private static void handlePacketPlayer(final ByteBuffer bf) {
    int playerNameLength = bf.getInt();
    String playerName = "";
    for (int i = 0; i < playerNameLength; i++) {
      playerName += (char) bf.get();
    }

    PacketPlayer packetPlayer = new PacketPlayer();
    packetPlayer.entityId = playerName;
    packetPlayer.xPosition = bf.getFloat();
    packetPlayer.yPosition = bf.getFloat();
    packetPlayer.zPosition = bf.getFloat();
    packetPlayer.xDirection = bf.getFloat();
    packetPlayer.yDirection = bf.getFloat();
    packetPlayer.zDirection = bf.getFloat();
    packetPlayer.wDirection = bf.getFloat();

    PacketScheduler.addPacket(packetPlayer);
  }

  private static void handlePacketPlayerAttack(final ByteBuffer bf) {
    int playerNameLength = bf.getInt();
    String playerName = "";
    for (int i = 0; i < playerNameLength; i++) {
      playerName += (char) bf.get();
    }
    int attackId = bf.getInt();
    int colorId = bf.getInt();
    String targetId = "";
    if (bf.get() == CONTINUE) {
      int targetIdLength = bf.getInt();
      for (int i = 0; i < targetIdLength; i++) {
        targetId += (char) bf.get();
      }
    } else {
      targetId = null;
    }

    PacketPlayerAttack packetPlayerAttack = new PacketPlayerAttack();
    packetPlayerAttack.entityId = playerName;
    packetPlayerAttack.attackId = attackId;
    packetPlayerAttack.colorId = colorId;
    packetPlayerAttack.targetId = targetId;

    PacketScheduler.addPacket(packetPlayerAttack);
  }

  private static void handlePacketAttackEffectEntityCollision(final ByteBuffer bf) {
    int attackId = bf.getInt();
    float damageCoef = bf.getFloat();
    int attackedEntityIdLength = bf.getInt();
    String attackedEntityId = "";
    for (int i = 0; i < attackedEntityIdLength; i++) {
      attackedEntityId += (char) bf.get();
    }

    PacketAttackEffectEntityCollision packetAttackEffectEntityCollision = new PacketAttackEffectEntityCollision();
    packetAttackEffectEntityCollision.attackId = attackId;
    packetAttackEffectEntityCollision.damageCoef = damageCoef;
    packetAttackEffectEntityCollision.attackedEntityId = attackedEntityId;

    PacketScheduler.addPacket(packetAttackEffectEntityCollision);
  }

  private static void handlePacketPlayerMessage(final ByteBuffer bf) {
    int playerNameLength = bf.getInt();
    String playerName = "";
    for (int i = 0; i < playerNameLength; i++) {
      playerName += (char) bf.get();
    }

    PacketPlayerMessage packetPlayerMessage = new PacketPlayerMessage();
    packetPlayerMessage.entityId = playerName;
    try {
      int start = BYTE + INT + playerName.getBytes().length;
      int end = bf.array().length;
      packetPlayerMessage.message = new String(Arrays.copyOfRange(bf.array(), start, end), "UTF-8");
    } catch (UnsupportedEncodingException e) {
      Gdx.app.error(TAG, e.getMessage());
    }

    PacketScheduler.addPacket(packetPlayerMessage);
  }

  private static void handlePacketRemoveTeamDeathmatchTicket() {
    PacketScheduler.addPacket(KryonetPacketBuilder.getPacketRemoveTeamDeathmatchTicket());
  }

  private static void handlePacketNumberOfParticipants(final ByteBuffer bf) {
    MultiplayerContext.numberOfParticipants = bf.getInt();
  }
}
