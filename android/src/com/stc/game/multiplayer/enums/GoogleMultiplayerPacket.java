package com.stc.game.multiplayer.enums;

import lombok.Getter;

public enum GoogleMultiplayerPacket {
    PACKET_ROOM_ID((byte) 0),
    PACKET_PLAYER((byte) 1),
    PACKET_PLAYER_ATTACK((byte) 2),
    PACKET_ATTACK_EFFECT_ENTITY_COLLISION((byte) 3),
    PACKET_PLAYER_MESSAGE((byte) 4),
    PACKET_REMOVE_TEAM_DEATHMATCH_TICKET((byte) 5),
    PACKET_NUMBER_OF_PARTICIPANTS((byte) 6),
    PACKET_PLAYER_NAME((byte) 7);

    @Getter
    private final byte packetId;

    GoogleMultiplayerPacket(final byte packetId) {
        this.packetId = packetId;
    }
}
