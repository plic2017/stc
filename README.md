# README #

### Project ###
* Slash the Cloud

![ic_launcher.png](https://bitbucket.org/repo/aqaRge/images/3197734257-ic_launcher.png)

### Language ###
* [Java](https://fr.wikipedia.org/wiki/Java_(langage))

### Frameworks used ###
* [libGDX](https://libgdx.badlogicgames.com/)
* [KryoNet](https://github.com/EsotericSoftware/kryonet)

### Services used ###
* [Google Play Games Services](https://developers.google.com/games/services/)
* [Nearby Connections API](https://developers.google.com/nearby/connections/overview)
* [AdMob Service](http://www.google.fr/admob/)

### Extensions used ###
* [gdx-dialogs](https://github.com/TomGrill/gdx-dialogs)

### Plugins used ###
* [Project Lombok](https://projectlombok.org/)
* [2D Particle Editor](https://libgdx.badlogicgames.com/nightlies/runnables/runnable-2D-particles.jar)
* [3D Particle Editor](https://libgdx.badlogicgames.com/nightlies/runnables/runnable-3D-particles.jar)

### Scripts used ###
* [SpaceshipGenerator](https://github.com/a1studmuffin/SpaceshipGenerator)

### Build system ###
* [Gradle](http://gradle.org/)

### See also ###
* [Slash the Cloud - Server](https://bitbucket.org/plic2017/stc-server/)

### Authors ###

* Nicolas Naudot
* David Baron
* Ruben Moutot

### Website ###
* [slashthecloud.bitbucket.org](http://slashthecloud.bitbucket.org/)

### Contact ###

* slashthecloud@gmail.com
